use std::env::args;

use ascii_logos::{AsciiLogo, LogoSearch};

pub fn main() {
    let mut args: Vec<_> = args().collect();
    let logo_search = LogoSearch::default();
    args = args[1..].to_vec();
    for arg in args {
        println!("{}", arg);
        if arg.trim().is_empty() {
            continue;
        }
        if let Some(logo) = logo_search.search(&arg) {
            logo.print_uncolored(Some("/// "));
        } else {
            AsciiLogo::unknown().print_uncolored(Some("/// "));
        }
    }
}
