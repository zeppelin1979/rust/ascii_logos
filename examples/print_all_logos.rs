use ascii_logos::LogoSelection;
use colored::Colorize;
// needed to iter over enum LogoSelection
use strum::IntoEnumIterator;

/// Prints all logos colored on the terminal
pub fn main() {
    print_all_logos();
}

pub fn print_all_logos() {
    for logo_selection in LogoSelection::iter() {
        let logo = logo_selection.logo();
        println!(
            "{}",
            format!("{}:", logo.names()[0]).color(logo.colors()[0])
        );
        logo.print_colored();
        println!();
    }
}
