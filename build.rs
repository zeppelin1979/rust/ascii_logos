use std::{
    collections::HashMap,
    fs::{self, read_dir, File},
    io::{BufWriter, Write},
    path::PathBuf,
};

fn main() {
    let mut ascii_art = HashMap::new();
    for entry in read_dir("resources/logos").unwrap().flatten() {
        let file_name = entry.file_name().to_string_lossy().to_string();
        let const_name = format!(
            "FASTFETCH_DATATEXT_LOGO_{}",
            file_name.trim_end_matches(".txt").to_uppercase()
        );
        ascii_art.insert(file_name, const_name);
    }
    println!("cargo:rerun-if-changed=build.rs");

    let out_dir = std::env::var_os("OUT_DIR").unwrap();
    let path = std::path::Path::new(&out_dir).join("logos.rs");
    let mut outfile = BufWriter::new(File::create(path).unwrap());
    for (textfile, constname) in ascii_art {
        let full_textfile_name = PathBuf::from(format!("resources/logos/{}", textfile));
        println!("cargo:rerun-if-changed=resources/logos/{}", textfile);
        writeln!(
            outfile,
            "const {}: &str = r#\"{}\"#;\n",
            constname,
            fs::read_to_string(&full_textfile_name).unwrap()
        )
        .unwrap();
    }
}
