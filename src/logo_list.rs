use super::AsciiLogo;

impl AsciiLogo {
    /// Returns a List with all logos grouped by beginning
    pub fn all() -> Vec<Vec<Self>> {
        vec![
            Self::a(),
            Self::b(),
            Self::c(),
            Self::d(),
            Self::e(),
            Self::f(),
            Self::g(),
            Self::h(),
            Self::i(),
            Self::j(),
            Self::k(),
            Self::l(),
            Self::m(),
            Self::n(),
            Self::o(),
            Self::p(),
            Self::q(),
            Self::r(),
            Self::s(),
            Self::t(),
            Self::u(),
            Self::v(),
            Self::w(),
            Self::x(),
            Self::y(),
            Self::z(),
        ]
    }

    fn a() -> Vec<Self> {
        vec![
            Self::adelie(),
            Self::aer_os(),
            Self::afterglow(),
            Self::aix(),
            Self::almalinux(),
            Self::alpine(),
            Self::alpine_small(),
            Self::alpine2_small(),
            Self::alter(),
            Self::amazon(),
            Self::amazon_linux(),
            Self::amog_os(),
            Self::anarchy(),
            Self::android(),
            Self::android_small(),
            Self::antergos(),
            Self::antix(),
            Self::aosc_os_retro(),
            Self::aosc_os_retro_small(),
            Self::aosc_os(),
            Self::aosc_os_old(),
            Self::aperture(),
            Self::apricity(),
            Self::arch_box(),
            Self::archcraft(),
            Self::archcraft2(),
            Self::arch(),
            Self::arch2(),
            Self::arch_small(),
            Self::arch_labs(),
            Self::arch_strike(),
            Self::artix(),
            Self::artix_small(),
            Self::artix2_small(),
            Self::arco(),
            Self::arco_small(),
            Self::arse(),
            Self::arya(),
            Self::asahi(),
            Self::asahi2(),
            Self::aster(),
            Self::asteroid_os(),
            Self::ast_os(),
            Self::astra(),
            Self::athena(),
            Self::azos(),
        ]
    }
    fn b() -> Vec<Self> {
        vec![
            Self::bedrock(),
            Self::big_linux(),
            Self::bitrig(),
            Self::blackarch(),
            Self::black_mesa(),
            Self::black_panther(),
            Self::blag(),
            Self::blank_on(),
            Self::blue_light(),
            Self::bodhi(),
            Self::bonsai(),
            Self::bsd(),
            Self::bunsen_labs(),
        ]
    }

    fn c() -> Vec<Self> {
        vec![
            Self::cachy_os(),
            Self::cachy_os_small(),
            Self::calculate(),
            Self::calinix(),
            Self::calinix_small(),
            Self::carbs(),
            Self::cbl_mariner(),
            Self::celos(),
            Self::center(),
            Self::cent_os(),
            Self::cent_os_small(),
            Self::chakra(),
            Self::chalet_os(),
            Self::chapeau(),
            Self::chimera_linux(),
            Self::chonky_seal_os(),
            Self::chrom_os(),
            Self::cleanjaro(),
            Self::cleanjaro_small(),
            Self::clear_linux(),
            Self::clear_os(),
            Self::clover(),
            Self::cobalt(),
            Self::condres(),
            Self::crux(),
            Self::crux_small(),
            Self::crystal(),
            Self::cucumber(),
            Self::cutefish_os(),
            Self::cute_os(),
            Self::cyber_os(),
            Self::cycledream(),
        ]
    }

    fn d() -> Vec<Self> {
        vec![
            Self::dahlia(),
            Self::dark_os(),
            Self::debian(),
            Self::debian_small(),
            Self::deepin(),
            Self::desa_os(),
            Self::devuan(),
            Self::devuan_small(),
            Self::diet_pi(),
            Self::drac_os(),
            Self::dragon_fly(),
            Self::dragon_fly_small(),
            Self::dragon_fly_old(),
            Self::drauger_os(),
            Self::droidian(),
        ]
    }

    fn e() -> Vec<Self> {
        vec![
            Self::elbrus(),
            Self::elementary(),
            Self::elementary_small(),
            Self::elive(),
            Self::encrypt_os(),
            Self::endeavour(),
            Self::endeavour_small(),
            Self::endless(),
            Self::enso(),
            Self::eshanized_os(),
            Self::euro_linux(),
            Self::evolinx(),
            Self::evolution_os(),
            Self::evolution_os_small(),
            Self::evolution_os_old(),
            Self::exherbo(),
            Self::exodia_predator(),
        ]
    }

    fn f() -> Vec<Self> {
        vec![
            Self::fedora(),
            Self::fedora_small(),
            Self::fedora_old(),
            Self::fedora_silverblue(),
            Self::fedora_kinoite(),
            Self::fedora_sericea(),
            Self::fedora_coreos(),
            Self::femboy_os(),
            Self::feren(),
            Self::finnix(),
            Self::floflis(),
            Self::freebsd(),
            Self::freebsd_small(),
            Self::free_mint(),
            Self::frugalware(),
            Self::funtoo(),
        ]
    }

    fn g() -> Vec<Self> {
        vec![
            Self::gallium_os(),
            Self::garuda(),
            Self::garuda_dragon(),
            Self::garuda_small(),
            Self::gentoo(),
            Self::gentoo_small(),
            Self::ghost_bsd(),
            Self::glaucus(),
            Self::g_new_sense(),
            Self::gnome(),
            Self::gnu(),
            Self::gobo_linux(),
            Self::graphene_os(),
            Self::grombyang(),
            Self::guix(),
            Self::guix_small(),
        ]
    }

    fn h() -> Vec<Self> {
        vec![
            Self::haiku(),
            Self::haiku_small(),
            Self::hamoni_kr(),
            Self::har_dc_lan_z(),
            Self::hardened_bsd(),
            Self::hash(),
            Self::huayra(),
            Self::hybrid(),
            Self::hydro_os(),
            Self::hyperbola(),
            Self::hyperbola_small(),
        ]
    }

    fn i() -> Vec<Self> {
        vec![
            Self::iglunix(),
            Self::instant_os(),
            Self::interix(),
            Self::irix(),
            Self::ironclad(),
            Self::itc(),
        ]
    }

    fn j() -> Vec<Self> {
        vec![Self::janus_linux()]
    }

    fn k() -> Vec<Self> {
        vec![
            Self::kaisen(),
            Self::kali(),
            Self::kali_small(),
            Self::ka_os(),
            Self::kernel_os(),
            Self::kde_neon(),
            Self::kibojoe(),
            Self::kiss_linux(),
            Self::kogaion(),
            Self::korora(),
            Self::krass_os(),
            Self::ks_linux(),
            Self::kubuntu(),
        ]
    }

    fn l() -> Vec<Self> {
        vec![
            Self::langitketujuh(),
            Self::laxeros(),
            Self::lede(),
            Self::libre_elec(),
            Self::linspire(),
            Self::linux(),
            Self::linux_small(),
            Self::linux_lite(),
            Self::linux_lite_small(),
            Self::live_raizo(),
            Self::lmde(),
            Self::lain_os(),
            Self::lunar(),
        ]
    }

    fn m() -> Vec<Self> {
        vec![
            Self::mac_os(),
            Self::mac_os_small(),
            Self::mac_os2(),
            Self::mac_os2_small(),
            Self::mainsail_os(),
            Self::mainsail_os_small(),
            Self::mageia(),
            Self::mageia_small(),
            Self::magpie_os(),
            Self::mandriva(),
            Self::manjaro(),
            Self::manjaro_small(),
            Self::mass_os(),
            Self::matuus_os(),
            Self::ma_ui(),
            Self::meowix(),
            Self::mer(),
            Self::mint(),
            Self::mint_small(),
            Self::mint_old(),
            Self::minix(),
            Self::miracle_linux(),
            Self::mos(),
            Self::msys2(),
            Self::mx(),
            Self::mx_small(),
            Self::mx2(),
        ]
    }

    fn n() -> Vec<Self> {
        vec![
            Self::namib(),
            Self::nekos(),
            Self::neptune(),
            Self::net_runner(),
            Self::nitrux(),
            Self::nix_os(),
            Self::nix_os_small(),
            Self::nix_os_old(),
            Self::nix_os_old_small(),
            Self::net_bsd(),
            Self::nobara(),
            Self::nomad_bsd(),
            Self::nurunner(),
            Self::nu_tyx(),
        ]
    }

    fn o() -> Vec<Self> {
        vec![
            Self::obarun(),
            Self::ob_revenge(),
            Self::omni_os(),
            Self::open_kylin(),
            Self::open_bsd(),
            Self::open_bsd_small(),
            Self::open_euler(),
            Self::open_indiana(),
            Self::open_mamba(),
            Self::open_stage(),
            Self::open_suse(),
            Self::open_suse_small(),
            Self::open_suse_micro_os(),
            Self::open_suse_leap(),
            Self::open_suse_tumbleweed(),
            Self::open_mandriva(),
            Self::open_wrt(),
            Self::opn_sense(),
            Self::oracle(),
            Self::orchid(),
            Self::orchid_small(),
            Self::os_elbrus(),
            Self::osmc(),
        ]
    }

    fn p() -> Vec<Self> {
        vec![
            Self::pac_bsd(),
            Self::panwah(),
            Self::parabola(),
            Self::parabola_small(),
            Self::parch(),
            Self::pardus(),
            Self::parrot(),
            Self::parsix(),
            Self::pcbsd(),
            Self::pc_linux_os(),
            Self::pear_os(),
            Self::pengwin(),
            Self::pentoo(),
            Self::peppermint(),
            Self::peropesis(),
            Self::phy_os(),
            Self::pika_os(),
            Self::pisi(),
            Self::pnm_linux(),
            Self::pop(),
            Self::pop_small(),
            Self::porteus(),
            Self::post_market_os(),
            Self::post_market_os_small(),
            Self::proxmox(),
            Self::puff_os(),
            Self::puppy(),
            Self::pure_os(),
            Self::pure_os_small(),
        ]
    }

    fn q() -> Vec<Self> {
        vec![Self::q4dos(), Self::qubes(), Self::qubyt(), Self::quibian()]
    }

    fn r() -> Vec<Self> {
        vec![
            Self::radix(),
            Self::raspbian(),
            Self::raspbian_small(),
            Self::ravyn_os(),
            Self::reborn(),
            Self::reborn_small(),
            Self::red_core(),
            Self::red_hat_enterprise_linux(),
            Self::red_hat_enterprise_linux_old(),
            Self::redstar_os(),
            Self::refracted_devuan(),
            Self::regata(),
            Self::regolith(),
            Self::rhaym_os(),
            Self::rocky_linux(),
            Self::rocky_linux_small(),
            Self::rosa_linux(),
        ]
    }

    fn s() -> Vec<Self> {
        vec![
            Self::sabayon(),
            Self::sabotage(),
            Self::sailfish(),
            Self::salent_os(),
            Self::salient_os(),
            Self::salix(),
            Self::samba_box(),
            Self::sasanqua(),
            Self::scientific(),
            Self::semc(),
            Self::septor(),
            Self::serene(),
            Self::shark_linux(),
            Self::shastra_os(),
            Self::siduction(),
            Self::skiff_os(),
            Self::slitaz(),
            Self::slackel(),
            Self::slackware(),
            Self::slackware_small(),
            Self::smart_os(),
            Self::soda(),
            Self::source_mage(),
            Self::solaris(),
            Self::solaris_small(),
            Self::solus(),
            Self::sparky(),
            Self::star(),
            Self::stock_linux(),
            Self::steam_os(),
            Self::sulin(),
            Self::suse(),
            Self::swagarch(),
        ]
    }

    fn t() -> Vec<Self> {
        vec![
            Self::t2(),
            Self::tails(),
            Self::tatra(),
            Self::te_arch(),
            Self::tile_os(),
            Self::torizon_core(),
            Self::trisquel(),
            Self::tuxedo_os(),
            Self::twister(),
        ]
    }
    fn u() -> Vec<Self> {
        vec![
            Self::ubuntu(),
            Self::ubuntu_budgie(),
            Self::ubuntu_cinnamon(),
            Self::ubuntu_gnome(),
            Self::ubuntu_kylin(),
            Self::ubuntu_mate(),
            Self::ubuntu_old(),
            Self::ubuntu_small(),
            Self::ubuntu_studio(),
            Self::ubuntu_sway(),
            Self::ubuntu_touch(),
            Self::ubuntu_unity(),
            Self::ubuntu2_old(),
            Self::ubuntu2_small(),
            Self::ultramarine(),
            Self::univalent(),
            Self::univention(),
            Self::uos(),
            Self::uruk_os(),
            Self::uwuntu(),
        ]
    }
    fn v() -> Vec<Self> {
        vec![
            Self::vanilla(),
            Self::venom(),
            Self::venom_small(),
            Self::vnux(),
            Self::vzlinux(),
            Self::void(),
            Self::void_small(),
        ]
    }
    fn w() -> Vec<Self> {
        vec![
            Self::wii_linux_ngx(),
            Self::windows_11(),
            Self::windows_11_small(),
            Self::windows_8(),
            Self::windows(),
            Self::windows_95(),
        ]
    }
    fn x() -> Vec<Self> {
        vec![Self::xenia(), Self::xferience(), Self::xray_os()]
    }
    fn y() -> Vec<Self> {
        vec![Self::yiff_os()]
    }
    fn z() -> Vec<Self> {
        vec![Self::zorin(), Self::z_os()]
    }
}
