use colored::Color;

use super::{AsciiLogo, LogoType};

include!(concat!(env!("OUT_DIR"), "/logos.rs"));

impl AsciiLogo {
    /// returns logo of Unknown distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::unknown().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        ________
    ///    _jgN########Ngg_
    ///  _N##N@@""  ""9NN##Np_
    /// d###P            N####p
    /// "^^"              T####
    ///                   d###P
    ///                _g###@F
    ///             _gN##@P
    ///           gN###F"
    ///          d###F
    ///         0###F
    ///         0###F
    ///         0###F
    ///         "NN@'
    ///
    ///          ___
    ///         q###r
    ///          ""
    /// ```
    pub fn unknown() -> Self {
        Self {
            names: vec!["unknown"],
            lines: FASTFETCH_DATATEXT_LOGO_UNKNOWN,
            colors: vec![Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Adelie distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::adelie().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   ,-^-___
    ///                 /\\\///
    /// refined.       /\\\\//
    /// reliable.     /\\\///
    /// ready.       /\\/////\
    ///         __///\\\\/////\
    ///      _//////\\\\\\\////
    ///    ///////\\\\\\\\\\//
    ///           //////\\\\\/
    ///           /////\\\\\/
    ///           /////\\\\/
    ///          /\\///\\\/
    ///          /\\\/\\/
    ///          /\\\\//
    ///         //////
    ///       /// \\\\\
    /// ```
    pub fn adelie() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ADELIE,
            names: vec!["Adélie", "Adelie"],
            colors: vec![Color::Blue, Color::White, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of AerOs distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aer_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ooo OOO OOO ooo
    ///              oOO                 OOo
    ///          oOO                         OOo
    ///       oOO                               OOo
    ///     oOO                                   OOo
    ///   oOO                                       OOo
    ///  oOO                                         OOo
    ///                                               OOo
    ///                                                OOo
    ///                                                OOo
    ///                                                OOo
    ///                                                OOo
    ///                                                OOo
    /// oOO                                           OOo
    ///  oOO                                         OOo
    ///   oOO                                       OOo
    ///     oOO                                   OOo
    ///       oO                                OOo
    ///          oOO                         OOo
    ///              oOO                 OOo
    ///                  ooo OOO OOO ooo
    /// ```
    pub fn aer_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AEROS,
            names: vec!["aerOS"],
            colors: vec![Color::Cyan, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Afterglow distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::afterglow().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                         .
    ///                .      .{!
    ///              .L!     J@||*
    ///            gJJJJL` g@FFS"
    ///         ,@FFFJF`_g@@LLP`
    ///       _@FFFFF`_@@@@@P`        .
    ///     J@@@LLF _@@@@@P`        .J!
    ///   g@@@@@" _@@@@@P`.       .L|||*
    /// g@@@@M"     "VP`.L!     <@JJJJ`
    ///  "@N"         :||||!  JFFFFS"
    ///            .{JJ||F`_gFFFF@'
    ///          .@FJJJF`,@LFFFF`
    ///        _@FFFFF   VLLLP`
    ///      J@@LL@"      `"
    ///       V@@"
    /// ```
    pub fn afterglow() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AFTERGLOW,
            names: vec!["Afterglow"],
            colors: vec![Color::Magenta, Color::Red, Color::Yellow, Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of AIX distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            `:+ssssossossss+-`
    ///         .oys///oyhddddhyo///sy+.
    ///       /yo:+hNNNNNNNNNNNNNNNNh+:oy/
    ///     :h/:yNNNNNNNNNNNNNNNNNNNNNNy-+h:
    ///   `ys.yNNNNNNNNNNNNNNNNNNNNNNNNNNy.ys
    ///  `h+-mNNNNNNNNNNNNNNNNNNNNNNNNNNNNm-oh
    ///  h+-NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN.oy
    /// /d`mNNNNNNN/::mNNNd::m+:/dNNNo::dNNNd`m:
    /// h//NNNNNNN: . .NNNh  mNo  od. -dNNNNN:+y
    /// N.sNNNNNN+ -N/ -NNh  mNNd.   sNNNNNNNo-m
    /// N.sNNNNNs  +oo  /Nh  mNNs` ` /mNNNNNNo-m
    /// h//NNNNh  ossss` +h  md- .hm/ `sNNNNN:+y
    /// :d`mNNN+/yNNNNNd//y//h//oNNNNy//sNNNd`m-
    ///  yo-NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNm.ss
    ///  `h+-mNNNNNNNNNNNNNNNNNNNNNNNNNNNNm-oy
    ///    sy.yNNNNNNNNNNNNNNNNNNNNNNNNNNs.yo
    ///     :h+-yNNNNNNNNNNNNNNNNNNNNNNs-oh-
    ///       :ys:/yNNNNNNNNNNNNNNNmy/:sy:
    ///         .+ys///osyhhhhys+///sy+.
    ///             -/osssossossso/-
    /// ```
    pub fn aix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AIX,
            names: vec!["aix"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Almalinux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::almalinux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          'c:.
    ///         lkkkx, ..       ..   ,cc,
    ///         okkkk:ckkx'  .lxkkx.okkkkd
    ///         .:llcokkx'  :kkkxkko:xkkd,
    ///       .xkkkkdood:  ;kx,  .lkxlll;
    ///        xkkx.       xk'     xkkkkk:
    ///        'xkx.       xd      .....,.
    ///       .. :xkl'     :c      ..''..
    ///     .dkx'  .:ldl:'. '  ':lollldkkxo;
    ///   .''lkkko'                     ckkkx.
    /// 'xkkkd:kkd.       ..  ;'        :kkxo.
    /// ,xkkkd;kk'      ,d;    ld.   ':dkd::cc,
    ///  .,,.;xkko'.';lxo.      dx,  :kkk'xkkkkc
    ///      'dkkkkkxo:.        ;kx  .kkk:;xkkd.
    ///        .....   .;dk:.   lkk.  :;,
    ///              :kkkkkkkdoxkkx
    ///               ,c,,;;;:xkkd.
    ///                 ;kkkkl...
    ///                 ;kkkkl
    ///                  ,od;
    /// ```
    pub fn almalinux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ALMALINUX,
            names: vec!["almalinux"],
            colors: vec![
                Color::Red,
                Color::BrightYellow,
                Color::Blue,
                Color::BrightGreen,
                Color::Cyan,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Alpine distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::alpine().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        .hddddddddddddddddddddddh.
    ///       :dddddddddddddddddddddddddd:
    ///      /dddddddddddddddddddddddddddd/
    ///     +dddddddddddddddddddddddddddddd+
    ///   `sdddddddddddddddddddddddddddddddds`
    ///  `ydddddddddddd++hdddddddddddddddddddy`
    /// .hddddddddddd+`  `+ddddh:-sdddddddddddh.
    /// hdddddddddd+`      `+y:    .sddddddddddh
    /// ddddddddh+`   `//`   `.`     -sddddddddd
    /// ddddddh+`   `/hddh/`   `:s-    -sddddddd
    /// ddddh+`   `/+/dddddh/`   `+s-    -sddddd
    /// ddd+`   `/o` :dddddddh/`   `oy-    .yddd
    /// hdddyo+ohddyosdddddddddho+oydddy++ohdddh
    /// .hddddddddddddddddddddddddddddddddddddh.
    ///  `yddddddddddddddddddddddddddddddddddy`
    ///   `sdddddddddddddddddddddddddddddddds`
    ///     +dddddddddddddddddddddddddddddd+
    ///      /dddddddddddddddddddddddddddd/
    ///       :dddddddddddddddddddddddddd:
    ///        .hddddddddddddddddddddddh.
    /// ```
    pub fn alpine() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ALPINE,
            names: vec!["alpine", "alpinelinux", "alpine-linux"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of small Alpine distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::alpine_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    /\ /\
    ///   // \  \
    ///  //   \  \
    /// ///    \  \
    /// //      \  \
    ///          \
    /// ```
    pub fn alpine_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ALPINE_SMALL,
            names: vec!["alpine_small", "alpine-linux-small"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of alternative small Alpine distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::alpine2_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    /\\ /\\
    ///   // \\  \\
    ///  ///  \\  \\
    /// ///    \\  \\
    /// //      \\  \\
    ///          \\
    /// ```
    pub fn alpine2_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ALPINE2_SMALL,
            names: vec!["alpine2_small", "alpine-linux2-small"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Alter distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::alter().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                       %,
    ///                     ^WWWw
    ///                    'wwwwww
    ///                   !wwwwwwww
    ///                  #`wwwwwwwww
    ///                 @wwwwwwwwwwww
    ///                wwwwwwwwwwwwwww
    ///               wwwwwwwwwwwwwwwww
    ///              wwwwwwwwwwwwwwwwwww
    ///             wwwwwwwwwwwwwwwwwwww,
    ///            w~1i.wwwwwwwwwwwwwwwww,
    ///          3~:~1lli.wwwwwwwwwwwwwwww.
    ///         :~~:~?ttttzwwwwwwwwwwwwwwww
    ///        #<~:~~~~?llllltO-.wwwwwwwwwww
    ///       #~:~~:~:~~?ltlltlttO-.wwwwwwwww
    ///      @~:~~:~:~:~~(zttlltltlOda.wwwwwww
    ///     @~:~~: ~:~~:~:(zltlltlO    a,wwwwww
    ///    8~~:~~:~~~~:~~~~_1ltltu          ,www
    ///   5~~:~~:~~:~~:~~:~~~_1ltq             N,,
    ///  g~:~~:~~~:~~:~~:~:~~~~1q                N,
    /// ```
    pub fn alter() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ALTER,
            names: vec!["Alter"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Amazon distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::amazon().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `-/oydNNdyo:.`
    ///       `.:+shmMMMMMMMMMMMMMMmhs+:.`
    ///     -+hNNMMMMMMMMMMMMMMMMMMMMMMNNho-
    /// .``      -/+shmNNMMMMMMNNmhs+/-      ``.
    /// dNmhs+:.       `.:/oo/:.`       .:+shmNd
    /// dMMMMMMMNdhs+:..        ..:+shdNMMMMMMMd
    /// dMMMMMMMMMMMMMMNds    odNMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// .:+ydNMMMMMMMMMMMh    yMMMMMMMMMMMNdy+:.
    ///      `.:+shNMMMMMh    yMMMMMNhs+:``
    ///             `-+shy    shs+:`
    /// ```
    pub fn amazon() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AMAZON,
            names: vec!["Amazon"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Amazon Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::amazon_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   ,     #_
    ///   ~\_  ####_
    ///  ~~  \_#####\
    ///  ~~     \###|
    ///  ~~       \#/ ___
    ///   ~~       V~' '->
    ///    ~~~         /
    ///      ~~._.   _/
    ///         _/ _/
    ///       _/m/'
    /// ```
    pub fn amazon_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AMAZON_LINUX,
            names: vec!["Amazon Linux", "amzn"],
            colors: vec![
                Color::White,
                Color::TrueColor {
                    r: 0xd7,
                    g: 0xaf,
                    b: 8,
                },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of AmogOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::amog_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ___________
    ///             /           \
    ///            /   ______    \
    ///           /   /      \    \
    ///           |  (        )    \
    ///          /    \______/     |
    ///          |                 |
    ///         /                   \
    ///         |                   |
    ///         |                   |
    ///        /                    |
    ///        |                    |
    ///        |     _______        |
    ///   ____/     /       \       |
    ///  /          |       |       |
    ///  |          /   ____/       |
    ///  \_________/   /            |
    ///                \         __/
    ///                 \_______/
    /// ```
    pub fn amog_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AMOGOS,
            names: vec!["AmogOS"],
            colors: vec![Color::White, Color::Cyan],
            color_keys: Some(Color::White),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Anarchy distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::anarchy().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          ..
    ///                         ..
    ///                       :..
    ///                     :+++.
    ///               .:::+++++++::.
    ///           .:+######++++######+:.
    ///        .+#########+++++##########:.
    ///      .+##########+++++++##+#########+.
    ///     +###########+++++++++############:
    ///    +##########++++++#++++#+###########+
    ///   +###########+++++###++++#+###########+
    ///  :##########+#++++####++++#+############:
    ///  ###########+++++#####+++++#+###++######+
    /// .##########++++++#####++++++++++++#######.
    /// .##########+++++++++++++++++++###########.
    ///  #####++++++++++++++###++++++++#########+
    ///  :###++++++++++#########+++++++#########:
    ///   +######+++++##########++++++++#######+
    ///    +####+++++###########+++++++++#####+
    ///     :##++++++############++++++++++##:
    ///      .++++++#############+++++++++++.
    ///       :++++###############+++++++::
    ///      .++. .:+##############+++++++..
    ///      .:.      ..::++++++::..:+++++.
    ///      .                       .:+++.
    ///                                 .::
    ///                                    ..
    ///                                     ..
    /// ```
    pub fn anarchy() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ANARCHY,
            names: vec!["Anarchy"],
            colors: vec![Color::White, Color::Blue],
            color_keys: Some(Color::White),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Android distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::android().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          -o          o-
    ///           +hydNNNNdyh+
    ///         +mMMMMMMMMMMMMm+
    ///       `dMMm:NMMMMMMN:mMMd`
    ///       hMMMMMMMMMMMMMMMMMMh
    ///   ..  yyyyyyyyyyyyyyyyyyyy  ..
    /// .mMMm`MMMMMMMMMMMMMMMMMMMM`mMMm.
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// -MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM-
    ///  +yy+ MMMMMMMMMMMMMMMMMMMM +yy+
    ///       mMMMMMMMMMMMMMMMMMMm
    ///       `/++MMMMh++hMMMM++/`
    ///           MMMMo  oMMMM
    ///           MMMMo  oMMMM
    ///           oNMm-  -mMNs
    /// ```
    pub fn android() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ANDROID,
            names: vec!["android"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Android distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::android_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   ;,           ,;
    ///    ';,.-----.,;'
    ///   ,'           ',
    ///  /    O     O    \
    /// |                 |
    /// '-----------------'
    /// ```
    pub fn android_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ANDROID_SMALL,
            names: vec!["android-small", "android_small"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Antergos distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::antergos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               `.-/::/-``
    ///             .-/osssssssso/.
    ///            :osyysssssssyyys+-
    ///         `.+yyyysssssssssyyyyy+.
    ///        `/syyyyyssssssssssyyyyys-`
    ///       `/yhyyyyysss++ssosyyyyhhy/`
    ///      .ohhhyyyyso++/+oso+syy+shhhho.
    ///     .shhhhysoo++//+sss+++yyy+shhhhs.
    ///    -yhhhhs+++++++ossso+++yyys+ohhddy:
    ///   -yddhhyo+++++osyyss++++yyyyooyhdddy-
    ///  .yddddhso++osyyyyys+++++yyhhsoshddddy`
    /// `odddddhyosyhyyyyyy++++++yhhhyosddddddo
    /// .dmdddddhhhhhhhyyyo+++++shhhhhohddddmmh.
    /// ddmmdddddhhhhhhhso++++++yhhhhhhdddddmmdy
    /// dmmmdddddddhhhyso++++++shhhhhddddddmmmmh
    /// -dmmmdddddddhhyso++++oshhhhdddddddmmmmd-
    /// .smmmmddddddddhhhhhhhhhdddddddddmmmms.
    ///    `+ydmmmdddddddddddddddddddmmmmdy/.
    ///       `.:+ooyyddddddddddddyyso+:.`
    /// ```
    pub fn antergos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ANTERGOS,
            names: vec!["Antergos"],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Antix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::antix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     \
    ///          , - ~ ^ ~ - \        /
    ///      , '              \ ' ,  /
    ///    ,                   \   '/
    ///   ,                     \  / ,
    ///  ,___,                   \/   ,
    ///  /   |   _  _  _|_ o     /\   ,
    /// |,   |  / |/ |  |  |    /  \  ,
    ///  \,_/\_/  |  |_/|_/|_/_/    \,
    ///    ,                  /     ,\
    ///      ,               /  , '   \
    ///       ' - , _ _ _ ,  '
    /// ```
    pub fn antix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ANTIX,
            names: vec!["antix"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Aosc OS/Retro distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aosc_os_retro().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           .........
    ///      ...................
    ///    .....................################
    ///  ..............     ....################
    /// ..............       ...################
    /// .............         ..****************
    /// ............     .     .****************
    /// ...........     ...     ................
    /// ..........     .....     ...............
    /// .........     .......     ...
    ///  .......                   .
    ///   .....      .........    ...........
    ///   ....      .......       ...........
    ///   ...      .......        ...........
    ///   ................        ***********
    ///   ................        ###########
    ///   ****************
    ///   ################
    /// ```
    pub fn aosc_os_retro() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AOSCOSRETRO,
            names: vec!["Aosc OS/Retro", "aoscosretro"],
            colors: vec![Color::Blue, Color::White, Color::Red, Color::Yellow],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Aosc OS/Retro distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aosc_os_retro_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     _____   _____
    ///   -'     '-|     |
    ///  /     ___ |     |
    /// |     / _ \\|_____|
    /// '    / /_\\ \\
    ///  \\  / _____ \\___
    ///   |/_/  |   |   |
    ///   |     |   |___|
    ///   |_____|
    /// ```
    pub fn aosc_os_retro_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AOSCOSRETRO_SMALL,
            names: vec!["Aosc OS/Retro_small", "aoscosretro_small"],
            colors: vec![Color::Blue, Color::White, Color::Red, Color::Yellow],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Aosc OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aosc_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 __
    ///              gpBBBBBBBBBP
    ///          _gBBBBBBBBBRP
    ///        4BBBBBBBBRP  ,_____
    ///             `"" _g@@@@@@@@@@@@@%g>
    ///             __@@@@@@@@@@@@@@@@P"  ___
    ///          _g@@@@@@@@@@@@@@@N"` _gN@@@@@N^
    ///      _w@@@@@@@@@@@@@@@@P" _g@@@@@@@P"
    ///   _g@@@@@@@@@@@@@@@N"`  VMNN@NNNM^`
    /// ^MMM@@@@@@@@@@@MP" ,ggppww__
    ///         `""""" _wNNNNNNNNNNNNNNNNNNN
    ///             _gBNNNNNNNNNNNNNNNNNP"
    ///         _wNNNNNNNNNNNNNNNNNNMP`
    ///      _gBNNNNNNNNNNNNNNNNNP"
    ///  _wNNNNNNNNNNNNNNNNNNNM^
    ///  ""Y^^MNNNNNNNNNNNNP`
    ///          `"""""""
    /// ```
    pub fn aosc_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AOSCOS,
            names: vec!["Aosc OS", "aoscos"],
            colors: vec![Color::Blue, Color::Black, Color::Red, Color::Yellow],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns old logo of Aosc OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aosc_os_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              .:+syhhhhys+:.
    ///          .ohNMMMMMMMMMMMMMMNho.
    ///       `+mMMMMMMMMMMmdmNMMMMMMMMm+`
    ///      +NMMMMMMMMMMMM/   `./smMMMMMN+
    ///    .mMMMMMMMMMMMMMMo        -yMMMMMm.
    ///   :NMMMMMMMMMMMMMMMs          .hMMMMN:
    ///  .NMMMMhmMMMMMMMMMMm+/-         oMMMMN.
    ///  dMMMMs  ./ymMMMMMMMMMMNy.       sMMMMd
    /// -MMMMN`      oMMMMMMMMMMMN:      `NMMMM-
    /// /MMMMh       NMMMMMMMMMMMMm       hMMMM/
    /// /MMMMh       NMMMMMMMMMMMMm       hMMMM/
    /// -MMMMN`      :MMMMMMMMMMMMy.     `NMMMM-
    ///  dMMMMs       .yNMMMMMMMMMMMNy/. sMMMMd
    ///  .NMMMMo         -/+sMMMMMMMMMMMmMMMMN.
    ///   :NMMMMh.          .MMMMMMMMMMMMMMMN:
    ///    .mMMMMMy-         NMMMMMMMMMMMMMm.
    ///      +NMMMMMms/.`    mMMMMMMMMMMMN+
    ///       `+mMMMMMMMMNmddMMMMMMMMMMm+`
    ///          .ohNMMMMMMMMMMMMMMNho.
    ///              .:+syhhhhys+:.
    /// ```
    pub fn aosc_os_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AOSCOS_OLD,
            names: vec!["Aosc OS_old", "aoscos_old"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Aperture distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aperture().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               .,-:;//;:=,
    ///           . :H@@@MM@M#H/.,+%;,
    ///        ,/X+ +M@@M@MM%=,-%HMMM@X/,
    ///      -+@MM; SM@@MH+-,;XMMMM@MMMM@+-
    ///     ;@M@@M- XM@X;. -+XXXXXHHH@M@M#@/.
    ///   ,%MM@@MH ,@%=             .---=-=:=,.
    ///   =@#@@@MX.,                -%HXSS%%%:;
    ///  =-./@M@MS                   .;@MMMM@MM:
    ///  X@/ -SMM/                    . +MM@@@MS
    /// ,@M@H: :@:                    . =X#@@@@-
    /// ,@@@MMX, .                    /H- ;@M@M=
    /// .H@@@@M@+,                    %MM+..%#S.
    ///  /MMMM@MMH/.                  XM@MH; =;
    ///   /%+%SXHH@S=              , .H@@@@MX,
    ///    .=--------.           -%H.,@@@@@MX,
    ///    .%MM@@@HHHXXSSS%+- .:SMMX =M@@MM%.
    ///      =XMMM@MM@MM#H;,-+HMM@M+ /MMMX=
    ///        =%@M@M#@S-.=S@MM@@@M; %M%=
    ///          ,:+S+-,/H#MMMMMMM@= =,
    ///                =++%%%%+/:-.
    /// ```
    pub fn aperture() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_APERTURE,
            names: vec!["Aperture"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Apricity distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::apricity().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                     ./o-
    ///           ``...``              `:. -/:
    ///      `-+ymNMMMMMNmho-`      :sdNNm/
    ///    `+dMMMMMMMMMMMMMMMmo` sh:.:::-
    ///   /mMMMMMMMMMMMMMMMMMMMm/`sNd/
    ///  oMMMMMMMMMMMMMMMMMMMMMMMs -`
    /// :MMMMMMMMMMMMMMMMMMMMMMMMM/
    /// NMMMMMMMMMMMMMMMMMMMMMMMMMd
    /// MMMMMMMmdmMMMMMMMMMMMMMMMMd
    /// MMMMMMy` .mMMMMMMMMMMMmho:`
    /// MMMMMMNo/sMMMMMMMNdy+-.`-/
    /// MMMMMMMMMMMMNdy+:.`.:ohmm:
    /// MMMMMMMmhs+-.`.:+ymNMMMy.
    /// MMMMMM/`.-/ohmNMMMMMMy-
    /// MMMMMMNmNNMMMMMMMMmo.
    /// MMMMMMMMMMMMMMMms:`
    /// MMMMMMMMMMNds/.
    /// dhhyys+/-`
    /// ```
    pub fn apricity() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_APRICITY,
            names: vec!["Apricity"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of ArchBox distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch_box().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               ...:+oh/:::..
    ///          ..-/oshhhhhh`   `::::-.
    ///      .:/ohhhhhhhhhhhh`        `-::::.
    ///  .+shhhhhhhhhhhhhhhhh`             `.::-.
    ///  /`-:+shhhhhhhhhhhhhh`            .-/+shh
    ///  /      .:/ohhhhhhhhh`       .:/ohhhhhhhh
    ///  /           `-:+shhh`  ..:+shhhhhhhhhhhh
    ///  /                 .:ohhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /      .+o+        `hhhhhhhhhhhhhhhhhhhh
    ///  /     -hhhhh       `hhhhhhhhhhhhhhhhhhhh
    ///  /     ohhhhho      `hhhhhhhhhhhhhhhhhhhh
    ///  /:::+`hhhhoos`     `hhhhhhhhhhhhhhhhhs+`
    ///     `--/:`   /:     `hhhhhhhhhhhho/-
    ///              -/:.   `hhhhhhs+:-`
    ///                 ::::/ho/-`
    /// ```
    pub fn arch_box() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCHBOX,
            names: vec!["ArchBox"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Archcraft distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::archcraft().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢰⡆⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢠⣿⣿⡄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣾⣿⣿⣿⡀⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⣿⣷⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢼⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣤⣈⠻⢿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣮⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⡿⣿⣿⡟⠄⠄⠸⣿⣿⡿⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⡏⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠈⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⢀⣼⣿⣿⣿⣿⣿⣿⡗⠄⠄⠄⢀⣠⣤⣀⠄⠄⠄⠸⣿⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⢀⣾⣿⣿⣿⣿⣿⡏⠁⠄⠄⠄⢠⣿⣿⣿⣿⡇⠄⠄⠄⠄⢙⣿⣿⣻⠿⣿⣷⡀⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⢀⣾⣿⣿⣿⣿⣿⣿⣷⣤⡀⠄⠄⠄⠻⣿⣿⡿⠃⠄⠄⠄⢀⣼⣿⣿⣿⣿⣦⣌⠙⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⢠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⠏⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠄⠄⠄
    /// ⠄⠄⢠⣿⣿⣿⣿⣿⣿⣿⡿⠟⠋⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠙⠻⣿⣿⣿⣿⣿⣿⣿⣿⡄⠄⠄
    /// ⠄⣠⣿⣿⣿⣿⠿⠛⠋⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠉⠙⠻⢿⣿⣿⣿⣿⣆⠄
    /// ⡰⠟⠛⠉⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠉⠙⠛⠿⢆
    /// ```
    pub fn archcraft() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCHCRAFT,
            names: vec!["Archcraft"],
            colors: vec![
                Color::Cyan,
                Color::Red,
                Color::Blue,
                Color::White,
                Color::Yellow,
                Color::Green,
            ],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns alternate logo of Archcraft distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::archcraft2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    -o\
    ///                   :ooo:
    ///                  .ooooo.
    ///                  ooooooo.
    ///                 +oooooooo.
    ///                -oooooooooo.
    ///               --:-+oooooooo.
    ///              yooo+=+sooooooo.
    ///             yoooooosooooooooo.
    ///            y+ooooooooooooooooo.
    ///           yoooooooooooooooooooo`
    ///          yoooooo+oo=  :oo++ooooo`
    ///         :oooooo.           +ooooo-
    ///        -ooooooo.   .::.    +ooosoo=
    ///       -oooooo`    .oooo`     +os-=o=
    ///      =ooooooo=:    `oo+    :=ooo=--`.
    ///     +ooooooooos.          .=sooooooo+-
    ///   .+osossos+-`              `-+osososs+.
    ///  :sss+=-:`                     `:-=+ssss:
    /// :=-:`                                `-=+:
    /// ```
    pub fn archcraft2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCHCRAFT2,
            names: vec!["Archcraft2"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Red),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Arch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   -`
    ///                  .o+`
    ///                 `ooo/
    ///                `+oooo:
    ///               `+oooooo:
    ///               -+oooooo+:
    ///             `/:-:++oooo+:
    ///            `/++++/+++++++:
    ///           `/++++++++++++++:
    ///          `/+++ooooooooooooo/`
    ///         ./ooosssso++osssssso+`
    ///        .oossssso-````/ossssss+`
    ///       -osssssso.      :ssssssso.
    ///      :osssssss/        osssso+++.
    ///     /ossssssss/        +ssssooo/-
    ///   `/ossssso+/:-        -:/+osssso+-
    ///  `+sso+:-`                 `.-/+oso:
    /// `++:.                           `-/+/
    /// .`                                 `/
    /// ```
    pub fn arch() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCH,
            names: vec!["arch", "archlinux", "arch-linux", "archmerge"],
            colors: vec![Color::Cyan, Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns alternative logo of Arch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   ▄
    ///                  ▟█▙
    ///                 ▟███▙
    ///                ▟█████▙
    ///               ▟███████▙
    ///              ▂▔▀▜██████▙
    ///             ▟██▅▂▝▜█████▙
    ///            ▟█████████████▙
    ///           ▟███████████████▙
    ///          ▟█████████████████▙
    ///         ▟███████████████████▙
    ///        ▟█████████▛▀▀▜████████▙
    ///       ▟████████▛      ▜███████▙
    ///      ▟█████████        ████████▙
    ///     ▟██████████        █████▆▅▄▃▂
    ///    ▟██████████▛        ▜█████████▙
    ///   ▟██████▀▀▀              ▀▀██████▙
    ///  ▟███▀▘                       ▝▀███▙
    /// ▟▛▀                               ▀▜▙
    /// ```
    pub fn arch2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCH2,
            names: vec!["arch2", "archlinux2", "arch-linux2"],
            colors: vec![Color::Cyan, Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Alter,
        }
    }
    /// returns small logo of Arch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       /\
    ///      /  \
    ///     /    \
    ///    /      \
    ///   /   ,,   \
    ///  /   |  |   \
    /// /_-''    ''-_\
    /// ```
    pub fn arch_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCH_SMALL,
            names: vec!["arch_small", "archlinux_small", "arch-linux-small"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of ARCHlabs distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch_labs().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      'c'
    ///                     'kKk,
    ///                    .dKKKx.
    ///                   .oKXKXKd.
    ///                  .l0XXXXKKo.
    ///                  c0KXXXXKX0l.
    ///                 :0XKKOxxOKX0l.
    ///                :OXKOc. .c0XX0l.
    ///               :OK0o. ...'dKKX0l.
    ///              :OX0c  ;xOx''dKXX0l.
    ///             :0KKo..o0XXKd'.lKXX0l.
    ///            c0XKd..oKXXXXKd..oKKX0l.
    ///          .c0XKk;.l0K0OO0XKd..oKXXKo.
    ///         .l0XXXk:,dKx,.'l0XKo..kXXXKo.
    ///        .o0XXXX0d,:x;   .oKKx'.dXKXXKd.
    ///       .oKXXXXKK0c.;.    :00c'cOXXXXXKd.
    ///      .dKXXXXXXXXk,.     cKx''xKXXXXXXKx'
    ///     'xKXXXXK0kdl:.     .ok; .cdk0KKXXXKx'
    ///    'xKK0koc,..         'c
    /// ```
    pub fn arch_labs() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCHLABS,
            names: vec!["ARCHlabs"],
            colors: vec![Color::Cyan, Color::Red],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of ArchStrike distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch_strike().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    *
    ///                   **.
    ///                  ****
    ///                 ******
    ///                 *******
    ///               ** *******
    ///              **** *******
    ///             ****_____***/*
    ///            ***/*******//***
    ///           **/********///*/**
    ///          **/*******////***/**
    ///         **/****//////.,****/**
    ///        ***/*****/////////**/***
    ///       ****/****    /////***/****
    ///      ******/***  ////   **/******
    ///     ********/* ///      */********
    ///   ,******     // ______ /    ******,
    /// ```
    pub fn arch_strike() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCHSTRIKE,
            names: vec!["ArchStrike"],
            colors: vec![Color::Cyan, Color::Black],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Artix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::artix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    '
    ///                   'o'
    ///                  'ooo'
    ///                 'ooxoo'
    ///                'ooxxxoo'
    ///               'oookkxxoo'
    ///              'oiioxkkxxoo'
    ///             ':;:iiiioxxxoo'
    ///                `'.;::ioxxoo'
    ///           '-.      `':;jiooo'
    ///          'oooio-..     `'i:io'
    ///         'ooooxxxxoio:,.   `'-;'
    ///        'ooooxxxxxkkxoooIi:-.  `'
    ///       'ooooxxxxxkkkkxoiiiiiji'
    ///      'ooooxxxxxkxxoiiii:'`     .i'
    ///     'ooooxxxxxoi:::'`       .;ioxo'
    ///    'ooooxooi::'`         .:iiixkxxo'
    ///   'ooooi:'`                `'';ioxxo'
    ///  'i:'`                          '':io'
    /// '`                                   `'
    /// ```
    pub fn artix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARTIX,
            names: vec!["artix", "artixlinux", "artix-linux"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Artix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::artix_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       /\
    ///      /  \
    ///     /`'.,\
    ///    /     ',
    ///   /      ,`\
    ///  /   ,.'`.  \
    /// /.,'`     `'.\
    /// ```
    pub fn artix_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARTIX_SMALL,
            names: vec!["artix_small", "artixlinux_small", "artix-linux-small"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small,
        }
    }
    /// returns alternative small logo of Artix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::artix2_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             '
    ///            'A'
    ///           'ooo'
    ///          'ookxo'
    ///          `ookxxo'
    ///        '.   `ooko'
    ///       'ooo`.   `oo'
    ///      'ooxxxoo`.   `'
    ///     'ookxxxkooo.`   .
    ///    'ookxxkoo'`   .'oo'
    ///   'ooxoo'`     .:ooxxo'
    ///  'io'`             `'oo'
    /// '`                     `'
    /// ```
    pub fn artix2_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARTIX2_SMALL,
            names: vec!["artix2_small", "artixlinux2_small", "artix-linux2-small"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small | LogoType::Alter,
        }
    }
    /// returns logo of Arco distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arco().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    /-
    ///                   ooo:
    ///                  yoooo/
    ///                 yooooooo
    ///                yooooooooo
    ///               yooooooooooo
    ///             .yooooooooooooo
    ///            .oooooooooooooooo
    ///           .oooooooarcoooooooo
    ///          .ooooooooo-oooooooooo
    ///         .ooooooooo-  oooooooooo
    ///        :ooooooooo.    :ooooooooo
    ///       :ooooooooo.      :ooooooooo
    ///      :oooarcooo         .oooarcooo
    ///     :ooooooooy           .ooooooooo
    ///    :ooooooooo   /ooooooooooooooooooo
    ///   :ooooooooo      .-ooooooooooooooooo.
    ///   ooooooooo-            -ooooooooooooo.
    ///  ooooooooo-                .-oooooooooo.
    /// ooooooooo.                    -ooooooooo
    /// ```
    pub fn arco() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCO,
            names: vec!["arco", "arcolinux", "arco-linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Arco distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arco_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           A
    ///          ooo
    ///         ooooo
    ///        ooooooo
    ///       ooooooooo
    ///      ooooo ooooo
    ///     ooooo   ooooo
    ///    ooooo     ooooo
    ///   ooooo  <oooooooo>
    ///  ooooo      <oooooo>
    /// ooooo          <oooo>
    /// ```
    pub fn arco_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARCO_SMALL,
            names: vec!["arco_small", "arcolinux_small", "arco-linux_small"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Arse distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arse().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                       ⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⠀⠀⠀⠀⠀⠀⣴⣶⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣄⠀⠀⠀⠀⣼⠟⠁⠀⠀⢀⣀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡀⠀⢀⣤⡀⠀⠀⠀⠉⢻⣷⡄⠀⠀⠁⠀⢀⣤⣾⡿⠟⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣷⣿⠏⠀⠀⠀⠀⠀⠀⠹⣿⡄⠀⠀⠀⠙⠉⠁⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⣿⡄⠀⠀⠀⠀⠀⠀⠀⢹⣿⠀⠀⠀⠀⠠⣶⣶⣶⡶
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣿⠀⠀⠀⠀⠀⠀⠀⠀⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⠀⠀⠀⠀⠀⠀⠀⢠⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⠂⠀⠀⠀⠀⠀⢀⣾⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣿⠇⠀⠀⠀⠀⠀⣠⣾⠟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⣿⣇⣀⣀⣀⣠⣴⣾⣿⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⣤⣤⣴⣶⣾⠿⠟⣿⡏⠙⠛⠛⠛⠋⠉⢀⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⣿⡄⠀⠀⠀⠀⠈⠉⠉⠀⠀⠀⠀⣿⡇⠀⠀⠀⠀⠀⠀⢸⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⠇⠀⠀⠀⠀⠀⠀⠘⠿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ```
    pub fn arse() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARSELINUX,
            names: vec!["arse", "arselinux", "arse-linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Arya distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arya().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 `oyyy/-yyyyyy+
    ///                -syyyy/-yyyyyy+
    ///               .syyyyy/-yyyyyy+
    ///               :yyyyyy/-yyyyyy+
    ///            `/ :yyyyyy/-yyyyyy+
    ///           .+s :yyyyyy/-yyyyyy+
    ///          .oys :yyyyyy/-yyyyyy+
    ///         -oyys :yyyyyy/-yyyyyy+
    ///        :syyys :yyyyyy/-yyyyyy+
    ///       /syyyys :yyyyyy/-yyyyyy+
    ///      +yyyyyys :yyyyyy/-yyyyyy+
    ///    .oyyyyyyo. :yyyyyy/-yyyyyy+ ---------
    ///   .syyyyyy+`  :yyyyyy/-yyyyy+-+syyyyyyyy
    ///  -syyyyyy/    :yyyyyy/-yyys:.syyyyyyyyyy
    /// :syyyyyy/     :yyyyyy/-yyo.:syyyyyyyyyyy
    /// ```
    pub fn arya() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ARYA,
            names: vec!["Arya"],
            colors: vec![Color::Green, Color::Red],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Asahi distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::asahi().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    ##  **
    ///                 *####****.
    ///                   ###,
    ///                ...,/#,,,..
    ///           /*,,,,,,,,*,........,,
    ///         ,((((((//*,,,,,,,,,......
    ///        ((((((((((((((%............
    ///      ,(((((((((((((((@@(............
    ///     (((((((((((((((((@@@@/............
    ///   ,((((((((((((((((((@@@@@&*...........
    ///  ((((((((((((((((((((@@@@@@@&,...........
    /// (((((((((((((((((((((@@@&%&@@@%,..........
    ///  /(((((((((((((((((((@@@&%%&@@@@(........
    ///     ,((((((((((((((((@@@&&@@&/&@@@/..
    ///         /((((((((((((@@@@@@/.../&&
    ///            .(((((((((@@@@(....
    ///                /(((((@@#...
    ///                   .((&,
    /// ```
    pub fn asahi() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ASAHI,
            names: vec!["asahi", "asahi-linux", "fedora-asahi-remix"],
            colors: vec![
                Color::Yellow,
                Color::Green,
                Color::Red,
                Color::TrueColor {
                    r: 0,
                    g: 0xaf,
                    b: 0xd7,
                },
                Color::White,
                Color::Cyan,
                Color::Blue,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns alternative logo of Asahi distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::asahi2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  _wwM _ww
    ///                   MMMMMMM
    ///                    MM
    ///            _ww##############yy_
    ///           wMMMMM###########MMMMm
    ///         ,MMMMMMMMM######MMMMMMMM0_
    ///        wMMMMMMMMMMMMMMM0MMMMMMMMMMm
    ///      ,MMMMMMMMMMMMMMMMMMM0MMMMMMMMM0,
    ///     wMMMMMMMMMMMMMMMMMMMMM0MMMMMMMMMMb
    ///   _MMMMMMMMMMMMMMMMMMMMMMMMM0MMMMMMMMM0,
    ///  _MMMMMMMMMMMMMMMMMMMMMMMMMMMWMMMMMMMMMM_
    /// _MMMMMMMMMMMMMMMMMMMMMMMMM0MMMWMMMMMMMMMM_
    ///    ~MMMMMMMMMMMMMMMMMMMMMMMMMWMMMMM00~
    ///        ~MMMMMMMMMMMMMMMMWMMM0MMMMM~
    ///           ~MMMMMMMMMMMMMMM0MMM~~
    ///               ~MMMMMMMMM0MM~
    ///                   ~MMM@~
    ///                     M
    /// ```
    pub fn asahi2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ASAHI2,
            names: vec!["asahi2", "asahi-linux2"],
            colors: vec![
                Color::BrightYellow,
                Color::Cyan,
                Color::Red,
                Color::BrightRed,
                Color::White,
                Color::Black,
                Color::BrightCyan,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Aster distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::aster().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 ...''...
    ///            .;oOXWMWNXXXNMMN0d:.
    ///         .oXMWOo;..       ..:oO;
    ///       ;KMWx,       co,
    ///     'KMNl         dMMW.
    ///    oMMx          xMMMMk
    ///   xMM:          dMMMMMM;
    ///  cMMl          dMMMMMMMW
    ///  NMK          xMMMx::dXMx
    /// ,MMl         xMMN'     .o.
    /// cMM;        dMMW'
    /// ;MMc       oMMW,
    ///  WMK      dMMW,  ccccccc.
    ///  lMMl    oMMM;   ooooooo.
    ///   OMMc   ...
    ///    xMMx
    ///     ;XMN:
    ///       ,.
    /// ```
    pub fn aster() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ASTER,
            names: vec!["aster"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of AsteroidOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::asteroid_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     ***
    ///                    *****
    ///                 **********
    ///               ***************
    ///            *///****////****////.
    ///          (/////// /////// ///////(
    ///       /(((((//*     //,     //((((((.
    ///     (((((((((((     (((        ((((((((
    ///  *(((((((((((((((((((((((        ((((((((
    ///     (((((#(((((((#(((((        ((#(((((
    ///      (#(#(#####(#(#,       ####(#(#
    ///          #########        ########
    ///            /########   ########
    ///               #######%#######
    ///                 (#%%%%%%%#
    ///                    %%%%%
    ///                     %%%
    /// ```
    pub fn asteroid_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ASTEROIDOS,
            names: vec!["AsteroidOS"],
            colors: vec![
                Color::TrueColor {
                    r: 0xd7,
                    g: 0,
                    b: 0,
                },
                Color::TrueColor {
                    r: 0xff,
                    g: 0x87,
                    b: 0,
                },
                Color::TrueColor {
                    r: 0xff,
                    g: 0x5f,
                    b: 0,
                },
                Color::TrueColor {
                    r: 0xff,
                    g: 0xaf,
                    b: 0,
                },
            ],
            color_keys: Some(Color::TrueColor {
                r: 0xd7,
                g: 0,
                b: 0,
            }),
            color_title: Some(Color::TrueColor {
                r: 0xff,
                g: 0x87,
                b: 0,
            }),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of astOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ast_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 oQA#%UMn
    ///                 H       9
    ///                 G       #
    ///                 6       %
    ///                 ?#M#%KW3"
    ///                   // \\\
    ///                 //     \\\
    ///               //         \\\
    ///             //             \\\
    ///         n%@DK&ML       .0O3#@&M_
    ///         P       #       8       W
    ///         H       U       G       #
    ///         B       N       O       @
    ///         C&&#%HNAR       'WS3QMHB"
    ///           // \\\              \\\
    ///         //     \\\              \\\
    ///       //         \\\              \\\
    ///     //             \\\              \\\
    /// uURF##Bv       nKWB%ABc       aM@3R@D@b
    /// 8       M       @       O       #       %
    /// %       &       G       U       @       @
    /// &       @       #       %       %       #
    /// !HGN@MNCf       t&S9#%HQr       ?@G#6S@QP
    /// ```
    pub fn ast_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ASTOS,
            names: vec!["astOS"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Astra distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::astra().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   AA
    ///                  AaaA
    ///                 Aa/\\aA
    ///                Aa/aa\\aA
    ///               Aa/aAAa\\aA
    ///              aA/aaAAaa\\Aa
    ///             aA/aaAAAAaa\\Aa
    ///   aaaaaaAAAAa/aaAAAAAAaa\\aAAAAaaaaa
    /// aAAa-----aaaaaAAAAAAAAAAaaaaa-----aAAa
    ///   aAA\ aAAAAAAAAAAAAAAAAAAAAAAa /AAa
    ///     aAa\\aAAA\\AAAA\\AAAA\\AAA\\AAa/aAa
    ///       aAa\\aA\\\\AAA\\\\AAA\\\\AA\\\\/aAa
    ///        aAA\\aA\\\\AAA\\\\AAA\\\\Aa/AAa
    ///          aA\\aA\\\\AAA\\\\AAA\\\\/Aa
    ///          aA/AA\\\\\\AA\\\\\\AA\\\\\\Aa
    ///         aA/\\AAa\\\\\\Aa\\\\\\Aa\\\\\\Aa
    ///         aA/\\\\AAa\\\\/\\a\\\\\\Aa\\\\Aa
    ///        aA/a\\\\\\Aa\\/AA\\\\\\\\\\Aa\\\\Aa
    ///        aA/aA\\\\/aAa  aAa\\\\\\Aa\\Aa
    ///       aA/\\A\\/Aa        aA\\\\A\\\\Aa
    ///       A|/aaAa            aAaa\\|A
    ///      aAaa                    aaAa
    /// ```
    pub fn astra() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ASTRA_LINUX,
            names: vec!["Astra", "Astra Linux", "astralinux"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Athena distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::athena().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     .          ..
    ///    :####:     ####.
    ///   .################
    ///  :##################
    /// .###################.
    /// ########     #######
    /// #######  #### #####
    /// :#######.      ####
    ///  #########  #   ##   #
    ///  #######   ##      ####
    /// ########  ####    #######
    /// ########  #####   ########
    /// ########  #######  #######
    ///  #######  ########  #######
    ///  ########  #########  ######
    ///   ########  #########  #####
    ///     #######  #########  ####
    ///      #######  #########  ##
    ///        #######  ######## ##
    ///           ###### ######## #
    ///                ### #######
    ///                      ######
    ///                         ####
    ///                           ##
    /// ```
    pub fn athena() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ATHENA,
            names: vec!["Athena"],
            colors: vec![Color::White, Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Azos distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::azos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   ////.                                           (((((
    /// ////////                                        @((((((((
    /// ////////                                        @((((((((
    /// ////////  ///////                      (((((((  @((((((((
    /// //////// /////////                    ((((((((( @((((((((
    /// //////// /////////                    ((((((((( @((((((((
    /// //////// /////////  //////    ((((((  ((((((((( @((((((((
    /// //////// ///////// ////////  (((((((( ((((((((( @((((((((
    /// //////// ///////// ////////  (((((((( ((((((((( @((((((((
    /// //////// ///////// ////////   ((((((( ((((((((( @((((((((
    /// //////// /////////   ///         (    ((((((((( @((((((((
    /// //////// /////////                    ((((((((( @((((((((
    /// //////// /////////                    &(((((((( @((((((((
    /// ////////  //////                        @((((   @((((((((
    /// ////////                                        @((((((((
    /// ////////                                        @((((((((
    ///  /////                                            (((((
    /// ```
    pub fn azos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_AZOS,
            names: vec!["Azos"],
            colors: vec![Color::Cyan, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Bedrock distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::bedrock().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// --------------------------------------
    /// --------------------------------------
    /// --------------------------------------
    /// ---\\\\\\\\\\\\-----------------------
    /// ----\\\      \\\----------------------
    /// -----\\\      \\\---------------------
    /// ------\\\      \\\\\\\\\\\\\\\\\------
    /// -------\\\                    \\\-----
    /// --------\\\                    \\\----
    /// ---------\\\        ______      \\\---
    /// ----------\\\                   ///---
    /// -----------\\\                 ///----
    /// ------------\\\               ///-----
    /// -------------\\\////////////////------
    /// --------------------------------------
    /// --------------------------------------
    /// --------------------------------------
    /// ```
    pub fn bedrock() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BEDROCK,
            names: vec!["bedrock", "bedrocklinux", "bedrock-linux"],
            colors: vec![Color::BrightBlack, Color::White],
            color_keys: Some(Color::BrightBlack),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BigLinux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::big_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                  ...
    ///                               :OWMMMNd.
    ///                             :NMMMMMMMMWc
    ///                   okkl.    kMMMMMW0xdOWMl
    ///   :             xMMMMMW.  kMMMMNc      lW.
    ///  :x             NMMMMMO  ,MMMM0.        'l
    ///  Xx              "lkk"   kMMMX      .okx,
    /// .MX      .cc;.    .xXKx. KMMM:    .OMMMMMl
    /// :MM'   'KMMMMWK:  0MMMMk xMMM.   lWMMMMMMM'
    /// cMMN:;xMMMMk::MMO oMMMMX .XMM. .KMMMWOOMMMd
    /// 'MMMMMMMMN,   NMMx OMMMMl .kM0OMMMMk.  ;MMd
    ///  xMMMMMMd    .MMMW  :NMMMd  .ckKKx'     KMc
    ///   dWMNd.     oMMMN    lkNMX,            oM.
    ///  ;.         ;MMMMx      "MM:.           cO
    ///  .X.       oMMMMW.                      l.
    ///   dMk:..;xWMMMMW,
    ///    kMMMMMMMMMMX.
    ///     :XMMMMMMK:
    ///       ':MM:"      Made in Brazil
    /// ```
    pub fn big_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BIGLINUX,
            names: vec!["BigLinux"],
            colors: vec![Color::Cyan, Color::Yellow, Color::Blue],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Bitrig distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::bitrig().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    `hMMMMN+
    ///    -MMo-dMd`
    ///    oMN- oMN`
    ///    yMd  /NM:
    ///   .mMmyyhMMs
    ///   :NMMMhsmMh
    ///   +MNhNNoyMm-
    ///   hMd.-hMNMN:
    ///   mMmsssmMMMo
    ///  .MMdyyhNMMMd
    ///  oMN.`/dMddMN`
    ///  yMm/hNm+./MM/
    /// .dMMMmo.``.NMo
    /// :NMMMNmmmmmMMh
    /// /MN/-------oNN:
    /// hMd.       .dMh
    /// sm/         /ms
    /// ```
    pub fn bitrig() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BITRIG,
            names: vec!["Bitrig"],
            colors: vec![Color::Green],
            color_keys: Some(Color::White),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Blackarch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::blackarch().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    00
    ///                    11
    ///                   ====
    ///                   .//
    ///                  `o//:
    ///                 `+o//o:
    ///                `+oo//oo:
    ///                -+oo//oo+:
    ///              `/:-:+//ooo+:
    ///             `/+++++//+++++:
    ///            `/++++++//++++++:
    ///           `/+++oooo//ooooooo/`
    ///          ./ooosssso//osssssso+`
    ///         .oossssso-`//`/ossssss+`
    ///        -osssssso.  //  :ssssssso.
    ///       :osssssss/   //   osssso+++.
    ///      /ossssssss/   //   +ssssooo/-
    ///    `/ossssso+/:-   //   -:/+osssso+-
    ///   `+sso+:-`        //       `.-/+oso:
    ///  `++:.             //            `-/+/
    ///  .`                /                `/
    /// ```
    pub fn blackarch() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BLACKARCH,
            names: vec!["Blackarch"],
            colors: vec![Color::Red, Color::BrightRed, Color::Black],
            color_keys: Some(Color::BrightRed),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BlackMesa
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::black_mesa().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .-;+XHHHHHHX+;-
    ///         ,:X@@X%/;=----=:\%X@@X:,
    ///       =@@%=.              .=+H@X:
    ///     -XMX:                      =XMX=
    ///    /@@:                          =H@+
    ///   %@X.                            .@
    ///  +@X,                               @%
    /// /@@,                                .@@\
    /// %@%                                  +@
    /// H@:                                  :@H
    /// H@:         :HHHHHHHHHHHHHHHHHHX,    =@H
    /// %@%         ;@M@@@@@@@@@@@@@@@@@H-   +@
    /// =@@,        :@@@@@@@@@@@@@@@@@@@@@= .@@:
    ///  =@X        :@@@@@@@@@@@@@@@@@@@@@@:%@%
    ///   @,      ;@@@@@@@@@@@@@@@@@M@@@@@@
    ///    +@@HHHHHHH@@@@@@@@@@@@@@@@@@@@@@@
    ///     =X@@@@@@@@@@@@@@@@@@@@@@@@@@@X=
    ///       :@@@@@@@@@@@@@@@@@@M@@@@:
    ///         \@@@@@@@@@@@@@@@@@@X/-
    ///            .-;+XXHHHHHX+;-.
    /// ```
    pub fn black_mesa() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BLACKMESA,
            names: vec!["BlackMesa", "black-mesa"],
            colors: vec![Color::Black],
            color_keys: Some(Color::Black),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BlackPather distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::black_panther().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          ........
    ///                   .,»╔╗╗╬▄▄╫█▀▓▄▄╬╗╗g≈,.
    ///                ,j╗╬╣▓▓███████▌;»╙▀▀▀▀█▄▄╗j,
    ///             .≈╗╬▓██▀▀▀▀▀╠╙░░»»;:```>▄ ▐ ▓╫╗⌂,
    ///           .j╬▓█▀▒░░░░░░░░░»»»;:````      ╙▀█▌╬░,
    ///          ;╗▓█▄▄███████▀░░»»»»;```` ╓▄▄█▄▄φ  ██▌Ñ>.
    ///        .j╣█████▀▀░░░░░░░░»»╓▄▄¿``▄███████/▄████▓╬U.
    ///       .j╣▓██▀ÜÑ╦╦░░░░░░▐█@▄████⌐▐███████████████▓╬H.
    ///       «╫▓█▀░ÑÑ╩╦░░░░░░░░▀██████M"▀███████████████▓╫░
    ///      :]╣█▌ÑÑÑÑ▄▄██▀░░░░»»██████████████████████████Ñ~
    ///      »╫▓█╫ÑÑ▄███▀░░░░░»»▐██████████████████████████▌░
    ///     `j╣█▌Ñ╬████░░░░░░░»»▐████████████████████████▌▐█U`
    ///     `/╫█▌▄███▌░░░░░░░»»»;▀██████████████▀████████w▐█░`
    ///      ;╟█▌███▌░░░░░░░▄▄»»;:`▀▀████████▀Ü▄████████▌ ▐▌>`
    ///      `]▓████░░░░░░░░██⌂;:````╓▄▄µp╓▄▄██████████▀ ,█M`
    ///       "╠╣██▌░░░░░░░»██▌;````  ╙▀██████████████M  █▀"
    ///        "╟╣█░░░░░░░░»███⌂```      ▐▀████████▀░   █▌░`
    ///         "╩█▄░░░░░░»»▀███ ``           └└`     ,█▀"`
    ///          `░▀█▄░░░»»»»████@                  .▄█Ü`
    ///            `╙▀█▄@»»»;`▀███▌¿              ,▄▀Ñ"`
    ///              `"╨▀█▄▄▄░`▐█████▄,       ,▄▄▀▀░`
    ///                 `"╙╩▀▀▀▀████████▓▌▌▌▀▀▀╨"``
    ///                     ``""░╚╨╝╝╝╝╨╨░""``
    /// ```
    pub fn black_panther() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BLACKPANTHER,
            names: vec!["BlackPanther"],
            colors: vec![Color::Red, Color::Yellow, Color::BrightBlue],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Blag distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::blag().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              d
    ///             ,MK:
    ///             xMMMX:
    ///            .NMMMMMX;
    ///            lMMMMMMMM0clodkO0KXWW:
    ///            KMMMMMMMMMMMMMMMMMMX'
    ///       .;d0NMMMMMMMMMMMMMMMMMMK.
    ///  .;dONMMMMMMMMMMMMMMMMMMMMMMx
    /// 'dKMMMMMMMMMMMMMMMMMMMMMMMMl
    ///    .:xKWMMMMMMMMMMMMMMMMMMM0.
    ///        .:xNMMMMMMMMMMMMMMMMMK.
    ///           lMMMMMMMMMMMMMMMMMMK.
    ///           ,MMMMMMMMWkOXWMMMMMM0
    ///           .NMMMMMNd.     `':ldko
    ///            OMMMK:
    ///            oWk,
    ///            ;:
    /// ```
    pub fn blag() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BLAG,
            names: vec!["BLAG"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BlankOn distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::blank_on().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         `./ohdNMMMMNmho+.`        .+oo:`
    ///       -smMMMMMMMMMMMMMMMMmy-`    `yyyyy+
    ///    `:dMMMMMMMMMMMMMMMMMMMMMMd/`  `yyyyys
    ///   .hMMMMMMMNmhso/++symNMMMMMMMh- `yyyyys
    ///  -mMMMMMMms-`         -omMMMMMMN-.yyyyys
    /// .mMMMMMMy.              .yMMMMMMm:yyyyys
    /// sMMMMMMy                 `sMMMMMMhyyyyys
    /// NMMMMMN:                  .NMMMMMNyyyyys
    /// MMMMMMm.                   NMMMMMNyyyyys
    /// hMMMMMM+                  /MMMMMMNyyyyys
    /// :NMMMMMN:                :mMMMMMM+yyyyys
    ///  oMMMMMMNs-            .sNMMMMMMs.yyyyys
    ///   +MMMMMMMNho:.`  `.:ohNMMMMMMNo `yyyyys
    ///    -hMMMMMMMMNNNmmNNNMMMMMMMMh-  `yyyyys
    ///      :yNMMMMMMMMMMMMMMMMMMNy:`   `yyyyys
    ///        .:sdNMMMMMMMMMMNds/.      `yyyyyo
    ///            `.:/++++/:.`           :oys+.
    /// ```
    pub fn blank_on() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BLANKON,
            names: vec!["BlankOn"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BlueLight distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::blue_light().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               oMMNMMMMMMMMMMMMMMMMMMMMMM
    ///               oMMMMMMMMMMMMMMMMMMMMMMMMM
    ///               oMMMMMMMMMMMMMMMMMMMMMMMMM
    ///               oMMMMMMMMMMMMMMMMMMMMMMMMM
    ///               -+++++++++++++++++++++++mM
    ///              ```````````````````````..dM
    ///            ```````````````````````....dM
    ///          ```````````````````````......dM
    ///        ```````````````````````........dM
    ///      ```````````````````````..........dM
    ///    ```````````````````````............dM
    /// .::::::::::::::::::::::-..............dM
    ///  `-+yyyyyyyyyyyyyyyyyyyo............+mMM
    ///      -+yyyyyyyyyyyyyyyyo..........+mMMMM
    ///         ./syyyyyyyyyyyyo........+mMMMMMM
    ///            ./oyyyyyyyyyo......+mMMMMMMMM
    ///               omdyyyyyyo....+mMMMMMMMMMM
    ///               oMMMmdhyyo..+mMMMMMMMMMMMM
    ///               oNNNNNNmdsomMMMMMMMMMMMMMM
    /// ```
    pub fn blue_light() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BLUELIGHT,
            names: vec!["BlueLight"],
            colors: vec![Color::White, Color::Blue],
            color_keys: Some(Color::White),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Bodhi distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::bodhi().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// |           ,,mmKKKKKKKKWm,,
    ///  '      ,aKKPLL**********|L*TKp,
    ///    t  aKPL**```          ```**L*Kp
    ///     IXELL,wwww,              ``*||Kp
    ///   ,#PL|KKKpPP@IPPTKmw,          `*||K
    ///  ,KLL*{KKKKKKPPbKPhpKKPKp        `||K
    ///  #PL  !KKKKKKPhKPPPKKEhKKKKp      `||K
    /// !HL*   1KKKKKKKphKbPKKKKKKKKp      `|IW
    /// bL     KKKKKKKKBQKhKbKKKKKKKK       |IN
    /// bL     !KKKKKKKKKKNKKKKKKKPP`       |Ib
    /// THL*     TKKKKKK##KKKN@KKKK^         |IM
    ///  K@L      *KKKKKKKKKKKEKE5          ||K
    ///  `NLL      `KKKKKKKKKK"```|L       ||#P
    ///   `K@LL       `"**"`        '.   :||#P
    ///     YpLL                      ' |LM`
    ///      `TppLL,                ,|||p'L
    ///         "KppLL++,.,    ,,|||#K*   '.
    ///            `"MKWpppppppp#KM"`        `h,
    /// ```
    pub fn bodhi() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BODHI,
            names: vec!["Bodhi"],
            colors: vec![Color::White, Color::BrightYellow, Color::Green],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Bonsai distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::bonsai().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    ,####,
    ///    #######,  ,#####,
    ///    #####',#  '######
    ///     ''###'';,,,'###'
    ///           ,;  ''''
    ///          ;;;   ,#####,
    ///         ;;;'  ,,;;;###
    ///         ';;;;'''####'
    ///          ;;;
    ///       ,.;;';'',,,
    ///      '     '
    ///  #
    ///  #                        O
    ///  ##, ,##,',##, ,##  ,#,   ,
    ///  # # #  # #''# #,,  # #   #
    ///  '#' '##' #  #  ,,# '##;, #h,
    /// ```
    pub fn bonsai() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BONSAI,
            names: vec!["Bonsai"],
            colors: vec![Color::Cyan, Color::Green, Color::Yellow],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ,        ,
    ///             /(        )`
    ///             \ \___   / |
    ///             /- _  `-/  '
    ///            (/\/ \ \   /\
    ///            / /   | `    \
    ///            O O   ) /    |
    ///            `-^--'`<     '
    ///           (_.)  _  )   /
    ///            `.___/`    /
    ///              `-----' /
    /// <----.     __ / __   \
    /// <----|====O)))==) \) /====|
    /// <----'    `--' `.__,' \
    ///              |        |
    ///               \       /       /\
    ///          ______( (_  / \______/
    ///        ,'  ,-----'   |
    ///        `--{__________)
    /// ```
    pub fn bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BSD,
            names: vec!["BSD"],
            colors: vec![
                Color::Red,
                Color::White,
                Color::Blue,
                Color::Yellow,
                Color::Cyan,
            ],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of BunsenLabs distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::bunsen_labs().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         `++
    ///       -yMMs
    ///     `yMMMMN`
    ///    -NMMMMMMm.
    ///   :MMMMMMMMMN-
    ///  .NMMMMMMMMMMM/
    ///  yMMMMMMMMMMMMM/
    /// `MMMMMMNMMMMMMMN.
    /// -MMMMN+ /mMMMMMMy
    /// -MMMm`   `dMMMMMM
    /// `MMN.     .NMMMMM.
    ///  hMy       yMMMMM`
    ///  -Mo       +MMMMN
    ///   /o       +MMMMs
    ///            +MMMN`
    ///            hMMM:
    ///           `NMM/
    ///           +MN:
    ///           mh.
    ///          -/
    /// ```
    pub fn bunsen_labs() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_BUNSENLABS,
            names: vec!["BunsenLabs"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of CachyOs distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cachy_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .-------------------------:
    ///           .+=========================.
    ///          :++===++==================-       :++-
    ///         :*++====+++++=============-        .==:
    ///        -*+++=====+***++==========:
    ///       =*++++========------------:
    ///      =*+++++=====-                     ...
    ///    .+*+++++=-===:                    .=+++=:
    ///   :++++=====-==:                     -*****+
    ///  :++========-=.                      .=+**+.
    /// .+==========-.                          .
    ///  :+++++++====-                                .--==-.
    ///   :++==========.                             :+++++++:
    ///    .-===========.                            =*****+*+
    ///     .-===========:                           .+*****+:
    ///       -=======++++:::::::::::::::::::::::::-:  .---:
    ///        :======++++====+++******************=.
    ///         :=====+++==========++++++++++++++*-
    ///          .====++==============++++++++++*-
    ///           .===+==================+++++++:
    ///            .-=======================+++:
    ///              ..........................
    /// ```
    pub fn cachy_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CACHYOS,
            names: vec!["Cachy", "cachyos", "cachy-linux", "cachyos-linux"],
            colors: vec![Color::Cyan, Color::Green, Color::Black],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of CachyOs distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cachy_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    /''''''''''''/
    ///   /''''''''''''/
    ///  /''''''/
    /// /''''''/
    /// \......\
    ///  \......\
    ///   \.............../
    ///    \............./
    /// ```
    pub fn cachy_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CACHYOS_SMALL,
            names: vec![
                "Cachy_small",
                "cachyos_small",
                "cachy-linux-small",
                "cachyos-linux-small",
            ],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Calculate distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::calculate().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                               ......
    ///                            ,,+++++++,.
    ///                          .,,,....,,,+**+,,.
    ///                        ............,++++,,,
    ///                       ...............
    ///                     ......,,,........
    ///                   .....+*#####+,,,*+.
    ///               .....,*###############,..,,,,,,..
    ///            ......,*#################*..,,,,,..,,,..
    ///          .,,....*####################+***+,,,,...,++,
    ///        .,,..,..*#####################*,
    ///      ,+,.+*..*#######################.
    ///    ,+,,+*+..,########################*
    /// .,++++++.  ..+##**###################+
    /// .....      ..+##***#################*.
    ///            .,.*#*****##############*.
    ///            ..,,*********#####****+.
    ///      .,++*****+++*****************+++++,.
    ///       ,++++++**+++++***********+++++++++,
    ///      .,,,,++++,..  .,,,,,.....,+++,.,,
    /// ```
    pub fn calculate() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CALCULATE,
            names: vec!["Calculate"],
            colors: vec![Color::White, Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Calinix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::calinix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣠⠤⠔⠒⠒⠋⠉⠉⠉⠉⠓⠒⠒⠦⠤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠤⠒⠉⣁⣠⣤⣶⣶⣿⣿⣿⣿⣿⣿⣿⣿⣶⣶⣤⣄⣈⠙⠲⢤⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠴⠋⢁⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⡈⠑⢦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⣠⠞⢁⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄⠈⠢⡀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⢀⠞⠁⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⠋⠉⠁⠀⠀⠀⠀⠈⠉⠙⠛⠿⣿⣿⣿⣿⣿⣿⠏⠀⠀⠀⠈⢢⡀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⡰⠃⣠⣾⣿⣿⣿⣿⣿⣿⡿⠛⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠻⢿⡿⠁⠀⠀⠀⠀⠀⠀⠙⣄⠀⠀⠀⠀
    /// ⠀⠀⠀⡼⠁⣴⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢆⠀⠀⠀
    /// ⠀⠀⡼⠀⣼⣿⣿⣿⣿⣿⣿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣆⠀⠀
    /// ⠀⣰⠁⣸⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠉⠻⣿⣿⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⡄⠀
    /// ⢀⡇⢠⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠛⢿⣿⣿⣿⣿⣿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢳⠀
    /// ⢸⠀⣸⣿⣿⣿⣿⣿⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿⣿⣿⣿⣿⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⡄
    /// ⣼⠀⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣿⣿⣿⣷⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇
    /// ⡇⠀⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢛⣿⣿⣿⣿⣿⣿⣿⡦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇
    /// ⢻⠀⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣶⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇
    /// ⢸⡀⢹⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣾⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⠃
    /// ⠀⣇⠘⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⣿⣿⣿⡿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡼⠀
    /// ⠀⠸⡄⢹⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠀⣠⣶⣿⣿⣿⣿⣿⣿⠟⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⠃⠀
    /// ⠀⠀⢳⡀⢻⣿⣿⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀⠈⠉⠉⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠏⠀⠀
    /// ⠀⠀⠀⠳⡀⠻⣿⣿⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣷⣄⡀⠀⠀⠀⠀⢠⠏⠀⠀⠀
    /// ⠀⠀⠀⠀⠙⣄⠙⢿⣿⣿⣿⣿⣿⣿⣷⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣴⣾⣿⣿⣿⣿⣿⣦⡀⠀⡰⠃⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠈⠢⡈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣤⣄⣀⡀⠀⠀⠀⠀⢀⣀⣠⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⣠⠞⠁⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠈⠢⡈⠙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠋⣡⠞⠁⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠓⢤⡈⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⣁⠴⠊⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠑⠢⢄⣉⠙⠛⠿⠿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠿⠛⠋⣉⡤⠖⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠓⠒⠢⠤⠤⠤⠤⠤⠤⠤⠤⠖⠒⠋⠉⠀
    /// ```
    pub fn calinix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CALINIXOS,
            names: vec!["Calinix", "calinixos"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Calinix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::calinix_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⣀⠤⠐⣂⣈⣩⣭⣭⣍⣀⣐⠀⠄⡀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⡀⠔⣨⣴⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣦⣅⠢⡀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠠⢊⣴⣾⣿⣿⣿⣿⠿⠟⠛⠛⠛⠛⠻⠿⣿⣿⣿⣿⠃⠀⠠⡀⠀⠀⠀
    /// ⠀⠀⡐⢡⣾⣿⣿⣿⠟⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠁⠀⠀⠀⠈⢆⠀⠀
    /// ⠀⡘⢰⣿⣿⣿⡟⠁⠀⠀⢀⣀⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢂⠀
    /// ⢠⢠⣿⣿⣿⡟⠀⠀⠀⠀⠀⠙⠿⣿⣿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⡀
    /// ⡄⢸⣿⣿⣿⠁⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁
    /// ⡇⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣹⣿⣿⣿⣷⠄⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠃⢸⣿⣿⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣿⣿⡿⠛⠁⠀⠀⠀⠀⠀⠀⠀⠀⡀
    /// ⠘⡘⣿⣿⣿⣧⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⠿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⠁
    /// ⠀⠡⠸⣿⣿⣿⣧⡀⠀⠀⠀⠉⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⢀⠀⠀⠀⠀⢀⠆⠀
    /// ⠀⠀⠡⡘⢿⣿⣿⣿⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣴⣿⣷⣦⡀⢀⠊⠀⠀
    /// ⠀⠀⠀⠈⠊⡻⢿⣿⣿⣿⣿⣶⣤⣤⣤⣤⣤⣤⣶⣿⣿⣿⣿⡿⢟⠕⠁⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠈⠢⢙⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⡩⠐⠁⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠈⠐⠂⠭⠉⠙⣛⣛⠋⠉⠭⠐⠂⠁⠀⠀⠀⠀
    /// ```
    pub fn calinix_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CALINIXOS_SMALL,
            names: vec!["Calinix_small", "calinixos_small"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Carbs distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::carbs().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ..........
    ///           ..,;:ccccccc:;'..
    ///        ..,clllc:;;;;;:cllc,.
    ///       .,cllc,...     ..';;'.
    ///      .;lol;..           ..
    ///     .,lol;.
    ///     .coo:.
    ///    .'lol,.
    ///    .,lol,.
    ///    .,lol,.
    ///     'col;.
    ///     .:ooc'.
    ///     .'col:.
    ///      .'cllc'..          .''.
    ///       ..:lolc,'.......',cll,.
    ///         ..;cllllccccclllc;'.
    ///           ...',;;;;;;,,...
    ///                 .....
    /// ```
    pub fn carbs() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CARBS,
            names: vec!["Carbs"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of CBL-Mariner distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cbl_mariner().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     .
    ///                   :-  .
    ///                 :==. .=:
    ///               :===:  -==:
    ///             :-===:  .====:
    ///           :-====-   -=====:
    ///          -======   :=======:
    ///         -======.  .=========:
    ///        -======:   -==========.
    ///       -======-    -===========.
    ///      :======-      :===========.
    ///     :=======.       .-==========.
    ///    :=======:          -==========.
    ///   :=======-            :==========.
    ///  :=======-              .-========-
    /// :--------.                :========-
    ///                     ..:::--=========-
    ///             ..::---================-=-
    /// ```
    pub fn cbl_mariner() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CBL_MARINER,
            names: vec!["CBL-Mariner"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Cel distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::celos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `-:/++++/:-`
    ///           -/syyyyyyyyyyyyy+-
    ///         :ssssyyyyyyyyyyyyyyyy/
    ///       .osymmmmmmmmmmmmmmmNNNNNmmhy+
    ///      .sssshhhhhhhddddddddddddddds-
    ///     `osssssssyyyyyyyyyyyyyyyyyyhy`
    ///     :ssssssyyyyyyyyyyyyyyyyyyyyhh/
    /// sMMMMMMMMMMMMMMMMMMMMMMMhyyyyyyhho
    ///     :sssssssyyyyyyyyyyyyyyyyyyyhh/
    ///     `ssssssssyyyyyyyyyyyyyyyyyyhy.
    ///      -sssssyddddddddddddddddddddy
    ///       -sssshmmmmmmmmmmmmmmmmmmmyssss-
    ///        `/ssssyyyyyyyyyyyyyyyy+`
    ///          `:osyyyyyyyyyyyyys/`
    ///             `.:/+ooooo+:-`
    /// ```
    pub fn celos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CELOS,
            names: vec!["Cel", "celos", "cel-linux", "celos-linux"],
            colors: vec![Color::Magenta, Color::Black],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Center distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::center().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .
    ///                 o,
    ///         .       d,       .
    ///         ';'   ..d;..  .cl'
    ///           .:; 'oldO,.oo.
    ///           ..,:,xKXxoo;'.
    ///     ,;;;;;ldxkONMMMXxkxc;;;;;.
    ///     .....':oddXWMNOxlcl:......
    ///            .:dlxk0c;:. .
    ///           :d:.,xcld,.,:.
    ///         ;l,    .l;     ';'
    ///                .o;
    ///                 l,
    /// ```
    pub fn center() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CENTER,
            names: vec!["Center"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of CentOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cent_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ..
    ///                .PLTJ.
    ///               <><><><>
    ///      KKSSV' 4KKK LJ KKKL.'VSSKK
    ///      KKV' 4KKKKK LJ KKKKAL 'VKK
    ///      V' ' 'VKKKK LJ KKKKV' ' 'V
    ///      .4MA.' 'VKK LJ KKV' '.4Mb.
    ///    . KKKKKA.' 'V LJ V' '.4KKKKK .
    ///  .4D KKKKKKKA.'' LJ ''.4KKKKKKK FA.
    /// <QDD ++++++++++++  ++++++++++++ GFD>
    ///  'VD KKKKKKKK'.. LJ ..'KKKKKKKK FV
    ///    ' VKKKKK'. .4 LJ K. .'KKKKKV '
    ///       'VK'. .4KK LJ KKA. .'KV'
    ///      A. . .4KKKK LJ KKKKA. . .4
    ///      KKA. 'KKKKK LJ KKKKK' .4KK
    ///      KKSSA. VKKK LJ KKKV .4SSKK
    ///               <><><><>
    ///                'MKKM'
    ///                  ''
    /// ```
    pub fn cent_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CENTOS,
            names: vec!["Cent", "centos", "cent-linux", "centos-linux"],
            colors: vec![
                Color::Yellow,
                Color::Green,
                Color::Blue,
                Color::Magenta,
                Color::White,
            ],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of CentOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cent_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  ____^____
    ///  |\  |  /|
    ///  | \ | / |
    /// <---- ---->
    ///  | / | \ |
    ///  |/__|__\|
    ///      v
    /// ```
    pub fn cent_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CENTOS_SMALL,
            names: vec![
                "Cent_small",
                "centos_small",
                "cent-linux_small",
                "cent-linux-small",
                "centos-linux-small",
            ],
            colors: vec![Color::Yellow, Color::Green, Color::Blue, Color::Magenta],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Chakra distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::chakra().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///      _ _ _        "kkkkkkkk.
    ///    ,kkkkkkkk.,    'kkkkkkkkk,
    ///    ,kkkkkkkkkkkk., 'kkkkkkkkk.
    ///   ,kkkkkkkkkkkkkkkk,'kkkkkkkk,
    ///  ,kkkkkkkkkkkkkkkkkkk'kkkkkkk.
    ///   "''"''',;::,,"''kkk''kkkkk;   __
    ///       ,kkkkkkkkkk, "k''kkkkk' ,kkkk
    ///     ,kkkkkkk' ., ' .: 'kkkk',kkkkkk
    ///   ,kkkkkkkk'.k'   ,  ,kkkk;kkkkkkkkk
    ///  ,kkkkkkkk';kk 'k  "'k',kkkkkkkkkkkk
    /// .kkkkkkkkk.kkkk.'kkkkkkkkkkkkkkkkkk'
    /// ;kkkkkkkk''kkkkkk;'kkkkkkkkkkkkk''
    /// 'kkkkkkk; 'kkkkkkkk.,""''"''""
    ///   ''kkkk;  'kkkkkkkkkk.,
    ///      ';'    'kkkkkkkkkkkk.,
    ///              ';kkkkkkkkkk'
    ///                ';kkkkkk'
    ///                   "''"
    /// ```
    pub fn chakra() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CHAKRA,
            names: vec!["Chakra"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of ChaletOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::chalet_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `.//+osso+/:``
    ///          `/sdNNmhyssssydmNNdo:`
    ///        :hNmy+-`          .-+hNNs-
    ///      /mMh/`       `+:`       `+dMd:
    ///    .hMd-        -sNNMNo.  /yyy  /mMs`
    ///   -NM+       `/dMd/--omNh::dMM   `yMd`
    ///  .NN+      .sNNs:/dMNy:/hNmo/s     yMd`
    ///  hMs    `/hNd+-smMMMMMMd+:omNy-    `dMo
    /// :NM.  .omMy:/hNMMMMMMMMMMNy:/hMd+`  :Md`
    /// /Md` `sm+.omMMMMMMMMMMMMMMMMd/-sm+  .MN:
    /// /Md`      MMMMMMMMMMMMMMMMMMMN      .MN:
    /// :NN.      MMMMMMm....--NMMMMMN      -Mm.
    /// `dMo      MMMMMMd      mMMMMMN      hMs
    ///  -MN:     MMMMMMd      mMMMMMN     oMm`
    ///   :NM:    MMMMMMd      mMMMMMN    +Mm-
    ///    -mMy.  mmmmmmh      dmmmmmh  -hMh.
    ///      oNNs-                    :yMm/
    ///       .+mMdo:`            `:smMd/`
    ///          -ohNNmhsoo++osshmNNh+.
    ///             `./+syyhhyys+:``
    /// ```
    pub fn chalet_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CHALETOS,
            names: vec!["ChaletOS"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Chapeau distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::chapeau().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                .-/-.
    ///             ////////.
    ///           ////////y+//.
    ///         ////////mMN/////.
    ///       ////////mMN+////////.
    ///     ////////////////////////.
    ///   /////////+shhddhyo+////////.
    ///  ////////ymMNmdhhdmNNdo///////.
    /// ///////+mMms////////hNMh///////.
    /// ///////NMm+//////////sMMh///////
    /// //////oMMNmmmmmmmmmmmmMMm///////
    /// //////+MMmssssssssssssss+///////
    /// `//////yMMy////////////////////
    ///  `//////smMNhso++oydNm////////
    ///   `///////ohmNMMMNNdy+///////
    ///     `//////////++//////////
    ///        `////////////////.
    ///            -////////-
    /// ```
    pub fn chapeau() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CHAPEAU,
            names: vec!["Chapeau"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Chimera Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::chimera_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ddddddddddddddc  ,cc:
    /// ddddddddddddddc  ,cc:
    /// ddddddddddddddd  ,cc:
    /// ddddddddddddl:'  ,cc:
    /// dddddddddl'    ..;cc:
    /// dddddddo.   ,:cccccc:
    /// ddddddl   ,ccc:'''''
    /// dddddo.  ;ccc.          ............
    ///         .ccc.           cccccccccccc
    /// ......  .ccc.          .ccc'''''''''
    /// OOOOOk.  ;ccc.        .ccc;   ......
    /// OOOOOOd   'ccc:,....,:ccc'   coooooo
    /// OOOOOOOx.   ':cccccccc:'   .looooooo
    /// OOOOOOOOOd,     `'''`     .coooooooo
    /// OOOOOOOOOOOOdc,.    ..,coooooooooooo
    /// OOOOOOOOOOOOOOOO'  .oooooooooooooooo
    /// OOOOOOOOOOOOOOOO'  .oooooooooooooooo
    /// OOOOOOOOOOOOOOOO'  .oooooooooooooooo
    /// ```
    pub fn chimera_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CHIMERA_LINUX,
            names: vec!["Chimera Linux"],
            colors: vec![Color::Red, Color::Magenta, Color::Blue, Color::Red],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Chonky Seal OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::chonky_seal_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   .-/-.
    ///             .:-=++****++=-:.
    ///         .:=+*##%%%%%%%%%%##*+=:.
    ///       :=*#%%%%%%%%%%%%%%%%%%%%#*=:
    ///     :=*#%%%%%%%%%%%%%%%%%%%%%%%%#*=.
    ///    -+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%#+-
    ///   =+#%%%%@@@@@@@%%%%%%%@@@@@@@%%%%%#+=
    ///  =+#@%%%%*+=-==*%%%%%%%#+====*%%%%%@#+=
    /// :+*%%%%@*       +@%%%@#       -@%%%%%*+:
    /// =+#%%%%%%#+====*###%%##*=--=+*%%%%%%%#+=
    /// +*%%%%%%%@@##%%%%*=::=#%%%##%@%%%%%%%%*+
    /// +*%%%%%%%@**@%%%%%@==@%%%%%@+#%%%%%%%%*+
    /// =+#%%%%%%@#*@%%%%%%**%%%%%@%+%%%%%%%%#+=
    /// :+*%%%%%%%@#*####**###*####*%@%%%%%%%*+:
    ///  =+#@%%%%%%@%%%%%%%@@%%%%%%%%%%%%%%@#+=
    ///   =+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#+=
    ///    -+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%*+-
    ///     .=*#%%%%%%%%%%%%%%%%%%%%%%%%#*=.
    ///       :=*##%%%%%%%%%%%%%%%%%%##*=:
    ///         .:=+*##%%%%%%%%%%##*+=:.
    ///             .:-=++****++=-:.
    /// ```
    pub fn chonky_seal_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CHONKYSEALOS,
            names: vec!["ChonkySealOS"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Chrome OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::chrom_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             .,:loool:,.
    ///         .,coooooooooooooc,.
    ///      .,lllllllllllllllllllll,.
    ///     ;ccccccccccccccccccccccccc;
    ///   'ccccccccccccccccccccccccccccc.
    ///  ,ooc::::::::okO0000OOkkkkkkkkkkk:
    /// .ooool;;;;:xK0kxxxxxk0XK0000000000.
    /// :oooool;,;OKdddddddddddKX000000000d
    /// lllllool;lNdllllllllllldNK000000000
    /// llllllllloMdcccccccccccoWK000000000
    /// ;cllllllllXXc:::::::::c0X000000000d
    /// .ccccllllllONkc;,,,;cxKK0000000000.
    ///  .cccccclllllxOOOOOOkxO0000000000;
    ///   .:cccccccclllllllloO0000000OOO,
    ///     ,:ccccccccclllcd0000OOOOOOl.
    ///       '::cccccccccdOOOOOOOkx:.
    ///         ..,::ccccxOOOkkko;.
    ///             ..,:dOkxl:.
    /// ```
    pub fn chrom_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CHROM,
            names: vec!["Chrome", "ChromeOS"],
            colors: vec![
                Color::Green,
                Color::Red,
                Color::Yellow,
                Color::Blue,
                Color::White,
            ],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Cleanjaro distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cleanjaro().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ███████▌ ████████████████
    /// ███████▌ ████████████████
    /// ███████▌ ████████████████
    /// ███████▌
    /// ███████▌
    /// ███████▌
    /// ███████▌
    /// ███████▌
    /// █████████████████████████
    /// █████████████████████████
    /// █████████████████████████
    /// ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
    /// ```
    pub fn cleanjaro() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CLEANJARO,
            names: vec!["Cleanjaro"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Cleanjaro distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cleanjaro_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// █████ ██████████
    /// █████ ██████████
    /// █████
    /// █████
    /// █████
    /// ████████████████
    /// ████████████████
    /// ```
    pub fn cleanjaro_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CLEANJARO_SMALL,
            names: vec!["Cleanjaro_small"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Clear Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::clear_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           BBB
    ///        BBBBBBBBB
    ///      BBBBBBBBBBBBBBB
    ///    BBBBBBBBBBBBBBBBBBBB
    ///    BBBBBBBBBBB         BBB
    ///   BBBBBBBBYYYYY
    ///   BBBBBBBBYYYYYY
    ///   BBBBBBBBYYYYYYY
    ///   BBBBBBBBBYYYYYW
    ///  GGBBBBBBBYYYYYWWW
    ///  GGGBBBBBBBYYWWWWWWWW
    ///  GGGGGGBBBBBBWWWWWWWW
    ///  GGGGGGGGBBBBWWWWWWWW
    /// GGGGGGGGGGGBBBWWWWWWW
    /// GGGGGGGGGGGGGBWWWWWW
    /// GGGGGGGGWWWWWWWWWWW
    /// GGWWWWWWWWWWWWWWWW
    ///  WWWWWWWWWWWWWWWW
    ///       WWWWWWWWWW
    ///           WWW
    /// ```
    pub fn clear_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CLEAR_LINUX,
            names: vec!["Clear Linux", "clearlinux", "Clear Linux OS"],
            colors: vec![
                Color::Blue,
                Color::Yellow,
                Color::BrightBlue,
                Color::BrightYellow,
            ],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Clear OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::clear_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `.--::::::--.`
    ///          .-:////////////////:-.
    ///       `-////////////////////////-`
    ///      -////////////////////////////-
    ///    `//////////////-..-//////////////`
    ///   ./////////////:      ://///////////.
    ///  `//////:..-////:      :////-..-//////`
    ///  ://////`    -///:.``.:///-`    ://///:
    /// `///////:.     -////////-`    `:///////`
    /// .//:--////:.     -////-`    `:////--://.
    /// ./:    .////:.     --`    `:////-    :/.
    /// `//-`    .////:.        `:////-    `-//`
    ///  :///-`    .////:.    `:////-    `-///:
    ///  `/////-`    -///:    :///-    `-/////`
    ///   `//////-   `///:    :///`   .//////`
    ///    `:////:   `///:    :///`   -////:`
    ///      .://:   `///:    :///`   -//:.
    ///        .::   `///:    :///`   -:.
    ///              `///:    :///`
    ///               `...    ...`
    /// ```
    pub fn clear_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CLEAROS,
            names: vec!["ClearOS"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Clover distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::clover().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                `omo``omo`
    ///              `oNMMMNNMMMNo`
    ///            `oNMMMMMMMMMMMMNo`
    ///           oNMMMMMMMMMMMMMMMMNo
    ///           `sNMMMMMMMMMMMMMMNs`
    ///      `omo`  `sNMMMMMMMMMMNs`  `omo`
    ///    `oNMMMNo`  `sNMMMMMMNs`  `oNMMMNo`
    ///  `oNMMMMMMMNo`  `oNMMNs`  `oNMMMMMMMNo`
    /// oNMMMMMMMMMMMNo`  `sy`  `oNMMMMMMMMMMMNo
    /// `sNMMMMMMMMMMMMNo.oNNs.oNMMMMMMMMMMMMNs`
    /// `oNMMMMMMMMMMMMNs.oNNs.oNMMMMMMMMMMMMNo`
    /// oNMMMMMMMMMMMNs`  `sy`  `oNMMMMMMMMMMMNo
    ///  `oNMMMMMMMNs`  `oNMMNo`  `oNMMMMMMMNs`
    ///    `oNMMMNs`  `sNMMMMMMNs`  `oNMMMNs`
    ///      `oNs`  `sNMMMMMMMMMMNs`  `oNs`
    ///           `sNMMMMMMMMMMMMMMNs`
    ///           +NMMMMMMMMMMMMMMMMNo
    ///            `oNMMMMMMMMMMMMNo`
    ///              `oNMMMNNMMMNs`
    ///                `omo``oNs`
    /// ```
    pub fn clover() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CLOVER,
            names: vec!["Clover"],
            colors: vec![Color::Green, Color::Cyan],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Cobalt distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cobalt().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                           ///
    ///                   ,//////////////
    ///     ///////////////////////////////
    ///     ///////////////***********//////
    ///     ////***********************/////
    ///     /////***********************////
    ///    //////,,,,,,,,,,,,,,,,,,,,,,///
    ///  //////,,,,,,,,,,,,,,,,,,,,,,,,,/////
    ///  /////,,,,,,,,,,,,,,,,,,,,,,,,,,,,/////
    ///  *****,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*****
    ///  ******,,,,,,,,,,,,,,,,,,,,,,,,,,,,*****
    ///   *******,,,,,,,,,,,,,,,,,,,,,,,,,******
    ///     *******......................*******
    ///       ******....***********************
    ///         ****************************
    ///          *****
    /// ```
    pub fn cobalt() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_COBALT,
            names: vec!["Cobalt"],
            colors: vec![
                Color::Blue,
                Color::Blue,
                Color::BrightBlack,
                Color::BrightBlue,
                Color::Black,
            ],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Condres distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::condres().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// syyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy+.+.
    /// `oyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy+:++.
    /// /o+oyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy/oo++.
    /// /y+syyyyyyyyyyyyyyyyyyyyyyyyyyyyy+ooo++.
    /// /hy+oyyyhhhhhhhhhhhhhhyyyyyyyyy+oo+++++.
    /// /hhh+shhhhhdddddhhhhhhhyyyyyyy+oo++++++.
    /// /hhdd+oddddddddddddhhhhhyyyys+oo+++++++.
    /// /hhddd+odmmmdddddddhhhhyyyy+ooo++++++++.
    /// /hhdddmoodmmmdddddhhhhhyyy+oooo++++++++.
    /// /hdddmmms/dmdddddhhhhyyys+oooo+++++++++.
    /// /hddddmmmy/hdddhhhhyyyyo+oooo++++++++++:
    /// /hhdddmmmmy:yhhhhyyyyy++oooo+++++++++++:
    /// /hhddddddddy-syyyyyys+ooooo++++++++++++:
    /// /hhhddddddddy-+yyyy+/ooooo+++++++++++++:
    /// /hhhhhdddddhhy./yo:+oooooo+++++++++++++/
    /// /hhhhhhhhhhhhhy:-.+sooooo+++++++++++///:
    /// :sssssssssssso++`:/:--------.````````
    /// ```
    pub fn condres() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CONDRES,
            names: vec!["Condres"],
            colors: vec![Color::Green, Color::Yellow, Color::Cyan],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of CRUX distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::crux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          odddd
    ///       oddxkkkxxdoo
    ///      ddcoddxxxdoool
    ///      xdclodod  olol
    ///      xoc  xdd  olol
    ///      xdc  k00Okdlol
    ///      xxdkOKKKOkdldd
    ///      xdcoxOkdlodldd
    ///      ddc:clllloooodo
    ///    odxxddxkO000kxooxdo
    ///   oxddx0NMMMMMMWW0odkkxo
    ///  oooxd0WMMMMMMMMMW0odxkx
    /// docldkXWMMMMMMMWWNOdolco
    /// xxdxkxxOKNWMMWN0xdoxo::c
    /// xOkkO0ooodOWWWXkdodOxc:l
    /// dkkkxkkkOKXNNNX0Oxxxc:cd
    ///  odxxdxxllodddooxxdc:ldo
    ///    lodddolcccccoxxoloo
    /// ```
    pub fn crux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CRUX,
            names: vec!["CRUX"],
            colors: vec![Color::Blue, Color::Magenta, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of CRUX distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::crux_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     ___
    ///    (.· |
    ///    (<> |
    ///   / __  \\
    ///  ( /  \\ /|
    /// _/\\ __)/_)
    /// \/-____\/
    /// ```
    pub fn crux_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CRUX_SMALL,
            names: vec!["CRUX_small"],
            colors: vec![Color::Blue, Color::Magenta, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Crystal distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::crystal().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   mysssym
    ///                 mysssym
    ///               mysssym
    ///             mysssym
    ///           mysssyd
    ///         mysssyd    N
    ///       mysssyd    mysym
    ///     mysssyd      dysssym
    ///   mysssyd          dysssym
    /// mysssyd              dysssym
    /// mysssyd              dysssym
    ///   mysssyd          dysssym
    ///     mysssyd      dysssym
    ///       mysym    dysssym
    ///         N    dysssym
    ///            dysssym
    ///          dysssym
    ///        dysssym
    ///      dysssym
    ///    dysssym
    /// ```
    pub fn crystal() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CRYSTAL,
            names: vec!["Crystal", "Crystal", "crystal-linux", "Crystal-Linux"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Cucumber distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cucumber().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            `.-://++++++//:-.`
    ///         `:/+//::--------:://+/:`
    ///       -++/:----..........----:/++-
    ///     .++:---...........-......---:++.
    ///    /+:---....-::/:/--//:::-....---:+/
    ///  `++:--.....:---::/--/::---:.....--:++`
    ///  /+:--.....--.--::::-/::--.--.....--:+/
    /// -o:--.......-:::://--/:::::-.......--:o-
    /// /+:--...-:-::---:::..:::---:--:-...--:+/
    /// o/:-...-:.:.-/:::......::/:.--.:-...-:/o
    /// o/--...::-:/::/:-......-::::::-/-...-:/o
    /// /+:--..-/:/:::--:::..:::--::////-..--:+/
    /// -o:--...----::/:::/--/:::::-----...--:o-
    ///  /+:--....://:::.:/--/:.::://:....--:+/
    ///  `++:--...-:::.--.:..:.--.:/:-...--:++`
    ///    /+:---....----:-..-:----....---:+/
    ///     .++:---..................---:++.
    ///       -/+/:----..........----:/+/-
    ///         `:/+//::--------:::/+/:`
    ///            `.-://++++++//:-.`
    /// ```
    pub fn cucumber() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CUCUMBER,
            names: vec!["Cucumber", "CucumberOS"],
            colors: vec![Color::Green, Color::Yellow],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of CutefishOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cutefish_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      ___ww___
    /// _              _wwMMM@M^^^^MMMMww_
    /// M0w_       _wMMM~~             ~~MMm_
    ///   ~MMy _ww0M~                      ~MMy
    ///     ~MMMM~                      o    "MM
    ///   jw0M~~MMMw_                      _wMM'
    /// wMM~      ~~MMmw__             __w0M~
    /// ~             ~~MM0MmwwwwwwwwwMMM~
    ///                     ~~~~^^~~~
    /// ```
    pub fn cutefish_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CUTEFISHOS,
            names: vec!["CutefishOS"],
            colors: vec![Color::Cyan, Color::White, Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Cute OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cute_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                        1ua
    ///                   MMM1ua
    ///  MMEE         MMMMM1uazE
    /// MM EEEE     M1MM1uazzEn EEEE  MME
    ///     EEEEE  MMM uazEno EEEE
    ///     EEEEEMMMMMMEno~; EE          E
    ///      EE MMMMMMMM~;;E  MMMMM      M
    ///      E MMMMMMMMM            E  E   
    ///       MMMMMMMMMMM
    ///            MMMMMMMMM EE
    ///                 MM1MMMM EEE
    ///                      MMMMM
    ///                           MMM
    ///                               M
    /// ```
    pub fn cute_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CUTEOS,
            names: vec!["CuteOS"],
            colors: vec![
                Color::Blue,
                Color::Cyan,
                Color::TrueColor {
                    r: 0x5f,
                    g: 0,
                    b: 0xff,
                },
            ],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of CyberOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cyber_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              !MEEEEEEEEEEEP
    ///             .MMMMM000000Nr.
    ///             &MMMMMMMMMMMMMMMMMMM9
    ///            ~MMMMMMMMMMMMMMMMMMMC
    ///       "    MMMMMMMMMMMMMMMMMMs
    ///     iMMMM&&MMMMMMMMMMMMMMMM\\
    ///    BMMMMMMMMMMMMMMMMMMMMM"
    ///   9MMMMMMMMMMMMMMMMMMMMMMMf-
    ///         sMMMMMMMMMMMMMMMMMMMM3_
    ///          +ffffffffPMMMMMMMMMMMM0
    ///                     CMMMMMMMMMMM
    ///                       }MMMMMMMMM
    ///                         ~MMMMMMM
    ///                           "RMMMM
    ///                             .PMB
    /// ```
    pub fn cyber_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CYBEROS,
            names: vec!["CyberOS"],
            colors: vec![
                Color::Blue,
                Color::Cyan,
                Color::TrueColor {
                    r: 0x5f,
                    g: 0,
                    b: 0xff,
                },
            ],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Cycledream distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::cycledream().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .:ox00000kdc,              
    ///       ;;    'cdO.            dxl,.         
    ///       00 'oO'                    0x;       
    ///       00ko                         ,Oc     
    ///       x00Okxx                        lO'   
    ///                     .x,               .0c  
    ///               ;c   .O00'   ::          '0:
    ///  .                 O0000.               o0.
    /// .0c        ,,;;:clO00000Olc:;;,,        .0l
    /// x0          ;00000000000000000c          00
    /// 00             0000000000000.            0O
    /// x0.        ...  00000000000. ...        '0:
    /// .0c            ,00000000000o            .o
    ///  o0.           O000;   '0000               
    ///   0k          .0     O     O'              
    ///    lO.                        ''.....      
    ///     .0o                          d000      
    ///        Oo'                     ,dO O0      
    ///          ;Oo;.             .;oO,   00      
    ///               xkdocc:ccodko                
    ///                   ;k0k,                  
    /// ```
    pub fn cycledream() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_CYCLEDREAM,
            names: vec!["cycledream"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Dahlia distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::dahlia().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   .#.
    ///                 *%@@@%*
    ///         .,,,,,(&@@@@@@@&/,,,,,.
    ///        ,#@@@@@@@@@@@@@@@@@@@@@#.
    ///        ,#@@@@@@@&#///#&@@@@@@@#.
    ///      ,/%&@@@@@%/,    .,(%@@@@@&#/.
    ///    *#&@@@@@@#,.         .*#@@@@@@&#,
    ///  .&@@@@@@@@@(            .(@@@@@@@@@&&.
    /// #@@@@@@@@@@(               )@@@@@@@@@@@#
    ///  °@@@@@@@@@@(            .(@@@@@@@@@@@°
    ///    *%@@@@@@@(.           ,#@@@@@@@%*
    ///      ,(&@@@@@@%*.     ./%@@@@@@%(,
    ///        ,#@@@@@@@&(***(&@@@@@@@#.
    ///        ,#@@@@@@@@@@@@@@@@@@@@@#.
    ///         ,*****#&@@@@@@@&(*****,
    ///                ,/%@@@%/.
    ///                   ,#,
    /// ```
    pub fn dahlia() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DAHLIA,
            names: vec!["dahlia"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of DarkOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::dark_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ⠀⠀⠀⠀  ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠢⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣶⠋⡆⢹⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡆⢀⣤⢛⠛⣠⣿⠀⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣶⣿⠟⣡⠊⣠⣾⣿⠃⣠⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣴⣯⣿⠀⠊⣤⣿⣿⣿⠃⣴⣧⣄⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣶⣿⣿⡟⣠⣶⣿⣿⣿⢋⣤⠿⠛⠉⢁⣭⣽⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    ///   ⠀⠀⠀⠀⠀⠀ ⠀⣠⠖⡭⢉⣿⣯⣿⣯⣿⣿⣿⣟⣧⠛⢉⣤⣶⣾⣿⣿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⣴⣫⠓⢱⣯⣿⢿⠋⠛⢛⠟⠯⠶⢟⣿⣯⣿⣿⣿⣿⣿⣿⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⢀⡮⢁⣴⣿⣿⣿⠖⣠⠐⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠛⠛⠛⢿⣶⣄⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⢀⣤⣷⣿⣿⠿⢛⣭⠒⠉⠀⠀⠀⣀⣀⣄⣤⣤⣴⣶⣶⣶⣿⣿⣿⣿⣿⠿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⢀⣶⠏⠟⠝⠉⢀⣤⣿⣿⣶⣾⣿⣿⣿⣿⣿⣿⣟⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢴⣯⣤⣶⣿⣿⣿⣿⣿⡿⣿⣯⠉⠉⠉⠉⠀⠀⠀⠈⣿⡀⣟⣿⣿⢿⣿⣿⣿⣿⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠉⠛⣿⣧⠀⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⠃⣿⣿⣯⣿⣦⡀⠀⠉⠻⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠉⢿⣮⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⣿⠀⣯⠉⠉⠛⢿⣿⣷⣄⠀⠈⢻⣆⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠢⠀⠀⠀⠀⠀⠀⠀⢀⢡⠃⣾⣿⣿⣦⠀⠀⠀⠙⢿⣿⣤⠀⠙⣄⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⢋⡟⢠⣿⣿⣿⠋⢿⣄⠀⠀⠀⠈⡄⠙⣶⣈⡄⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⠚⢲⣿⠀⣾⣿⣿⠁⠀⠀⠉⢷⡀⠀⠀⣇⠀⠀⠈⠻⡀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢢⣀⣿⡏⠀⣿⡿⠀⠀⠀⠀⠀⠀⠙⣦⠀⢧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠿⣧⣾⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⣮⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠙⠛⠀⠀⠀⠀⠀⠀
    /// ```
    pub fn dark_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DARKOS,
            names: vec!["DarkOs"],
            colors: vec![
                Color::Red,
                Color::Cyan,
                Color::Magenta,
                Color::Yellow,
                Color::Green,
                Color::BrightRed,
            ],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Debian distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::debian().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        _,metgg.
    ///     ,gP.
    ///   ,gP"         """Y.".
    ///  ,P'               `.
    /// ',P       ,ggs.     `b:
    /// `d'     ,P"'   .    
    ///  P      d'     ,    P
    ///  :      .   -    ,d'
    ///  ;      Yb._   _,dP'
    ///  Y.    `.`"YP"'
    ///  `b      "-.__
    ///   `Y
    ///    `Y.
    ///      `b.
    ///        `Yb.
    ///           `"Yb._
    ///              `"""
    /// ```
    pub fn debian() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DEBIAN,
            names: vec!["Debian", "debian-linux"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Debian distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::debian_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   _____
    ///  /  __ \
    /// |  /    |
    /// |  \___-
    /// -_
    ///   --_
    /// ```
    pub fn debian_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DEBIAN_SMALL,
            names: vec!["Debian_small", "debian-linux-small"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Deepin distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::deepin().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ............
    ///          .';;;;;.       .,;,.
    ///       .,;;;;;;;.       ';;;;;;;.
    ///     .;::::::::'     .,::;;,''''',.
    ///    ,'.::::::::    .;;'.          ';
    ///   ;'  'cccccc,   ,' :: '..        .:
    ///  ,,    :ccccc.  ;: .c, '' :.       ,;
    /// .l.     cllll' ., .lc  :; .l'       l.
    /// .c       :lllc  ;cl:  .l' .ll.      :'
    /// .l        'looc. .   ,o:  'oo'      c,
    /// .o.         .:ool::coc'  .ooo'      o.
    ///  ::            .....   .;dddo      ;c
    ///   l:...            .';lddddo.     ,o
    ///    lxxxxxdoolllodxxxxxxxxxc      :l
    ///     ,dxxxxxxxxxxxxxxxxxxl.     'o,
    ///       ,dkkkkkkkkkkkkko;.    .;o;
    ///         .;okkkkkdl;.    .,cl:.
    ///             .,:cccccccc:,.
    /// ```
    pub fn deepin() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DEEPIN,
            names: vec!["Deepin", "deepin-linux"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of DesaOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::desa_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ███████████████████████
    /// ███████████████████████
    /// ███████████████████████
    /// ███████████████████████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ██████████████████████████████
    /// ██████████████████████████████
    /// ████████████████████████
    /// ████████████████████████
    /// ████████████████████████
    /// ```
    pub fn desa_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DESAOS,
            names: vec!["DesaOS"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Devuan distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::devuan().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    ..,,;;;::;,..
    ///            `':ddd;:,.
    ///                  `'dPPd:,.
    ///                      `:bb`.
    ///                         'Pd`
    ///                          .`
    ///                          ;P
    ///                       .:P`
    ///                   .,:b;'
    ///              .,:dPb:'
    ///       .,:;dbPd'`
    ///  ,dbb:'`
    /// :b:'`
    ///  `bd:''`
    ///    `'''`
    /// ```
    pub fn devuan() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DEVUAN,
            names: vec!["Devuan", "devuan-linux"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Devuan distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::devuan_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  ..:::.
    ///     ..-==-
    ///         .+#:
    ///          =@@
    ///       :+%@#:
    /// .:=+#@@%*:
    /// #@@@#=:    /// ```
    pub fn devuan_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DEVUAN_SMALL,
            names: vec!["Devuan_small", "devuan-linux-small"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of DietPI distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::diet_pi().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   :=+******+-    -+******+=:
    ///  =#-::-::::-=#:-#=-::::-::-#=
    ///  :%-::--==-::-%%-::-==--::-%:
    ///   +#-:::::=+++@@+++=-::::-#=
    ///    :#+-::::=%@@@@@=::::-+#:
    ///      =@%##%@@@@@@@@%##%@=
    ///    .#@@@@@@@@@@@@@@@@@@@@#.
    ///    %@@@@@@@@@@@@@@@@@@@@@@%
    ///   -@@@@@@@@@@@@@@@@@@@@@@@@:
    /// .#@@@@@@@@@@%%%%%@@@@@@@@@@@#.
    /// #@@@+-=*#%%%%%%%%%%%%#+--#@@@#
    /// %@@%*.   .:=*%%%%*=:    .#@@@%
    /// :%@@@#+=-::-*%%%%+:::-=+%@@@%:
    ///  :@@@@%@%%%%@###%@%%%%@%@@@@.
    ///   +@@@@@@@@@%=*+%@%@@@@@@@@+
    ///    #@@@@@@@@@@@@@@@@@@@@@@#
    ///     -#@@@@@@@@@@@@@@@@@@#-
    ///        -*%@@@@@@@@@@%*-
    ///           .+%@@@@%+.
    /// ```
    pub fn diet_pi() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DIETPI,
            names: vec!["DietPi"],
            colors: vec![Color::Green, Color::Black],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of DracOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::drac_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        `-:/-
    ///           -os:
    ///             -os/`
    ///               :sy+-`
    ///                `/yyyy+.
    ///                  `+yyyyo-
    ///                    `/yyyys:
    /// `:osssoooo++-        +yyyyyy/`
    ///    ./yyyyyyo         yo`:syyyy+.
    ///       -oyyy+         +-   :yyyyyo-
    ///         `:sy:        `.    `/yyyyys:
    ///            ./o/.`           .oyyso+oo:`
    ///               :+oo+//::::///:-.`
    /// ```
    pub fn drac_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DRACOS,
            names: vec!["DracOS"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of DragonFly distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::dragon_fly().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ,--,           |           ,--,
    /// |   `-,       ,^,       ,-'   |
    ///  `,    `-,   (/ \)   ,-'    ,'
    ///    `-,    `-,/   \,-'    ,-'
    ///       `------(   )------'
    ///   ,----------(   )----------,
    ///  |        _,-(   )-,_        |
    ///   `-,__,-'   \   /   `-,__,-'
    ///               | |
    ///               | |
    ///               | |
    ///               | |
    ///               | |
    ///               | |
    ///               `|'
    /// ```
    pub fn dragon_fly() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DRAGONFLY,
            names: vec!["DragonFly", "DragonFly-BSD"],
            colors: vec![Color::Red, Color::White, Color::Yellow],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of DragonFly distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::dragon_fly_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    ,_,
    /// ('-_|_-')
    ///  >--|--<
    /// (_-'|'-_)
    ///     |
    ///     |
    ///     |
    /// ```
    pub fn dragon_fly_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DRAGONFLY_SMALL,
            names: vec!["DragonFly_small", "DragonFly-BSD_small"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns alternative logo of DragonFly distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::dragon_fly_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                         .-.
    ///                   ()I()
    ///              "==.__:-:__.=="
    ///             "==.__/~|~\__.=="
    ///             "==._(  Y  )_.=="
    ///  .-'~~""~=--...,__\/|\/__,...--=~""~~'-.
    /// (               ..=\\=/=..               )
    ///  `'-.        ,.-"`;/=\\;"-.,_        .-'`
    ///      `~"-=-~` .-~` |=| `~-. `~-=-"~`
    ///           .-~`    /|=|\    `~-.
    ///        .~`       / |=| \       `~.
    ///    .-~`        .'  |=|  `.        `~-.
    ///  (`     _,.-="`    |=|    `"=-.,_     `)
    ///   `~"~"`           |=|           `"~"~`
    ///                    /=\\
    ///                    \\=/
    ///                     ^
    /// ```
    pub fn dragon_fly_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DRAGONFLY_OLD,
            names: vec!["DragonFly_old", "DragonFly-BSD_old"],
            colors: vec![Color::Red, Color::White, Color::Yellow],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of DraugerOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::drauger_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   -``-
    ///                 `:+``+:`
    ///                `/++``++/.
    ///               .++/.  ./++.
    ///              :++/`    `/++:
    ///            `/++:        :++/`
    ///           ./+/-          -/+/.
    ///          -++/.            ./++-
    ///         :++:`              `:++:
    ///       `/++-                  -++/`
    ///      ./++.                    ./+/.
    ///     -++/`                      `/++-
    ///    :++:`                        `:++:
    ///  `/++-                            -++/`
    /// .:-.`..............................`.-:.
    /// `.-/++++++++++++++++++++++++++++++++/-.`
    /// ```
    pub fn drauger_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DRAUGER,
            names: vec!["DraugerOS", "Drauger"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Droidian distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::droidian().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        _,metgg.
    ///    ,gP.
    ///  ,P'              `.
    /// ',P       ,ggs.     `b:
    /// `d'     ,P"'   .    
    ///  P      d'     ,    P
    ///  :      .   -    ,d'
    ///  ;      Yb._   _,dP'
    ///  Y.    `.`"YP"'
    ///  `b      "-.__
    ///   `Y
    ///   `Y.
    ///      `b.
    ///        `Yb.
    ///           `"Yb._
    /// ```
    pub fn droidian() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_DROIDIAN,
            names: vec!["Droidian"],
            colors: vec![Color::Green, Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Elbrus distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::elbrus().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
    /// ██▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ██   ███████   ███████   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ███████   ███████
    /// ██   ██                  ██
    /// ██   ██▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄██
    /// ██   ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ███████████████████████████
    /// ```
    pub fn elbrus() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ELBRUS,
            names: vec!["elbrus"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Elementary distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::elementary().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          eeeeeeeeeeeeeeeee
    ///       eeeeeeeeeeeeeeeeeeeeeee
    ///     eeeee  eeeeeeeeeeee   eeeee
    ///   eeee   eeeee       eee     eeee
    ///  eeee   eeee          eee     eeee
    /// eee    eee            eee       eee
    /// eee   eee            eee        eee
    /// ee    eee           eeee       eeee
    /// ee    eee         eeeee      eeeeee
    /// ee    eee       eeeee      eeeee ee
    /// eee   eeee   eeeeee      eeeee  eee
    /// eee    eeeeeeeeee     eeeeee    eee
    ///  eeeeeeeeeeeeeeeeeeeeeeee    eeeee
    ///   eeeeeeee eeeeeeeeeeee      eeee
    ///     eeeee                 eeeee
    ///       eeeeeee         eeeeeee
    ///          eeeeeeeeeeeeeeeee
    /// ```
    pub fn elementary() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ELEMENTARY,
            names: vec!["Elementary"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Elementary distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::elementary_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   _______
    ///  / ____  \\
    /// /  |  /  /\\
    /// |__\\ /  / |
    /// \\   /__/  /
    ///  \\_______/
    /// ```
    pub fn elementary_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ELEMENTARY_SMALL,
            names: vec!["Elementary_small", "elementary-small"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Elive distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::elive().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              *@,,&(%%%..%*.
    ///          (@&%/##############((/*,
    ///       @@&#########*..../########%*..
    ///     @&#%%%%%.              ,.,%%%%%%.
    ///   /%(%%%%.                      (%%%%#.
    ///  /*%%##,.                       .,%%###,
    ///  ,####.   ,*#%#/,(/               /####,
    /// ((###/   ,,##########(/(#          %####,
    /// %#(((.   ../(((((((((((((((#/*..   *.(((/
    /// %#///.        ***.*/////////////
    /// ##////*              ***.*/////.
    ///  ((*****                   ***
    ///   ,*****..
    ///    ..******..                 *%/****.
    ///      .,,*******,,,../##(%&&#******,.
    ///         ,*,,,,,,,,,,,,,,,,,,,,,..
    ///             *///,,,,,,,,,,..
    /// ```
    pub fn elive() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ELIVE,
            names: vec!["Elive"],
            colors: vec![Color::White, Color::BrightCyan, Color::Cyan],
            color_keys: Some(Color::White),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of EncryptOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::encrypt_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///      *******
    ///    ***       **.
    ///    **         **
    ///    **         **
    ///
    ///  *****************
    /// ,,,,,,,,,,,,,,,,***
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,,,,,,,,,,,,,
    ///     ,,,,,,,,,,,,.
    /// ```
    pub fn encrypt_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ENCRYPTOS,
            names: vec!["EncryptOS"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Endeavour distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::endeavour().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      ./o.
    ///                    ./sssso-
    ///                  `:osssssss+-
    ///                `:+sssssssssso/.
    ///              `-/ossssssssssssso/.
    ///            `-/+sssssssssssssssso+:`
    ///          `-:/+sssssssssssssssssso+/.
    ///        `.://osssssssssssssssssssso++-
    ///       .://+ssssssssssssssssssssssso++:
    ///     .:///ossssssssssssssssssssssssso++:
    ///   `:////ssssssssssssssssssssssssssso+++.
    /// `-////+ssssssssssssssssssssssssssso++++-
    ///  `..-+oosssssssssssssssssssssssso+++++/`
    ///    ./++++++++++++++++++++++++++++++/:.
    ///   `:::::::::::::::::::::::::------
    /// ```
    pub fn endeavour() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ENDEAVOUR,
            names: vec![
                "Endeavour",
                "endeavour-linux",
                "endeavouros",
                "endeavouros-linux",
            ],
            colors: vec![Color::Magenta, Color::Red, Color::Blue],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Endeavour distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::endeavour_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            /o.
    ///          :sssso-
    ///        :ossssssso:
    ///      /ssssssssssso+
    ///   -+ssssssssssssssso+
    ///  //osssssssssssssso+-
    ///   `+++++++++++++++-`
    /// ```
    pub fn endeavour_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ENDEAVOUR_SMALL,
            names: vec![
                "Endeavour_small",
                "endeavour-linux_small",
                "endeavouros_small",
                "endeavouros-linux_small",
            ],
            colors: vec![Color::Magenta, Color::Red, Color::Blue],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Red),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Endless distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::endless().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            `:+yhmNMMMMNmhy+:`
    ///         -odMMNhso//////oshNMMdo-
    ///       /dMMh+.              .+hMMd/
    ///     /mMNo`                    `oNMm:
    ///   `yMMo`                        `oMMy`
    ///  `dMN-                            -NMd`
    ///  hMN.                              .NMh
    /// /MM/                  -os`          /MM/
    /// dMm    `smNmmhs/- `:sNMd+   ``       mMd
    /// MMy    oMd--:+yMMMMMNo.:ohmMMMNy`    yMM
    /// MMy    -NNyyhmMNh+oNMMMMMy:.  dMo    yMM
    /// dMm     `/++/-``/yNNh+/sdNMNddMm-    mMd
    /// /MM/          `dNy:       `-::-     /MM/
    ///  hMN.                              .NMh
    ///  `dMN-                            -NMd`
    ///   `yMMo`                        `oMMy`
    ///     /mMNo`                    `oNMm/
    ///       /dMMh+.              .+hMMd/
    ///         -odMMNhso//////oshNMMdo-
    ///            `:+yhmNMMMMNmhy+:`
    /// ```
    pub fn endless() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ENDLESS,
            names: vec!["Endless"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Enso distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::enso().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .:--==--:.                     
    ///             :=*#############*+-.               
    ///          .+##################*##*:             
    ///        .*##########+==-==++*####*##-           
    ///       =########=:           .-+**#***.         
    ///      *#######-                  ++*#**.        
    ///     +######+                     -*+#**        
    ///    :######*                       .*+**=       
    ///    *######:                        --#*#       
    ///    #######                          +++#.      
    ///    #######.                         ++=*.      
    ///    *######+                        .-+*+       
    ///    :#######-                       -:*+:       
    ///     =#######*.                    :.*+-        
    ///      +########*-                  :*=-         
    ///       =###########+=:            =+=:          
    ///        .+#############.       .-==:            
    ///          .=###########=   ..:--:.              
    ///             .-+######+                 
    /// ```
    pub fn enso() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ENSO,
            names: vec!["Enso"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of EshanizedOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::eshanized_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  .:-==++++++++++++++++++++++-.
    ///               .:=+##############################+
    ///            .=*###################################-
    ///          :+######################################:
    ///        :+######################################*-
    ///      .+#############*+--:::::::::::::::::::::.
    ///     :*##########*=:
    ///    -###########=
    ///   :##########+.
    ///   *#########=
    ///  =*********+             .::::::::::::::::.
    ///  **********.           -*###################+:
    /// :**********           =#######################=
    /// -*********+           +************************
    /// :*********+           :***********************:
    /// .**********            .=******************+-
    ///  +*********-
    ///  .**********.
    ///   =**********:
    ///    +**********=.
    ///     =***********=:
    ///      -*************+-.
    ///       :+****************+++++++++++++++++++++==:
    ///         :+**************************************+.
    ///            -+************************************-
    ///              .-+*********************************.
    ///                 :-==+*************************=.
    /// ```
    pub fn eshanized_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ESHANIZEDOS,
            names: vec!["EshanizedOS"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of EuroLinux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::euro_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 __
    ///          -wwwWWWWWWWWWwww-
    ///         -WWWWWWWWWWWWWWWWWWw-
    ///           \WWWWWWWWWWWWWWWWWWW-
    ///   _Ww      `WWWWWWWWWWWWWWWWWWWw
    ///  -WEWww                -WWWWWWWWW-
    /// _WWUWWWW-                _WWWWWWWW
    /// _WWRWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW-
    /// wWWOWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWLWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWw
    /// WWWIWWWWWWWWWWWWWWWWWWWWWWWWWWWWww-
    /// wWWNWWWWw
    ///  WWUWWWWWWw
    ///  wWXWWWWWWWWww
    ///    wWWWWWWWWWWWWWWWw
    ///     wWWWWWWWWWWWWWWWw
    ///        WWWWWWWWWWWWWw
    ///            wWWWWWWWw
    /// ```
    pub fn euro_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EUROLINUX,
            names: vec!["EuroLinux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Evolinx distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::evolinx().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// #-----------------------------------------------------------------------#
    /// |#SSSSSSSS\*SS\****SS\**SSSSSS\**SS\*******SSSSSS\*SS\***SS\*SS\***SS\*#|
    /// |#SS**_____|SS*|***SS*|SS**__SS\*SS*|******\_SS**_|SSS\**SS*|SS*|**SS*|#|
    /// |#SS*|******SS*|***SS*|SS*/**SS*|SS*|********SS*|**SSSS\*SS*|\SS\*SS**|#|
    /// |#SSSSS\****\SS\**SS**|SS*|**SS*|SS*|********SS*|**SS*SS\SS*|*\SSSS**/*#|
    /// |#SS**__|****\SS\SS**/*SS*|**SS*|SS*|********SS*|**SS*\SSSS*|*SS**SS<**#|
    /// |#SS*|********\SSS**/**SS*|**SS*|SS*|********SS*|**SS*|\SSS*|SS**/\SS\*#|
    /// |#SSSSSSSS\****\S**/****SSSSSS**|SSSSSSSS\*SSSSSS\*SS*|*\SS*|SS*/**SS*|#|
    /// |#\________|****\_/*****\______/*\________|\______|\__|**\__|\__|**\__|#|
    /// #-----------------------------------------------------------------------#
    /// ```
    pub fn evolinx() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EVOLINX,
            names: vec!["Evolinx"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of EvolutionOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::evolution_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                .':ldxxxxdo:,.
    ///            .lXMMMMMMMMMMMMMMMMNo'
    ///          dWMMMMMMMMMMMMMMMMMMMMMMWk
    ///       .OMMMMWWWWWWWWWWWWWWWWWWWMMMMM0;
    ///      kMMMMMXxxxxkkkkkkkkkkkkkkkKWMMMMMK.
    ///    .kMMMMMMXddd0KKKKKKKKKKKKKKKNMMMMMMMN.
    ///    KMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMX.
    ///   cMMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMMo
    ///   KMMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMMN
    ///   XMMMMMMMMXdddXWOkkkkkkkKWKOOOXMMMMMMMMMW
    ///   XMMMMMMMMXdddXWOkkkkkkkKWKOOOXMMMMMMMMMW
    ///   0MMMMMMMMXdddXMWWWWWWWWWMWWWWWMMMMMMMMMN
    ///   cMMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMMd
    ///   .kMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMM0.
    ///    .kMMMMMMXxxxNW0OOOOOOOOOOOOOKMMMMMMMK.
    ///      oMMMMMXxxxNW0OOOOOOOOOOOOOKWMMMMMk.
    ///       '0MMMWNNNWMWWWWWWWWWWWWWWWMMMMX;
    ///          cWMMMMMMMMMMMMMMMMMMMMMMWd
    ///             :KWMMMMMMMMMMMMMMWXo.
    ///                 .cdO0KK00xl'
    ///                      ..
    /// ```
    pub fn evolution_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EVOLUTIONOS,
            names: vec!["EvolutionOS"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of EvolutionOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::evolution_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       ,coddoc'
    ///    'cddddddddddc'
    ///  'dddOWWXXXXXXKddo.
    /// .ddddOMXddddddddddd.
    /// oddddOMXk00OkOOddddo
    /// .ddddOMXkOOOxOkdddd.
    ///  .dddOWWXXXXXXKddd'
    ///    'dddddddddddd'
    ///       'cddddd,
    /// ```
    pub fn evolution_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EVOLUTIONOS_SMALL,
            names: vec!["EvolutionOS_small"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Small,
        }
    }
    /// returns alternative logo of EvolutionOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::evolution_os_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     dddddddddddddddddddddddd
    /// .dddd''''''''''''''''''''''dddd.
    /// dd:   dddddddddddddddddddd;   dd:
    /// dd:   ldl:''''''''''''''''    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl: ddddddd;  ddddd;   dd:
    /// dd:   ldl: '''''''   '''''    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl: ddddddddddddddd;   dd:
    /// dddd:.'''  '''''''''''''''  dddd:
    ///    dddddddddddddddddddddddddd;;'
    ///     '''''''''''''''''''''''''
    /// ```
    pub fn evolution_os_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EVOLUTIONOS_OLD,
            names: vec!["EvolutionOS_old"],
            colors: vec![Color::BrightBlue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Exherbo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::exherbo().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  ,
    /// OXo.
    /// NXdX0:    .cok0KXNNXXK0ko:.
    /// KX  '0XdKMMK;.xMMMk, .0MMMMMXx;  ...
    /// 'NO..xWkMMx   kMMM    cMMMMMX,NMWOxOXd.
    ///   cNMk  NK    .oXM.   OMMMMO. 0MMNo  kW.
    ///   lMc   o:       .,   .oKNk;   ;NMMWlxW'
    ///  ;Mc    ..   .,,'    .0Mg;WMN'dWMMMMMMO
    ///  XX        ,WMMMMW.  cMcfliWMKlo.   .kMk
    /// .Mo        .WMGDMW.   XMWO0MMk        oMl
    /// ,M:         ,XMMWx::,''oOK0x;          NM.
    /// 'Ml      ,kNKOxxxxxkkO0XXKOd:.         oMk
    ///  NK    .0Nxc:::::::::::::::fkKNk,      .MW
    ///  ,Mo  .NXc::qXWXb::::::::::oo::lNK.    .MW
    ///   ;Wo oMd:::oNMNP::::::::oWMMMx:c0M;   lMO
    ///    'NO;W0c:::::::::::::::dMMMMO::lMk  .WM'
    ///      xWONXdc::::::::::::::oOOo::lXN. ,WMd
    ///       'KWWNXXK0Okxxo,:::::::,lkKNo  xMMO
    ///         :XMNxl,';:lodxkOO000Oxc. .oWMMo
    ///           'dXMMXkl;,.        .,o0MMNo'
    ///              ':d0XWMMMMWNNNNMMMNOl'
    ///                    ':okKXWNKkl'
    /// ```
    pub fn exherbo() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EXHERBO,
            names: vec!["Exherbo", "exherbo-linux"],
            colors: vec![Color::Blue, Color::White, Color::Red],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Exodia Predator distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::exodia_predator().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// -                                  :
    /// +:                                :+
    /// ++.                              .++
    /// +++             :  .             +++
    /// +++=           .+  +            =+++
    /// ++++-          ++  +=          -++++
    /// ++++++-       -++  ++-       -++++++
    /// ++++++++:    .+++  +++.    :++++++++
    /// ++++++++++:  ++++  ++++  :++++++++++
    /// +++++++++++==++++  ++++=++++++=+++++
    /// +++++.:++++++++++  ++++++++++:.+++++
    /// +++++. .+++++++++  +++++++++. .+++++
    /// +++++:   ++++++++  ++++++++   :+++++
    /// ++++++-  =+++++++  +++++++=  -++++++
    ///  :+++++= =+++++++  +++++++= =+++++:
    ///    :+++= =+++++++  +++++++= =+++:
    ///      -+= =+++++++  +++++++= ++-
    ///        : =++++++-  -++++++= :
    ///          =++++-      -++++=
    ///          =++=          =++=
    ///          =++            ++=
    ///          =+.            .+=
    ///          =-              -=
    ///          :                :
    /// ```
    pub fn exodia_predator() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_EXODIA_PREDATOR,
            names: vec!["Exodia Predator", "exodia-predator", "Exodia Predator OS"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Fedora distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              .',;::::;,'.
    ///          .';:cccccccccccc:;,.
    ///       .;cccccccccccccccccccccc;.
    ///     .:cccccccccccccccccccccccccc:.
    ///   .;ccccccccccccc;.:dddl:.;ccccccc;.
    ///  .:ccccccccccccc;OWMKOOXMWd;ccccccc:.
    /// .:ccccccccccccc;KMMc;cc;xMMc;ccccccc:.
    /// ,cccccccccccccc;MMM.;cc;;WW:;cccccccc,
    /// :cccccccccccccc;MMM.;cccccccccccccccc:
    /// :ccccccc;oxOOOo;MMM000k.;cccccccccccc:
    /// cccccc;0MMKxdd:;MMMkddc.;cccccccccccc;
    /// ccccc;XMO';cccc;MMM.;cccccccccccccccc'
    /// ccccc;MMo;ccccc;MMW.;ccccccccccccccc;
    /// ccccc;0MNc.ccc.xMMd;ccccccccccccccc;
    /// cccccc;dNMWXXXWM0:;cccccccccccccc:,
    /// cccccccc;.:odl:.;cccccccccccccc:,.
    /// ccccccccccccccccccccccccccccc:'.
    /// :ccccccccccccccccccccccc:;,..
    ///  ':cccccccccccccccc::;,.
    /// ```
    pub fn fedora() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA,
            names: vec!["Fedora", "fedora-linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Fedora distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         ,'''''.
    ///        |   ,.  |
    ///        |  |  '_'
    ///   ,....|  |..
    /// .'  ,_;|   ..'
    /// |  |   |  |
    /// |  ',_,'  |
    ///  '.     ,'
    ///    '''''
    /// ```
    pub fn fedora_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA_SMALL,
            names: vec!["Fedora_small", "fedora-linux-small"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Small,
        }
    }
    /// returns alternative logo of Fedora distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           /:-------------:\
    ///        :-------------------::
    ///      :-----------/shhOHbmp---:\
    ///    /-----------omMMMNNNMMD  ---:
    ///   :-----------sMMMMNMNMP.    ---:
    ///  :-----------:MMMdP-------    ---\
    /// ,------------:MMMd--------    ---:
    /// :------------:MMMd-------    .---:
    /// :----    oNMMMMMMMMMNho     .----:
    /// :--     .+shhhMMMmhhy++   .------/
    /// :-    -------:MMMd--------------:
    /// :-   --------/MMMd-------------;
    /// :-    ------/hMMMy------------:
    /// :-- :dMNdhhdNMMNo------------;
    /// :---:sdNMMMMNds:------------:
    /// :------:://:-------------::
    /// :---------------------://
    /// ```
    pub fn fedora_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA_OLD,
            names: vec![
                "Fedora_old",
                "fedora-old",
                "fedora-linux-old",
                "fedora-linux_old",
            ],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Fedora-Silverblue distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora_silverblue().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     .;ooooooooooooooooooooooooooo.
    ///   ,dddddddddddddddddddddddddddddd';
    ///  lddddddddddddddddddddddddddddd';;;
    /// ddddd,XXX.ddddd,XXX.dddd',XXX.;;;;;
    /// dddddXXxXXdddddXXxXXddd',XXxXX;;;;;
    /// ddddd'XXX'ddddd'XXX'dd'XXXXXX';;;;;
    /// dddddd;X;ddddddd;X:d'XXX;;;;;;;;;;;
    /// dddddd;X;ddddddd;X:XXX;;;;;;;;;;;;;
    /// dddddd;X;dddddd';XXX,,,,,,XXX.;;;;;
    /// dddddd;X;dddd'XXXXXXXXXXXXXxXX;;;;;
    /// dddddd;X;dd'XXX;;;;;;;;;;;XXX;;;;;;
    /// dddddd;X;'XXX;;;;;;;;;;;;;;;;;;;;;;
    /// dddddd;XXXXX,,,,,,,,,,,,,;XXX:;;;;;
    /// dddddd:XXXXXXXXXXXXXXXXXXXXxXX;;;;;
    /// ddddd';;;;;;;;;;;;;;;;;;;'XXX';;;;'
    /// ddd';;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    /// o';;;;;;;;;;;;;;;;;;;;;;;;;;;;'
    /// ```
    pub fn fedora_silverblue() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA_SILVERBLUE,
            names: vec![
                "Fedora_silverblue",
                "fedora-silverblue",
                "fedora-linux-silverblue",
                "fedora-linux_silverblue",
            ],
            colors: vec![Color::Blue, Color::White, Color::Cyan],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Fedora-Kinoite distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora_kinoite().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  ,clll:.          .,::::::::::::'
    /// :ooooooo        .;::::::::::::::
    /// looooooo       ,:::::::::::::::'
    /// looooooo      .::::::::::::::::
    /// looooooo      ;:::::::::::::::.
    /// looooooo     .::::::::::::::::
    /// looooool;;;;,::::::::::::::::
    /// looool::,   .::::::::::::::
    /// looooc::     ;::
    /// looooc::;.  .::;
    /// loooool:::::::::::.
    /// looooooo.    .::::::'
    /// looooooo       .::::::,;,..
    /// looooooo          :::;' ';:;.
    /// looooooo          :::     :::
    /// cooooooo          .::'   '::.
    ///  .ooooc             ::, ,::
    ///                       ''''
    /// ```
    pub fn fedora_kinoite() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA_KINOITE,
            names: vec![
                "Fedora_kinoite",
                "fedora-kinoite",
                "fedora-linux-kinoite",
                "fedora-linux_kinoite",
            ],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Fedora-Sericea distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora_sericea().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               :oooo,  .','
    ///       .';;;.;oooooooolooooo'
    ///      coooooooooooooooooooooooolc'
    ///   .':oooooooooooollooooooooooooool
    /// .oooooooooooooooolloooooooooooolool
    /// ooooooooooooooooollooooooooooolloo'
    ///  ooooloooooooooolllooooooooollloo
    ///  .ooooolllooooolllooooooooolllool
    ///  .ooooooollloolllloooolllllooooo:
    ///   'oooooooolllllllllllloooooooo'
    ///      .c,.oollllloooooooo.
    ///            'll;
    ///            'll.
    ///            lll
    ///            lll
    ///           ;ll,
    ///           .l:
    /// ```
    pub fn fedora_sericea() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA_SERICEA,
            names: vec![
                "Fedora_sericea",
                "fedora-sericea",
                "fedora-linux-sericea",
                "fedora-linux_sericea",
            ],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Fedora-CoreOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::fedora_coreos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .....
    ///           .';:cccccccc:;'.
    ///         ':ccccclclllllllllcc:.
    ///      .;cccccccclllllllllllllllc,
    ///     ;clllcccccllllllllllllllllllc,
    ///   .cllclcccccllllllllllllllllllllc:
    ///   ccclclcccccllllkWMMNKkllllllllllc:
    ///  :ccclclcccclllloWMMMMMMWOlllllllllc,
    /// .ccllllllcccclllOMMMMMMMMM0lllllllllc
    /// .lllllclccccllllKMMMMMMMMMMollllllllc.
    /// .lllllllccccclllKMMMMMMMMN0lllllllllc.
    /// .cclllllcccclllldxkkxxdollllllllllclc
    ///  :cccllllllcccclllccllllcclccccccccc;
    ///  .ccclllllllcccccccclllccccclccccccc
    ///   .cllllllllllclcccclccclccllllcllc
    ///     :cllllllllccclcllllllllllllcc;
    ///      .cccccccccccccclcccccccccc:.
    ///        .;cccclccccccllllllccc,.
    ///           .';ccccclllccc:;..
    ///                 .....
    /// ```
    pub fn fedora_coreos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEDORA_COREOS,
            names: vec![
                "Fedora_coreos",
                "fedora-coreos",
                "fedora-linux-coreos",
                "fedora-linux_coreos",
                "ContainerLinux",
                "Container Linux",
                "Container Linux by CoreOS",
            ],
            colors: vec![Color::Blue, Color::White, Color::Red],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of FemboyOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::femboy_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// MMMMWKkxkKWMMMMMMMMMMMMMMMMMMMMWKkxkKWMM
    /// MMMMXo. .;xKWMMMMMMMMMMMMMMMMMMXo. .oXMM
    /// MMWXx,..'..oXMMMMMMMMMMMMMMMMWKx,  .lXMM
    /// MMNo. .cOc.,xKWMMMMMMMMMMMMWXx;.....cXMM
    /// MMXl..;kKl. .oXMMMMMMMMMMWKx;..,ok:.'o0W
    /// WKx,.cKWNk;..lXMMMMMMMMWKx;..,o0NXl. .oN
    /// No. .lXMMWKc.,dKWMMMMMMNo..;d0NWMNx,..lX
    /// Nk:,:kNMMMNk:,ckNMMMMMMNxcxXWMMMMMN0ockN
    /// MWNNNWMMMMMWNNNWMMMMMMMMWWWMMMMMMMMMWWWM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// MMMMMMMNXKXNWMMMMMMMMMMMWNKOKWMMMMMMMMMM
    /// MMMMMMWKdccxXMMMMMMMMMMW0o'.oXMMMMMMMMMM
    /// MMMMMMMNO:.'o0NKkkkkkOXXo. .lXMMMMMMMMMM
    /// MMMMMMMMNx,..;o;.    .:o,..;kNMMMMMMMMMM
    /// MMMMMMMMMNO:     ...     .cKWMMMMMMMMMMM
    /// MMMMMMMMMMNx,. .;dk:.   .;kNMMMMMMMMMMMM
    /// MMMMMMMMMMMN0ocxXWNkl:,:xXWMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMWNWMMMWWNNNWMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// ```
    pub fn femboy_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEMBOYOS,
            names: vec!["FemboyOS"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Feren distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::feren().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  `----------`
    ///  :+ooooooooo+.
    /// -o+oooooooooo+-
    /// ..`/+++++++++++/...`````````````````
    ///    .++++++++++++++++++++++++++/////-
    ///     ++++++++++++++++++++++++++++++++//:`
    ///     -++++++++++++++++++++++++++++++/-`
    ///      ++++++++++++++++++++++++++++:.
    ///      -++++++++++++++++++++++++/.
    ///       +++++++++++++++++++++/-`
    ///       -++++++++++++++++++//-`
    ///         .:+++++++++++++//////-
    ///            .:++++++++//////////-
    ///              `-++++++---:::://///.
    ///            `.:///+++.             `
    ///           `.........
    /// ```
    pub fn feren() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FEREN,
            names: vec!["Feren"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Finnix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::finnix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ,,:;;;;:,,
    ///         ,;*%S########S%*;,
    ///       ;?#################S?:
    ///     :%######################?:
    ///    +##########################;
    ///   +############################;
    ///  :#############.**,#############,
    ///  *###########+      +###########+
    ///  ?##########  Finnix  ##########*
    ///  *###########,      ,###########+
    ///  :#############%..%#############,
    ///   *############################+
    ///    *##########################+
    ///     ;S######################%:
    ///      ,+%##################%;
    ///         :+?S##########S?+:
    ///             ,:;++++;:,
    /// ```
    pub fn finnix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FINNIX,
            names: vec!["Finnix"],
            colors: vec![Color::Blue, Color::White, Color::BrightBlue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Floflis distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::floflis().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               ,▄▄▄▌▓▓███▓▓▌▄▄▄,
    ///          ,▄▒▓███████████████████▓▄▄
    ///        ▄▓███████████████████████████▌
    ///       ▓███████████████████████████████
    ///    ,  ╙▓████████████████████████████▀   ▄
    ///   ╓█▓▄   ╙▀▓████████████████████▀▀`  ,▄██▓
    ///  ╓█████▌▄,    '▀▀▀▀▓▓▓▓▓▓▀▀Å╙`    ▄▄▓█████▌
    ///  ██████████▓▌▄  ,             ▄▓███████████▄
    /// ╢████████████▓  ║████▓▓███▌  ╣█████████████▓
    /// ▓█████████████  ▐█████████▀  ▓██████████████
    /// ▓█████████████  ▐█████████▄  ███████████████
    /// ▀████████████▌  ║█████████▌  ▀█████████████▌
    ///  ████████████M  ▓██████████  ▐█████████████⌐
    ///  ▀██████████▌  ▐███████████▌  ▀███████████▌
    ///    ╙▓█████▓   ▓██████████████▄  ▀███████▀
    ///      ╝▓██▀  ╓▓████████████████▓   ▀▓██▀
    ///           ,▄████████████████████▌,
    ///           ╝▀████████████████████▓▀'
    ///              `╙▀▀▓▓███████▓▀▀╩'
    /// ```
    pub fn floflis() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FLOFLIS,
            names: vec!["Floflis"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of FreeBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::freebsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ```                        `
    ///   ` `.....---.......--.```   -/
    ///   +o   .--`         /y:`      +.
    ///    yo`:.            :o      `+-
    ///     y/               -/`   -o/
    ///    .-                  ::/sy+:.
    ///    /                     `--  /
    ///   `:                          :`
    ///   `:                          :`
    ///    /                          /
    ///    .-                        -.
    ///     --                      -.
    ///      `:`                  `:`
    ///        .--             `--.
    ///           .---.....----.
    /// ```
    pub fn freebsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FREEBSD,
            names: vec!["Freebsd"],
            colors: vec![Color::White, Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of FreeBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::freebsd_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ```                        `
    /// /\,-'''''-,/\
    /// \_)       (_/
    /// |           |
    /// |           |
    ///  ;         ;
    ///   '-_____-'
    /// ```
    pub fn freebsd_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FREEBSD_SMALL,
            names: vec!["freebsd_small"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of FreeMINT distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::free_mint().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           ##
    ///           ##         #########
    ///                     ####      ##
    ///             ####  ####        ##
    /// ####        ####  ##        ##
    ///         ####    ####      ##  ##
    ///         ####  ####  ##  ##  ##
    ///             ####  ######
    ///         ######  ##  ##  ####
    ///       ####    ################
    ///     ####        ##  ####
    ///     ##            ####  ######
    ///     ##      ##    ####  ####
    ///     ##    ##  ##    ##  ##  ####
    ///       ####  ##          ##  ##
    /// ```
    pub fn free_mint() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FREEMINT,
            names: vec!["FreeMiNT"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Frugalware distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::frugalware().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           `++/::-.`
    ///          /o+++++++++/::-.`
    ///         `o+++++++++++++++o++/::-.`
    ///         /+++++++++++++++++++++++oo++/:-.``
    ///        .o+ooooooooooooooooooosssssssso++oo++/:-`
    ///        ++osoooooooooooosssssssssssssyyo+++++++o:
    ///       -o+ssoooooooooooosssssssssssssyyo+++++++s`
    ///       o++ssoooooo++++++++++++++sssyyyyo++++++o:
    ///      :o++ssoooooo/-------------+syyyyyo+++++oo
    ///     `o+++ssoooooo/-----+++++ooosyyyyyyo++++os:
    ///     /o+++ssoooooo/-----ooooooosyyyyyyyo+oooss
    ///    .o++++ssooooos/------------syyyyyyhsosssy-
    ///    ++++++ssooooss/-----+++++ooyyhhhhhdssssso
    ///   -s+++++syssssss/-----yyhhhhhhhhhhhddssssy.
    ///   sooooooyhyyyyyh/-----hhhhhhhhhhhddddyssy+
    ///  :yooooooyhyyyhhhyyyyyyhhhhhhhhhhdddddyssy`
    ///  yoooooooyhyyhhhhhhhhhhhhhhhhhhhddddddysy/
    /// -ysooooooydhhhhhhhhhhhddddddddddddddddssy
    ///  .-:/+osssyyyysyyyyyyyyyyyyyyyyyyyyyyssy:
    ///        ``.-/+oosysssssssssssssssssssssss
    ///                ``.:/+osyysssssssssssssh.
    ///                         `-:/+osyyssssyo
    ///                                 .-:+++`
    /// ```
    pub fn frugalware() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FRUGALWARE,
            names: vec!["Frugalware", "frugalware-linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Funtoo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::funtoo().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    .dKXXd                         .
    ///   :XXl;:.                      .OXo
    /// .'OXO''  .''''''''''''''''''''':XNd..'oco.lco,
    /// xXXXXXX, cXXXNNNXXXXNNXXXXXXXXNNNNKOOK; d0O .k
    ///   kXX  xXo  KNNN0  KNN.       'xXNo   :c; 'cc.
    ///   kXX  xNo  KNNN0  KNN. :xxxx. 'NNo
    ///   kXX  xNo  loooc  KNN. oNNNN. 'NNo
    ///   kXX  xN0:.       KNN' oNNNX' ,XNk
    ///   kXX  xNNXNNNNNNNNXNNNNNNNNXNNOxXNX0Xl
    ///   ...  ......................... .;cc;.
    /// ```
    pub fn funtoo() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FUNTOO,
            names: vec!["Funtoo"],
            colors: vec![Color::Magenta, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of GalliumOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::gallium_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// sooooooooooooooooooooooooooooooooooooo+:
    /// yyooooooooooooooooooooooooooooooooo+/:::
    /// yyysoooooooooooooooooooooooooooo+/::::::
    /// yyyyyoooooooooooooooooooooooo+/:::::::::
    /// yyyyyysoooooooooooooooooo++/::::::::::::
    /// yyyyyyysoooooooooooooo++/:::::::::::::::
    /// yyyyyyyyysoooooosydddys+/:::::::::::::::
    /// yyyyyyyyyysooosmMMMMMMMNd+::::::::::::::
    /// yyyyyyyyyyyyosMMMMMMMMMMMN/:::::::::::::
    /// yyyyyyyyyyyyydMMMMMMMMMMMMo//:::::::::::
    /// yyyyyyyyyyyyyhMMMMMMMMMMMm--//::::::::::
    /// yyyyyyyyyyyyyyhmMMMMMMMNy:..-://::::::::
    /// yyyyyyyyyyyyyyyyyhhyys+:......://:::::::
    /// yyyyyyyyyyyyyyys+:--...........-///:::::
    /// yyyyyyyyyyyys+:--................://::::
    /// yyyyyyyyyo+:-.....................-//:::
    /// yyyyyyo+:-..........................://:
    /// yyyo+:-..............................-//
    /// o/:-...................................:
    /// ```
    pub fn gallium_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GALLIUMOS,
            names: vec!["GalliumOS"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Garuda distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::garuda().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    .%;888:8898898:
    ///                  x;XxXB%89b8:b8%b88:
    ///               .8Xxd                8X:.
    ///             .8Xx;                    8x:.
    ///           .tt8x          .d            x88;
    ///        .@8x8;          .db:              xx@;
    ///      ,tSXX°          .bbbbbbbbbbbbbbbbbbbB8x@;
    ///    .SXxx            bBBBBBBBBBBBBBBBBBBBbSBX8;
    ///  ,888S                                     pd!
    /// 8X88/                                       q
    /// 8X88/
    /// GBB.
    ///  x%88        d888@8@X@X@X88X@@XX@@X@8@X.
    ///    dxXd    dB8b8b8B8B08bB88b998888b88x.
    ///     dxx8o                      .@@;.
    ///       dx88                   .t@x.
    ///         d:SS@8ba89aa67a853Sxxad.
    ///           .d988999889889899dd.
    /// ```
    pub fn garuda() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GARUDA,
            names: vec!["Garuda", "garuda-linux"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Garuda Dragon distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::garuda_dragon().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .:--=========--:..
    ///            .:-=+++++===-----=======-:.
    ///          :=++++-:..            ..:-===-:.
    ///        .+++=:.                      .-=---.
    ///      . :-:                            :---:
    ///       :=           .                     :---:
    ///        .=-:        :-.                    .---:
    ///          :-===--::. .::::.                  ---:
    ///       .::--====-===+=-----=-:                ---:
    ///  ::  :-++++====--=-:=====--=--:              .---
    /// +**=:+++===++++++==- -===== -==-.             ---:
    /// *****+==++==--::::::======== . ===-.          :---
    /// ****==++-::.:::::-.::--======-.-===           .---
    /// ***+=+-::::--:.     -==:-==--=======-::. .    :---
    /// =**++-::::==       -+===-==: :::.:-=-==---    :--:
    /// .*+*-:::-+=        ...::====  .::  .=-:--.     ..
    ///  -*++:::=+-:             .:=-   -- .:  .  ...
    ///   =*++:.++-+:   .:.         =-.: :-:    ::   :.
    ///    -*++-=+=-+=-=++===-.     .====: .::::.
    ///     :++++++-:::--.-:===       --.
    ///      .=+++++- .=  +.=:=                .::
    ///        :=+++++:. .: : .              :----
    ///          .-++++=-:..            .:--===-.
    ///             .-=+++++===-----=======-:.
    /// ```
    pub fn garuda_dragon() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GARUDA_DRAGON,
            names: vec!["GarudaDragon", "garuda-dragon", "garuda-linux-dragon"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Garuda distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::garuda_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///      .----.
    ///    .'   ,  '.
    ///  .'    '-----|
    /// '.   -----,
    ///   '.____.'
    /// ```
    pub fn garuda_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GARUDA_SMALL,
            names: vec!["Garuda_small", "garudalinux_small", "garuda-linux-small"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Gentoo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::gentoo().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          -/oyddmdhs+:.
    ///      -odNMMMMMMMMNNmhy+-`
    ///    -yNMMMMMMMMMMMNNNmmdhy+-
    ///  `omMMMMMMMMMMMMNmdmmmmddhhy/`
    ///  omMMMMMMMMMMMNhhyyyohmdddhhhdo`
    /// .ydMMMMMMMMMMdhs++so/smdddhhhhdm+`
    ///  oyhdmNMMMMMMMNdyooydmddddhhhhyhNd.
    ///   :oyhhdNNMMMMMMMNNNmmdddhhhhhyymMh
    ///     .:+sydNMMMMMNNNmmmdddhhhhhhmMmy
    ///        /mMMMMMMNNNmmmdddhhhhhmMNhs:
    ///     `oNMMMMMMMNNNmmmddddhhdmMNhs+`
    ///   `sNMMMMMMMMNNNmmmdddddmNMmhs/.
    ///  /NMMMMMMMMNNNNmmmdddmNMNdso:`
    /// +MMMMMMMNNNNNmmmmdmNMNdso/-
    /// yMMNNNNNNNmmmmmNNMmhs+/-`
    /// /hMMNNNNNNNNMNdhs++/-`
    /// `/ohdmmddhys+++/:.`
    ///   `-//////:--.
    /// ```
    pub fn gentoo() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GENTOO,
            names: vec!["Gentoo", "gentoo-linux"],
            colors: vec![Color::Magenta, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Gentoo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::gentoo_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  _-----_
    /// (       \
    /// \    0   \
    ///  \        )
    ///  /      _/
    /// (     _-
    /// \____-
    /// ```
    pub fn gentoo_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GENTOO_SMALL,
            names: vec!["Gentoo_small", "gentoo-linux-small"],
            colors: vec![Color::Magenta, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of GhostBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ghost_bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            ,gggggg.
    ///         ,agg9*   .g)
    ///       .agg* ._.,gg*
    ///     ,gga*  (ggg*'
    ///    ,ga*       ,ga*
    ///   ,ga'     .ag*
    ///  ,ga'   .agga'
    ///  9g' .agg'g*,a
    ///  'gggg*',gga'
    ///       .gg*'
    ///     .gga*
    ///   .gga*
    ///  (ga*
    /// ```
    pub fn ghost_bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GHOSTBSD,
            names: vec!["GhostBSD"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Glaucus distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::glaucus().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ,,        ,d88P
    ///            ,d8P    ,ad8888*
    ///          ,888P    d88888*     ,,ad8888P*
    ///     d   d888P   a88888P*  ,ad8888888*
    ///   .d8  d8888:  d888888* ,d888888P*
    ///  .888; 88888b d8888888b8888888P
    ///  d8888J888888a88888888888888P*    ,d
    ///  88888888888888888888888888P   ,,d8*
    ///  888888888888888888888888888888888*
    ///  *8888888888888888888888888888888*
    ///   Y888888888P* `*``*888888888888*
    ///    *^888^*            *Y888P**
    /// ```
    pub fn glaucus() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GLAUCUS,
            names: vec!["Glaucus"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of gNewSense distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::g_new_sense().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      ..,,,,..
    ///                .oocchhhhhhhhhhccoo.
    ///         .ochhlllllllc hhhhhh ollllllhhco.
    ///     ochlllllllllll hhhllllllhhh lllllllllllhco
    ///  .cllllllllllllll hlllllo  +hllh llllllllllllllc.
    /// ollllllllllhco''  hlllllo  +hllh  ``ochllllllllllo
    /// hllllllllc'       hllllllllllllh       `cllllllllh
    /// ollllllh          +llllllllllll+          hllllllo
    ///  `cllllh.           ohllllllho           .hllllc'
    ///     ochllc.            ++++            .cllhco
    ///        `+occooo+.                .+ooocco+'
    ///               `+oo++++      ++++oo+'
    /// ```
    pub fn g_new_sense() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GNEWSENSE,
            names: vec!["gNewSense"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Gnome desktop
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::gnome().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                ,@@@@@@@@,
    ///                  @@@@@@      @@@@@@@@@@@@
    ///         ,@@.    @@@@@@@    *@@@@@@@@@@@@
    ///        @@@@@%   @@@@@@(    @@@@@@@@@@@&
    ///        @@@@@@    @@@@*     @@@@@@@@@#
    /// @@@@*   @@@@,              *@@@@@%
    /// @@@@@.
    ///  @@@@#         @@@@@@@@@@@@@@@@
    ///          ,@@@@@@@@@@@@@@@@@@@@@@@,
    ///       ,@@@@@@@@@@@@@@@@@@@@@@@@@@&
    ///     .@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///     @@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///    @@@@@@@@@@@@@@@@@@@@@@@@(
    ///    @@@@@@@@@@@@@@@@@@@@%
    ///     @@@@@@@@@@@@@@@@
    ///      @@@@@@@@@@@@*        @@@@@@@@/
    ///       &@@@@@@@@@@        @@@@@@@@@*
    ///         @@@@@@@@@@@,    @@@@@@@@@*
    ///           ,@@@@@@@@@@@@@@@@@@@@&
    ///               &@@@@@@@@@@@@@@
    ///                      ...
    /// ```
    pub fn gnome() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GNOME,
            names: vec!["Gnome"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of GNU
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::gnu().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     _-`````-,           ,- '- .
    ///   .'   .- - |          | - -.  `.
    ///  /.'  /                     `.   \
    /// :/   :      _...   ..._      ``   :
    /// ::   :     /._ .`:'_.._\.    ||   :
    /// ::    `._ ./  ,`  :    \ . _.''   .
    /// `:.      /   |  -.  \-. \\_      /
    ///   \:._ _/  .'   .@)  \@) ` `\ ,.'
    ///      _/,--'       .- .\,-.`--`.
    ///        ,'/''     (( \ `  )
    ///         /'/'  \    `-'  (
    ///          '/''  `._,-----'
    ///           ''/'    .,---'
    ///            ''/'      ;:
    ///              ''/''  ''/
    ///                ''/''/''
    ///                  '/'/'
    ///                   `;
    /// ```
    pub fn gnu() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GNU,
            names: vec!["GNU"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Gobo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::gobo_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   _____       _
    ///  / ____|     | |
    /// | |  __  ___ | |__   ___
    /// | | |_ |/ _ \| '_ \ / _ \
    /// | |__| | (_) | |_) | (_) |
    ///  \_____|\___/|_.__/ \___/
    /// ```
    pub fn gobo_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GOBOLINUX,
            names: vec!["GoboLinux", "Gobo"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of GrapheneOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::graphene_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                         B?
    ///                         G~
    ///                         G~&
    ///                       G!^:^?#
    ///                      &^.:::.J
    ///     &PG&          #G5JJ7~^~?JY5B&          #PG
    ///      B5JJPGJ77YG5JYP#   &&   &B5JYPGJ7?YG5JYP#
    ///         &Y..::.:P&               &?..::.:G
    ///          #!::::?                  B~::::J
    ///            B~J#                     B!?#
    ///             !P                       75
    ///             !P                       75
    ///             !5                       7Y
    ///          &Y~:^!P                  &J~:^!P
    ///          P..::.:B                 Y..::.:#
    ///       #PYJJ~^^!JJYP#          &B5YJ?~^^!JJYG#
    ///     &YYG#   &&   #PYJ5G5??JGGYJ5G&   &&   #PYP
    ///                      B^.::..7&
    ///                       J::::^G
    ///                        #Y^G&
    ///                         B~
    ///                         G!
    ///                         #
    /// ```
    pub fn graphene_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GRAPHENEOS,
            names: vec!["GrapheneOS"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Grombyang distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::grombyang().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             eeeeeeeeeeee
    ///          eeeeeeeeeeeeeeeee
    ///       eeeeeeeeeeeeeeeeeeeeeee
    ///     eeeee       .o+       eeee
    ///   eeee         `ooo/         eeee
    ///  eeee         `+oooo:         eeee
    /// eee          `+oooooo:          eee
    /// eee          -+oooooo+:         eee
    /// ee         `/:oooooooo+:         ee
    /// ee        `/+   +++    +:        ee
    /// ee              +o+\             ee
    /// eee             +o+\            eee
    /// eee        //  \\ooo/  \\\      eee
    ///  eee      //++++oooo++++\\\    eee
    ///   eeee    ::::++oooo+:::::   eeee
    ///     eeeee   Grombyang OS   eeee
    ///       eeeeeeeeeeeeeeeeeeeeeee
    ///          eeeeeeeeeeeeeeeee
    /// ```
    pub fn grombyang() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GROMBYANG,
            names: vec!["Grombyang"],
            colors: vec![Color::Blue, Color::Green, Color::Red],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Guix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::guix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  ..                             `.
    ///  `--..```..`           `..```..--`
    ///    .-:///-:::.       `-:::///:-.
    ///       ````.:::`     `:::.````
    ///            -//:`    -::-
    ///             ://:   -::-
    ///             `///- .:::`
    ///              -+++-:::.
    ///               :+/:::-
    ///               `-....`
    /// ```
    pub fn guix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GUIX,
            names: vec!["Guix"],
            colors: vec![Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Guix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::guix_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// |.__          __.|
    /// |__ \\        / __|
    ///    \\ \\      / /
    ///     \\ \\    / /
    ///      \\ \\  / /
    ///       \\ \\/ /
    ///        \\__/
    /// ```
    pub fn guix_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_GUIX_SMALL,
            names: vec!["Guix_small"],
            colors: vec![Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Haiku distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::haiku().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            MMMM       .ciO| /YMMMMM*"
    ///            MMMM   .cOMMMMM|/MMMMM/`
    ///  ,         ,iMM|/MMMMMMMMMMMMMMM*
    ///   `*.__,-cMMMMMMMMMMMMMMMMM/`.MMM
    ///            MMMMMMMMM/`:MMM/  MMMM
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            """"              """"
    /// ```
    pub fn haiku() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HAIKU,
            names: vec!["Haiku"],
            colors: vec![Color::Red, Color::Yellow, Color::White, Color::Green],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Haiku distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::haiku_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        ,^,
    ///       /   \\
    /// *--_ ;     ; _--*
    /// \\   '"     "'   /
    ///  '.           .'
    /// .-'"         "'-.
    ///  '-.__.   .__.-'
    ///        |_|
    /// ```
    pub fn haiku_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HAIKU_SMALL,
            names: vec!["Haiku-small", "Haiku_small"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of HamoniKR distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hamoni_kr().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      cO0Ox.
    ///                   .ldddddddo.
    ///                 .lddddddddddo
    ///               'lddddddddddddc
    ///             ,oddddddddddddd;
    ///           'ldddddddddddddo.
    ///         .oddddddddddddddc.
    ///       ,dddddddddddddddo.
    ///     ,ccoooooooocoddooo:
    ///   ,cooooooooooooooooop                  c000x.
    /// .cooooooooooooooopcllll              .cddddddo.
    /// coooooooooooooop' .qlll.           .ddoooooooo;
    /// cooooooooooc;        'qlllp.     .ddoooooooooo;
    /// .cooooooc;             'lllbc...coooooooooooo;
    ///   .cooc'                .llllcoooooooooooooo.
    ///                          .coooooooooooooop:
    ///                        .coooooooooooooop'
    ///                       .cooooooooooooop.
    ///                     .cooooooooooooop.
    ///                    .coooooooooooop.
    ///                   .cooooooooooop.
    ///                   .cooooooooop.
    ///                    .cooooop'
    /// ```
    pub fn hamoni_kr() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HAMONIKR,
            names: vec!["HamoniKR"],
            colors: vec![
                Color::Blue,
                Color::White,
                Color::TrueColor {
                    r: 0x87,
                    g: 0x5f,
                    b: 0xff,
                },
            ],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of HarDClanZ distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::har_dc_lan_z().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           ........::::....
    ///         ::################::..
    ///       :########################:.
    ///      :######**###################:
    ///     :###&&&&^############ &#######:
    ///    :#&&&&&.:##############:^&o`:###:
    ///   :#&&&&.:#################:.&&&`###:
    ///  :##&^:######################:^&&::##:
    ///  :#############################:&:##::
    ///  :##########@@###########@@#####:.###:
    /// :#########@@o@@#########@@o@@########:
    /// :#######:@@o0o@@@@###@@@@o0o@@######: :
    ///  :######:@@@o@@@@@@V@@@@@@o@@@######:
    ///    :#####:@@@@@@@@@@@@@@@@@@@:####;
    ///     :####:.@@@@@@@@@@@@@@@@:#####:
    ///     `:####:.@@@@@@@@@@@@@@:#####:
    ///       ``:##:.@@@@@@@@@@@@^## # :
    ///          : ##  \@@@;@@@/ :: # :
    ///                   VVV
    /// ```
    pub fn har_dc_lan_z() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HARDCLANZ,
            names: vec!["HarDClanZ"],
            colors: vec![
                Color::Blue,
                Color::White,
                Color::Red,
                Color::BrightBlue,
                Color::BrightWhite,
            ],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of HardenedBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hardened_bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ```                        `
    ///   ` `.....---.......--.```   -/
    ///   +o   .--`         /y:`      +.
    ///    yo`:.            :o      `+-
    ///     y/               -/`   -o/
    ///    .-                  ::/sy+:.
    ///    /                     `--  /
    ///   `:                          :`
    ///   `:                          :`
    ///    /                          /
    ///    .-                        -.
    ///     --                      -.
    ///      `:`                  `:`
    ///        .--             `--.
    ///           .---.....----.
    /// ```
    pub fn hardened_bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_FREEBSD,
            names: vec!["HardenedBSD"],
            colors: vec![Color::White, Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Hash distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hash().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       +   ######   +
    ///     ###   ######   ###
    ///   #####   ######   #####
    ///  ######   ######   ######
    ///
    /// ####### '"###### '"########
    /// #######   ######   ########
    /// #######   ######   ########
    ///
    ///  ###### '"###### '"######
    ///   #####   ######   #####
    ///     ###   ######   ###
    ///       ~   ######   ~
    /// ```
    pub fn hash() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HASH,
            names: vec!["Hash"],
            colors: vec![
                Color::TrueColor {
                    r: 0x87,
                    g: 0xff,
                    b: 0xff,
                },
                Color::TrueColor {
                    r: 0x87,
                    g: 0xff,
                    b: 0xff,
                },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Huayra distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::huayra().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      `
    ///             .       .       `
    ///        ``    -      .      .
    ///         `.`   -` `. -  `` .`
    ///           ..`-`-` + -  / .`     ```
    ///           .--.+--`+:- :/.` .-``.`
    ///             -+/so::h:.d-`./:`.`
    ///               :hNhyMomy:os-...-.  ````
    ///                .dhsshNmNhoo+:-``.```
    ///                 `ohy:-NMds+::-.``
    ///             ````.hNN+`mMNho/:-....````
    ///        `````     `../dmNhoo+/:..``
    ///     ````            .dh++o/:....`
    /// .+s/`                `/s-.-.:.`` ````
    /// ::`                    `::`..`
    ///                           .` `..
    ///                                 ``
    /// ```
    pub fn huayra() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HUAYRA,
            names: vec!["Huayra"],
            colors: vec![Color::White, Color::Blue],
            color_keys: Some(Color::White),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Hybrid distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hybrid().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    /                          #
    /// ////&                      #####
    /// /////                      ######
    /// /////      //////////      ######
    /// ///// //////////////////// ######
    /// ////////////////////////// ######
    /// /////////              /// ######
    /// ///////                  / ######
    /// //////                     ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      #########
    /// ////&                       ########
    /// ```
    pub fn hybrid() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HYBRID,
            names: vec!["Hybrid"],
            colors: vec![Color::Blue, Color::BrightBlue],
            color_keys: Some(Color::BrightBlue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of HydroOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hydro_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  _    _           _            ____   _____
    /// | |  | |         | |          / __ \ / ____|
    /// | |__| |_   _  __| |_ __ ___ | |  | | (___
    /// |  __  | | | |/ _` | '__/ _ \| |  | |\___ \
    /// | |  | | |_| | (_| | | | (_) | |__| |____) |
    /// |_|  |_|\__, |\__,_|_|  \___/ \____/|_____/
    ///          __/ |
    ///         |___/
    /// ```
    pub fn hydro_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HYDROOS,
            names: vec!["HydroOS"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Hyperbola distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hyperbola().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      WW
    ///                      KX              W
    ///                     WO0W          NX0O
    ///                     NOO0NW  WNXK0OOKW
    ///                     W0OOOOOOOOOOOOKN
    ///                      N0OOOOOOO0KXW
    ///                        WNXXXNW
    ///                  NXK00000KN
    ///              WNK0OOOOOOOOOO0W
    ///            NK0OOOOOOOOOOOOOO0W
    ///          X0OOOOOOO00KK00OOOOOK
    ///        X0OOOO0KNWW      WX0OO0W
    ///      X0OO0XNW              KOOW
    ///    N00KNW                   KOW
    ///  NKXN                       W0W
    /// WW                           W
    /// ```
    pub fn hyperbola() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HYPERBOLA,
            names: vec!["Hyperbola"],
            colors: vec![Color::Black],
            color_keys: Some(Color::Black),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Hyperbola distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::hyperbola_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     |`__.`/
    ///     \____/
    ///     .--.
    ///    /    \\
    ///   /  ___ \\
    ///  / .`   `.\\
    /// /.`      `.\\
    /// ```
    pub fn hyperbola_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_HYPERBOLA_SMALL,
            names: vec!["Hyperbola_small"],
            colors: vec![Color::Black],
            color_keys: Some(Color::Black),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Iglunix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::iglunix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///      |
    ///      |        |
    ///               |
    /// |    ________
    /// |  /\   |    \
    ///   /  \  |     \  |
    ///  /    \        \ |
    /// /      \________\
    /// \      /        /
    ///  \    /        /
    ///   \  /        /
    ///    \/________/
    /// ```
    pub fn iglunix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_IGLUNIX,
            names: vec!["Iglunix", "Iglu"],
            colors: vec![Color::Black],
            color_keys: Some(Color::Black),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of InstantOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::instant_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///      'cx0XWWMMWNKOd:'.
    ///   .;kNMMMMMMMMMMMMMWNKd'
    ///  'kNMMMMMMWNNNWMMMMMMMMXo.
    /// ,0MMMMMW0o;'..,:dKWMMMMMWx.
    /// OMMMMMXl.        .xNMMMMMNo
    /// WMMMMNl           .kWWMMMMO'
    /// MMMMMX;            oNWMMMMK,
    /// NMMMMWo           .OWMMMMMK,
    /// kWMMMMNd.        ,kWMMMMMMK,
    /// 'kWMMMMWXxl:;;:okNMMMMMMMMK,
    ///  .oXMMMMMMMWWWMMMMMMMMMMMMK,
    ///    'oKWMMMMMMMMMMMMMMMMMMMK,
    ///      .;lxOKXXXXXXXXXXXXXXXO;......
    ///           ................,d0000000kd:.
    ///                           .kMMMMMMMMMW0;
    ///                           .kMMMMMMMMMMMX
    ///                           .xMMMMMMMMMMMW
    ///                            cXMMMMMMMMMM0
    ///                             :0WMMMMMMNx,
    ///                              .o0NMWNOc.
    /// ```
    pub fn instant_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_INSTANTOS,
            names: vec!["InstantOS"],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Interix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::interix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    ..
    ///                   75G!
    ///                 ^?PG&&J.
    ///               :!5GPP&&&B!
    ///              :YPPPPP&&&&&Y:
    ///             !5PPPPPP&&&&&&B!
    ///           :?PPPPPPPP&&&&&&&&Y~
    ///          !5PPPPPPPPP###&&&&&&B7
    ///        :?PPPP5555555B####&&&&&&5:
    ///       ~5PPPP555YJ7!~7?5B###&&&&&B?.
    ///    .:JPPPP5555Y?^....:^?G####&&&&&5:
    ///    75PPP555555Y7:....:^!5#####&&&&&B7.
    ///  :JPPPP555555YY?~::::^~7YPGBB###&&&&&5^
    /// 75GGPPPPPP555555YJ?77??YYYYYY55PPGGB#&B?
    /// ~!!7JY5PGGBBBBBBBBGGGGGGGBGGGGGP5YJ?7~~~
    ///        .::^~7?JYPGBB#BGPYJ?7!7^:.
    ///                  ..:^...
    /// ```
    pub fn interix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_INTERIX,
            names: vec!["Interix"],
            colors: vec![
                Color::Red,
                Color::White,
                Color::Blue,
                Color::Black,
                Color::Yellow,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Irix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::irix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            ./ohmNd/  +dNmho/-
    ///      `:+ydNMMMMMMMM.-MMMMMMMMMdyo:.
    ///    `hMMMMMMNhs/sMMM-:MMM+/shNMMMMMMh`
    ///    -NMMMMMmo-` /MMM-/MMM- `-omMMMMMN.
    ///  `.`-+hNMMMMMNhyMMM-/MMMshmMMMMMmy+...`
    /// +mMNds:-:sdNMMMMMMMyyMMMMMMMNdo:.:sdMMm+
    /// dMMMMMMmy+.-/ymNMMMMMMMMNmy/-.+hmMMMMMMd
    /// oMMMMmMMMMNds:.+MMMmmMMN/.-odNMMMMmMMMM+
    /// .MMMM-/ymMMMMMmNMMy..hMMNmMMMMMmy/-MMMM.
    ///  hMMM/ `/dMMMMMMMN////NMMMMMMMd/. /MMMh
    ///  /MMMdhmMMMmyyMMMMMMMMMMMMhymMMMmhdMMM:
    ///  `mMMMMNho//sdMMMMM//NMMMMms//ohNMMMMd
    ///   `/so/:+ymMMMNMMMM` mMMMMMMMmh+::+o/`
    ///      `yNMMNho-yMMMM` NMMMm.+hNMMNh`
    ///      -MMMMd:  oMMMM. NMMMh  :hMMMM-
    ///       -yNMMMmooMMMM- NMMMyomMMMNy-
    ///         .omMMMMMMMM-`NMMMMMMMmo.
    ///           `:hMMMMMM. NMMMMMh/`
    ///              .odNm+  /dNms.
    /// ```
    pub fn irix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_IRIX,
            names: vec!["IRIX"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ironclad distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ironclad().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  &#BGPPPPPG#&
    ///               B5?77!!?YJJ7!7YBB&
    ///            &G5YJ77!7JYYYYYBPJ&PY#
    ///          #PYYYYYY?!?YYYYY7?7JP5JJ
    ///         B?YYYYYY7!!7JYYYYJ!!?JJJ5
    ///  &&    B7?J?77?7!!!!!77777!7Y5YYBBPGGG&
    /// G77?YBB!!!!!!!!!!!!!JYJ??7JYJJY# PYPPG&
    /// J777JB?!7JJ???!!!7?JYYYYYPJ!7JB
    /// GYYG #JJJJJ??7!!!JYYY5PGB&GB&
    ///    #Y!?GB5YYJY5PG###&
    ///    GJJP
    /// ```
    pub fn ironclad() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_IRONCLAD,
            names: vec!["Ironclad"],
            colors: vec![Color::Black],
            color_keys: Some(Color::White),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Itc distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::itc().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ....................-==============+...
    /// ....................-==============:...
    /// ...:===========-....-==============:...
    /// ...-===========:....-==============-...
    /// ....*==========+........-::********-...
    /// ....*===========+.:*====**==*+-.-......
    /// ....:============*+-..--:+**====*---...
    /// ......::--........................::...
    /// ..+-:+-.+::*:+::+:-++::++-.:-.*.:++:++.
    /// ..:-:-++++:-::--:+::-::.:++-++:++--:-:.
    /// ```
    pub fn itc() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ITC,
            names: vec!["Itc"],
            colors: vec![Color::Red],
            color_keys: Some(Color::White),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Janus Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::janus_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                'l:
    ///         loooooo
    ///           loooo coooool
    ///  looooooooooooooooooool
    ///   looooooooooooooooo
    ///          lool   cooo
    ///         coooooooloooooooo
    ///      clooooo  ;lood  cloooo
    ///   :loooocooo cloo      loooo
    ///  loooo  :ooooool       loooo
    /// looo    cooooo        cooooo
    /// looooooooooooo      ;loooooo looooooc
    /// looooooooo loo   cloooooool    looooc
    ///  cooo       cooooooooooo       looolooooool
    ///             cooo:     coooooooooooooooooool
    ///                        loooooooooooolc:   loooc;
    ///                              cooo:    loooooooooooc
    ///                             ;oool         looooooo:
    ///                            coool          olc,
    ///                           looooc   ,,
    ///                         coooooc    loc
    ///                        :oooool,    coool:, looool:,
    ///                        looool:      ooooooooooooooo:
    ///                        cooolc        .ooooooooooool
    /// ```
    pub fn janus_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_JANUSLINUX,
            names: vec!["januslinux", "janus", "Ataraxia Linux", "Ataraxia"],
            colors: vec![Color::Blue, Color::Magenta],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Kaisen distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kaisen().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   `:+oyyho.
    ///              `+:`sdddddd/
    ///         `+` :ho oyo++ohds-`
    ///        .ho :dd.  .: `sddddddhhyso+/-
    ///        ody.ddd-:yd- +hysssyhddddddddho`
    ///        yddddddhddd` ` `--`   -+hddddddh.
    ///        hddy-+dddddy+ohh/..+sddddy/:::+ys
    ///       :ddd/sdddddddddd- oddddddd       `
    ///      `yddddddddddddddd/ /ddddddd/
    /// :.  :ydddddddddddddddddo..sddddddy/`
    /// odhdddddddo- `ddddh+-``....-+hdddddds.
    /// -ddddddhd:   /dddo  -ydddddddhdddddddd-
    ///  /hdy:o - `:sddds   .`./hdddddddddddddo
    ///   `/-  `+hddyosy+       :dddddddy-.-od/
    ///       :sydds           -hddddddd`    /
    ///        .+shd-      `:ohddddddddd`
    ///                 `:+ooooooooooooo:
    /// ```
    pub fn kaisen() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KAISEN,
            names: vec!["Kaisen"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Kali distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kali().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ..............
    ///             ..,;:ccc,.
    ///           ......''';lxO.
    /// .....''''..........,:ld;
    ///            .';;;:::;,,.x,
    ///       ..'''.            0Xxoc:,.  ...
    ///   ....                ,ONkc;,;cokOdc',.
    ///  .                   OMo           ':ddo.
    ///                     dMc               :OO;
    ///                     0M.                 .:o.
    ///                     ;Wd
    ///                      ;XO,
    ///                        ,d0Odlc;,..
    ///                            ..',;:cdOOd::,.
    ///                                     .:d;.':;.
    ///                                        'd,  .'
    ///                                          ;l   ..
    ///                                           .o
    ///                                             c
    ///                                             .'
    ///                                              .
    /// ```
    pub fn kali() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KALI,
            names: vec!["Kali", "Kalilinux"],
            colors: vec![Color::Blue, Color::Black],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Kali distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kali_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///      -#. #
    ///       @###
    ///   -######
    ///  @#########
    /// =##.  .#####
    /// ##      ## ##
    /// ##       ## #
    /// ##       @###
    /// ##.        ###
    ///  ##%       ##-
    ///   -##%    -*
    ///    :*##+
    ///      :*#*
    ///        -#
    ///         @
    ///         :
    /// ```
    pub fn kali_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KALI_SMALL,
            names: vec!["Kali_small", "Kalilinux_small"],
            colors: vec![Color::Blue, Color::Black],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of KaOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ka_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      ..
    ///   .....         ..OSSAAAAAAA..
    ///  .KKKKSS.     .SSAAAAAAAAAAA.
    /// .KKKKKSO.    .SAAAAAAAAAA...
    /// KKKKKKS.   .OAAAAAAAA.
    /// KKKKKKS.  .OAAAAAA.
    /// KKKKKKS. .SSAA..
    /// .KKKKKS..OAAAAAAAAAAAA........
    ///  DKKKKO.=AA=========A===AASSSO..
    ///   AKKKS.==========AASSSSAAAAAASS.
    ///   .=KKO..========ASS.....SSSSASSSS.
    ///     .KK.       .ASS..O.. =SSSSAOSS:
    ///      .OK.      .ASSSSSSSO...=A.SSA.
    ///        .K      ..SSSASSSS.. ..SSA.
    ///                  .SSS.AAKAKSSKA.
    ///                     .SSS....S..
    /// ```
    pub fn ka_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KAOS,
            names: vec!["KaOS"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of KernelOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kernel_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               .''''....''''.
    ///          .''...            ...''.
    ///        ''..                    ..''
    ///      '..                         ..''
    ///    .'.      .,,'..       .',,,'    ..'.
    ///   .'.       .,,,,'    .,,,,,'        .'.
    ///  .'.        .,,,,'  .,,,,,.           .'.
    ///  '..        .,,,,'',,,,;.             .''
    /// .'.         .,,,,,,,;;.               ..'
    /// .'.         .,,,,;;;;;,               ..'.
    ///  '..        .,;;;,';;;;;,             ..'
    ///  .'.        .;;;;'  .;;::::           .,.
    ///   '..       .;;;;'    .::::::.       .,'
    ///    '..      .;;;;'      .::::::.    .,'
    ///     .'..                          .',.
    ///       ',..                      .','
    ///          .,,...              ..',,.
    ///            ..,,''........',,,.
    ///                   ......
    /// ```
    pub fn kernel_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KERNELOS,
            names: vec!["KernelOS"],
            colors: vec![Color::Red, Color::Magenta],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of KDE-Neon distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kde_neon().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `..---+/---..`
    ///          `---.``   ``   `.---.`
    ///       .--.`        ``        `-:-.
    ///     `:/:     `.----//----.`     :/-
    ///    .:.    `---`          `--.`    .:`
    ///   .:`   `--`                .:-    `:.
    ///  `/    `:.      `.-::-.`      -:`   `/`
    ///  /.    /.     `:++++++++:`     .:    .:
    /// `/    .:     `+++++++++++/      /`   `+`
    /// /+`   --     .++++++++++++`     :.   .+:
    /// `/    .:     `+++++++++++/      /`   `+`
    ///  /`    /.     `:++++++++:`     .:    .:
    ///  ./    `:.      `.:::-.`      -:`   `/`
    ///   .:`   `--`                .:-    `:.
    ///    .:.    `---`          `--.`    .:`
    ///     `:/:     `.----//----.`     :/-
    ///       .-:.`        ``        `-:-.
    ///          `---.``   ``   `.---.`
    ///              `..---+/---..`
    /// ```
    pub fn kde_neon() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KDE,
            names: vec!["KDE", "kde-neon"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Kibojoe distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kibojoe().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            ./+oooooo+/.
    ///            -/+ooooo+/:.`
    ///           `yyyo+++/++osss.
    ///          +NMNyssssssssssss.
    ///        .dMMMMNsssssssssssyNs`
    ///       +MMMMMMMmssssssssssshMNo`
    ///     `hMMMMMNNNMdsssssssssssdMMN/
    ///    .syyyssssssyNNmmmmdssssshMMMMd:
    ///   -NMmhyssssssssyhhhhyssyhmMMMMMMMy`
    ///  -NMMMMMNNmdhyyyyyyyhdmNMMMMMMMMMMMN+
    /// `NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.
    /// ods+/:-----://+oyydmNMMMMMMMMMMMMMMMMMN-
    /// `                     .-:+osyhhdmmNNNmdo
    /// ```
    pub fn kibojoe() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KIBOJOE,
            names: vec!["Kibojoe"],
            colors: vec![Color::Green, Color::White, Color::Blue],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Kiss-Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kiss_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     ___     
    ///    (.· |     
    ///    (<> |     
    ///   / __  \    
    ///  ( /  \ /|   
    /// _/\ __)/_)   
    /// \/-____\/   
    /// ```
    pub fn kiss_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KISS,
            names: vec!["KISS", "kiss-linux", "kisslinux"],
            colors: vec![Color::Magenta, Color::White, Color::Blue],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Kogaion distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kogaion().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ;;      ,;
    ///            ;;;     ,;;
    ///          ,;;;;     ;;;;
    ///       ,;;;;;;;;    ;;;;
    ///      ;;;;;;;;;;;   ;;;;;
    ///     ,;;;;;;;;;;;;  ';;;;;,
    ///     ;;;;;;;;;;;;;;, ';;;;;;;
    ///     ;;;;;;;;;;;;;;;;;, ';;;;;
    /// ;    ';;;;;;;;;;;;;;;;;;, ;;;
    /// ;;;,  ';;;;;;;;;;;;;;;;;;;,;;
    /// ;;;;;,  ';;;;;;;;;;;;;;;;;;,
    /// ;;;;;;;;,  ';;;;;;;;;;;;;;;;,
    /// ;;;;;;;;;;;;, ';;;;;;;;;;;;;;
    /// ';;;;;;;;;;;;; ';;;;;;;;;;;;;
    ///  ';;;;;;;;;;;;;, ';;;;;;;;;;;
    ///   ';;;;;;;;;;;;;  ;;;;;;;;;;
    ///     ';;;;;;;;;;;; ;;;;;;;;
    ///         ';;;;;;;; ;;;;;;
    ///            ';;;;; ;;;;
    ///              ';;; ;;
    /// ```
    pub fn kogaion() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KOGAION,
            names: vec!["Kogaion"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Korora distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::korora().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ~~~term
    ///                 ____________
    ///              _add55555555554:
    ///            _w?'``````````')k:
    ///           _Z'`            ]k:
    ///           m(`             )k:
    ///      _.ss`m[`,            ]e:
    ///    .uY"^``Xc`?Ss.         d(`
    ///   jF'`    `@.  `Sc      .jr`
    ///  jr`       `?n_ `;   _a2"`
    /// .m:          `~M`1k`5?!``
    /// :#:             `)e```
    /// :m:             ,#'`
    /// :#:           .s2'`
    /// :m,________.aa7^`
    /// :#baaaaaaas!J'`
    ///  ```````````
    /// ~~~
    pub fn korora() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KORORA,
            names: vec!["Korora"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of KrassOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::krass_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   **@@@@@@@@@@@*
    ///              ,@@@@%(((((((((((((%@@@@,
    ///           #@@&(((((((((((((((((((((((&@@%
    ///         @@&(((((((((((((((((((((((((((((@@@
    ///       @@&(((((((((((((((((((((((((((((((((&@@
    ///     .@@(((((((((((((((((((((((((((((((((((((@@.
    ///     @@(((((((((((((((((((((((((((((((((((((((@@
    ///    @@#(((((((((((((((((((((((((((((%@@@@@@@#(#@@
    ///   .@@((((((((((((((((#%@@@@@@@@@&%#((((%@&((((@@.
    ///   .@@(((((((/(&@@@@@@%(/((((((((((((((@@/(((((@@.
    ///   .@@(///////////////////////////////@(///////@@
    ///    %@#/////////////////////////////(#////////%@%
    ///     @@(///////////////////////////%/////////(@@
    ///      @@#***********************************%@@
    ///       *@@********************************/@@/
    ///         ,@@#***************************%@@*
    ///            @@@&********************/@@@@
    ///                &@@@@&(//***//(&@@@@&
    ///                   **@@@@@@@@@@@*
    /// ```
    pub fn krass_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KRASSOS,
            names: vec!["KrassOS", "Krass"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of KSLinux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ks_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// K   K U   U RRRR   ooo
    /// K  K  U   U R   R o   o
    /// KKK   U   U RRRR  o   o
    /// K  K  U   U R  R  o   o
    /// K   K  UUU  R   R  ooo
    ///
    ///  SSS   AAA  W   W  AAA
    /// S     A   A W   W A   A
    ///  SSS  AAAAA W W W AAAAA
    ///     S A   A WW WW A   A
    ///  SSS  A   A W   W A   A
    /// ```
    pub fn ks_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KSLINUX,
            names: vec!["KSLinux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Kubuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::kubuntu().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            `.:/ossyyyysso/:.
    ///         .:oyyyyyyyyyyyyyyyyyyo:`
    ///       -oyyyyyyyodMMyyyyyyyysyyyyo-
    ///     -syyyyyyyyyydMMyoyyyydmMMyyyyys-
    ///    oyyysdMysyyyydMMMMMMMMMMMMMyyyyyyyo
    ///  `oyyyydMMMMysyysoooooodMMMMyyyyyyyyyo`
    ///  oyyyyyydMMMMyyyyyyyyyyyysdMMysssssyyyo
    /// -yyyyyyyydMysyyyyyyyyyyyyyysdMMMMMysyyy-
    /// oyyyysoodMyyyyyyyyyyyyyyyyyyydMMMMysyyyo
    /// yyysdMMMMMyyyyyyyyyyyyyyyyyyysosyyyyyyyy
    /// yyysdMMMMMyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// oyyyyysosdyyyyyyyyyyyyyyyyyyydMMMMysyyyo
    /// -yyyyyyyydMysyyyyyyyyyyyyyysdMMMMMysyyy-
    ///  oyyyyyydMMMysyyyyyyyyyyysdMMyoyyyoyyyo
    ///  `oyyyydMMMysyyyoooooodMMMMyoyyyyyyyyo
    ///    oyyysyyoyyyysdMMMMMMMMMMMyyyyyyyyo
    ///     -syyyyyyyyydMMMysyyydMMMysyyyys-
    ///       -oyyyyyyydMMyyyyyyysosyyyyo-
    ///         ./oyyyyyyyyyyyyyyyyyyo/.
    ///            `.:/oosyyyysso/:.`
    /// ```
    pub fn kubuntu() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_KUBUNTU,
            names: vec![
                "Kubuntu",
                "kubuntu-linux",
                "kde-ubuntu",
                "ubuntu kde",
                "ubuntu-kde",
                "ubuntu-plasma",
            ],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Langitketujuh distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::langitketujuh().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// . '7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7
    /// L7.   '7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7
    /// L7L7L      7L7L7L7L7L7L7L7L7L7L7L7L7L7
    /// L7L7L7                          L7L7L7
    /// L7L7L7           'L7L7L7L7L7L7L7L7L7L7
    /// L7L7L7               'L7L7L7L7L7L7L7L7
    /// L7L7L7                   'L7L7L7L7L7L7
    /// L7L7L7                          L7L7L7
    /// L7L7L7L7L7L7L7L7L7L7LL7L7L7.    '7L7L7
    /// L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L.   'L7
    /// L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7.  '
    /// ```
    pub fn langitketujuh() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LANGITKETUJUH,
            names: vec!["langitketujuh", "l7"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Laxeros distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::laxeros().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     /.
    ///                  `://:-
    ///                 `//////:
    ///                .////////:`
    ///               -//////////:`
    ///              -/////////////`
    ///             :///////////////.
    ///           `://////.```-//////-
    ///          `://///:`     .//////-
    ///         `//////:        `//////:
    ///        .//////-          `://///:`
    ///       -//////-            `://///:`
    ///      -//////.               ://////`
    ///     ://////`                 -//////.
    ///    `/////:`                   ./////:
    ///     .-::-`                     .:::-`
    ///
    /// .:://////////////////////////////////::.
    /// ////////////////////////////////////////
    /// .:////////////////////////////////////:.
    /// ```
    pub fn laxeros() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LAXEROS,
            names: vec!["Laxeros"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of LEDE distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::lede().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     _________
    ///    /        /\
    ///   /  LE    /  \
    ///  /    DE  /    \
    /// /________/  LE  \
    /// \        \   DE /
    ///  \    LE  \    /
    ///   \  DE    \  /
    ///    \________\/
    /// ```
    pub fn lede() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LEDE,
            names: vec!["LEDE"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of libreELEC distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::libre_elec().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           :+ooo/.      ./ooo+:
    ///         :+ooooooo/.  ./ooooooo+:
    ///       :+ooooooooooo::ooooooooooo+:
    ///     :+ooooooooooo+-  -+ooooooooooo+:
    ///   :+ooooooooooo+-  --  -+ooooooooooo+:
    /// .+ooooooooooo+-  :+oo+:  -+ooooooooooo+-
    /// -+ooooooooo+-  :+oooooo+:  -+oooooooooo-
    ///   :+ooooo+-  :+oooooooooo+:  -+oooooo:
    ///     :+o+-  :+oooooooooooooo+:  -+oo:
    ///      ./   :oooooooooooooooooo:   /.
    ///    ./oo+:  -+oooooooooooooo+-  :+oo/.
    ///  ./oooooo+:  -+oooooooooo+-  :+oooooo/.
    /// -oooooooooo+:  -+oooooo+-  :+oooooooooo-
    /// .+ooooooooooo+:  -+oo+-  :+ooooooooooo+.
    ///   -+ooooooooooo+:  ..  :+ooooooooooo+-
    ///     -+ooooooooooo+:  :+ooooooooooo+-
    ///       -+oooooooooo+::+oooooooooo+-
    ///         -+oooooo+:    :+oooooo+-
    ///           -+oo+:        :+oo+-
    ///             ..            ..
    /// ```
    pub fn libre_elec() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LIBREELEC,
            names: vec!["LibreELEC"],
            colors: vec![
                Color::Green,
                Color::Yellow,
                Color::White,
                Color::Cyan,
                Color::Magenta,
            ],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Linspire distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::linspire().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                                    __^
    ///                                                 __/    \\
    ///    MMy        dMy                            __/        \\
    ///   dMMy        MMy                            MM          \\
    ///   MMMy            ,,        dMMMMn                        \\
    ///  dMMy        dMM dMMMMMMy  dMM  MM dMMMMMy  dMM MM.nMMM dMMMMMM
    /// MMM          MMy MMy  MMy dMM      MMy  MMy MMy MMy    dy   dMy
    /// MMM         dMM dMM   MMy  dMMMMy dMM  dMM dMM dMM    dMMMMMMM
    ///  dMMy       MMy MMy  MMy     dMMy MM  MMy MMy  MMy    dMM
    /// dMMy       dMM dMM  dMM dMM  MMy dMMMMMy dMM  dMM     MMy   MM
    /// MMMMMMMMMM MMy MMy  MMy dMMMyyy MMy     MMy  MMy      dMMMMMMy
    ///                                 dy
    /// ```
    pub fn linspire() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LINSPIRE,
            names: vec!["Linspire", "Lindows"],
            colors: vec![Color::Blue, Color::Green],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Linux
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         #####
    ///        #######
    ///        ##O#O##
    ///        #######
    ///      ###########
    ///     #############
    ///    ###############
    ///    ################
    ///   #################
    /// #####################
    /// #####################
    ///   #################
    /// ```
    pub fn linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LINUX,
            names: vec!["Linux", "linux-generic"],
            colors: vec![Color::White, Color::Black, Color::Yellow],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Linux
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::linux_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     ___
    ///    (.. \
    ///    (<> |
    ///   //  \ \
    ///  ( |  | /|
    /// _/\ __)/_)
    /// \/-____\/
    /// ```
    pub fn linux_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LINUX_SMALL,
            names: vec!["Linux_small", "linux-generic_small"],
            colors: vec![Color::Black, Color::White, Color::Yellow],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Linux Lite distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::linux_lite().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           ,xXc
    ///       .l0MMMMMO
    ///    .kNMMMMMWMMMN,
    ///    KMMMMMMKMMMMMMo
    ///   'MMMMMMNKMMMMMM:
    ///   kMMMMMMOMMMMMMO
    ///  .MMMMMMX0MMMMMW.
    ///  oMMMMMMxWMMMMM:
    ///  WMMMMMNkMMMMMO
    /// :MMMMMMOXMMMMW
    /// .0MMMMMxMMMMM;
    /// :;cKMMWxMMMMO
    /// 'MMWMMXOMMMMl
    ///  kMMMMKOMMMMMX:
    ///  .WMMMMKOWMMM0c
    ///   lMMMMMWO0MNd:'
    ///    oollXMKXoxl;.
    ///      ':. .: .'
    ///               ..
    ///                 .
    /// ```
    pub fn linux_lite() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LINUXLITE,
            names: vec!["LinuxLite", "Linux Lite", "linux_lite"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Linux Lite distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::linux_lite_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    /\\
    ///   /  \\
    ///  / / /
    /// > / /
    /// \\ \\ \\
    ///  \\_\\_\\
    ///     \\
    /// ```
    pub fn linux_lite_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LINUXLITE_SMALL,
            names: vec!["LinuxLite_small", "Linux Lite_small"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Live Raizo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::live_raizo().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `......`
    ///         -+shmNMMMMMMNmhs/.
    ///      :smMMMMMmmhyyhmmMMMMMmo-
    ///    -hMMMMd+:. `----` .:odMMMMh-
    ///  `hMMMN+. .odNMMMMMMNdo. .yMMMMs`
    ///  hMMMd. -dMMMMmdhhdNMMMNh` .mMMMh
    /// oMMMm` :MMMNs.:sddy:-sMMMN- `NMMM+
    /// mMMMs  dMMMo sMMMMMMd yMMMd  sMMMm
    /// ----`  .---` oNMMMMMh `---.  .----
    ///               .sMMy:
    ///                /MM/
    ///               +dMMms.
    ///              hMMMMMMN
    ///             `dMMMMMMm:
    ///       .+ss+sMNysMMoomMd+ss+.
    ///      +MMMMMMN` +MM/  hMMMMMNs
    ///      sMMMMMMm-hNMMMd-hMMMMMMd
    ///       :yddh+`hMMMMMMN :yddy/`
    ///              .hMMMMd:
    ///                `..`
    /// ```
    pub fn live_raizo() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LIVE_RAIZO,
            names: vec!["Live Raizo", "Live_Raizo"],
            colors: vec![Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of LMDE distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::lmde().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           `.-::---..
    ///       .:++++ooooosssoo:.
    ///     .+o++::.      `.:oos+.
    ///    :oo:.`             -+oo:
    ///  `+o/`    .::::::-.    .++-`
    /// `/s/    .yyyyyyyyyyo:   +o-`
    /// `so     .ss       ohyo` :s-:
    /// `s/     .ss  h  m  myy/ /s``
    /// `s:     `oo  s  m  Myy+-o:`
    /// `oo      :+sdoohyoydyso/.
    ///  :o.      .:////////++:
    ///  `/++        -:::::-
    ///   `++-
    ///    `/+-
    ///      .+/.
    ///        .:+-.
    ///           `--.``
    /// ```
    pub fn lmde() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LMDE,
            names: vec!["LMDE"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of LainOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::lain_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     /==\
    ///                     \==/
    ///                · · · · · · ·
    ///             · · · · · · · · · ·
    ///            · · · .-======-.· · · ·
    ///         .::. ·.-============-.· .::.
    ///      .:==:· .:===:'. ·· .':===:. ·:==:.
    ///   .:===: · :===. ·  .--.  · .===: · :===:.
    ///  :===:· · :===. · .:====:. · .===: · ·:===:
    /// (===:· · :===- ·  :======:  · -===: · ·:===)
    ///  :===:· · :===. · ':====:' · .===: · ·:===:
    ///   ':===: · :===. ·  '--'  · .===: · :===:'
    ///      ':==:· ':===:.' ·· '.:===:' ·:==:'
    ///         '::' · '===-.  .-===' · '::'
    ///   /==\     · · · :===  ===: · · ·     /==\
    ///   \==/      · · ·:=== ·===:· · ·      \==/
    ///          .-.   · :===· ===: ·   .-.
    ///          .===.   .===  ===.   .===.
    ///            .========    ========.
    ///              '''''        '''''
    /// ```
    pub fn lain_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LAINOS,
            names: vec!["LainOS"],
            colors: vec![
                Color::Blue,
                Color::TrueColor {
                    r: 0,
                    g: 0xff,
                    b: 0xff,
                },
                Color::White,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Lunar distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::lunar().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// `-.                                 `-.
    ///   -ohys/-`                    `:+shy/`
    ///      -omNNdyo/`          :+shmNNy/`
    ///                    -
    ///                  /mMmo
    ///                  hMMMN`
    ///                  .NMMs
    ///       -:+oooo+//: /MN. -///oooo+/-`
    ///      /:.`          /           `.:/`
    ///           __
    ///          |  |   _ _ ___ ___ ___
    ///          |  |__| | |   | .'|  _|
    ///          |_____|___|_|_|__,|_|
    /// ```
    pub fn lunar() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_LUNAR,
            names: vec!["Lunar"],
            colors: vec![Color::Blue, Color::White, Color::Yellow],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of MacOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mac_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      ..'
    ///                  ,xNMM.
    ///                .OMMMMo
    ///                lMM"
    ///      .;loddo:.  .olloddol;.
    ///    cKMMMMMMMMMMNWMMMMMMMMMM0:
    ///  .KMMMMMMMMMMMMMMMMMMMMMMMWd.
    ///  XMMMMMMMMMMMMMMMMMMMMMMMX.
    /// ;MMMMMMMMMMMMMMMMMMMMMMMM:
    /// :MMMMMMMMMMMMMMMMMMMMMMMM:
    /// .MMMMMMMMMMMMMMMMMMMMMMMMX.
    ///  kMMMMMMMMMMMMMMMMMMMMMMMMWd.
    ///  'XMMMMMMMMMMMMMMMMMMMMMMMMMMk
    ///   'XMMMMMMMMMMMMMMMMMMMMMMMMK.
    ///     kMMMMMMMMMMMMMMMMMMMMMMd
    ///      ;KMMMMMMMWXXWMMMMMMMk.
    ///        "cooc*"    "*coo'"
    /// ```
    pub fn mac_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MACOS,
            names: vec!["Apple", "macos", "mac", "OSX"],
            colors: vec![
                Color::Green,
                Color::Yellow,
                Color::Red,
                Color::Magenta,
                Color::Blue,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of MacOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mac_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         .:'
    ///     __ :'__
    ///  .'`__`-'__``.
    /// :__________.-'
    /// :_________:
    ///  :_________`-;
    ///   `.__.-.__.'
    /// ```
    pub fn mac_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MACOS_SMALL,
            names: vec!["Apple_small", "macos_small", "mac_small", "OSX_small"],
            colors: vec![
                Color::Green,
                Color::Yellow,
                Color::Red,
                Color::Magenta,
                Color::Blue,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of MacOS2 distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mac_os2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      ..'
    ///                  ,xN  .
    ///                .O    o
    ///                l  "
    ///      .;loddo:.  .olloddol;.
    ///    cK          NW          0:
    ///  .K                       Wd.
    ///  X                       X.
    /// ;                        :
    /// :                        :
    /// .                        X.
    ///  k                        Wd.
    ///  'X                          k
    ///   'X                        K.
    ///     k                      d
    ///      ;K       WXXW       k.
    ///        "cooc*"    "*coo'"
    /// ```
    pub fn mac_os2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MACOS2,
            names: vec!["macos2", "mac2"],
            colors: vec![
                Color::Green,
                Color::Yellow,
                Color::Red,
                Color::Magenta,
                Color::Blue,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Alter,
        }
    }
    /// returns small logo of MacOS2 distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mac_os2_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         .:'
    ///     __ :'__
    ///  .'`  `-'  ``.
    /// :          .-'
    /// :         :
    ///  :         `-;
    ///   `.__.-.__.'
    /// ```
    pub fn mac_os2_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MACOS2_SMALL,
            names: vec!["Apple2_small", "macos2_small", "mac2_small"],
            colors: vec![
                Color::Green,
                Color::Yellow,
                Color::Red,
                Color::Magenta,
                Color::Blue,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small | LogoType::Alter,
        }
    }
    /// returns logo of MainsailOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mainsail_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                            -
    ///                           *%:
    ///                         :%%%#
    ///                        =%%%%%-
    ///                       *%%%%%%#
    ///                     :#%%%%%%%#.
    ///                    -%%%%%%%%+
    ///                   *%%%%%%%%-    :
    ///                 .#%%%%%%%#.    *%=
    ///                -%%%%%%%%+    :#%%%*
    ///               +%%%%%%%%-    =%%%%%%#.
    ///             .#%%%%%%%#.    *%%%%%%%%:
    ///            -%%%%%%%%*    :#%%%%%%%#.
    ///           +%%%%%%%%-    =%%%%%%%%+    :%*.
    ///         .#%%%%%%%#:    *%%%%%%%%-    +%%%%*:
    ///        :%%%%%%%%*    :#%%%%%%%#.   .*%%%%%%%*
    ///       +%%%%%%%%=    -%%%%%%%%+    :%%%%%%%%*
    ///     .#%%%%%%%%:    *%%%%%%%%-    =%%%%%%%%=
    /// ```
    pub fn mainsail_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MAINSAILOS,
            names: vec!["MainsailOS"],
            colors: vec![Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of MainsailOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mainsail_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           -:
    ///          +%*
    ///        .#%%+
    ///       -%%%: +=
    ///      +%%#..#%%-
    ///    .#%%+ -%%%- +=
    ///   -%%%- +%%#..#%%+
    /// ```
    pub fn mainsail_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MAINSAILOS_SMALL,
            names: vec!["MainsailOS_small"],
            colors: vec![Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Mageia distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mageia().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         .°°.
    ///          °°   .°°.
    ///          .°°°. °°
    ///          .   .
    ///           °°° .°°°.
    ///       .°°°.   '___'
    ///      .'___'        .
    ///    :dkxc;'.  ..,cxkd;
    ///  .dkk. kkkkkkkkkk .kkd.
    /// .dkk.  ';cloolc;.  .kkd
    /// ckk.                .kk;
    /// xO:                  cOd
    /// xO:                  lOd
    /// lOO.                .OO:
    /// .k00.              .00x
    ///  .k00;            ;00O.
    ///   .lO0Kc;,,,,,,;c0KOc.
    ///      ;d00KKKKKK00d;
    ///         .,KKKK,.
    /// ```
    pub fn mageia() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MAGEIA,
            names: vec!["Mageia"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Mageia distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mageia_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    *
    ///     *
    ///    **
    ///  /\\__/\\
    /// /      \\
    /// \\      /
    ///  \\____/
    /// ```
    pub fn mageia_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MAGEIA_SMALL,
            names: vec!["Mageia_small"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of MagpieOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::magpie_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         ;00000     :000Ol
    ///      .x00kk00:    O0kk00k;
    ///     l00:   :00.  o0k   :O0k.
    ///   .k0k.     xdddddk'    .d00;
    ///   k0k.      .dddddl       o00,
    ///  o00.        ':cc:.        d0O
    /// .00l                       ,00.
    /// l00.                       d0x
    /// k0O                     .:k0o
    /// O0k                 ;dO0000d.
    /// k0O               .O0Oxxxxk00:
    /// o00.              k0Oddddddocc
    /// '00l              x0Odddddo;..
    ///  x00.             .x00kxxd:..
    ///  .O0x               .:oxxxOkl.
    ///   .x0d                     ,xx,
    ///     .:o.          .xd       ckd
    ///        ..          dxl     .xx;
    ///                     :xxolldxd'
    ///                       ;oxdl.
    /// ```
    pub fn magpie_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MAGPIEOS,
            names: vec!["MagpieOS", "Magpie"],
            colors: vec![Color::Green, Color::Red, Color::Yellow, Color::Magenta],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Mandriva distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mandriva().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                         ``
    ///                        `-.
    ///       `               .---
    ///     -/               -::--`
    ///   `++    `----...```-:::::.
    ///  `os.      .::::::::::::::-```     `  `
    ///  +s+         .::::::::::::::::---...--`
    /// -ss:          `-::::::::::::::::-.``.``
    /// /ss-           .::::::::::::-.``   `
    /// +ss:          .::::::::::::-
    /// /sso         .::::::-::::::-
    /// .sss/       -:::-.`   .:::::
    ///  /sss+.    ..`  `--`    .:::
    ///   -ossso+/:://+/-`        .:`
    ///     -/+ooo+/-.              `
    /// ```
    pub fn mandriva() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MANDRIVA,
            names: vec!["mandriva", "mandrake"],
            colors: vec![Color::Blue, Color::Yellow],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Manjaro distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::manjaro().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ██████████████████  ████████
    /// ██████████████████  ████████
    /// ██████████████████  ████████
    /// ██████████████████  ████████
    /// ████████            ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ```
    pub fn manjaro() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MANJARO,
            names: vec!["manjaro", "manjaro-linux"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Manjaro distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::manjaro_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ||||||||| ||||
    /// ||||||||| ||||
    /// ||||      ||||
    /// |||| |||| ||||
    /// |||| |||| ||||
    /// |||| |||| ||||
    /// |||| |||| ||||
    /// ```
    pub fn manjaro_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MANJARO_SMALL,
            names: vec!["manjaro_small", "manjaro-linux-small"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of MassOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mass_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// -+++/+++osyyhdmNNMMMMNdy/
    /// /MMMMMMMMMMMMMMMMMMMMMMMMm.
    /// /MMMMMMMMMMMMMMMMMMMMMMMMMm
    /// /MMMMMMMMMMMMMMMMMMMMMMMMMM:
    /// :ddddddddhddddddddhdddddddd/
    /// /NNNNNNNm:NNNNNNNN-NNNNNNNNo
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// :MMMMMMMN:MMMMMMMM-MMMMMMMMs
    ///  dMMMMMMN:MMMMMMMM-MMMMMMMMs
    ///  `yNMMMMN:MMMMMMMM-MMMMMMMMs
    ///    `:+oss.ssssssss.ssssssss/
    /// ```
    pub fn mass_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MASSOS,
            names: vec!["MassOS", "mass"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of MatuusOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::matuus_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░▒▓▓████▓▒▒░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░▒▓████████████▓░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░▓████████████████▒░░░░░░░░░░░░
    /// ░░░░░░░░░░░▓██████████████████▒░░░░░░░░░░░
    /// ░░░░░░░░░░▓████▒▓███████▓▓█████░░░░░░░░░░░
    /// ░░░░░░░░░░██████▓░▓████▒░██████▓░░░░░░░░░░
    /// ░░░░░░░░░▒███████▓░▒▓▒░░████████░░░░░░░░░░
    /// ░░░░░░░░░▒█████████▒░░░█████████░░░░░░░░░░
    /// ░░░░░░░░░░██████████▓▒██████████░░░░░░░░░░
    /// ░░░░░░░░░░▓████████████████████▒░░░░░░░░░░
    /// ░░░░░░░░░░░███████████████████▓░░░░░░░░░░░
    /// ░░░░░░░░░░░░█████████████████▓░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░▓██████████████▒░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░▒▓████████▓░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ```
    pub fn matuus_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MATUUSOS,
            names: vec!["MatuusOS", "Matuus"],
            colors: vec![Color::Yellow, Color::Red],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of MaUI distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ma_ui().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `.-://////:--`
    ///          .:/oooooooooooooooo+:.
    ///       `:+ooooooooooooooooooooooo:`
    ///     `:oooooooooooooooooooooooooooo/`
    ///     ..```-oooooo/-`` `:oooooo+:.` `--
    ///   :.      +oo+-`       /ooo/`       -/
    ///  -o.     `o+-          +o/`         -o:
    /// `oo`     ::`  :o/     `+.  .+o`     /oo.
    /// /o+      .  -+oo-     `   /oo/     `ooo/
    /// +o-        /ooo+`       .+ooo.     :ooo+
    /// ++       .+oooo:       -oooo+     `oooo+
    /// :.      .oooooo`      :ooooo-     :oooo:
    /// `      .oooooo:      :ooooo+     `ooo+-`
    ///       .+oooooo`     -oooooo:     `o/-
    ///       +oooooo:     .ooooooo.
    ///      /ooooooo`     /ooooooo/       ..
    ///     `:oooooooo/:::/ooooooooo+:--:/:`
    ///       `:+oooooooooooooooooooooo+:`
    ///          .:+oooooooooooooooo+:.
    ///              `.-://////:-.`
    /// ```
    pub fn ma_ui() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MAUI,
            names: vec!["MaUI"],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Meowix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::meowix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          #%            &*
    ///         ##%%          &&**
    ///        ##  %%        &&  **
    ///       ##    %%      &&    **
    ///      ##      %%    &&      **
    ///     ##        %%  &&        **
    ///    ##          %%&&          **
    ///   ##            %%            **
    ///  ##                            **
    /// ##                              **
    /// ```
    pub fn meowix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MEOWIX,
            names: vec!["Meowix"],
            colors: vec![Color::Red, Color::Yellow, Color::Yellow, Color::Blue],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Mer distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mer().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          dMs
    ///                          .-`
    ///                        `y`-o+`
    ///                         ``NMMy
    ///                       .--`:++.
    ///                     .hNNNNs
    ///                     /MMMMMN
    ///                     `ommmd/ +/
    ///                       ````  +/
    ///                      `:+sssso/-`
    ///   .-::. `-::-`     `smNMNmdmNMNd/      .://-`
    /// .ymNMNNdmNMMNm+`  -dMMh:.....+dMMs   `sNNMMNo
    /// dMN+::NMMy::hMM+  mMMo `ohhy/ `dMM+  yMMy::-
    /// MMm   yMM-  :MMs  NMN` `:::::--sMMh  dMM`
    /// MMm   yMM-  -MMs  mMM+ `ymmdsymMMMs  dMM`
    /// NNd   sNN-  -NNs  -mMNs-.--..:dMMh`  dNN
    /// ---   .--`  `--.   .smMMmdddmMNdo`   .--
    ///                      ./ohddds+:`
    ///                      +h- `.:-.
    ///                      ./`.dMMMN+
    ///                         +MMMMMd
    ///                         `+dmmy-
    ///                       ``` .+`
    ///                      .dMNo-y.
    ///                      `hmm/
    ///                          .:`
    ///                          dMs
    /// ```
    pub fn mer() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MER,
            names: vec!["Mer"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Mint distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mint().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ...-:::::-...
    ///           .-MMMMMMMMMMMMMMM-.
    ///       .-MMMM`..-:::::::-..`MMMM-.
    ///     .:MMMM.:MMMMMMMMMMMMMMM:.MMMM:.
    ///    -MMM-M---MMMMMMMMMMMMMMMMMMM.MMM-
    ///  `:MMM:MM`  :MMMM:....::-...-MMMM:MMM:`
    ///  :MMM:MMM`  :MM:`  ``    ``  `:MMM:MMM:
    /// .MMM.MMMM`  :MM.  -MM.  .MM-  `MMMM.MMM.
    /// :MMM:MMMM`  :MM.  -MM-  .MM:  `MMMM-MMM:
    /// :MMM:MMMM`  :MM.  -MM-  .MM:  `MMMM:MMM:
    /// :MMM:MMMM`  :MM.  -MM-  .MM:  `MMMM-MMM:
    /// .MMM.MMMM`  :MM:--:MM:--:MM:  `MMMM.MMM.
    ///  :MMM:MMM-  `-MMMMMMMMMMMM-`  -MMM-MMM:
    ///   :MMM:MMM:`                `:MMM:MMM:
    ///    .MMM.MMMM:--------------:MMMM.MMM.
    ///      '-MMMM.-MMMMMMMMMMMMMMM-.MMMM-'
    ///        '.-MMMM``--:::::--``MMMM-.'
    ///             '-MMMMMMMMMMMMM-'
    ///                ``-:::::-``
    /// ```
    pub fn mint() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MINT,
            names: vec!["linuxmint", "linux-mint", "mint", "mint-linux"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Mint distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mint_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  __________
    /// |_          \
    ///   | | _____ |
    ///   | | | | | |
    ///   | | | | | |
    ///   | \_____/ |
    ///   \_________/
    /// ```
    pub fn mint_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MINT_SMALL,
            names: vec![
                "linuxmint_small",
                "linux-mint_small",
                "mint_small",
                "mint-linux-small",
            ],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns old logo of Mint distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mint_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// MMMMMMMMMMMMMMMMMMMMMMMMMmds+.
    /// MMm----::-://////////////oymNMd+`
    /// MMd      /++                -sNMd:
    /// MMNso/`  dMM    `.::-. .-::.` .hMN:
    /// ddddMMh  dMM   :hNMNMNhNMNMNh: `NMm
    ///     NMm  dMM  .NMN/-+MMM+-/NMN` dMM
    ///     NMm  dMM  -MMm  `MMM   dMM. dMM
    ///     NMm  dMM  -MMm  `MMM   dMM. dMM
    ///     NMm  dMM  .mmd  `mmm   yMM. dMM
    ///     NMm  dMM`  ..`   ...   ydm. dMM
    ///     hMM- +MMd/-------...-:sdds  dMM
    ///     -NMm- :hNMNNNmdddddddddy/`  dMM
    ///     -dMNs-``-::::-------.``    dMM
    ///     `/dMNmy+/:-------------:/yMMM
    ///       ./ydNMMMMMMMMMMMMMMMMMMMMM
    ///           .MMMMMMMMMMMMMMMMMMM
    /// ```
    pub fn mint_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MINT_OLD,
            names: vec![
                "linux-mint_old",
                "linux-mint-old",
                "mint_old",
                "mint-old",
                "mint-linux_old",
                "mint-linux-old",
            ],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Minix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::minix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    -sdhyo+:-`                -/syymm:
    ///    sdyooymmNNy.     ``    .smNmmdysNd
    ///    odyoso+syNNmysoyhhdhsoomNmm+/osdm/
    ///     :hhy+-/syNNmddhddddddmNMNo:sdNd:
    ///      `smNNdNmmNmddddddddddmmmmmmmy`
    ///    `ohhhhdddddmmNNdmddNmNNmdddddmdh-
    ///    odNNNmdyo/:/-/hNddNy-`..-+ydNNNmd:
    ///  `+mNho:`   smmd/ sNNh :dmms`   -+ymmo.
    /// -od/       -mmmmo -NN+ +mmmm-       yms:
    /// +sms -.`    :so:  .NN+  :os/     .-`mNh:
    /// .-hyh+:////-     -sNNd:`    .--://ohNs-
    ///  `:hNNNNNNNMMd/sNMmhsdMMh/ymmNNNmmNNy/
    ///   -+sNNNNMMNNNsmNMo: :NNmymNNNNMMMms:
    ///     //oydNMMMMydMMNysNMMmsMMMMMNyo/`
    ///        ../-yNMMy--/::/-.sMMmos+.`
    ///            -+oyhNsooo+omy/```
    ///               `::ohdmds-`
    /// ```
    pub fn minix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MINIX,
            names: vec!["Minix"],
            colors: vec![Color::Red, Color::White, Color::Yellow],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Miracle Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::miracle_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ,A
    ///           .###
    ///      .#' .####   .#.
    ///    r##:  :####   ####.
    ///   +###;  :####  ######C
    ///   :####:  #### ,######".#.
    /// .# :####. :### #####'.#####.
    /// ##: `####. ### ###'.########+.
    /// #### `####::## ##'.#######+'
    ///  ####+.`###i## #',####:'
    ///  `+###MI`##### 'l###:'
    ///    `+####+`### ;#E'
    ///       `+###:## #'
    ///          `:### '
    ///            '##
    ///             ';
    /// ```
    pub fn miracle_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MIRACLE_LINUX,
            names: vec!["MIRACLE LINUX", "miracle_linux"],
            colors: vec![Color::TrueColor {
                r: 0,
                g: 0x87,
                b: 0x5f,
            }],
            color_keys: Some(Color::TrueColor {
                r: 0,
                g: 0x87,
                b: 0x5f,
            }),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of MOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   :--==========================--:
    /// .-=================================.
    /// -==================================-
    /// ====================================
    /// =======-....:==========:....:=======
    /// =======:      -======-.     .=======
    /// =======:       :====-       .=======
    /// =======:        :==:        .=======
    /// =======:         ..         .=======
    /// =======:    .:        .:    .=======
    /// =======:    .=-      :=:    .=======
    /// =======:    .===.  .-==:    .=======
    /// =======:    .==========:    .=======
    /// =======:    :==========:    :=======
    /// ====================================
    /// -===================================
    /// .==================================:
    ///   :--===========================-:.
    /// ```
    pub fn mos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MOS,
            names: vec!["MOS"],
            colors: vec![Color::Cyan, Color::Blue],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Msys2
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::msys2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ...
    ///               5GB###GJ.             !YPGGGG
    ///               7@@@@@@@B.          :G@@@@@@@
    ///               7@@@@@@@@Y         ~&@@@@@@@@YJYY5YY?L
    ///              !@@@@@@@@@@^       ^&@@@@@@@#PP555555PBY
    ///             ~&@@@@@@@@@@?      ^&@@@@@@#5YY5YYYYYYYY#7
    ///            ^&@@@@@@@@@@@B     :#@@@@@@@G5BBYGPYYYYYY#J
    ///           ^#@@@&J#@@@@@@@~   .B@@@@@@@@@@@P ?#YYYYYPB.
    ///          :#@@@@7 G@@@@@@@J   P@@@#!&@@@@@@G.GGYYYYGB^
    ///         :#@@@@J  Y@@@@@@@B  5@@@&:.&@@@@@@&BBYYY5B5.
    ///        :#@@@@Y   !@@@@@@@@!Y@@@&~ .#@@@@@@GYYYYYBP  JP~
    ///       :#@@@@P    :&@@@@@@@@@@@&~   B@@@@@#5YYYYYPGPGPGG
    ///      ^#@@@@G.     P@@@@@@@@@@@!    P@@@@GYYYYYYYYYYYYBY
    ///     ^#@@@@B:      ^@@@@@@@@@@7     !@@@#GGGGGGGPPPP5GB:
    ///    !&@@@@B:        Y@@@@@@@@?       P@@@@@@@@@&?  ^PY:
    ///   7&@@@@5.          P@@@@@@?         P@@@@@@@@@B
    ///  Y@@@&P!             5@@@@7           7G@@@@@&P~
    /// .JJ?~:                ^JY~              ^!5J!^:
    ///                              :@P5#B. #G  7&^ :@P5#B.
    ///                              !&P^.   ?@~ #P  !&P^.  
    ///                               .?BG!   #G5@~   .?BG!
    ///                                :.B@.  7@@5     :.B@.
    ///                              !PYY5Y   :&@^   !PYY5Y
    ///                                       ~@Y
    ///                                       !5:
    /// ```
    pub fn msys2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MSYS2,
            names: vec!["Msys2"],
            colors: vec![Color::Magenta, Color::White, Color::Red],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of MX distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mx().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNMMMMMMMMM
    /// MMMMMMMMMMNs..yMMMMMMMMMMMMMm: +NMMMMMMM
    /// MMMMMMMMMN+    :mMMMMMMMMMNo` -dMMMMMMMM
    /// MMMMMMMMMMMs.   `oNMMMMMMh- `sNMMMMMMMMM
    /// MMMMMMMMMMMMN/    -hMMMN+  :dMMMMMMMMMMM
    /// MMMMMMMMMMMMMMh-    +ms. .sMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMN+`   `  +NMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMNMMd:    .dMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMm/-hMd-     `sNMMMMMMMMMMMMM
    /// MMMMMMMMMMNo`   -` :h/    -dMMMMMMMMMMMM
    /// MMMMMMMMMd:       /NMMh-   `+NMMMMMMMMMM
    /// MMMMMMMNo`         :mMMN+`   `-hMMMMMMMM
    /// MMMMMMh.            `oNMMd:    `/mMMMMMM
    /// MMMMm/                -hMd-      `sNMMMM
    /// MMNs`                   -          :dMMM
    /// Mm:                                 `oMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// ```
    pub fn mx() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MX,
            names: vec!["MX", "MX Linux"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of MX distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mx_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     \\\\  /
    ///      \\\\/
    ///       \\\\
    ///    /\\/ \\\\
    ///   /  \\  /\\
    ///  /    \\/  \\
    /// /__________\\
    /// ```
    pub fn mx_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MX_SMALL,
            names: vec!["MX_small", "mx linux_small"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of MX2 distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::mx2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /// @@@@@@@@@@@@%*+--:------=+*%@@@@@@@@@@@@
    /// @@@@@@@@@#=. .-+#%@@@@@%#*+--=#@@@@@@@@@
    /// @@@@@@@+. .=%@@@@@@@@@@@@@@@@*-:+@@@@@@@
    /// @@@@@*.  *@@@@@@@@@@@@@@@@@@@@@%-.*@@@@@
    /// @@@@-  -@@@@@@@@@@@@@@@@@@@@@@@#:  -@@@@
    /// @@@:  -@@@@@@@=.*@@@@@@@@@@@@%-   = :@@@
    /// @@=  .@@@@@@@@%- :%@@@@@@@@@+   -%@# =@@
    /// @%   +@@@@@@@@@@#. =@@@@@@*.  .*@@@@. %@
    /// @+   *@@@@@@*..*@@+  *@@%-   =@@@@@@- +@
    /// @=   *@@@@%-    -%@@- :=   -%@@@@@@@: +@
    /// @+   :@@@=        +@@=   .#@@@@@@@@%  *@
    /// @%    +*.          .:     *@@#: +@@:  @@
    /// @@+                   :%@- :-    ::  +@@
    /// @@@-                  .=@@=         -@@@
    /// @@+.                     .           +@@
    /// %=..:.................::...........:..-%
    /// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /// ```
    pub fn mx2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_MX2,
            names: vec!["MX2"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of Namib distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::namib().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           .:+shysyhhhhysyhs+:.
    ///        -/yyys              syyy/-
    ///      -shy                      yhs-
    ///    -yhs                          shy-
    ///   +hy                              yh+
    ///  +ds                                sd+
    /// /ys                  so              sy/
    /// sh                 smMMNdyo           hs
    /// yo               ymMMMMNNMMNho        oy
    /// N             ydMMMNNMMMMMMMMMmy       N
    /// N         shmMMMMNNMMMMMMMMMMMMMNy     N
    /// yo  ooshmNMMMNNNNMMMMMMMMMMMMMMMMMms  oy
    /// sd yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy ds
    /// /ys                                  sy/
    ///  +ds                                sd+
    ///   +hy                              yh+
    ///    -yhs                          shy-
    ///      -shy                      yhs-
    ///        -/yyys              syyy/-
    ///           .:+shysyhyhhysyhs+:.
    /// ```
    pub fn namib() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NAMIB,
            names: vec!["Namib"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Nekos distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nekos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                         @@@@@
    ///                      @@@@@@@@@.
    ///                   @@@@@@@@    @@@
    ///                @@@@@@@@@@@     @@.
    ///               @@@@@@@@@@@@@      .
    ///              @@@@@@@@@@@@@@@@@   ,
    ///            @@@@@@@@@@@@@@@@@@@
    ///           @@@@@///@@@@@@@///@@@
    ///           @@@@/***@@@@@@@**//@@@@
    ///        @@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///           @@@@@@@@@@@@@@@@@@@@@@@
    ///          @@@/   /@@@@@@@@@/   /@@@
    ///       @@@@@@     @@@██@@@@     @@@@@@
    ///       @@@@@@/   /@██████@@/   /@@@@@@
    ///        @@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///                  ##########%%%%
    ///                  ##########%%  %%
    ///          @     @@@#######@@%%%
    ///       @@@      @@@@####@@@@   %
    ///    @@@        @@@@@@@#@@@@@@@
    ///    @@@        @@@@@@@@@@@@@@@
    ///    @@@@      @@@@@@@@@@@@@@@@@
    ///       @@@@@@@@@@@@@@@@@@@@@@@@
    /// ```
    pub fn nekos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NEKOS,
            names: vec!["Nekos"],
            colors: vec![Color::Yellow, Color::White, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Neptune distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::neptune().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ./+sydddddddys/-.
    ///         .+ymNNdyooo/:+oooymNNmy/`
    ///      `/hNNh/.`             `-+dNNy:`
    ///     /mMd/.          .++.:oy/   .+mMd-
    ///   `sMN/             oMMmdy+.     `oNNo
    ///  `hMd.           `/ymy/.           :NMo
    ///  oMN-          `/dMd:               /MM-
    /// `mMy          -dMN+`                 mMs
    /// .MMo         -NMM/                   yMs
    ///  dMh         mMMMo:`                `NMo
    ///  /MM/        /ymMMMm-               sMN.
    ///   +Mm:         .hMMd`              oMN/
    ///    +mNs.      `yNd/`             -dMm-
    ///     .yMNs:    `/.`            `/yNNo`
    ///       .odNNy+-`           .:ohNNd/.
    ///          -+ymNNmdyyyyyyydmNNmy+.
    ///              `-//sssssss//.
    /// ```
    pub fn neptune() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NEPTUNE,
            names: vec!["Neptune"],
            colors: vec![Color::White, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of NetRunner distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::net_runner().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .:oydmMMMMMMmdyo:`
    ///         -smMMMMMMMMMMMMMMMMMMds-
    ///       +mMMMMMMMMMMMMMMMMMMMMMMMMd+
    ///     /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMm/
    ///   `hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMy`
    ///  .mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd`
    ///  dMMMMMMMMMMMMMMMMMMMMMMNdhmMMMMMMMMMMh
    /// +MMMMMMMMMMMMMNmhyo+/-.   -MMMMMMMMMMMM/
    /// mMMMMMMMMd+:.`           `mMMMMMMMMMMMMd
    /// MMMMMMMMMMMdy/.          yMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMNh+`     +MMMMMMMMMMMMMMM
    /// mMMMMMMMMMMMMMMMMMs    -NMMMMMMMMMMMMMMd
    /// +MMMMMMMMMMMMMMMMMN.  `mMMMMMMMMMMMMMMM/
    ///  dMMMMMMMMMMMMMMMMMy  hMMMMMMMMMMMMMMMh
    ///  `dMMMMMMMMMMMMMMMMM-+MMMMMMMMMMMMMMMd`
    ///   `hMMMMMMMMMMMMMMMMmMMMMMMMMMMMMMMMy
    ///     /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMm:
    ///       +dMMMMMMMMMMMMMMMMMMMMMMMMd/
    ///         -odMMMMMMMMMMMMMMMMMMdo-
    ///            `:+ydmNMMMMNmhy+-`
    /// ```
    pub fn net_runner() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NETRUNNER,
            names: vec!["NetRunner"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Nitrux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nitrux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// `:/.
    /// `/yo
    /// `/yo
    /// `/yo      .+:.
    /// `/yo      .sys+:.`
    /// `/yo       `-/sys+:.`
    /// `/yo           ./sss+:.`
    /// `/yo              .:oss+:-`
    /// `/yo                 ./o///:-`
    /// `/yo              `.-:///////:`
    /// `/yo           `.://///++//-``
    /// `/yo       `.-:////++++/-`
    /// `/yo    `-://///++o+/-`
    /// `/yo `-/+o+++ooo+/-`
    /// `/s+:+oooossso/.`
    /// `//+sssssso:.
    /// `+syyyy+:`
    /// :+s+-
    /// ```
    pub fn nitrux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NITRUX,
            names: vec!["Nitrux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of NixOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nix_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           ▗▄▄▄       ▗▄▄▄▄    ▄▄▄▖
    ///           ▜███▙       ▜███▙  ▟███▛
    ///            ▜███▙       ▜███▙▟███▛
    ///             ▜███▙       ▜██████▛
    ///      ▟█████████████████▙ ▜████▛     ▟▙
    ///     ▟███████████████████▙ ▜███▙    ▟██▙
    ///            ▄▄▄▄▖           ▜███▙  ▟███▛
    ///           ▟███▛             ▜██▛ ▟███▛
    ///          ▟███▛               ▜▛ ▟███▛
    /// ▟███████████▛                  ▟██████████▙
    /// ▜██████████▛                  ▟███████████▛
    ///       ▟███▛ ▟▙               ▟███▛
    ///      ▟███▛ ▟██▙             ▟███▛
    ///     ▟███▛  ▜███▙           ▝▀▀▀▀
    ///     ▜██▛    ▜███▙ ▜██████████████████▛
    ///      ▜▛     ▟████▙ ▜████████████████▛
    ///            ▟██████▙       ▜███▙
    ///           ▟███▛▜███▙       ▜███▙
    ///          ▟███▛  ▜███▙       ▜███▙
    ///          ▝▀▀▀    ▀▀▀▀▘       ▀▀▀▘
    /// ```
    pub fn nix_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NIXOS,
            names: vec!["NixOS", "nix", "nixos-linux", "nix-linux"],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of NixOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nix_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   ▗▄   ▗▄ ▄▖
    ///  ▄▄🬸█▄▄▄🬸█▛ ▃
    ///    ▟▛    ▜▃▟🬕
    /// 🬋🬋🬫█      █🬛🬋🬋
    ///  🬷▛🮃▙    ▟▛
    ///  🮃 ▟█🬴▀▀▀█🬴▀▀
    ///   ▝▀ ▀▘   ▀▘
    /// ```
    pub fn nix_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NIXOS_SMALL,
            names: vec![
                "NixOS_small",
                "nix_small",
                "nixos-linux-small",
                "nix-linux-small",
            ],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Small,
        }
    }
    /// returns alternative logo of NixOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nix_os_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               ____       _______        ____
    ///              /####\      \######\      /####\
    ///              ######\      \######\    /#####/
    ///              \######\      \######\  /#####/
    ///               \######\      \######\/#####/    /\
    ///                \######\      \###########/    /##\
    ///         ________\######\______\#########/    /####\
    ///        /#######################\#######/    /######
    ///       /#########################\######\   /######/
    ///      /###########################\######\ /######/
    ///      ¯¯¯¯¯¯¯¯¯¯¯¯/######/¯¯¯¯¯¯¯¯¯\######/######/
    ///                 /######/           \####/######/________
    ///   _____________/######/             \##/################\
    ///  /###################/               \/##################\
    ///  \##################/\               /###################/
    ///   \################/##\             /######/¯¯¯¯¯¯¯¯¯¯¯¯¯
    ///    ¯¯¯¯¯¯¯¯/######/####\           /######/
    ///           /######/######\_________/######/____________
    ///          /######/ \######\###########################/
    ///         /######/   \######\#########################/
    ///         ######/    /#######\#######################/
    ///         \####/    /#########\¯¯¯¯¯¯\######\¯¯¯¯¯¯¯¯
    ///          \##/    /###########\      \######\
    ///           \/    /#####/\######\      \######\
    ///                /#####/  \######\      \######\
    ///               /#####/    \######\      \######
    ///               \####/      \######\      \####/
    ///                ¯¯¯¯        ¯¯¯¯¯¯¯       ¯¯¯¯
    /// ```
    pub fn nix_os_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NIXOS_OLD,
            names: vec!["nixos_old", "nix-old", "nixos-old", "nix_old"],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Alter,
        }
    }
    /// returns small alternative logo of NixOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nix_os_old_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   \\  \\ //
    ///  ==\\__\\/ //
    ///    //   \\//
    /// ==//     //==
    ///  //\\___//
    /// // /\\  \\==
    ///   // \\  \\
    /// ```
    pub fn nix_os_old_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NIXOS_OLD_SMALL,
            names: vec![
                "nixos_old_small",
                "nix-old-small",
                "nixos-old-small",
                "nix_old_small",
            ],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Alter | LogoType::Small,
        }
    }
    /// returns logo of NetBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::net_bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      `-/oshdmNMNdhyo+:-`
    /// y/s+:-``    `.-:+oydNMMMMNhs/-``
    /// -m+NMMMMMMMMMMMMMMMMMMMNdhmNMMMmdhs+/-`
    ///  -m+NMMMMMMMMMMMMMMMMMMMMmy+:`
    ///   -N/dMMMMMMMMMMMMMMMds:`
    ///    -N/hMMMMMMMMMmho:`
    ///     -N/-:/++/:.`
    ///      :M+
    ///       :Mo
    ///        :Ms
    ///         :Ms
    ///          :Ms
    ///           :Ms
    ///            :Ms
    ///             :Ms
    ///              :Ms
    ///               :Ms
    /// ```
    pub fn net_bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NETBSD,
            names: vec!["netbsd"],
            colors: vec![Color::Magenta, Color::White],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Nobara distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nobara().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ⢀⣤⣴⣶⣶⣶⣦⣤⡀⠀⣀⣠⣤⣴⣶⣶⣶⣶⣶⣶⣶⣶⣤⣤⣀⡀
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⡀
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠋⠉⠁⠀⠀⠉⠉⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⢀⣀⣀⡀⠀⠀⠀⠈⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡏⠀⠀⠀⢠⣾⣿⣿⣿⣿⣷⡄⠀⠀⠀⠻⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⣀⣀⣬⣽⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠈⠻⢿⣿⣿⡿⠟⠁⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⣤⣄⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠛⠉⠉⠛⠛⢿⣿⣿⠀⠀⠀⠀⠀⠸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿
    /// ⠘⢿⣿⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠈⢿⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿⣿⣿⣿⣿⣿⠟⠁
    ///   ⠈⠙⠛⠛⠛⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠛⠛⠛⠉⠁
    /// ```
    pub fn nobara() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NOBARA,
            names: vec!["nobara", "nobara-linux"],
            colors: vec![Color::White],
            color_keys: Some(Color::White),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of NomadBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nomad_bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          _======__
    ///      (===============\\
    ///    (===================\\
    ///    _              _---__
    ///   (=               ====-
    ///  (=                ======
    ///  (==                ======
    ///  (==                ======
    ///  (==\\ \\=-_      _=/ /====/
    ///   (==\\ \\========/ /====/  /====-_
    ///    (==\\ \\=====/ /==/   /===--
    /// /================/  /===-
    /// \\===========/
    /// ```
    pub fn nomad_bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NOMADBSD,
            names: vec!["nomadbsd"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Nurunner distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nurunner().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   ,xc
    ///                 ;00cxXl
    ///               ;K0,   .xNo.
    ///             :KO'       .lXx.
    ///           cXk.    ;xl     cXk.
    ///         cXk.    ;k:.,xo.    cXk.
    ///      .lXx.    :x::0MNl,dd.    :KO,
    ///    .xNx.    cx;:KMMMMMNo'dx.    ;KK;
    ///  .dNl.    cd,cXMMMMMMMMMWd,ox'    'OK:
    /// ;WK.    'K,.KMMMMMMMMMMMMMWc.Kx     lMO
    ///  'OK:    'dl'xWMMMMMMMMMM0::x:    'OK:
    ///    .kNo    .xo'xWMMMMMM0;:O:    ;KK;
    ///      .dXd.   .do,oNMMO;ck:    ;00,
    ///         oNd.   .dx,;'cO;    ;K0,
    ///           oNx.    okk;    ;K0,
    ///             lXx.        :KO'
    ///               cKk'    cXk.
    ///                 ;00:lXx.
    ///                   ,kd.
    /// ```
    pub fn nurunner() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NURUNNER,
            names: vec!["Nurunner"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of NuTyx distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::nu_tyx().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                       .
    ///                                     .
    ///                                  ...
    ///                                ...
    ///             ....     .........--.
    ///        ..-++-----....--++++++---.
    ///     .-++++++-.   .-++++++++++++-----..
    ///   .--...  .++..-+++--.....-++++++++++--..
    ///  .     .-+-. .**-            ....  ..-+----..
    ///      .+++.  .*+.         +            -++-----.
    ///    .+++++-  ++.         .*+.     .....-+++-----.
    ///   -+++-++. .+.          .-+***++***++--++++.  .
    ///  -+-. --   -.          -*- ......        ..--.
    /// .-. .+-    .          -+.
    /// .  .+-                +.
    ///    --                 --
    ///   -+----.              .-
    ///   -++-.+.                .
    ///  .++. --
    ///   +.  ----.
    ///   .  .+. ..
    ///       -  .
    ///       .
    /// ```
    pub fn nu_tyx() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_NUTYX,
            names: vec!["NuTyX"],
            colors: vec![Color::Blue, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Obarun distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::obarun().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ,;::::;
    ///              ;cooolc;,
    ///           ,coool;
    ///         ,loool,
    ///        loooo;
    ///      :ooool
    ///     cooooc            ,:ccc;
    ///    looooc           :oooooool
    ///   cooooo          ;oooooooooo,
    ///  :ooooo;         :ooooooooooo
    ///  oooooo          oooooooooooc
    /// :oooooo         :ooooooooool
    /// loooooo         ;oooooooool
    /// looooooc        .coooooooc
    /// cooooooo:           ,;co;
    /// ,ooooooool;       ,:loc
    ///  cooooooooooooloooooc
    ///   ;ooooooooooooool;
    ///     ;looooooolc;
    /// ```
    pub fn obarun() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OBARUN,
            names: vec!["Obarun"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OBRevenge distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ob_revenge().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         __   __
    ///      _@@@@   @@@g_
    ///    _@@@@@@   @@@@@@
    ///   _@@@@@@M   W@@@@@@_
    ///  j@@@@P        ^W@@@@
    ///  @@@@L____  _____Q@@@@
    /// Q@@@@@@@@@@j@@@@@@@@@@
    /// @@@@@    T@j@    T@@@@@
    /// @@@@@ ___Q@J@    _@@@@@
    /// @@@@@fMMM@@j@jggg@@@@@@
    /// @@@@@    j@j@^MW@P @@@@
    /// Q@@@@@ggg@@f@   @@@@@@L
    /// ^@@@@WWMMP  ^    Q@@@@
    ///  @@@@@_         _@@@@l
    ///   W@@@@@g_____g@@@@@P
    ///    @@@@@@@@@@@@@@@@l
    ///     ^W@@@@@@@@@@@P
    ///        ^TMMMMTll
    /// ```
    pub fn ob_revenge() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OBREVENGE,
            names: vec!["OBRevenge"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OmniOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::omni_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   ____   __  __  _   _  _
    ///  / __ \ |  \/  || \ | || |
    /// | |  | ||      ||  \| || |
    /// | |__| || |\/| || , `_||_|  ____
    ///  \____/ |_|  |_||_|\/ __ \ / ___|
    ///                    | |  | ||(__
    ///        community   | |__| | ___)|
    ///             edition \____/ |____/
    /// ```
    pub fn omni_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OMNIOS,
            names: vec!["OmniOS"],
            colors: vec![Color::White, Color::Yellow, Color::BrightBlack],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Open-Kylin distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_kylin().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///
    ///              /KKK]
    ///             KKKKKKK`   ]KKKK\
    ///            KKKKK/  /KKKKKKKKK\
    ///           KKKK/ ,KKKKKKKKKKKK^
    ///   ,]KKK  =KKK` /KKKKKKOOOOOO`
    /// ,KKKKKK  =KK  /`    [\OOOOOOO\
    ///  \KKKKK  =K            ,OOOOOOO`
    ///  ,KKKKK  =^              \OOOOOO
    ///   ,KKKK   ^               OOOOOO^
    ///    *KKK^                  =OOOOO^
    ///     OOKK^                 OOOOOO^
    ///     \OOOK\               /OOOOOO`
    ///      OOOOOO]           ,OOOOOOO^
    ///      ,OOOOOOOO\]   ,[OOOOOOOOO/
    ///        \OOOOOOOOOOOOOOOOOOOOO`
    ///          [OOOOOOOOOOOOOOOO/`
    ///             ,[OOOOOOOOO]
    /// ```
    pub fn open_kylin() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENKYLIN,
            names: vec!["openkylin", "open-kylin"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                      _
    ///                                     (_)
    ///               |    .
    ///           .   |L  /|   .          _
    ///       _ . |\ _| \--+._/| .       (_)
    ///      / ||\| Y J  )   / |/| ./
    ///     J  |)'( |        ` F`.'/        _
    ///   -<|  F         __     .-<        (_)
    ///     | /       .-'. `.  /-. L___
    ///     J \\      <    \  | | O\\|.-'  _
    ///   _J \\  .-    \\/ O | | \\  |F    (_)
    ///  '-F  -<_.     \\   .-'  `-' L__
    /// __J  _   _.     >-'  )._.   |-'
    ///  `-|.'   /_.          \_|   F
    ///   /.-   .                _.<
    ///  /'    /.'             .'  `\\
    ///   /L  /'   |/      _.-'-\\
    ///  /'J       ___.---'\|
    ///    |\  .--' V  | `. `
    ///    |/`. `-.     `._)
    ///       / .-.\\
    ///       \\ (  `\\
    ///        `.\\
    /// ```
    pub fn open_bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENBSD,
            names: vec!["openbsd"],
            colors: vec![
                Color::Yellow,
                Color::White,
                Color::Cyan,
                Color::Red,
                Color::BrightYellow,
            ],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of OpenBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_bsd_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       _____
    ///     \\-     -/
    ///  \\_/         \\
    ///  |        O O |
    ///  |_  <   )  3 )
    ///  / \\         /
    ///     /-_____-\\
    /// ```
    pub fn open_bsd_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENBSD_SMALL,
            names: vec!["openbsd_small", "openbsd-small"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of OpenEuler distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_euler().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  `.cc.`
    ///              ``.cccccccc..`
    ///           `.cccccccccccccccc.`
    ///       ``.cccccccccccccccccccccc.``
    ///    `..cccccccccccccccccccccccccccc..`
    /// `.ccccccccccccccc/++/ccccccccccccccccc.`
    /// .cccccccccccccccmNMMNdo+oso+ccccccccccc.
    /// .cccccccccc/++odms+//+mMMMMm/:+syso/cccc
    /// .cccccccccyNNMMMs:::/::+o+/:cdMMMMMmcccc
    /// .ccccccc:+NmdyyhNNmNNNd:ccccc:oyyyo:cccc
    /// .ccc:ohdmMs:cccc+mNMNmyccccccccccccccccc
    /// .cc/NMMMMMo////:c:///:cccccccccccccccccc
    /// .cc:syysyNMNNNMNyccccccccccccccccccccccc
    /// .cccccccc+MMMMMNyc:/+++/cccccccccccccccc
    /// .cccccccccohhhs/comMMMMNhccccccccccccccc
    /// .ccccccccccccccc:MMMMMMMM/cccccccccccccc
    /// .ccccccccccccccccsNNNNNd+cccccccccccccc.
    /// `..cccccccccccccccc/+/:cccccccccccccc..`
    ///    ``.cccccccccccccccccccccccccccc.``
    ///        `.cccccccccccccccccccccc.`
    ///           ``.cccccccccccccc.``
    ///               `.cccccccc.`
    ///                  `....`
    /// ```
    pub fn open_euler() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENEULER,
            names: vec!["OpenEuler"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenIndiana distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_indiana().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          .sy/
    ///                          .yh+
    ///
    ///            -+syyyo+-      /+.
    ///          +ddo/---/sdh/    ym-
    ///        `hm+        `sms   ym-```````.-.
    ///        sm+           sm/  ym-         +s
    ///        hm.           /mo  ym-         /h
    ///        omo           ym:  ym-       `os`
    ///         smo`       .ym+   ym-     .os-
    ///      ``  :ymy+///oyms-    ym-  .+s+.
    ///    ..`     `:+oo+/-`      -//oyo-
    ///  -:`                   .:oys/.
    /// +-               `./oyys/.
    /// h+`      `.-:+oyyyo/-`
    /// `/ossssysso+/-.`
    /// ```
    pub fn open_indiana() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENINDIANA,
            names: vec!["OpenIndiana"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenMamba distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_mamba().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  `````
    ///            .-/+ooooooooo+/:-`
    ///         ./ooooooooooooooooooo+:.
    ///       -+oooooooooooooooooooooooo+-
    ///     .+ooooooooo+/:---::/+ooooooooo+.
    ///    :oooooooo/-`          `-/oos´oooo.s´
    ///   :ooooooo/`                `sNdsooosNds
    ///  -ooooooo-                   :dmyooo:dmy
    ///  +oooooo:                      :oooooo-
    /// .ooooooo                        .://:`
    /// :oooooo+                        ./+o+:`
    /// -ooooooo`                      `oooooo+
    /// `ooooooo:                      /oooooo+
    ///  -ooooooo:                    :ooooooo.
    ///   :ooooooo+.                .+ooooooo:
    ///    :oooooooo+-`          `-+oooooooo:
    ///     .+ooooooooo+/::::://oooooooooo+.
    ///       -+oooooooooooooooooooooooo+-
    ///         .:ooooooooooooooooooo+:.
    ///            `-:/ooooooooo+/:.`
    ///                  ``````
    /// ```
    pub fn open_mamba() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENMAMBA,
            names: vec!["OpenMamba"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenStage distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_stage().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  /(/
    ///               .(((((((,
    ///              /(((((((((/
    ///            .(((((/,/(((((,
    ///           *(((((*   ,(((((/
    ///           (((((*      .*/((
    ///          *((((/  (//(/*
    ///          /((((*  ((((((((((,
    ///       .  /((((*  (((((((((((((.
    ///      ((. *((((/        ,((((((((
    ///    ,(((/  (((((/     **   ,((((((*
    ///   /(((((. .(((((/   //(((*  *(((((/
    ///  .(((((,    ((/   .(((((/.   .(((((,
    ///  /((((*        ,(((((((/      ,(((((
    ///  /(((((((((((((((((((/.  /(((((((((/
    ///  /(((((((((((((((((,   /(((((((((((/
    ///      */(((((//*.      */((/(/(/*
    /// ```
    pub fn open_stage() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENSTAGE,
            names: vec!["OpenStage"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenSuse distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_suse().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .;ldkO0000Okdl;.
    ///        .;d00xl:^''''''^:ok00d;.
    ///      .d00l'                'o00d.
    ///    .d0Kd'  Okxol:;,.          :O0d
    ///   .OKKKK0kOKKKKKKKKKKOxo:,      lKO.
    ///  ,0KKKKKKKKKKKKKKKK0P^,,,^dx:    ;00,
    /// .OKKKKKKKKKKKKKKKKk'.oOPPb.'0k.   cKO.
    /// :KKKKKKKKKKKKKKKKK: kKx..dd lKd   'OK:
    /// dKKKKKKKKKKKOx0KKKd ^0KKKO' kKKc   dKd
    /// dKKKKKKKKKKKK;.;oOKx,..^..;kKKK0.  dKd
    /// :KKKKKKKKKKKK0o;...^cdxxOK0O/^^'  .0K:
    ///  kKKKKKKKKKKKKKKK0x;,,......,;od  lKk
    ///  '0KKKKKKKKKKKKKKKKKKKKK00KKOo^  c00'
    ///   'kKKKOxddxkOO00000Okxoc;''   .dKk'
    ///     l0Ko.                    .c00l'
    ///      'l0Kk:.              .;xK0l'
    ///         'lkK0xl:;,,,,;:ldO0kl'
    ///             '^:ldxkkkkxdl:^'
    /// ```
    pub fn open_suse() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENSUSE,
            names: vec![
                "opensuse",
                "open_suse",
                "open-suse",
                "suse_small",
                "suse-linux_small",
            ],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of OpenSuse distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_suse_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   _______
    /// __|   __ \
    ///      / .\ \
    ///      \__/ |
    ///    _______|
    ///    \_______
    /// __________/
    /// ```
    pub fn open_suse_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENSUSE_SMALL,
            names: vec!["opensuse_small", "open_suse_small", "open-suse_small"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of OpenSuse-MicroOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_suse_micro_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ⣀⣠⣴⣶⣶⣿⣿⣿⣿⣶⣶⣦⣄⣀
    ///           ⢀⣴⣾⣿⠿⠛⠉⠉    ⠉⠉⠛⠿⣿⣷⣦⡀
    ///          ⣴⣿⡿⠋              ⠙⢿⣿⣦
    ///         ⣾⣿⡟     ⣠⣴⣶⣿⣿⣶⣦⣄     ⢻⣿⣷
    /// ⣠⣤⣤⣤⣤⣤⣤⣼⣿⣿     ⣼⣿⡟⠉  ⠉⢻⣿⣧     ⣿⣿⣧⣤⣤⣤⣤⣤⣤⣄
    /// ⠙⠛⠛⠛⠛⠛⠛⢻⣿⣿     ⢻⣿⣧⡀  ⢀⣼⣿⡟     ⣿⣿⡟⠛⠛⠛⠛⠛⠛⠋
    ///         ⢿⣿⣇     ⠙⠿⣿⣿⣿⣿⠿⠋     ⣸⣿⡿
    ///         ⠈⢻⣿⣧⣀              ⣀⣾⣿⡟⠁
    ///           ⠙⠻⣿⣷⣦⣄⣀      ⣀⣠⣴⣾⣿⠟⠋
    ///              ⠉⠛⠿⢿⣿⣿⣿⣿⣿⣿⡿⠿⠛⠉
    /// ```
    pub fn open_suse_micro_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENSUSE_MICROOS,
            names: vec!["opensuse-microos", "opensuse_microos"],
            colors: vec![Color::Green],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenSuse-Leap distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_suse_leap().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  .-++:.
    ///                ./oooooo/-
    ///             `:oooooooooooo:.
    ///           -+oooooooooooooooo+-`
    ///        ./oooooooooooooooooooooo/-
    ///       :oooooooooooooooooooooooooo:
    ///     `  `-+oooooooooooooooooooo/-   `
    ///  `:oo/-   .:ooooooooooooooo+:`  `-+oo/.
    /// `/oooooo:.   -/oooooooooo/.   ./oooooo/.
    ///   `:+ooooo+-`  `:+oooo+-   `:oooooo+:`
    ///      .:oooooo/.   .::`   -+oooooo/.
    ///         -/oooooo:.    ./oooooo+-
    ///           `:+ooooo+-:+oooooo:`
    ///              ./oooooooooo/.
    ///                 -/oooo+:`
    ///                   `:/.
    /// ```
    pub fn open_suse_leap() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENSUSE_LEAP,
            names: vec![
                "opensuse_leap",
                "open_suse_leap",
                "opensuse-leap",
                "open-suse-leap",
                "suse_leap",
                "suse-leap",
                "opensuseleap",
            ],
            colors: vec![Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenSuse-Tumbleweed distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_suse_tumbleweed().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                      ......
    ///      .,cdxxxoc,.               .:kKMMMNWMMMNk:.
    ///     cKMMN0OOOKWMMXo. A        ;0MWk:'      ':OMMk.
    ///   ;WMK;'       'lKMMNM,     :NMK'             'OMW;
    ///  cMW;             WMMMN   ,XMK'                 oMM.
    /// .MMc             ''^*~l. xMN:                    KM0
    /// 'MM.                   .NMO                      oMM
    /// .MM,                 .kMMl                       xMN
    ///  KM0               .kMM0' .dl>~,.               .WMd
    ///  'XM0.           ,OMMK'    OMMM7'              .XMK
    ///    *WMO:.    .;xNMMk'       NNNMKl.          .xWMx
    ///      ^ONMMNXMMMKx;          V  'xNMWKkxllox0NMWk'
    ///          '''''                    ':dOOXXKOxl'
    /// ```
    pub fn open_suse_tumbleweed() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENSUSE_TUMBLEWEED,
            names: vec![
                "opensuse_tumbleweed",
                "open_suse_tumbleweed",
                "opensuse-tumbleweed",
                "open-suse-tumbleweed",
                "suse_tumbleweed",
                "suse-tumbleweed",
                "opensusetumbleweed",
            ],
            colors: vec![Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Open Mandriva distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_mandriva().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ``````
    ///             `-:/+++++++//:-.`
    ///          .:+++oooo+/:.``   ``
    ///       `:+ooooooo+:.  `-:/++++++/:.`
    ///      -+oooooooo:` `-++o+/::::://+o+/-
    ///    `/ooooooooo-  -+oo/.`        `-/oo+.
    ///   `+ooooooooo.  :os/`              .+so:
    ///   +sssssssss/  :ss/                 `+ss-
    ///  :ssssssssss`  sss`                  .sso
    ///  ossssssssss  `yyo                    sys
    /// `sssssssssss` `yys                   `yys
    /// `sssssssssss:  +yy/                  +yy:
    ///  oyyyyyyyyyys. `oyy/`              `+yy+
    ///  :yyyyyyyyyyyo. `+yhs:.         `./shy/
    ///   oyyyyyyyyyyys:` .oyhys+:----/+syhy+. `
    ///   `syyyyyyyyyyyyo-` .:osyhhhhhyys+:``.:`
    ///    `oyyyyyyyyyyyyys+-`` `.----.```./oo.
    ///      /yhhhhhhhhhhhhhhyso+//://+osyhy/`
    ///       `/yhhhhhhhhhhhhhhhhhhhhhhhhy/`
    ///         `:oyhhhhhhhhhhhhhhhhhhyo:`
    ///             .:+syhhhhhhhhys+:-`
    ///                 ``....``
    /// ```
    pub fn open_mandriva() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENMANDRIVA,
            names: vec![
                "openmandriva",
                "open-mandriva",
                "open_mandriva",
                "openmandriva lx",
            ],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpenWrt distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::open_wrt().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  _______
    /// |       |.-----.-----.-----.
    /// |   -   ||  _  |  -__|     |
    /// |_______||   __|_____|__|__|
    ///          |__|
    ///  ________        __
    /// |  |  |  |.----.|  |_
    /// |  |  |  ||   _||   _|
    /// |________||__|  |____|
    /// ```
    pub fn open_wrt() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPENWRT,
            names: vec!["openwrt"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OpnSense distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::opn_sense().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     .'''''''''''''''''''''''''''''''''''
    ///   oocc:::::::::::::::::::::::::::::::cox
    ///  ;00;                                o0O
    ///  .,:'  .;;;;;;;;;;;;;;;;;;;;;;;;;;   ;:,
    ///   .',;;cxOOOOOOOOOOOOOOOOOOOOOOOkd:;;,..
    ///      .,cll:'                 ':llc,.
    ///     ,;;:okxdxd:           :dxdxko:;;,
    ///    .xxxx0XNNK0O.         .O0KNNX0xxxx.
    ///        ,cc:,.               .,:cc,
    ///  ........;ccc:;.         .;:ccc;........
    ///  ccccccccccccccc         ccccccccccccccc
    ///  ........;ccc:;.         .;:ccc;........
    ///        ,cc:,.               .,:cc,
    ///    .xxxx0XNNK0O.         .O0KNNX0xxxx.
    ///     ,;;:okxdxd:           :dxdxko:;;,
    ///      .,cll:'                 ':llc,.
    ///   .,,;,ckOOOOOOOOOOOOOOOOOOOOOOOOx;,;,'.
    ///  .:l'  ...........................   ;:;
    ///  lOk'                                cdd
    ///  ;lccccccccccccccccccccccccccccccccccc:.
    /// ```
    pub fn opn_sense() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OPNSENSE,
            names: vec!["OPNsense"],
            colors: vec![
                Color::White,
                Color::TrueColor {
                    r: 0xff,
                    g: 0x5f,
                    b: 0,
                },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Oracle distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::oracle().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       `-/+++++++++++++++++/-.`
    ///    `/syyyyyyyyyyyyyyyyyyyyyyys/.
    ///   :yyyyo/-...............-/oyyyy/
    ///  /yyys-                     .oyyy+
    /// .yyyy`                       `syyy-
    /// :yyyo                         /yyy/
    /// .yyyy`                       `syyy-
    ///  /yyys.                     .oyyyo
    ///   /yyyyo:-...............-:oyyyy/`
    ///    `/syyyyyyyyyyyyyyyyyyyyyyys+.
    ///      `.:/+ooooooooooooooo+/:.`
    /// ```
    pub fn oracle() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ORACLE,
            names: vec!["oracle", "oracle linux", "oracle linux server"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Orchid distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::orchid().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   .==.
    ///                 .-#@@#-.
    ///               .-##@@@@##-.
    ///             .-##@@@@@@@@##-.
    ///            :*@@@@@####@@@@@*:
    ///          ..:*@@@@==--==@@@@*:..
    ///       .-*%%#==#@@#====#@@#==#%%*-.
    ///     .-#@@@@@##==#@@++@@##==#@@@@@#-.
    ///   .-#@@@@@#@@@#++#====#++#@@@#@@@@@#-.
    /// .-#@@@@@#-==**###+:--:+###**==-#@@@@@#-.
    /// .-#@@@@@#-==**###+:--:+###**==-#@@@@@#-.
    ///   .-#@@@@@#@@@#++#====#++#@@@#@@@@@#-.
    ///     .-#@@@@@##==#@@++@@##==#@@@@@#-.
    ///       .-*%%#==#@@#====#@@#==#%%*-.
    ///          ..:*@@@@==--==@@@@*:..
    ///            :*@@@@@####@@@@@*:
    ///             .-##@@@@@@@@##-.
    ///               .-##@@@@##-.
    ///                 .-#@@#-.
    ///                   .==.
    /// ```
    pub fn orchid() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ORCHID,
            names: vec!["orchid"],
            colors: vec![Color::White, Color::Magenta, Color::Magenta],
            color_keys: Some(Color::White),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Orchid distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::orchid_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             :##:
    ///           -#@@@@#-
    ///          #@@=..=@@#
    ///          +@@-  -@@+
    ///     -#@@*..*@..@*..*@@#-
    ///   :#@@*+%@= .  . =@%+*@@#:
    /// +@@@:    :-.    .-:   :@@@+
    ///   :#@@*+%@= .  . =@%+*@@#:
    ///     -#@@*..*@..@*..*@@#-
    ///          +@@-  -@@+
    ///          #@@=..=@@#
    ///           -#@@@@#-
    ///             :##:
    /// ```
    pub fn orchid_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ORCHID_SMALL,
            names: vec!["orchid_small"],
            colors: vec![Color::White, Color::Magenta, Color::Magenta],
            color_keys: Some(Color::White),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of OS Elbrus distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::os_elbrus().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
    /// ██▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ██   ███████   ███████   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ███████   ███████
    /// ██   ██                  ██
    /// ██   ██▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄██
    /// ██   ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ███████████████████████████
    /// ```
    pub fn os_elbrus() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OS_ELBRUS,
            names: vec!["OS Elbrus"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of OSMC distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::osmc().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             -+shdmNNNNmdhs+-
    ///         .+hMNho/:..``..:/ohNMh+.
    ///       :hMdo.                .odMh:
    ///     -dMy-                      -yMd-
    ///    sMd-                          -dMs
    ///   hMy       +.            .+       yMh
    ///  yMy        dMs.        .sMd        yMy
    /// :Mm         dMNMs`    `sMNMd        `mM:
    /// yM+         dM//mNs``sNm//Md         +My
    /// mM-         dM:  +NNNN+  :Md         -Mm
    /// mM-         dM: `oNN+    :Md         -Mm
    /// yM+         dM/+NNo`     :Md         +My
    /// :Mm`        dMMNs`       :Md        `mM:
    ///  yMy        dMs`         -ms        yMy
    ///   hMy       +.                     yMh
    ///    sMd-                          -dMs
    ///     -dMy-                      -yMd-
    ///       :hMdo.                .odMh:
    ///         .+hMNho/:..``..:/ohNMh+.
    ///             -+shdmNNNNmdhs+-
    /// ```
    pub fn osmc() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_OSMC,
            names: vec!["OSMC", "Open Source Media Center"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PacBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pac_bsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       :+sMs.
    ///   `:ddNMd-                         -o--`
    ///  -sMMMMh:                          `+N+``
    ///  yMMMMMs`     .....-/-...           `mNh/
    ///  yMMMMMmh+-`:sdmmmmmmMmmmmddy+-``./ddNMMm
    ///  yNMMNMMMMNdyyNNMMMMMMMMMMMMMMMhyshNmMMMm
    ///  :yMMMMMMMMMNdooNMMMMMMMMMMMMMMMMNmy:mMMd
    ///   +MMMMMMMMMmy:sNMMMMMMMMMMMMMMMMMMMmshs-
    ///   :hNMMMMMMN+-+MMMMMMMMMMMMMMMMMMMMMMMs.
    ///  .omysmNNhy/+yNMMMMMMMMMMNMMMMMMMMMNdNNy-
    ///  /hMM:::::/hNMMMMMMMMMMMm/-yNMMMMMMN.mMNh`
    /// .hMMMMdhdMMMMMMMMMMMMMMmo  `sMMMMMMN mMMm-
    /// :dMMMMMMMMMMMMMMMMMMMMMdo+  oMMMMMMN`smMNo`
    /// /dMMMMMMMMMMMMMMMMMMMMMNd/` :yMMMMMN:-hMMM.
    /// :dMMMMMMMMMMMMMMMMMMMMMNh`  oMMMMMMNo/dMNN`
    /// :hMMMMMMMMMMMMMMMMMMMMMMNs--sMMMMMMMNNmy++`
    ///  sNMMMMMMMMMMMMMMMMMMMMMMMmmNMMMMMMNho::o.
    ///  :yMMMMMMMMMMMMMNho+sydNNNNNNNmysso/` -//
    ///   /dMMMMMMMMMMMMMs-  ````````..``
    ///    .oMMMMMMMMMMMMNs`               ./y:`
    ///      +dNMMNMMMMMMMmy`          ``./ys.
    ///       `/hMMMMMMMMMMMNo-``    `.+yy+-`
    ///         `-/hmNMNMMMMMMmmddddhhy/-`
    ///             `-+oooyMMMdsoo+/:.
    /// ```
    pub fn pac_bsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PACBSD,
            names: vec!["PacBSD"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Panwah distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::panwah().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          HHH
    ///         HAAAH                             HHH
    ///         HAAAAH                           HAAAH
    ///        HAAAAAAH                         HAAAAH
    ///        HAAAAAAH                       HAAAAAH
    ///       HAAAAAAAAHWWWWWWWWWWWWWWWW      HAAAAAH
    ///       HAAAAAAAAHWWWWWWWWWWWWWWWWWWWW HAAAAAH
    ///       HAAWWWWWWWWWWWWWWWWWWWWWWWWWWWWWAAAAAH
    ///      WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWAH
    ///     WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    ///   WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    ///  WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWWWWWAAAWWWW   WWWWWWWWWWWWWWWWWWWWWWWWWWW
    ///   WWWWAAAWWWWW    WWWWWWW    WWWWWWWWWWWWWWW
    ///    WWAAAWWWWWWWWWWWWWWWWW   WWWWWAAAWWWWWWWW
    ///     AAAWWWWWOOOOOOOOOOOWWWWWWWWWWWAAAWWWWWW
    ///           OOOOGGGGGGGOOOOWWWWWWWWWWAAAWWWW
    ///            OOOGGGGGGGOOOWWWWWWWWWWWWAAAW
    ///              OOOOOOOOO
    /// ```
    pub fn panwah() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PANWAH,
            names: vec!["Panwah"],
            colors: vec![Color::White, Color::Red, Color::Black],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Parabola distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::parabola().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                           `.-.    `.
    ///                    `.`  `:++.   `-+o+.
    ///              `` `:+/. `:+/.   `-+oooo+
    ///         ``-::-.:+/. `:+/.   `-+oooooo+
    ///     `.-:///-  ..`   .-.   `-+oooooooo-
    ///  `..-..`                 `+ooooooooo:
    /// ``                        :oooooooo/
    ///                           `ooooooo:
    ///                           `oooooo:
    ///                           -oooo+.
    ///                           +ooo/`
    ///                          -ooo-
    ///                         `+o/.
    ///                         /+-
    ///                        //`
    ///                       -.
    /// ```
    pub fn parabola() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PARABOLA,
            names: vec!["parabola", "parabola-gnulinux"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Parabola distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::parabola_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   __ __ __  _
    /// .`_//_//_/ / `.
    ///           /  .`
    ///          / .`
    ///         /.`
    ///        /`
    /// ```
    pub fn parabola_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PARABOLA_SMALL,
            names: vec!["parabola_small", "parabola-gnulinux_small"],
            colors: vec![Color::Magenta],
            color_keys: Some(Color::Magenta),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Parch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::parch().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ,:lodddd.
    ///           .:clooood.
    ///         ;clllooooc
    ///       ;cclllllloo
    ///      .cccccllllll
    ///    .   ,cccclllll
    ///   ':::;; ccccclll;
    ///  .:::cccccccccccll;
    ///  ;::::ccccllllllcll:
    /// .;::::cccclllloool::;
    /// ;;;::::cccclllolc::::;.
    /// ;;;::::cccclllccc:::::;.
    /// ;;;::::cccclccccc::::::;.
    /// ;;;;::::::llcccccc:::::'
    /// ;;;;:; ,clllccccccc::
    /// .;;  .cllllllcccccc::;::::'
    ///     .'''''''''',:lddoooolll
    ///    '.....'''',cdddooooollll
    ///   ........':oddddoooolllllc
    ///    ....';ldddddooooolllllc:
    ///      ,cdddddddooooollllccc
    ///       :ddddddoooolllllccc
    ///         ;ddooooolllllcc.
    ///            :ooollllc.
    ///                c'
    /// ```
    pub fn parch() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PARCH,
            names: vec!["Parch"],
            colors: vec![Color::Blue, Color::White, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Pardus distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pardus().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  .smNdy+-    `.:/osyyso+:.`    -+ydmNs.
    /// /Md- -/ymMdmNNdhso/::/oshdNNmdMmy/. :dM/
    /// mN.     oMdyy- -y          `-dMo     .Nm
    /// .mN+`  sMy hN+ -:             yMs  `+Nm.
    ///  `yMMddMs.dy `+`               sMddMMy`
    ///    +MMMo  .`  .                 oMMM+
    ///    `NM/    `````.`    `.`````    +MN`
    ///    yM+   `.-:yhomy    ymohy:-.`   +My
    ///    yM:          yo    oy          :My
    ///    +Ms         .N`    `N.      +h sM+
    ///    `MN      -   -::::::-   : :o:+`NM`
    ///     yM/    sh   -dMMMMd-   ho  +y+My
    ///     .dNhsohMh-//: /mm/ ://-yMyoshNd`
    ///       `-ommNMm+:/. oo ./:+mMNmmo:`
    ///      `/o+.-somNh- :yy: -hNmos-.+o/`
    ///     ./` .s/`s+sMdd+``+ddMs+s`/s. `/.
    ///         : -y.  -hNmddmNy.  .y- :
    ///          -+       `..`       +-
    /// ```
    pub fn pardus() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PARDUS,
            names: vec!["Pardus"],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Parrot distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::parrot().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///   `:oho/-`
    /// `mMMMMMMMMMMMNmmdhy-
    ///  dMMMMMMMMMMMMMMMMMMs`
    ///  +MMsohNMMMMMMMMMMMMMm/
    ///  .My   .+dMMMMMMMMMMMMMh.
    ///   +       :NMMMMMMMMMMMMNo
    ///            `yMMMMMMMMMMMMMm:
    ///              /NMMMMMMMMMMMMMy`
    ///               .hMMMMMMMMMMMMMN+
    ///                   ``-NMMMMMMMMMd-
    ///                      /MMMMMMMMMMMs`
    ///                       mMMMMMMMsyNMN/
    ///                       +MMMMMMMo  :sNh.
    ///                       `NMMMMMMm     -o/
    ///                        oMMMMMMM.
    ///                        `NMMMMMM+
    ///                         +MMd/NMh
    ///                          mMm -mN`
    ///                          /MM  `h:
    ///                           dM`   .
    ///                           :M-
    ///                            d:
    ///                            -+
    ///                             -
    /// ```
    pub fn parrot() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PARROT,
            names: vec!["Parrot"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Parsix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::parsix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  -/+/:.
    ///                .syssssys.
    ///        .--.    ssssssssso   ..--.
    ///      :++++++:  +ssssssss+ ./++/+++:
    ///     /+++++++++..yssooooy`-+///////o-
    ///     /++++++++++.+soooos::+////////+-
    ///      :+++++////o-oooooo-+/////////-
    ///       `-/++//++-.-----.-:+/////:-
    ///   -://::---:/:.--.````.--.:::---::::::.
    /// -/:::::::://:.:-`      `-:`:/:::::::--/-
    /// /::::::::::/---.        .-.-/://///::::/
    /// -/:::::::::/:`:-.      .-:`:///////////-
    ///  `-::::--.-://.---....---`:+/:---::::-`
    ///        -/+///+o/-.----..:oo+++o+.
    ///      -+/////+++o:syyyyy.o+++++++++:
    ///     .+////+++++-+sssssy+.++++++++++\
    ///     .+:/++++++..yssssssy-`+++++++++:
    ///      :/+++++-  +sssssssss  -++++++-
    ///        `--`    +sssssssso    `--`
    ///                 +sssssy+`
    ///                  `.::-`
    /// ```
    pub fn parsix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PARSIX,
            names: vec!["Parsix"],
            colors: vec![Color::Yellow, Color::Red, Color::White, Color::BrightBlack],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PCBSD distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pcbsd().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                        ..
    ///                         s.
    ///                         +y
    ///                         yN
    ///                        -MN  `.
    ///                       :NMs `m
    ///                     .yMMm` `No
    ///             `-/+++sdMMMNs+-`+Ms
    ///         `:oo+-` .yMMMMy` `-+oNMh
    ///       -oo-     +NMMMM/       oMMh-
    ///     .s+` `    oMMMMM/     -  oMMMhy.
    ///    +s`- ::   :MMMMMd     -o `mMMMy`s+
    ///   y+  h .Ny+oNMMMMMN/    sh+NMMMMo  +y
    ///  s+ .ds  -NMMMMMMMMMMNdhdNMMMMMMh`   +s
    /// -h .NM`   `hMMMMMMMMMMMMMMNMMNy:      h-
    /// y- hMN`     hMMmMMMMMMMMMNsdMNs.      -y
    /// m` mMMy`    oMMNoNMMMMMMo`  sMMMo     `m
    /// m` :NMMMdyydMMMMo+MdMMMs     sMMMd`   `m
    /// h-  `+ymMMMMMMMM--M+hMMN/    +MMMMy   -h
    /// :y     `.sMMMMM/ oMM+.yMMNddNMMMMMm   y:
    ///  y:   `s  dMMN- .MMMM/ :MMMMMMMMMMh  :y
    ///  `h:  `mdmMMM/  yMMMMs  sMMMMMMMMN- :h`
    ///    so  -NMMMN   /mmd+  `dMMMMMMMm- os
    ///     :y: `yMMM`       `+NMMMMMMNo`:y:
    ///       /s+`.omy      /NMMMMMNh/.+s:
    ///         .+oo:-.     /mdhs+::oo+.
    ///             -/o+++++++++++/-
    /// ```
    pub fn pcbsd() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PCBSD,
            names: vec!["PCBSD", "TrueOS"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PCLinuxOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pc_linux_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             mhhhyyyyhhhdN
    ///         dyssyhhhhhhhhhhhssyhN
    ///      Nysyhhyo/:-.....-/oyhhhssd
    ///    Nsshhy+.              `/shhysm
    ///   dohhy/                    -shhsy
    ///  dohhs`                       /hhys
    /// N+hho   +ssssss+-   .+syhys+   /hhsy
    /// ohhh`   ymmo++hmm+`smmy/::+y`   shh+
    /// +hho    ymm-  /mmy+mms          :hhod
    /// /hh+    ymmhhdmmh.smm/          .hhsh
    /// +hhs    ymm+::-`  /mmy`    `    /hh+m
    /// yyhh-   ymm-       /dmdyosyd`  `yhh+
    ///  ohhy`  ://`         -/+++/-   ohhom
    ///  N+hhy-                      `shhoh
    ///    sshho.                  `+hhyom
    ///     dsyhhs/.            `:ohhhoy
    ///       dysyhhhso///://+syhhhssh
    ///          dhyssyhhhhhhyssyyhN
    ///               mddhdhdmN
    /// ```
    pub fn pc_linux_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PCLINUXOS,
            names: vec!["PCLinuxOS"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PearOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pear_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   .+yh
    ///                  sMMMo
    ///                 sMMN+
    ///                 +o:
    ///            ./oyyys+.
    ///          :dMMMMMMMMMm/
    ///         :MMMMMMMMMMMMMy
    ///         yMMMMMMMMMMMMMN
    ///         mMMMMMMMMMMMMs`
    ///        yMMMMMMMMMMMMo
    ///      -mMMMMMMMMMMMMM`
    ///     oMMMMMMMMMMMMMMM`
    ///    oMMMMMMMMMMMMMMMMy
    ///   .MMMMMMMMMMMMMMMMMMy`
    ///   +MMMMMMMMMMMMMMMMMMMMy/`
    ///   /MMMMMMMMMMMMMMMMMMMMMMMNds
    ///   `mMMMMMMMMMMMMMMMMMMMMMMMM/
    ///    .mMMMMMMMMMMMMMMMMMMMMMM+
    ///     `oNMMMMMMMMMMMMMMMMMMd-
    ///       `+hMMMMMMMMMMMMMms-
    ///           -/osyhhyso:.
    /// ```
    pub fn pear_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PEAROS,
            names: vec!["PearOS"],
            colors: vec![
                Color::Green,
                Color::Yellow,
                Color::Red,
                Color::Magenta,
                Color::Blue,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Pengwin distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pengwin().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               ...`
    ///               `-///:-`
    ///                 .+ssys/
    ///                  +yyyyyo    
    ///                  -yyyyyy:
    ///     `.:/+ooo+/:` -yyyyyy+
    ///   `:oyyyyyys+:-.`syyyyyy:
    ///  .syyyyyyo-`   .oyyyyyyo
    /// `syyyyyy   `-+yyyyyyy/`
    /// /yyyyyy+ -/osyyyyyyo/.
    /// +yyyyyy-  `.-:::-.`
    /// .yyyyyy-
    ///  :yyyyyo
    ///   .+ooo+
    ///     `.::/:.
    /// ```
    pub fn pengwin() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PENGWIN,
            names: vec!["Pengwin"],
            colors: vec![Color::Magenta, Color::BrightMagenta, Color::Magenta],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Pentoo distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pentoo().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            `:oydNNMMMMNNdyo:`
    ///         :yNMMMMMMMMMMMMMMMMNy:
    ///       :dMMMMMMMMMMMMMMMMMMMMMMd:
    ///      oMMMMMMMho/-....-/ohMMMMMMMo
    ///     oMMMMMMy.            .yMMMMMMo
    ///    .MMMMMMo                oMMMMMM.
    ///    +MMMMMm                  mMMMMM+
    ///    oMMMMMh                  hMMMMMo
    ///  //hMMMMMm//`          `////mMMMMMh//
    /// MMMMMMMMMMM/      /o/`  .smMMMMMMMMMMM
    /// MMMMMMMMMMm      `NMN:    .yMMMMMMMMMM
    /// MMMMMMMMMMMh:.              dMMMMMMMMM
    /// MMMMMMMMMMMMMy.            -NMMMMMMMMM
    /// MMMMMMMMMMMd:`           -yNMMMMMMMMMM
    /// MMMMMMMMMMh`          ./hNMMMMMMMMMMMM
    /// MMMMMMMMMMs        .:ymMMMMMMMMMMMMMMM
    /// MMMMMMMMMMNs:..-/ohNMMMMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    ///  MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// ```
    pub fn pentoo() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PENTOO,
            names: vec!["Pentoo"],
            colors: vec![Color::Magenta, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Peppermint distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::peppermint().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              PPPPPPPPPPPPPP
    ///          PPPPMMMMMMMPPPPPPPPPPP
    ///        PPPPMMMMMMMMMMPPPPPPPPMMPP
    ///      PPPPPPPPMMMMMMMPPPPPPPPMMMMMPP
    ///    PPPPPPPPPPPPMMMMMMPPPPPPPMMMMMMMPP
    ///   PPPPPPPPPPPPMMMMMMMPPPPMPMMMMMMMMMPP
    ///  PPMMMMPPPPPPPPPPMMMPPPPPMMMMMMMPMMPPPP
    ///  PMMMMMMMMMMPPPPPPMMPPPPPMMMMMMPPPPPPPP
    /// PMMMMMMMMMMMMPPPPPMMPPMPMMPMMPPPPPPPPPPP
    /// PMMMMMMMMMMMMMMMMPPMPMMMPPPPPPPPPPPPPPPP
    /// PMMMPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPMMMMMP
    /// PPPPPPPPPPPPPPPPMMMPMPMMMMMMMMMMMMMMMMPP
    /// PPPPPPPPPPPMMPMMPPPPMMPPPPPMMMMMMMMMMMPP
    ///  PPPPPPPPMMMMMMPPPPPMMPPPPPPMMMMMMMMMPP
    ///  PPPPMMPMMMMMMMPPPPPPMMPPPPPPPPPPMMMMPP
    ///   PPMMMMMMMMMPMPPPPMMMMMMPPPPPPPPPPPPP
    ///    PPMMMMMMMPPPPPPPMMMMMMPPPPPPPPPPPP
    ///      PPMMMMPPPPPPPPPMMMMMMMPPPPPPPP
    ///        PPMMPPPPPPPPMMMMMMMMMMPPPP
    ///          PPPPPPPPPPMMMMMMMMPPPP
    ///              PPPPPPPPPPPPPP
    /// ```
    pub fn peppermint() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PEPPERMINT,
            names: vec!["Peppermint"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Peropesis distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::peropesis().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ####  #### ####   ###   ####  ####  #### #  ####
    /// #   # #    #   # #   #  #   # #     #    #  #
    /// ####  ###  #### #     # ####  ###     #  #    #
    /// #     #    #  #  #   #  #     #        # #     #
    /// #     #### #   #  ###   #     #### ####  # ####
    /// ```
    pub fn peropesis() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PEROPESIS,
            names: vec!["Peropesis", "Peropesis Linux"],
            colors: vec![Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PhyOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::phy_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// .^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^.^^^^^.
    ///  :777777777777777777777777777777^~7777:
    ///   .~~~~~~~~~~~~~~~~~~~~~^~7777!:!777!.
    ///     ~7!!!!!!!!!!!!!!!!!^:!777~^!777~
    ///      ^77777!!!!!!!!!7!^^7777^^7777^
    ///       ^7777~.~~~~^.  .~7777^~7777:
    ///        :!777~^!777~. !777!:~777!:
    ///         .!777!:~777!:~77~:!777!.
    ///           ~777!^~7777:^~^!777~
    ///            ^7777^^7777^^7777^
    ///             :7777~^!7777777:
    ///              .!777!:!7777!.
    ///               .~777!:~77~.
    ///                 ~7777^~~
    ///                  ^7777.
    ///                   :77:
    ///                    ..
    /// ```
    pub fn phy_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PHYOS,
            names: vec!["PhyOS"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PikaOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pika_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   '',,,               ,,,d,
    ///               ',,                           ,,'
    ///            ',                                   ,.
    ///          .,                                       '
    ///         .                                           .
    ///       '                                               .
    ///     ..                   oddddkdlc:;,..                ..
    ///    .                        ............lllc,            .
    ///   .                       ....................:           .
    ///  .                        .  ....................
    ///       '.                     ..........'o........d0XX0.
    ///      ....lllllllcOOOcllllll............cxlxc...;okkkx.
    ///      ..................................';lc'...lo.
    ///     .'''''''''''''.....................,;,.......
    ///     ',,,,,,,,,,,,,,,,''...............dkkkd......
    ///     ',,,,,,,,,,,,,,,,,,,'............;okkkd;....
    ///     .,,,,,,,,,,,,,,,,,,,,,............;cll;.....
    ///       ,,,,,,,,,,,,,,,,,,,,'....................:d,
    ///        ,,,,,,,,,,,,,,,,,,,....................oxxx:
    ///         .,,,,,,,,,,,,,,,,,'..................oxxxxx.  .
    ///          .,,,,,,,,,,,,,,'..........        ,oxxxxxxx .
    ///         .;,,,,,,,,,,,,'..                'cxxxxxxxxx,
    ///          :dl:,'',,'....               .;lxxxxxxxxxd;
    ///           ,codol:;'.           ...,;cldxxxxxxxxxoc.
    ///             .:cxxxxxdlccccc:ccldxxxxxxxxxxxxxx::.
    ///               .'::dxxxxxxxxxxxxxxxxxxxxxxxd::'.
    ///                  ..,::cdxxxxxxxxxxxxxdc::,..
    ///                       ...,;:::::::;,...
    /// ```
    pub fn pika_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PIKAOS,
            names: vec!["PikaOS"],
            colors: vec![Color::Yellow],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Pisi distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pisi().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    \Fv/!-                      `:?lzC
    ///    Q!::=zFx!  `;v6WBCicl;`  ,vCC\!::#.
    ///   ,%:::,'` +#%@@FQ@@.   ,cF%i``-',::a?
    ///   +m:,'```}3,/@@Q\@@       "af- `-'"7f
    ///   =o'.` /m'   :Q@:Qg         ,kl  `.|o
    ///   :k` '+      'Narm           >d,  ii
    ///    #`!p.        `C ,            'd+ %'
    ///    !0m                           `6Kv
    ///    =a                              m+
    ///   !A     !\L|:            :|L\!     :
    ///  .8`     Q''%Q#'        '#Q%''Q     `0-
    ///  :6      E|.6QQu        uQQ6.|E      p:
    ///   i{      \jts9?        ?9stj\      u\
    ///    |a`            -''.            `e>
    ///     ,m+     '^ !`s@@@@a'"`+`     >e'
    ///       !3|`|=>>r-  'U%:  '>>>=:`\3!
    ///        'xopE|      `'     `ledoz-
    ///     `;=>>+``^llci/|==|/iclc;`'>>>>:
    ///    `^`+~          ````          !!-^
    /// ```
    pub fn pisi() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PISI,
            names: vec!["Pisi"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PNM Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pnm_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                ``.---..` `--`
    ///             ``.---........-:.-::`
    ///            ./::-........--::.````
    ///           .:://:::----::::-..
    ///           ..--:::::--::::++-.`
    ///   `-:-`   .-ohy+::-:::/sdmdd:.   `-:-
    ///    .-:::...sNNmdsyo/+sy+NNmd+.`-:::-.
    ///      `.-:-./dN()yyooosd()mdy-.::-.`
    ///       `.-...-+hNdyyyyyydmy:......`
    ///  ``..--.....-yNNmhsssshmmdo.........```
    /// `-:://:.....hNNNNNmddmNNNmds.....//::--`
    ///   ```.:-...oNNNNNNNNNNNNNNmd/...:-.```
    ///       .....hNNNNNNNNNNNNNNmds....`
    ///       --...hNNNNNNNNNNNNNNmdo.....
    ///       .:.../NNNNNNNNNNNNNNdd:....`
    ///        `-...+mNNNNNNNNNNNmh:...-.
    ///      .:+o+/:-:+oo+///++o+/:-:/+ooo/:.
    ///        +oo/:o-            +oooooso.`
    ///        .`   `             `/  .-//-
    /// ```
    pub fn pnm_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PNM_LINUX,
            names: vec!["PNM Linux"],
            colors: vec![
                Color::Blue,
                Color::Red,
                Color::White,
                Color::TrueColor {
                    r: 0xff,
                    g: 0x5f,
                    b: 0,
                },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Pop distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pop().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              /////////////
    ///          /////////////////////
    ///       ///////*767////////////////
    ///     //////7676767676*//////////////
    ///    /////76767//7676767//////////////
    ///   /////767676///*76767///////////////
    ///  ///////767676///76767.///7676*///////
    /// /////////767676//76767///767676////////
    /// //////////76767676767////76767/////////
    /// ///////////76767676//////7676//////////
    /// ////////////,7676,///////767///////////
    /// /////////////*7676///////76////////////
    /// ///////////////7676////////////////////
    ///  ///////////////7676///767////////////
    ///   //////////////////////'////////////
    ///    //////.7676767676767676767,//////
    ///     /////767676767676767676767/////
    ///       ///////////////////////////
    ///          /////////////////////
    ///              /////////////
    /// ```
    pub fn pop() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_POP,
            names: vec!["pop", "popos", "pop_os", "pop-linux"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Pop distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pop_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ______
    /// \   _ \        __
    ///  \ \ \ \      / /
    ///   \ \_\ \    / /
    ///    \  ___\  /_/
    ///     \ \    _
    ///    __\_\__(_)_
    ///   (___________)`
    /// ```
    pub fn pop_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_POP_SMALL,
            names: vec![
                "pop_small",
                "popos_small",
                "pop_os_small",
                "pop-linux-small",
            ],
            colors: vec![Color::Cyan],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Porteus distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::porteus().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `.-:::-.`
    ///          -+ydmNNNNNNNmdy+-
    ///       .+dNmdhs+//////+shdmdo.
    ///     .smmy+-`             ./sdy:
    ///   `omdo.    `.-/+osssso+/-` `+dy.
    ///  `yms.   `:shmNmdhsoo++osyyo-``oh.
    ///  hm/   .odNmds/.`    ``.....:::-+s
    /// /m:  `+dNmy:`   `./oyhhhhyyooo++so
    /// ys  `yNmy-    .+hmmho:-.`     ```
    /// s:  yNm+`   .smNd+.
    /// `` /Nm:    +dNd+`
    ///    yN+   `smNy.
    ///    dm    oNNy`
    ///    hy   -mNm.
    ///    +y   oNNo
    ///    `y`  sNN:
    ///     `:  +NN:
    ///      `  .mNo
    ///          /mm`
    ///           /my`
    ///            .sy`
    ///              .+:
    ///                 `
    /// ```
    pub fn porteus() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PORTEUS,
            names: vec!["Porteus"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PostMarketOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::post_market_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  /\
    ///                 /  \
    ///                /    \
    ///               /      \
    ///              /        \
    ///             /          \
    ///             \           \
    ///           /\ \____       \
    ///          /  \____ \       \
    ///         /       /  \       \
    ///        /       /    \    ___\
    ///       /       /      \  / ____
    ///      /       /        \/ /    \
    ///     /       / __________/      \
    ///    /        \ \                 \
    ///   /          \ \                 \
    ///  /           / /                  \
    /// /___________/ /____________________\
    /// ```
    pub fn post_market_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_POSTMARKETOS,
            names: vec!["PostMarketOS"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of PostMarketOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::post_market_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         /\
    ///        /  \
    ///       /    \
    ///       \__   \
    ///     /\__ \  _\
    ///    /   /  \/ __
    ///   /   / ____/  \
    ///  /    \ \       \
    /// /_____/ /________\
    /// ```
    pub fn post_market_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_POSTMARKETOS_SMALL,
            names: vec!["PostMarketOS_small"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Proxmox distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::proxmox().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          .://:`              `://:.
    ///        `hMMMMMMd/          /dMMMMMMh`
    ///         `sMMMMMMMd:      :mMMMMMMMs`
    /// `-/+oo+/:`.yMMMMMMMh-  -hMMMMMMMy.`:/+oo+/-`
    /// `:oooooooo/`-hMMMMMMMyyMMMMMMMh-`/oooooooo:`
    ///   `/oooooooo:`:mMMMMMMMMMMMMm:`:oooooooo/`
    ///     ./ooooooo+- +NMMMMMMMMN+ -+ooooooo/.
    ///       .+ooooooo+-`oNMMMMNo`-+ooooooo+.
    ///         -+ooooooo/.`sMMs`./ooooooo+-
    ///           :oooooooo/`..`/oooooooo:
    ///           :oooooooo/`..`/oooooooo:
    ///         -+ooooooo/.`sMMs`./ooooooo+-
    ///       .+ooooooo+-`oNMMMMNo`-+ooooooo+.
    ///     ./ooooooo+- +NMMMMMMMMN+ -+ooooooo/.
    ///   `/oooooooo:`:mMMMMMMMMMMMMm:`:oooooooo/`
    /// `:oooooooo/`-hMMMMMMMyyMMMMMMMh-`/oooooooo:`
    /// `-/+oo+/:`.yMMMMMMMh-  -hMMMMMMMy.`:/+oo+/-`
    ///         `sMMMMMMMm:      :dMMMMMMMs`
    ///        `hMMMMMMd/          /dMMMMMMh`
    ///          `://:`              `://:`
    /// ```
    pub fn proxmox() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PROXMOX,
            names: vec!["Proxmox", "pve"],
            colors: vec![
                Color::White,
                Color::TrueColor {
                    r: 0xff,
                    g: 0x5f,
                    b: 0,
                },
            ],
            color_keys: Some(Color::White),
            color_title: Some(Color::TrueColor {
                r: 0xff,
                g: 0x5f,
                b: 0,
            }),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PuffOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::puff_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               _,..._,m,
    ///             ,/'      '"";
    ///            /             ".
    ///          ,'mmmMMMMmm.      \
    ///        _/-"^^^^^"""%#%mm,   ;
    ///  ,m,_,'              "###)  ;,
    /// (###%                 \#/  ;##mm.
    ///  ^#/  __        ___    ;  (######)
    ///   ;  //.\\     //.\\   ;   \####/
    ///  _; (#\"//     \\"/#)  ;  ,/
    /// @##\ \##/   =   `"=" ,;mm/
    /// `\##>.____,...,____,<####@
    /// ```
    pub fn puff_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PUFFOS,
            names: vec!["PuffOs"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Puppy distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::puppy().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            `-/osyyyysosyhhhhhyys+-
    ///   -ohmNNmh+/hMMMMMMMMNNNNd+dMMMMNM+
    ///  yMMMMNNmmddo/NMMMNNNNNNNNNo+NNNNNy
    /// .NNNNNNmmmddds:MMNNNNNNNNNNNh:mNNN/
    /// -NNNdyyyhdmmmd`dNNNNNmmmmNNmdd/os/
    /// .Nm+shddyooo+/smNNNNmmmmNh.   :mmd.
    ///  NNNNy:`   ./hmmmmmmmNNNN:     hNMh
    ///  NMN-    -++- +NNNNNNNNNNm+..-sMMMM-
    /// .MMo    oNNNNo hNNNNNNNNmhdNNNMMMMM+
    /// .MMs    /NNNN/ dNmhs+:-`  yMMMMMMMM+
    ///  mMM+     .. `sNN+.      hMMMMhhMMM-
    ///  +MMMmo:...:sNMMMMMms:` hMMMMm.hMMy
    ///   yMMMMMMMMMMMNdMMMMMM::/+o+//dMMd`
    ///    sMMMMMMMMMMN+:oyyo:sMMMNNMMMNy`
    ///     :mMMMMMMMMMMMmddNMMMMMMMMmh/
    ///       /dMMMMMMMMMMMMMMMMMMNdy/`
    ///         .+hNMMMMMMMMMNmdhs/.
    ///             .:/+ooo+/:-.
    /// ```
    pub fn puppy() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PUPPY,
            names: vec!["Puppy"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of PureOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pure_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// dmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmd
    /// dNm//////////////////////////////////mNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNm//////////////////////////////////mNd
    /// dmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmd
    /// ```
    pub fn pure_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PUREOS,
            names: vec!["PureOS"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of PureOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::pure_os_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  _____________
    /// |  _________  |
    /// | |         | |
    /// | |         | |
    /// | |_________| |
    /// |_____________|
    /// ```
    pub fn pure_os_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_PUREOS_SMALL,
            names: vec!["PureOS_small"],
            colors: vec![Color::Green, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Q4DOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::q4dos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .:*****  :=====.
    ///         .:********  :========.
    ///       .***********  :===========.
    ///     .:************  :============-
    ///    .**************  :==============
    ///   :***************  :===============
    ///  :**************.    :===============
    /// .*************:        .=============.
    /// *************.          .============:
    ///
    /// :############.          :==:
    /// :##############.      :======:
    ///  :################  .==========:
    ///   :###############   .===========:
    ///    :##############     .===========:
    ///     :#############       .=========:
    ///       :###########         .=====:
    ///         .#########           .=:
    ///             .#####
    /// ```
    pub fn q4dos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_Q4OS,
            names: vec!["Q4DOS"],
            colors: vec![Color::Blue, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Qubes distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::qubes().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                `..--..`
    ///             `.----------.`
    ///         `..----------------..`
    ///      `.------------------------.``
    ///  `..-------------....-------------..`
    /// .::----------..``    ``..----------:+:
    /// :////:----..`            `..---:/ossso
    /// :///////:`                  `/osssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :////////-`                .:sssssssso
    /// :///////////-.`        `-/osssssssssso
    /// `//////////////:-```.:+ssssssssssssso-
    ///   .-://////////////sssssssssssssso/-`
    ///      `.:///////////sssssssssssssso:.
    ///          .-:///////ssssssssssssssssss/`
    ///             `.:////ssss+/+ssssssssssss.
    ///                 `--//-    `-/osssso/.
    /// ```
    pub fn qubes() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_QUBES,
            names: vec!["Qubes"],
            colors: vec![Color::Blue, Color::Magenta],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Qubyt distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::qubyt().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     ########################(ooo
    ///     ########################(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo           ##o    ((((ooo
    /// ###(ooo          o((###   oooooo
    /// ###(ooo           oo((###o
    /// ###(ooo             ooo((###
    /// ################(oo    oo((((o
    /// (((((((((((((((((ooo     ooooo
    ///   oooooooooooooooooo        o
    /// ```
    pub fn qubyt() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_QUBYT,
            names: vec!["Qubyt"],
            colors: vec![Color::Blue, Color::Magenta, Color::Black],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Quibian distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::quibian().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             `.--::::::::--.`
    ///         `.-:::-..``   ``..-::-.`
    ///       .::::-`   .+:``       `.-::.`
    ///     .::::.`    -::::::-`       `.::.
    ///   `-:::-`    -:::::::::--..``     .::`
    ///  `::::-     .oy:::::::---.```.:    `::`
    ///  -::::  `.-:::::::::::-.```         `::
    /// .::::.`-:::::::::::::.               `:.
    /// -::::.:::::::::::::::                 -:
    /// ::::::::::::::::::::`                 `:
    /// :::::::::::::::::::-                  `:
    /// :::::::::::::::::::                   --
    /// .:::::::::::::::::`                  `:`
    /// `:::::::::::::::::                   -`
    ///  .:::::::::::::::-                  -`
    ///   `::::::::::::::-                `.`
    ///     .::::::::::::-               ``
    ///       `.--:::::-.
    /// ```
    pub fn quibian() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_QUIBIAN,
            names: vec!["Quibian"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Radix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::radix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .:oyhdmNo
    ///              `/yhyoosdms`
    ///             -o+/ohmmho-
    ///            ..`.:/:-`
    ///      `.--:::-.``
    ///   .+ydNMMMMMMNmhs:`
    /// `omMMMMMMMMMMMMMMNh-
    /// oNMMMNmddhhyyhhhddmy.
    /// mMMMMNmmddhhysoo+/:-`
    /// yMMMMMMMMMMMMMMMMNNh.
    /// -dmmmmmNNMMMMMMMMMMs`
    ///  -+oossyhmMMMMMMMMd-
    ///  `sNMMMMMMMMMMMMMm:
    ///   `yMMMMMMNmdhhhh:
    ///    `sNMMMMMNmmho.
    ///     `+mMMMMMMMy.
    ///       .yNMMMm+`
    ///        `:yd+.
    /// ```
    pub fn radix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RADIX,
            names: vec!["Radix"],
            colors: vec![Color::Green, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Raspbian distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::raspbian().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    `.::///+:/-.        --///+//-:`
    ///  `+oooooooooooo:   `+oooooooooooo:
    ///   /oooo++//ooooo:  ooooo+//+ooooo.
    ///   `+ooooooo:-:oo-  +o+::/ooooooo:
    ///    `:oooooooo+``    `.oooooooo+-
    ///      `:++ooo/.        :+ooo+/.`
    ///         ...`  `.----.` ``..
    ///      .::::-``:::::::::.`-:::-`
    ///     -:::-`   .:::::::-`  `-:::-
    ///    `::.  `.--.`  `` `.---.``.::`
    ///        .::::::::`  -::::::::` `
    ///  .::` .:::::::::- `::::::::::``::.
    /// -:::` ::::::::::.  ::::::::::.`:::-
    /// ::::  -::::::::.   `-::::::::  ::::
    /// -::-   .-:::-.``....``.-::-.   -::-
    ///  .. ``       .::::::::.     `..`..
    ///    -:::-`   -::::::::::`  .:::::`
    ///    :::::::` -::::::::::` :::::::.
    ///    .:::::::  -::::::::. ::::::::
    ///     `-:::::`   ..--.`   ::::::.
    ///       `...`  `...--..`  `...`
    ///             .::::::::::
    ///              `.-::::-`
    /// ```
    pub fn raspbian() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RASPBIAN,
            names: vec!["raspbian", "raspi", "raspberrypi", "raspberrypios"],
            colors: vec![Color::Red, Color::Green],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Raspbian distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::raspbian_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    .~~.   .~~.
    ///   '. \ ' ' / .'
    ///    .~ .~~~..~.
    ///   : .~.'~'.~. :
    ///  ~ (   ) (   ) ~
    /// ( : '~'.~.'~' : )
    ///  ~ .~ (   ) ~. ~
    ///   (  : '~' :  )
    ///    '~ .~~~. ~'
    ///        '~'
    /// ```
    pub fn raspbian_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RASPBIAN_SMALL,
            names: vec![
                "raspbian_small",
                "raspi_small",
                "raspberrypi_small",
                "raspberrypios_small",
            ],
            colors: vec![Color::Red, Color::Green],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of RavynOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ravyn_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 ..oooo..
    ///            .oo.
    ///         odo
    ///       oo
    ///     ..
    ///    d********b
    ///   d*            °****?b
    ///   *                     °
    ///  d**                     .oob
    ///  *°                     o
    ///                       o
    ///                     oP
    ///                     *
    ///                       ?P
    ///                        P
    ///                        P
    ///                       ?*
    ///                        *°
    ///                       d*°
    ///                        °
    /// ```
    pub fn ravyn_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RAVYNOS,
            names: vec!["RavynOS"],
            colors: vec![
                Color::TrueColor {
                    r: 0xff,
                    g: 0xff,
                    b: 0xff,
                },
                Color::White,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Reborn distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::reborn().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           .======================.          
    ///          .##*********%%*********#%:         
    ///         :%#**********%%**********#%-        
    ///        -%************%%************%=       
    ///       +%******%%#####%%#####%%******%+      
    ///      *%%#****%#+=====%%=====+#%****#%%*     
    ///     *%*##%%#%#====+++%%+++====#%#%%##*##.   
    ///   .##*****#%%%#*++%######%*+*#%%%#*****#%.  
    ///  :%#*****#%*=+*#%%*++++++*%%#*+=*%#*****#%:
    /// -%#*****#%+====*%*++++++++*%#====+%#******%-
    /// -%#*****#%+====*%*++++++++*%#====+%#******%=
    ///  :%#*****#%*=+*#%%*++++++*%%#*+=*%#*****#%-
    ///   .##*****#%%%#*+*%######%*+*#%%%#*****#%:  
    ///    .##**#%%#%#====+++%%+++====#%#%%##*##.   
    ///      *%%#****%#+=====%%=====+#%****#%%*     
    ///       +%******%%#####%%#####%%******%*      
    ///        -%************%%************%=       
    ///         :%#**********%%**********#%-        
    ///          :%#*********%%*********#%:         
    ///           .======================.   
    /// ```
    pub fn reborn() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REBORN,
            names: vec![
                "Reborn",
                "Reborn OS",
                "reborn-os",
                "rebornos",
                "rebornos-linux",
                "reborn-os-linux",
            ],
            colors: vec![Color::Black, Color::Blue, Color::Cyan],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Reborn distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::reborn_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    _______   
    ///   /\_____/\  
    ///  / /\___/\ \
    /// /_/_/   \_\_\
    /// \ \ \___/ / /
    ///  \ \/___\/ /
    ///   \/_____\/  
    /// ```
    pub fn reborn_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REBORN_SMALL,
            names: vec![
                "Reborn_small",
                "Reborn OS_small",
                "reborn-os-small",
                "rebornos_small",
                "rebornos-linux-small",
                "reborn-os-linux-small",
            ],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of RedCore distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::red_core().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  RRRRRRRRR
    ///                RRRRRRRRRRRRR
    ///         RRRRRRRRRR      RRRRR
    ///    RRRRRRRRRRRRRRRRRRRRRRRRRRR
    ///  RRRRRRR  RRR         RRR RRRRRRRR
    /// RRRRR    RR                 RRRRRRRRR
    /// RRRR    RR     RRRRRRRR      RR RRRRRR
    /// RRRR   R    RRRRRRRRRRRRRR   RR   RRRRR
    /// RRRR   R  RRRRRRRRRRRRRRRRRR  R   RRRRR
    /// RRRR     RRRRRRRRRRRRRRRRRRR  R   RRRR
    ///  RRR     RRRRRRRRRRRRRRRRRRRR R   RRRR
    ///   RRR    RRRRRRRRRRRRRRRRRRRR    RRRR
    ///     RR   RRRRRRRRRRRRRRRRRRR    RRR
    ///      RR   RRRRRRRRRRRRRRRRR    RRR
    ///        RR   RRRRRRRRRRRRRR   RR
    ///          R       RRRR      RR
    /// ```
    pub fn red_core() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REDCORE,
            names: vec!["RedCore"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of RHEL distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::red_hat_enterprise_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .MMM..:MMMMMMM
    ///           MMMMMMMMMMMMMMMMMM
    ///           MMMMMMMMMMMMMMMMMMMM.
    ///          MMMMMMMMMMMMMMMMMMMMMM
    ///         ,MMMMMMMMMMMMMMMMMMMMMM:
    ///         MMMMMMMMMMMMMMMMMMMMMMMM
    ///   .MMMM'  MMMMMMMMMMMMMMMMMMMMMM
    ///  MMMMMM    `MMMMMMMMMMMMMMMMMMMM.
    /// MMMMMMMM      MMMMMMMMMMMMMMMMMM .
    /// MMMMMMMMM.       `MMMMMMMMMMMMM' MM.
    /// MMMMMMMMMMM.                     MMMM
    /// `MMMMMMMMMMMMM.                 ,MMMMM.
    ///  `MMMMMMMMMMMMMMMMM.          ,MMMMMMMM.
    ///     MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    ///       MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:
    ///          MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    ///             `MMMMMMMMMMMMMMMMMMMMMMMM:
    ///                 ``MMMMMMMMMMMMMMMMM'
    /// ```
    pub fn red_hat_enterprise_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RHEL,
            names: vec!["rhel", "redhat", "redhat-linux"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns old logo of RHEL distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::red_hat_enterprise_linux_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              `.-..........`
    ///             `////////::.`-/.
    ///             -: ....-////////.
    ///             //:-::///////////`
    ///      `--::: `-://////////////:
    ///      //////-    ``.-:///////// .`
    ///      `://////:-.`    :///////::///:`
    ///        .-/////////:---/////////////:
    ///           .-://////////////////////.
    ///          yMN+`.-::///////////////-`
    ///       .-`:NMMNMs`  `..-------..`
    ///        MN+/mMMMMMhoooyysshsss
    /// MMM    MMMMMMMMMMMMMMyyddMMM+
    ///  MMMM   MMMMMMMMMMMMMNdyNMMh`     hyhMMM
    ///   MMMMMMMMMMMMMMMMyoNNNMMM+.   MMMMMMMM
    ///    MMNMMMNNMMMMMNM+ mhsMNyyyyMNMMMMsMM
    /// ```
    pub fn red_hat_enterprise_linux_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RHEL_OLD,
            names: vec!["rhel_old", "redhat_old", "redhat-linux_old"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Alter,
        }
    }
    /// returns logo of RedstarOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::redstar_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     ..
    ///                   .oK0l
    ///                  :0KKKKd.
    ///                .xKO0KKKKd
    ///               ,Od' .d0000l
    ///              .c;.   .'''...           ..'.
    /// .,:cloddxxxkkkkOOOOkkkkkkkkxxxxxxxxxkkkx:
    /// ;kOOOOOOOkxOkc'...',;;;;,,,'',;;:cllc:,.
    ///  .okkkkd,.lko  .......',;:cllc:;,,'''''.
    ///    .cdo. :xd' cd:.  ..';'',,,'',,;;;,'.
    ///       . .ddl.;doooc'..;oc;'..';::;,'.
    ///         coo;.oooolllllllcccc:'.  .
    ///        .ool''lllllccccccc:::::;.
    ///        ;lll. .':cccc:::::::;;;;'
    ///        :lcc:'',..';::::;;;;;;;,,.
    ///        :cccc::::;...';;;;;,,,,,,.
    ///        ,::::::;;;,'.  ..',,,,'''.
    ///         ........          ......
    /// ```
    pub fn redstar_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REDSTAR,
            names: vec![
                "redstar",
                "redstar-os",
                "redstaros",
                "redstaros-linux",
                "redstar-os-linux",
            ],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Refracted Devuan distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::refracted_devuan().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                              A
    ///                             VW
    ///                            VVW\\
    ///                          .yWWW\\
    ///  ,;,,u,;yy;;v;uyyyyyyy  ,WWWWW^
    ///     *WWWWWWWWWWWWWWWW/  VWWWWw      ,
    ///         ^*%WWWWWWVWWX  WWWW**    ,yy
    ///         ,    "**WWW/' **'   ,yy/WWW*`
    ///        &WWWWwy    `*`  <,ywWW%VWWW*
    ///      yWWWWWWWWWW*    .,   "**WW%W
    ///    ,&WWWWWM*"`  ,y/  &WWWww   ^*
    ///   XWWX*^   ,yWWWW09 .WWWWWWWWwy,
    ///  *`        &WWWWWM  WWWWWWWWWWWWWww,
    ///            (WWWWW` /#####WWW***********
    ///            ^WWWW
    ///             VWW
    ///             Wh.
    ///             V/
    /// ```
    pub fn refracted_devuan() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REFRACTED_DEVUAN,
            names: vec!["Refracted Devuan", "refracted-devuan"],
            colors: vec![Color::White, Color::Black],
            color_keys: Some(Color::Black),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Regata distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::regata().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ddhso+++++osydd
    ///         dho/.`hh.:/+/:.hhh`:+yd
    ///       do-hhhhhh/sssssss+`hhhhh./yd
    ///     h/`hhhhhhh-sssssssss:hhhhhhhh-yd
    ///   do`hhhhhhhhh`ossssssso.hhhhhhhhhh/d
    ///  d/hhhhhhhhhhhh`/ossso/.hhhhhhhhhhhh.h
    ///  /hhhhhhhhhhhh`-/osyso/-`hhhhhhhhhhhh.h
    /// shh-/ooo+-hhh:syyso+osyys/`hhh`+oo`hhh/
    /// h`ohhhhhhho`+yyo.hhhhh.+yyo`.sssssss.h`h
    /// s:hhhhhhhhhoyys`hhhhhhh.oyy/ossssssso-hs
    /// s.yhhhhhhhy/yys`hhhhhhh.oyy/ossssssso-hs
    /// hh./syyys+. +yy+.hhhhh.+yyo`.ossssso/h`h
    /// shhh``.`hhh`/syyso++oyys/`hhh`+++-`hh:h
    /// d/hhhhhhhhhhhh`-/osyso+-`hhhhhhhhhhhh.h
    ///  d/hhhhhhhhhhhh`/ossso/.hhhhhhhhhhhh.h
    ///   do`hhhhhhhhh`ossssssso.hhhhhhhhhh:h
    ///     h/`hhhhhhh-sssssssss:hhhhhhhh-yd
    ///       h+.hhhhhh+sssssss+hhhhhh`/yd
    ///         dho:.hhh.:+++/.hhh`-+yd
    ///             ddhso+++++osyhd
    /// ```
    pub fn regata() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REGATA,
            names: vec!["Regata"],
            colors: vec![
                Color::White,
                Color::Red,
                Color::Blue,
                Color::Magenta,
                Color::Yellow,
                Color::Green,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Regolith distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::regolith().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ``....```
    ///             `.:/++++++/::-.`
    ///           -/+++++++:.`
    ///         -++++++++:`
    ///       `/++++++++-
    ///      `/++++++++.                    -/+/
    ///      /++++++++/             ``   .:+++:.
    ///     -+++++++++/          ./++++:+++/-`
    ///     :+++++++++/         `+++++++/-`
    ///     :++++++++++`      .-/+++++++`
    ///    `:++++++++++/``.-/++++:-:::-`      `
    ///  `:+++++++++++++++++/:.`            ./`
    /// :++/-:+++++++++/:-..              -/+.
    /// +++++++++/::-...:/+++/-..````..-/+++.
    /// `......``.::/+++++++++++++++++++++/.
    ///          -/+++++++++++++++++++++/.
    ///            .:/+++++++++++++++/-`
    ///               `.-:://////:-.
    /// ```
    pub fn regolith() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_REGOLITH,
            names: vec!["Regolith"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of RhaymOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::rhaym_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     ###
    ///                    #####
    ///
    ///               #######        /########
    ///            #############   ###########
    ///    ,###########      ####  ####(..
    ///   ####   ####       ####*  ##########
    ///   ####    #####     #####         (####
    ///   ####      ###########    ###########
    ///   ####       #########     ##########
    ///
    ///   ###################################
    ///  #####################################
    /// #######################################
    /// ```
    pub fn rhaym_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_RHAYMOS,
            names: vec!["RhaymOS"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Rocky Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::rocky_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           __wgliliiligw_,
    ///        _williiiiiiliilililw,
    ///      _%iiiiiilililiiiiiiiiiii_
    ///    .Qliiiililiiiiiiililililiilm.
    ///   _iiiiiliiiiiililiiiiiiiiiiliil,
    ///  .lililiiilililiiiilililililiiiii,
    /// _liiiiiiliiiiiiiliiiiiF{iiiiiilili,
    /// jliililiiilililiiili@`  ~ililiiiiiL
    /// iiiliiiiliiiiiiili>`      ~liililii
    /// liliiiliiilililii`         -9liiiil
    /// iiiiiliiliiiiii~             "4lili
    /// 4ililiiiiilil~|      -w,       )4lf
    /// -liiiiililiF'       _liig,       )'
    ///  )iiiliii@`       _QIililig,
    ///   )iiii>`       .Qliliiiililw
    ///    )<>~       .mliiiiiliiiiiil,
    ///             _gllilililiililii~
    ///            giliiiiiiiiiiiiT`
    ///           -^~ililili@~~'
    /// ```
    pub fn rocky_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ROCKY,
            names: vec!["rocky", "rocky-linux", "rockylinux"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Rocky Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::rocky_linux_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     `-/+++++++++/-.`
    ///  `-+++++++++++++++++-`
    /// .+++++++++++++++++++++.
    /// -+++++++++++++++++++++++.
    /// +++++++++++++++/-/+++++++
    /// +++++++++++++/.   ./+++++
    /// +++++++++++:.       ./+++
    /// +++++++++:`   `:/:`   .:/
    /// -++++++:`   .:+++++:`
    ///  .+++-`   ./+++++++++:`
    ///   `-`   ./+++++++++++-
    ///        -+++++++++:-.`
    /// ```
    pub fn rocky_linux_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ROCKY_SMALL,
            names: vec!["rocky_small", "rocky-linux_small", "rockylinux_small"],
            colors: vec![Color::Green],
            color_keys: Some(Color::Green),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Rosa Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::rosa_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            ROSAROSAROSAROSAR
    ///         ROSA               AROS
    ///       ROS   SAROSAROSAROSAR   AROS
    ///     RO   ROSAROSAROSAROSAROSAR   RO
    ///   ARO  AROSAROSAROSARO      AROS  ROS
    ///  ARO  ROSAROS         OSAR   ROSA  ROS
    ///  RO  AROSA   ROSAROSAROSA    ROSAR  RO
    /// RO  ROSAR  ROSAROSAROSAR  R  ROSARO  RO
    /// RO  ROSA  AROSAROSAROSA  AR  ROSARO  AR
    /// RO AROS  ROSAROSAROSA   ROS  AROSARO AR
    /// RO AROS  ROSAROSARO   ROSARO  ROSARO AR
    /// RO  ROS  AROSAROS   ROSAROSA AROSAR  AR
    /// RO  ROSA  ROS     ROSAROSAR  ROSARO  RO
    ///  RO  ROS     AROSAROSAROSA  ROSARO  AR
    ///  ARO  ROSA   ROSAROSAROS   AROSAR  ARO
    ///   ARO  OROSA      R      ROSAROS  ROS
    ///     RO   AROSAROS   AROSAROSAR   RO
    ///      AROS   AROSAROSAROSARO   AROS
    ///         ROSA               SARO
    ///            ROSAROSAROSAROSAR
    /// ```
    pub fn rosa_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ROSA,
            names: vec!["rosa", "rosa-linux", "rosalinux"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Sabayon distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::sabayon().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             ...........
    ///          ..             ..
    ///       ..                   ..
    ///     ..           o           ..
    ///   ..            :W'            ..
    ///  ..             .d.             ..
    /// :.             .KNO              .:
    /// :.             cNNN.             .:
    /// :              dXXX,              :
    /// :   .          dXXX,       .cd,   :
    /// :   'kc ..     dKKK.    ,ll;:'    :
    /// :     .xkkxc;..dkkkc',cxkkl       :
    /// :.     .,cdddddddddddddo:.       .:
    ///  ..         :lllllll:           ..
    ///    ..         ',,,,,          ..
    ///      ..                     ..
    ///         ..               ..
    ///           ...............
    /// ```
    pub fn sabayon() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SABAYON,
            names: vec!["Sabayon"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Sabotage distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::sabotage().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  .|'''.|      |     '||''|.    ..|''||
    ///  ||..  '     |||     ||   ||  .|'    ||
    ///   ''|||.    |  ||    ||'''|.  ||      ||
    /// .     '||  .''''|.   ||    || '|.     ||
    /// |'....|'  .|.  .||. .||...|'   ''|...|'
    ///
    /// |''||''|     |      ..|'''.|  '||''''|
    ///    ||       |||    .|'     '   ||  .
    ///    ||      |  ||   ||    ....  ||''|
    ///    ||     .''''|.  '|.    ||   ||
    ///   .||.   .|.  .||.  ''|...'|  .||.....|
    /// ```
    pub fn sabotage() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SABOTAGE,
            names: vec!["Sabotage"],
            colors: vec![Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Sailfish distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::sailfish().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               _a@b
    ///            _#b (b
    ///          _@@   @_         _,
    ///        _#^@ _#*^^*gg,aa@^^
    ///        #- @@^  _a@^^
    ///        @_  *g#b
    ///        ^@_   ^@_
    ///          ^@_   @
    ///           @(b (b
    ///          #b(b#^
    ///        _@_#@^
    ///     _a@a*^
    /// ,a@*^
    /// ```
    pub fn sailfish() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SAILFISH,
            names: vec!["Sailfish"],
            colors: vec![Color::Blue, Color::Magenta],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SalentOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::salent_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  ``..``
    ///         .-:+oshdNMMMMMMNdhyo+:-.`
    ///   -oydmMMMMMMMMMMMMMMMMMMMMMMMMMMNdhs/
    ///  +hdddmNMMMMMMMMMMMMMMMMMMMMMMMMNmdddh+`
    /// `MMMMMNmdddddmMMMMMMMMMMMMmdddddmNMMMMM-
    ///  mMMMMMMMMMMMNddddhyyhhdddNMMMMMMMMMMMM`
    ///  dMMMMMMMMMMMMMMMMMooMMMMMMMMMMMMMMMMMN`
    ///  yMMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMMd
    ///  +MMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMMy
    ///  :MMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMMo
    ///  .MMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMM/
    ///  `NMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMM-
    ///   mMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMN`
    ///   hMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMm
    ///   /MMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMy
    ///    .+hMMMMMMMMMMMMMhhMMMMMMMMMMMMMms:
    ///       `:smMMMMMMMMMhhMMMMMMMMMNh+.
    ///           .+hMMMMMMhhMMMMMMdo:
    ///              `:smMMyyMMNy/`
    ///                  .- `:.
    /// ```
    pub fn salent_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SALENTOS,
            names: vec!["SalentOS"],
            colors: vec![Color::Green, Color::Red, Color::Yellow, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SalientOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::salient_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               00xxxx0
    ///           00xxxxxx0
    ///        0xxxxxxxxx         000000
    ///      0xxxxxxxxxx        xxxxxxxxxx0
    ///    0xxxxxxxxxxx0       xxxxxxxxxxxxx0
    ///   0xxxxxxxxxxxx0      0xxxxxxxxxxxxxx0
    ///  0xxxxxxxxxxxxx0      0xxxxxxxxxxxxxxx0
    /// 0xxxxxxxxxxxxxxx       xxxxxxxxxxxxxxxx0
    /// xxxxxxxxxxxxxxxx0      0xxxxxxxxxxxxxxxx
    /// xxxxxxxxxxxxxxxxx       0xxxxxxxxxxxxxxx
    /// xxxxxxxxxxxxxxxxxx       xxxxxxxxxxxxxxx
    /// xxxxxxxxxxxxxxxxxx0       xxxxxxxxxxxxxx
    /// 0xxxxxxxxxxxxxxxxxx0      0xxxxxxxxxxxx0
    ///  0xxxxxxxxxxxxxxxxxx       xxxxxxxxxxx0
    ///   0xxxxxxxxxxxxxxxxx       xxxxxxxxxx0
    ///    0xxxxxxxxxxxxxxxx       xxxxxxxxx0
    ///      0xxxxxxxxxxxx0       0xxxxxxx0
    ///         0xxxxxxx0         xxxxxx0
    ///                         0xxx00
    ///                       x00
    /// ```
    pub fn salient_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SALIENTOS,
            names: vec!["Salient OS", "SalientOS"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Salix distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::salix().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               __s_aaaaaaaaauuoXSSSSSSSS:
    ///           ._xSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///         _aSSSSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///       _xSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///      <XSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///     -"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
    ///   .ssssssssssssssssssssssssssssssssssss
    ///   {SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSl
    ///   oSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS;
    ///  :XSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS;
    ///  {SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
    ///  -"""""""""""""""""""""""""""""""""""'
    ///  <assssssssssssssssssssssssssssssss>
    ///  nSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS}
    ///  nSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS}`
    ///  XSSSSSSSSSSSSSSSSSSSSSSSSSSSS"`
    ///  SSSSSSSSSSSSSSSSSSSSSSSSS!"`
    /// -""""""""""""""""""""""`
    /// ```
    pub fn salix() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SALIX,
            names: vec!["Salix"],
            colors: vec![Color::Green, Color::Green],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SambaBox distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::samba_box().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                     #
    ///                *////#####
    ///            /////////#########(
    ///       .((((((/////    ,####(#(((((
    ///   /#######(((*             (#(((((((((.
    /// //((#(#(#,        ((##(        ,((((((//
    /// //////        #(##########(       //////
    /// //////    ((#(#(#(#(##########(/////////
    /// /////(    (((((((#########(##((((((/////
    /// /(((#(                             ((((/
    /// ####(#                             ((###
    /// #########(((/////////(((((((((,    (#(#(
    /// ########(   /////////(((((((*      #####
    /// ####///,        *////(((         (((((((
    /// .///////////                .//(((((((((
    ///      ///////////,       *(/////((((*
    ///          ,/(((((((((##########/.
    ///              .((((((#######
    ///                   ((##*
    /// ```
    pub fn samba_box() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SAMBABOX,
            names: vec!["SambaBOX", "Profelis SambaBOX"],
            colors: vec![Color::Yellow, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Sasanqua distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::sasanqua().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                      __,_
    ///              _╕⌐≡µ,√*    º≡,
    ///             ñ     "'       ░
    ///            ╞)         _,   ▒     __
    ///      _,,,_ _Ñ╜^≡µ   ≡'  1µ╕º^el    "%µ
    ///      ∩'     K      Yµ&          1l     ╞)
    ///   ▒     √"        ^Ü          1"     `1µ
    ///   Γ    ║h                  _¿▒∞√;,     ^≡,
    ///    K    ^u_              ⌐*      ╙¥     ╓Ñ
    ///   ⌠        º≡u,,                  ║I    Å
    ///   Ü     _∩"                       ║µ_¿╝"
    ///    Yu_ ▒               ╙º≡_       ║l1µ
    ///       ║l          ,        Y∞µ___≡ª  Γl
    ///        ╓hⁿ╖I     1l         Ñ        ╓Ñ
    ///        Ñ   ¥,___≡1l        ╓Ñ      ¿╕ª
    ///        Ü         ╙L     ¿¿∩ª     ╓P
    ///         ª≡,__      *ⁿ┤ⁿÑⁿ^µ     √ª
    ///               ⁿ≡,,__√╝*    "ⁿⁿ*"
    /// ```
    pub fn sasanqua() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SASANQUA,
            names: vec!["Sasanqua"],
            colors: vec![Color::Magenta, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Scientific distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::scientific().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  =/;;/-
    ///                 +:    //
    ///                /;      /;
    ///               -X        H.
    /// .//;;;:;;-,   X=        :+   .-;:=;:;#;.
    /// M-       ,=;;;#:,      ,:#;;:=,       ,@
    /// :#           :#.=/++++/=.=           #=
    ///  ,#;         #/:+/;,,/++:+/         ;+.
    ///    ,+/.    ,;@+,        ,#H;,    ,/+,
    ///       ;+;;/= @.  .H##X   -X :///+;
    ///       ;+=;;;.@,  .XM@.  =X.//;=#/.
    ///    ,;:      :@#=        =H:     .+#-
    ///  ,#=         #;-///==///-//         =#,
    /// ;+           :#-;;;:;;;;-X-           +:
    /// @-      .-;;;;M-        =M/;;;-.      -X
    ///  :;;::;;-.    #-        :+    ,-;;-;:==
    ///               ,X        H.
    ///                ;/      #=
    ///                 //    +;
    ///                  '////'
    /// ```
    pub fn scientific() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SCIENTIFIC,
            names: vec!["Scientific"],
            colors: vec![Color::Blue, Color::White, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SEMC distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::semc().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             /\
    ///      ______/  \
    ///     /      |()| E M C
    ///    |   (-- |  |
    ///     \   \  |  |
    /// .----)   | |__|
    /// |_______/ / "  \
    ///               "
    ///             "
    /// ```
    pub fn semc() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SEMC,
            names: vec!["semc"],
            colors: vec![Color::Green, Color::BrightBlack, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Septor distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::septor().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssss;okOOOOOOOOOOOOOOko;ssssssssss
    /// sssssssssoNWWWWWWWWWWWWWWWWWWNosssssssss
    /// ssssssss:WWWWWWWWWWWWWWWWWWWWWW:ssssssss
    /// sssssssslWWWWWksssssssssslddddd:ssssssss
    /// sssssssscWWWWWNKKKKKKKKKKKKOx:ssssssssss
    /// yysssssssOWWWWWWWWWWWWWWWWWWWWxsssssssyy
    /// yyyyyyyyyy:kKNNNNNNNNNNNNWWWWWW:yyyyyyyy
    /// yyyyyyyysccccc;yyyyyyyyyykWWWWW:yyyyyyyy
    /// yyyyyyyy:WWWWWWNNNNNNNNNNWWWWWW;yyyyyyyy
    /// yyyyyyyy.dWWWWWWWWWWWWWWWWWWWNdyyyyyyyyy
    /// yyyyyyyyyysdO0KKKKKKKKKKKK0Od;yyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// ```
    pub fn septor() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SEPTOR,
            names: vec!["Septor"],
            colors: vec![Color::Blue, Color::White, Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Serene distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::serene().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               __---''''''---__
    ///           .                      .
    ///         :                          :
    ///       -                       _______----_-
    ///      s               __----'''     __----
    ///  __h_            _-'           _-'     h
    ///  '-._''--.._    ;           _-'         y
    ///   :  ''-._  '-._/        _-'             :
    ///   y       ':_       _--''                y
    ///   m    .--'' '-._.;'                     m
    ///   m   :        :                         m
    ///   y    '.._     '-__                     y
    ///   :        '--._    '''----___           :
    ///    y            '--._         ''-- _    y
    ///     h                '--._          :  h
    ///      s                  __';         vs
    ///       -         __..--''             -
    ///         :_..--''                   :
    ///           .                     _ .
    ///             `''---______---''-``
    /// ```
    pub fn serene() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SERENE,
            names: vec!["Serene"],
            colors: vec![Color::Cyan, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SharkLinux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::shark_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                              `:shd/
    ///                          `:yNMMMMs
    ///                       `-smMMMMMMN.
    ///                     .+dNMMMMMMMMs
    ///                   .smNNMMMMMMMMm`
    ///                 .sNNNNNNNMMMMMM/
    ///               `omNNNNNNNMMMMMMm
    ///              /dNNNNNNNNMMMMMMM+
    ///            .yNNNNNNNNNMMMMMMMN`
    ///           +mNNNNNNNNNMMMMMMMMh
    ///         .hNNNNNNNNNNMMMMMMMMMs
    ///        +mMNNNNNNNNMMMMMMMMMMMs
    ///      .hNMMNNNNMMMMMMMMMMMMMMMd
    ///    .oNNNNNNNNNNMMMMMMMMMMMMMMMo
    /// `:+syyssoo++++ooooossssssssssso:
    /// ```
    pub fn shark_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SHARKLINUX,
            names: vec!["SharkLinux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of ShastraOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::shastra_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                ..,;;,'.
    ///             ':oo.     ;o:
    ///           :o,           ol
    ///         .oo        ..';co:
    ///         ooo',;:looo;
    ///     .;lddl
    ///   cx   .xl     .c:'
    ///  dd     xx     xx ,d;
    /// .xd     cx.    xx   dd.
    ///  cx:    .xo    xx    ,x:
    ///   'xl    xx    cx'    .xl
    ///     xd,  xx    .xd     dx.
    ///      .xo:xx     xx    .xx
    ///         'c      xx:.'lx:
    ///            ..,;cxxxo
    ///   .';:codxxl   lxo
    /// cd.           'xo
    /// :o,         'ld
    ///  .oc'...';lo
    /// ```
    pub fn shastra_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SHASTRAOS,
            names: vec!["ShastraOS"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Siduction distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::siduction().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 _aass,
    ///                jQh: =w
    ///                QWmwawQW
    ///                )QQQQ@(   ..
    ///          _a_a.   ~??^  syDY?Sa,
    ///        _mW>-<c       jWmi  imm.
    ///        ]QQwayQE       4QQmgwmQQ`
    ///         ?WWQWP'       -9QQQQQ@'._aas,
    ///  _a%is.        .adYYs,. -"?!` aQB*~^3c
    /// _Qh;.nm       .QWc. {QL      ]QQp;..vmQ/
    /// "QQmmQ@       -QQQggmQP      ]QQWmggmQQ(
    ///  -???"         "WQQQY`  __,  ?QQQQQQW!
    ///         _yZ!?q,   -   .yWY!!Sw, "???^
    ///        .QQa_=qQ       mQm>..vmm
    ///         QQWQQP       QQQgmQQ@
    ///          "???"   _aa, -9WWQQWY`
    ///                _mB>~)a  -~~
    ///                mQms_vmQ.
    ///                ]WQQQQQP
    ///                 -?T??"
    /// ```
    pub fn siduction() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SIDUCTION,
            names: vec!["Siduction"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SkiffOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::skiff_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              ,@@@@@@@@@@@w,_
    ///   ====~~~,,.A@@@@@@@@@@@@@@@@@W,_
    ///   `||||||||||||||L{"@@@@@@@@@B"
    ///    `|||||||||||||||||||||L{"D
    ///      @@@@@@@@@@@@@@@@@@@@@_||||}==,
    ///       *@@@@@@@@@@@@@@@@@@@@@@@@@p||||==,
    ///         `'||LLL{{""@B@@@@@@@@@@@@@@@p||
    ///             `~=|||||||||||L"@@@@@@@@@@@
    ///                    ````'"""""""'""""""""
    /// ```
    pub fn skiff_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SKIFFOS,
            names: vec!["SkiffOS"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Slitaz distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::slitaz().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         @    @(               @
    ///       @@   @@                  @    @/
    ///      @@   @@                   @@   @@
    ///     @@  %@@                     @@   @@
    ///    @@  %@@@       @@@@@.       @@@@  @@
    ///   @@@    @@@@    @@@@@@@    &@@@    @@@
    ///    @@@@@@@ %@@@@@@@@@@@@ &@@@% @@@@@@@/
    ///        ,@@@@@@@@@@@@@@@@@@@@@@@@@
    ///   .@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/
    /// @@@@@@.  @@@@@@@@@@@@@@@@@@@@@  /@@@@@@
    /// @@    @@@@@  @@@@@@@@@@@@,  @@@@@   @@@
    /// @@ @@@@.    @@@@@@@@@@@@@%    #@@@@ @@.
    /// @@ ,@@      @@@@@@@@@@@@@      @@@  @@
    /// @   @@.     @@@@@@@@@@@@@     @@@  *@
    /// @    @@     @@@@@@@@@@@@      @@   @
    ///       @      @@@@@@@@@.     #@
    ///        @      ,@@@@@       @
    /// ```
    pub fn slitaz() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SLITAZ,
            names: vec!["Slitaz"],
            colors: vec![Color::Yellow, Color::Yellow],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Slackel distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::slackel().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               _aawmmmmmwwaaaaas,,,_.
    ///            .ammmmm###mmmmmmm###BQmm##mws
    ///          .am###mmBmBmBmBmBmBmmmmm#mmmm#2
    ///         <q###mmBmBmBmBmBmBmBmBmBmBmmBmZ`
    ///        um#mmmBmBm##U##mmBmBmBmWmmBmWm#(
    ///      .wm#mmBBmm#Y~   ~XmBmBmWmmmmmBm#e
    ///     .dm#mmWmm#Z'      ]#mBmBmmBZ!""""`
    ///    .dm#mmBmm#2`       ]mmmBmBm#2
    ///    jm#mmWmm#2`        dmmBmBmB#(
    ///   )m##mBmmWZ`        )##mBmBmmZ
    ///  :dmmmBmBm#'        .d#mBmBmWZ(
    ///  j#mmBmBmme         jmmmBmBm#2
    /// _m#mBmWmmm'        )mmmBmBmmZ`
    /// ]##mBmmm#2        <m#mBmBmB#^
    /// dmmmBmWm#C       <m#mBmBmB#(
    /// ZmmBmBmmmh.    _jm#mmBmBm#(
    /// XBmBmBmBmm6s_aum##mmBmBm&^
    /// 3mBmBmBmmm#mmmmmmBmBm#2'
    /// +ZmBmBmWmBmBmWmmBmBm##!
    ///  )ZmBmBmmmBmBmmBmB##!`
    ///   -4U#mBmWmBmBm##2"
    ///     -!!XU##US*?"-
    /// ```
    pub fn slackel() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SLACKEL,
            names: vec!["Slackel"],
            colors: vec![Color::Yellow, Color::Yellow],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Slackware distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::slackware().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                   :::::::::
    ///             :::::::::::::::::::
    ///          :::::::::::::::::::::::::
    ///        ::::::::cllcccccllllllll::::::
    ///     :::::::::lc               dc:::::::
    ///    ::::::::cl   clllccllll    oc:::::::::
    ///   :::::::::o   lc::::::::co   oc::::::::::
    ///  ::::::::::o    cccclc:::::clcc::::::::::::
    ///  :::::::::::lc        cclccclc:::::::::::::
    /// ::::::::::::::lcclcc          lc::::::::::::
    /// ::::::::::cclcc:::::lccclc     oc:::::::::::
    /// ::::::::::o    l::::::::::l    lc:::::::::::
    ///  :::::cll:o     clcllcccll     o:::::::::::
    ///  :::::occ:o                  clc:::::::::::
    ///   ::::ocl:ccslclccclclccclclc:::::::::::::
    ///    :::oclcccccccccccccllllllllllllll:::::
    ///     ::lcc1lcccccccccccccccccccccccco::::
    ///       ::::::::::::::::::::::::::::::::
    ///         ::::::::::::::::::::::::::::
    ///            ::::::::::::::::::::::
    ///                 ::::::::::::
    /// ```
    pub fn slackware() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SLACKWARE,
            names: vec!["slackware", "slackware-linux", "slackwarelinux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Slackware distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::slackware_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    ________
    ///   /  ______|
    ///   | |______
    ///   \______  \
    ///    ______| |
    /// | |________/
    /// |____________
    /// ```
    pub fn slackware_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SLACKWARE_SMALL,
            names: vec![
                "slackware-small",
                "slackware-linux-small",
                "slackware_small",
                "slackwarelinux_small",
            ],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of SmartOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::smart_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyys             oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyy                         syyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  syyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  oyyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  oyyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  syyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy              yyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// ```
    pub fn smart_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SMARTOS,
            names: vec!["SmartOS"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Soda distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::soda().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 @&&&&        *'*,
    ///               @@@@@@@&&&  **     '*,
    ///              @@@@@@@@@@@&&&&
    ///            @&@@@@@@@@@@@@@@@&&&
    ///           ******@@@@@@@@@@@@@@@&&&&
    ///         ************@@@@@@@@@@@@@@
    ///       *****************@@@@@@@@@
    ///      **********************@@@
    ///    @@@@@********************
    ///   @@@@@@@@@***************
    /// @@@@@@@@@@@@@@@*********
    /// @@@@@@@@@@@@@@@@@@****
    ///    @@@@@@@@@@@@@@@@@@
    ///        @@@@@@@@@@@@
    ///           @@@@@@@
    /// ```
    pub fn soda() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SODA,
            names: vec!["Soda"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Source Mage distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::source_mage().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        :ymNMNho.
    /// .+sdmNMMMMMMMMMMy`
    /// .-::/yMMMMMMMMMMMm-
    ///       sMMMMMMMMMMMm/
    ///      /NMMMMMMMMMMMMMm:
    ///     .MMMMMMMMMMMMMMMMM:
    ///     `MMMMMMMMMMMMMMMMMN.
    ///      NMMMMMMMMMMMMMMMMMd
    ///      mMMMMMMMMMMMMMMMMMMo
    ///      hhMMMMMMMMMMMMMMMMMM.
    ///      .`/MMMMMMMMMMMMMMMMMs
    ///         :mMMMMMMMMMMMMMMMN`
    ///          `sMMMMMMMMMMMMMMM+
    ///            /NMMMMMMMMMMMMMN`
    ///              oMMMMMMMMMMMMM+
    ///           ./sd.-hMMMMMMMMmmN`
    ///       ./+oyyyh- `MMMMMMMMMmNh
    ///                  sMMMMMMMMMmmo
    ///                  `NMMMMMMMMMd:
    ///                   -dMMMMMMMMMo
    ///                     -shmNMMms.
    /// ```
    pub fn source_mage() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SOURCE_MAGE,
            names: vec![
                "Source Mage",
                "Source Mage GNU/Linux",
                "source_mage",
                "sourcemage",
            ],
            colors: vec![Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Solaris distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::solaris().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                  `-     `
    ///           `--    `+-    .:
    ///            .+:  `++:  -/+-     .
    ///     `.::`  -++/``:::`./+/  `.-/.
    ///       `++/-`.`          ` /++:`
    ///   ``   ./:`                .: `..`.-
    /// ``./+/:-                     -+++:-
    ///     -/+`                      :.
    /// ```
    pub fn solaris() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SOLARIS,
            names: vec!["solaris", "sunos"],
            colors: vec![Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Solaris distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::solaris_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        .   .;   .
    ///    .   :;  ::  ;:   .
    ///    .;. ..      .. .;.
    /// ..  ..             ..  ..
    ///  .;,                 ,;.
    /// ```
    pub fn solaris_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SOLARIS_SMALL,
            names: vec![
                "solaris-small",
                "solaris_small",
                "sunos-small",
                "sunos_small",
            ],
            colors: vec![Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Solus distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::solus().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             -```````````
    ///           `-+/------------.`
    ///        .---:mNo---------------.
    ///      .-----yMMMy:---------------.
    ///    `------oMMMMMm/----------------`
    ///   .------/MMMMMMMN+----------------.
    ///  .------/NMMMMMMMMm-+/--------------.
    /// `------/NMMMMMMMMMN-:mh/-------------`
    /// .-----/NMMMMMMMMMMM:-+MMd//oso/:-----.
    /// -----/NMMMMMMMMMMMM+--mMMMh::smMmyo:--
    /// ----+NMMMMMMMMMMMMMo--yMMMMNo-:yMMMMd/.
    /// .--oMMMMMMMMMMMMMMMy--yMMMMMMh:-yMMMy-`
    /// `-sMMMMMMMMMMMMMMMMh--dMMMMMMMd:/Ny+y.
    /// `-/+osyhhdmmNNMMMMMm-/MMMMMMMmh+/ohm+
    ///   .------------:://+-/++++++oshddys:
    ///    -hhhhyyyyyyyyyyyhhhhddddhysssso-
    ///     `:ossssssyysssssssssssssssso:`
    ///       `:+ssssssssssssssssssss+-
    ///          `-/+ssssssssssso+/-`
    ///               `.-----..`
    /// ```
    pub fn solus() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SOLUS,
            names: vec!["solus", "solus-linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Sparky distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::sparky().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            .            `-:-`
    ///           .o`       .-///-`
    ///          `oo`    .:/++:.
    ///          os+`  -/+++:` ``.........```
    ///         /ys+`./+++/-.-::::::----......``
    ///        `syyo`++o+--::::-::/+++/-``
    ///        -yyy+.+o+`:/:-:sdmmmmmmmmdy+-`
    /// ::-`   :yyy/-oo.-+/`ymho++++++oyhdmdy/`
    /// `/yy+-`.syyo`+o..o--h..osyhhddhs+//osyy/`
    ///   -ydhs+-oyy/.+o.-: ` `  :/::+ydhy+```-os-
    ///    .sdddy::syo--/:.     `.:dy+-ohhho    ./:
    ///      :yddds/:+oo+//:-`- /+ +hy+.shhy:     ``
    ///       `:ydmmdysooooooo-.ss`/yss--oyyo
    ///         `./ossyyyyo+:-/oo:.osso- .oys
    ///        ``..-------::////.-oooo/   :so
    ///     `...----::::::::--.`/oooo:    .o:
    ///            ```````     ++o+:`     `:`
    ///                      ./+/-`        `
    ///                    `-:-.
    ///                    ``
    /// ```
    pub fn sparky() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SPARKY,
            names: vec!["Sparky"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Star distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::star().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                    ./
    ///                   `yy-
    ///                  `y.`y`
    ///     ``           s-  .y            `
    ///     +h//:..`    +/    /o    ``..:/so
    ///      /o``.-::/:/+      o/://::-.`+o`
    ///       :s`     `.        .`     `s/
    ///        .y.                    .s-
    ///         `y-                  :s`
    ///       .-//.                  /+:.
    ///    .:/:.                       .:/:.
    /// -+o:.                             .:+:.
    /// -///++///:::`              .-::::///+so-
    ///        ``..o/              d-....```
    ///            s.     `/.      d
    ///            h    .+o-+o-    h.
    ///            h  -o/`   `/o:  s:
    ///           -s/o:`       `:o/+/
    ///           /s-             -yo
    /// ```
    pub fn star() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_STAR,
            names: vec!["Star"],
            colors: vec![Color::White, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Stock Linux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::stock_linux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               #G5J5G#
    ///           &BPYJJJJJJJYPB&
    ///       &#G5JJJJJJY5YJJJJJJ5G#&
    ///    #G5YJJJJJY5G#& &#G5YJJJJJY5G#
    /// BPYJJJJJJJ5B&         &BPYJJJJJJYPB
    /// JJJJJJJJJJY5G#&           &BPYJJJJJ
    /// JJJJJJJJJJJJJJY5G#           &JJJJJ
    /// PYJJJJJJJJJJJJJJJJYPB&        GYJJJ
    ///   &BPYJJJJJJJJJJJJJJJJ5PB&      &BP
    ///       #G5YJJJJJJJJJJJJJJJY5G#
    /// PB&      &BP5JJJJJJJJJJJJJJJJYPB&
    /// JJJYG        &BPYJJJJJJJJJJJJJJJJYP
    /// JJJJJ&           #G5YJJJJJJJJJJJJJJ
    /// JJJJJYPB&           &#G5YJJJJJJJJJJ
    /// BPYJJJJJJYPB&         &B5JJJJJJJYPB
    ///    #G5YJJJJJY5G#& &#G5YJJJJJY5G#
    ///       &#G5JJJJJJY5YJJJJJJ5G#&
    ///           &BPYJJJJJJJYPB&
    ///               #G5J5G#
    /// ```
    pub fn stock_linux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_STOCK_LINUX,
            names: vec!["Stock Linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of SteamOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::steam_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               .,,,,.
    ///         .,'onNMMMMMNNnn',.
    ///      .'oNMANKMMMMMMMMMMMNNn'.
    ///    .'ANMMMMMMMXKNNWWWPFFWNNMNn.
    ///   ;NNMMMMMMMMMMNWW'' ,.., 'WMMM,
    ///  ;NMMMMV+##+VNWWW' .+;'':+, 'WMW,
    /// ,VNNWP+######+WW,  +:    :+, +MMM,
    /// '+#############,   +.    ,+' +NMMM
    ///   '*#########*'     '*,,*' .+NMMMM.
    ///      `'*###*'          ,.,;###+WNM,
    ///          .,;;,      .;##########+W
    /// ,',.         ';  ,+##############'
    ///  '###+. :,. .,; ,###############'
    ///   '####.. `'' .,###############'
    ///     '#####+++################'
    ///       '*##################*'
    ///          ''*##########*''
    ///               ''''''
    /// ```
    pub fn steam_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_STEAMOS,
            names: vec!["steamos"],
            colors: vec![Color::Blue, Color::White],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Sulin distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::sulin().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          /\          /\
    ///                         ( \\\\        // )
    ///                          \ \\\\      // /
    ///                           \_\\\\||||//_/
    ///                            \/ _  _ \
    ///                           \/|(O)(O)|
    ///                          \/ |      |
    ///      ___________________\/  \      /
    ///     //                //     |____|
    ///    //                ||     /      \
    ///   //|                \|     \ 0  0 /
    ///  // \       )         V    / \____/
    /// //   \     /        (     /
    ///       \   /_________|  |_/
    ///       /  /\   /     |  ||
    ///      /  / /  /      \  ||
    ///      | |  | |        | ||
    ///      | |  | |        | ||
    ///      |_|  |_|        |_||
    ///      \_\  \_\        \_\\
    /// ```
    pub fn sulin() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SULIN,
            names: vec!["Sulin"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Suse distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::suse().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  kKKKKKd'  Okxol:;,.
    /// kKKKKKKK0kOKKKKKKKKKKOxo:,
    /// KKKKKKKKKKKKKKKKKKK0P^,,,^dx:
    /// KKKKKKKKKKKKKKKKKKk'.oOPPb.'0k.
    /// KKKKKKKKKKKKKKKKKK: kKx..dd lKd
    /// KKKKKKKKKKKKOx0KKKd ^0KKKO' kKKc
    /// KKKKKKKKKKKKK;.;oOKx,..^..;kKKK0.
    /// KKKKKKKKKKKKK0o;...^cdxxOK0O/^^'
    /// KKKKKKKKKKKKKKKKK0x;,,......,;od
    /// kKKKKKKKKKKKKKKKKKKKKKKK00KKOo^
    ///  kKKKKKOxddxkOO00000Okxoc;''
    /// ```
    pub fn suse() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SUSE,
            names: vec!["suse", "suse-linux"],
            colors: vec![Color::Green, Color::Green],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Swagarch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::swagarch().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         .;ldkOKXXNNNNXXK0Oxoc,.
    ///    ,lkXMMNK0OkkxkkOKWMMMMMMMMMM;
    ///  'K0xo  ..,;:c:.     `'lKMMMMM0
    ///      .lONMMMMMM'         `lNMk'
    ///     ;WMMMMMMMMMO.              ....::...
    ///     OMMMMMMMMMMMMKl.       .,;;;;;ccccccc,
    ///     `0MMMMMMMMMMMMMM0:         .. .ccccccc.
    ///       'kWMMMMMMMMMMMMMNo.   .,:'  .ccccccc.
    ///         `c0MMMMMMMMMMMMMN,,:c;    :cccccc:
    ///  ckl.      `lXMMMMMMMMMXocccc:.. ;ccccccc.
    /// dMMMMXd,     `OMMMMMMWkccc;:''` ,ccccccc:
    /// XMMMMMMMWKkxxOWMMMMMNoccc;     .cccccccc.
    ///  `':ldxO0KXXXXXK0Okdocccc.     :cccccccc.
    ///                     :ccc:'     `cccccccc:,
    ///                                    ''
    /// ```
    pub fn swagarch() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_SWAGARCH,
            names: vec!["Swagarch"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of T2 distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::t2().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// TTTTTTTTTT
    ///     tt   222
    ///     tt  2   2
    ///     tt     2
    ///     tt    2
    ///     tt  22222
    /// ```
    pub fn t2() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_T2,
            names: vec!["T2"],
            colors: vec![Color::White, Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Tails distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::tails().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       ``
    ///   ./yhNh
    /// syy/Nshh         `:o/
    /// N:dsNshh  █   `ohNMMd
    /// N-/+Nshh      `yMMMMd
    /// N-yhMshh       yMMMMd
    /// N-s:hshh  █    yMMMMd so//.
    /// N-oyNsyh       yMMMMd d  Mms.
    /// N:hohhhd:.     yMMMMd  syMMM+
    /// Nsyh+-..+y+-   yMMMMd   :mMM+
    /// +hy-      -ss/`yMMMM     `+d+
    ///   :sy/.     ./yNMMMMm      ``
    ///     .+ys- `:+hNMMMMMMy/`
    ///       `hNmmMMMMMMMMMMMMdo.
    ///        dMMMMMMMMMMMMMMMMMNh:
    ///        +hMMMMMMMMMMMMMMMMMmy.
    ///          -oNMMMMMMMMMMmy+.`
    ///            `:yNMMMds/.`
    ///               .//`
    /// ```
    pub fn tails() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TAILS,
            names: vec!["Tails"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Tatra distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::tatra().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          . .:. .         .:.
    ///         .^^.!.:::.    .^!?J?^
    ///       .:^.^!!!~:~^.  .7??77!~~^.
    ///      .~^.!??77?!.^~:  ..:^^7JJJ7~~^.
    ///      .^~.^7???7~.~~.   .7??????????!
    ///       .:^:^^~^^:!^ ^:  .......^!:...
    ///     .!7~.::.!.::. ~BG~    :^  ^~:
    ///     :!!~   ~.    ?BBBB!  ^?J!.  .!~.
    ///      :!. .JBY. .Y#BBBY?~!???J7. :^^.
    ///      .. :5#B#P~P#BBP?7?55J?J7:
    ///        ^P#BBBBBBBB5?7J5555J!.....
    ///       !BBBBBBGBBGJ77::Y555J?77777^
    ///      ?BBBBG5JJ5PJ?!:  .?Y??????77?~.
    ///    .YBGPYJ??????Y?^^^^~7?????????7?!.
    ///    .^^:..::::::::.....:::::::::::..:.
    /// ```
    pub fn tatra() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TATRA,
            names: vec!["Tatra"],
            colors: vec![Color::Blue, Color::Green],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of TeArch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::te_arch().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           @@@@@@@@@@@@@@
    ///       @@@@@@@@@              @@@@@@
    ///      @@@@@                     @@@@@
    ///      @@                           @@
    ///       @%                         @@
    ///        @                         @
    ///        @@@@@@@@@@@@@@@@@@@@@@@@ @@
    ///        .@@@@@@@@@@@@/@@@@@@@@@@@@
    ///        @@@@@@@@@@@@///@@@@@@@@@@@@
    ///       @@@@@@@@@@@@@((((@@@@@@@@@@@@
    ///      @@@@@@@@@@@#(((((((#@@@@@@@@@@@
    ///     @@@@@@@@@@@#//////////@@@@@@@@@@&
    ///     @@@@@@@@@@////@@@@@////@@@@@@@@@@
    ///     @@@@@@@@//////@@@@@/////@@@@@@@@@
    ///     @@@@@@@//@@@@@@@@@@@@@@@//@@@@@@@
    ///  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /// @@     .@@@@@@@@@@@@@@@@@@@@@@@@@      @
    ///  @@@@@@           @@@.           @@@@@@@
    ///    @@@@@@@&@@@@@@@#  #@@@@@@@@@@@@@@@@
    ///       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///           @@@@@@@@@@@@@@@@@@@@@
    /// ```
    pub fn te_arch() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TEARCH,
            names: vec!["TeArch"],
            colors: vec![
                Color::TrueColor {
                    r: 0,
                    g: 0xaf,
                    b: 0xff,
                },
                Color::White,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of TileOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::tile_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                       ,.
    ///                   ##########
    ///              ###################.
    ///          ############################
    ///      ####################################
    ///     ######################################
    /// ,(/     *############################(     .%
    /// (((((((     (####################      %%####
    /// (((((((((((/     ############      %#########
    /// ((((((((((((((((      ##/     (##############
    /// ((((((((((((((((((((      ###################
    /// (((((((((((((((((((((   ####################
    /// (((((((((((((((((((((   ####################
    /// (((((((((((((((((((((   ####################
    /// (((((((((((((((((((((   ####################
    ///  ((((((((((((((((((((   ###################%
    ///      ((((((((((((((((   ################/
    ///          ((((((((((((   ###########%
    ///               (((((((   #######*
    ///                   (((   ##%*
    /// ```
    pub fn tile_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TILEOS,
            names: vec!["TileOS"],
            colors: vec![Color::Magenta, Color::Blue, Color::Green],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of TorizonCore distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::torizon_core().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                           `.::-.
    ///                        `-://////:-.
    ///                      `:////////////:.
    ///                 `.-.`  `-:///////-.     `
    ///              `.://///:.`  `-::-.     `-://:.`
    ///            .:///////////:.        `-:////////:.`
    ///            `.://///////:-`       .:////////////:`
    ///     `.-/:-`   `.:///:-`    `-+o/.  `.://////-.     `.`
    ///  `.:///////:-`   `.`    `-+sssssso/.  `.--.     `-:///:-`
    /// -/////////////:       `+sssssssssssso-       `-://///////:-`
    ///   .-///////:.`    ``    -/sssssssso/.    `   .:///////////-.
    ///      .-::.`    `-://:-.    -/sso/.    `.:/:-.  `.://///-.
    ///             `-:////////:-.    `    `.:////////-.  `.-.
    /// o-          -:///////////:`       .:////////////:`        -o
    /// hdho-         `-://///:.`    `..`   `-://////:.`       -ohdh
    /// /ydddho-         `--.`    `.:////:.`   `-::.`       -ohdddy/
    ///   ./ydddho:`           `.://////////:.          `:ohdddy/.
    /// `    `/shddho:`        `.://////////:.       `:ohddds/`    `
    /// ::-`    `/shddhs:`        `.:////:.`      `:shddhs/`    `-::
    /// :///:-`    `:shddhs/`        `..`      `/shddhs:`    `-:///-
    ///  `.:///:-`    `:ohddhs/`            `/shddho:`    `-:///:.`
    ///     `.:///:-.     -ohdddy/.      ./ydddho-     .-:///-.`
    ///        `.:///:-.     -+hdddy+//+ydddh+-     .-://:-.
    ///           `.-///:-.     -+yddddddy+-     .-://:-.
    ///               .-://:-.     ./++/.     .-///:-.
    ///                  .-:///:.`        `.:///:-`
    ///                     `-:///:.````.:///:-`
    ///                        `-:////////:-`
    ///                           `-::::-`
    /// ```
    pub fn torizon_core() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TORIZONCORE,
            names: vec!["TorizonCore"],
            colors: vec![
                Color::Yellow,
                Color::Blue,
                Color::BrightBlack,
                Color::Magenta,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Trisquel distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::trisquel().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          ,oo.
    ///                       ,oY"""Yb
    ///     ,oo.       ,'   ,   Yb
    ///  ,oo.    :      b   Y.
    /// ,"'      "Yo.   'b.   ,b  d
    /// '  .db  'o   'YY  d'
    /// '  q'    'b  'o._   _.o'
    /// .,_    _,d  ,Y'
    ///  'aaa' .'
    ///      """"     d"'
    ///              d'   .db.
    ///                ."   'a.
    ///                b      .
    ///              '. 'b,,.  
    ///               '.       .'
    ///                'ao._.oa'
    ///                   'aa'
    /// ```
    pub fn trisquel() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TRISQUEL,
            names: vec!["Trisquel"],
            colors: vec![Color::Blue, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Tuxedo OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::tuxedo_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  #############################+
    /// .%#######################%@@@+
    ///             .           :#@%-
    ///            .#@%%%-     =@@#.
    ///              *@%@@=   +@@+
    ///               =@@%@+.#@%-
    ///                -@@%@@@#:
    ///                 .#@%%@-
    ///                 :#@@%@@=
    ///                =@@+=@@%@+
    ///              .#@@=  -@@%@#
    ///             -%@#:    :%@%@%:
    ///            *@@+       .#%%%%-
    ///          :%@%-              .
    ///         +@@@%#%%%%%%%%%%%%%%%%%%%%%%%.
    ///        +######################*******
    /// ```
    pub fn tuxedo_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TUXEDO_OS,
            names: vec!["Tuxedo OS"],
            colors: vec![Color::White, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Twister distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::twister().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// ...............................,:ccllllcc:....
    /// ........................,;cldxkO0KXNNNWNNNX0x:
    /// ..................,:ldkOOKK0OkkkkkOKWMMMMMMMNo
    /// .............,;coxO0Oxo::'.........,oXMMMMMM0'
    /// .........,:ldkOOkc;'................;OMMMMW0:.
    /// .......;ldkOxol:'..................,dNMMX0''..
    /// ....;:oxkxl:'....................,lOWWXx:.,'..
    /// ..,ldxkkd:'.............:c....':lkNMNOc,cdl'..
    /// .;oxxkkkc'..........:clc;'.';lkKNNKk;;ck00l...
    /// .lxxxkkkxoooooddxkOOko'..cok0KOd':::o0NXd;'...
    /// .:odxxkkOOOOOOOkdoi'..:ddxdoc:::od0NWW0c'.....
    /// ...':;gggggg;::''.......::::x0XWMMMNO.::'.....
    /// ..............;......,;od0KNWMWNK0O:::do'.....
    /// ...............'cclgggggggxdll":::'XKoo,......
    /// .................',,,,,,,::::;ooNWMWOc'.......
    /// ..................,:loxxxO0KXNNXK0ko:'........
    /// ..................';::;oTcoggcoF":::'.........
    /// ..................':o,.:::::::::,p'...........
    /// ..................;'ccdxkOOOdxlf'.............
    /// .................,l;;XgggggXP:'...............
    /// ................;lco;::::::'..................
    /// ...............';ggggol'`.....................
    /// .............':oo:''..........................
    /// ```
    pub fn twister() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_TWISTER,
            names: vec!["Twister"],
            colors: vec![
                Color::Green,
                Color::Red,
                Color::Blue,
                Color::Magenta,
                Color::White,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                             ....
    ///               .',:clooo:  .:looooo:.
    ///            .;looooooooc  .oooooooooo'
    ///         .;looooool:,''.  :ooooooooooc
    ///        ;looool;.         'oooooooooo,
    ///       ;clool'             .cooooooc.  ,,
    ///          ...                ......  .:oo,
    ///   .;clol:,.                        .loooo'
    ///  :ooooooooo,                        'ooool
    /// 'ooooooooooo.                        loooo.
    /// 'ooooooooool                         coooo.
    ///  ,loooooooc.                        .loooo.
    ///    .,;;;'.                          ;ooooc
    ///        ...                         ,ooool.
    ///     .cooooc.              ..',,'.  .cooo.
    ///       ;ooooo:.           ;oooooooc.  :l.
    ///        .coooooc,..      coooooooooo.
    ///          .:ooooooolc:. .ooooooooooo'
    ///            .':loooooo;  ,oooooooooc
    ///                ..';::c'  .;loooo:'
    ///                              .
    ///                       .
    /// ```
    pub fn ubuntu() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU,
            names: vec!["ubuntu", "ubuntu-linux"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Budgie distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_budgie().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            ./oydmMMMMMMmdyo/.
    ///         :smMMMMMMMMMMMhs+:++yhs:
    ///      `omMMMMMMMMMMMN+`        `odo`
    ///     /NMMMMMMMMMMMMN-            `sN/
    ///   `hMMMMmhhmMMMMMMh               sMh`
    ///  .mMmo-     /yMMMMm`              `MMm.
    ///  mN/       yMMMMMMMd-              MMMm
    /// oN-        oMMMMMMMMMms+//+o+:    :MMMMo
    /// m/          +NMMMMMMMMMMMMMMMMm. :NMMMMm
    /// M`           .NMMMMMMMMMMMMMMMNodMMMMMMM
    /// M-            sMMMMMMMMMMMMMMMMMMMMMMMMM
    /// mm`           mMMMMMMMMMNdhhdNMMMMMMMMMm
    /// oMm/        .dMMMMMMMMh:      :dMMMMMMMo
    ///  mMMNyo/:/sdMMMMMMMMM+          sMMMMMm
    ///  .mMMMMMMMMMMMMMMMMMs           `NMMMm.
    ///   `hMMMMMMMMMMM.oo+.            `MMMh`
    ///     /NMMMMMMMMMo                sMN/
    ///      `omMMMMMMMMy.            :dmo`
    ///         :smMMMMMMMh+-`   `.:ohs:
    ///            ./oydmMMMMMMdhyo/.
    /// ```
    pub fn ubuntu_budgie() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_BUDGIE,
            names: vec!["ubuntu budgie", "ubuntu-budgie"],
            colors: vec![Color::Blue, Color::White, Color::Red],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Cinnamon distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_cinnamon().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             .-/+oooooooo+/-.
    ///         `:+oooooooooooooooooo+:`
    ///       -+oooooooooooooooooooooooo+-
    ///     .ooooooooooooooooooo:ohNdoooooo.
    ///    /oooooooooooo:/+oo++:/ohNdooooooo/
    ///   +oooooooooo:osNdhyyhdNNh+:+oooooooo+
    ///  /ooooooooo/dN/ooooooooo/sNNoooooooooo/
    /// .ooooooooooMd:oooooooooooo:yMyooooooooo.
    /// +ooooo:+o/Mdoooooo:sm/oo/oooyMooooooooo+
    /// ooo:sdMdosMoooooooNMd//dMd+o:soooooooooo
    /// oooo+ymdosMoooo+mMm+/hMMMMMh+hsooooooooo
    /// +oooooo::/Nm:/hMNo:yMMMMMMMMMM+oooooooo+
    /// .ooooooooo/NNMNy:oNMMMMMMMMMMoooooooooo.
    /// /oooooooooo:yh:+mMMMMMMMMMMd/ooooooooo/
    ///   +oooooooooo+/hmMMMMMMNds//ooooooooo+
    ///    /oooooooooooo+:////:o/ymMdooooooo/
    ///     .oooooooooooooooooooo/sdhoooooo.
    ///       -+oooooooooooooooooooooooo+-
    ///         `:+oooooooooooooooooo+:`
    ///             .-/+oooooooo+/-.
    /// ```
    pub fn ubuntu_cinnamon() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_CINNAMON,
            names: vec!["ubuntu cinnamon", "ubuntu-cinnamon"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Gnome distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_gnome().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///           ./o.
    ///         .oooooooo
    ///       .oooo```soooo
    ///     .oooo`     `soooo
    ///    .ooo`   .o.   `\/ooo.
    ///    :ooo   :oooo.   `\/ooo.
    ///     sooo    `ooooo    \/oooo
    ///      \/ooo    `soooo    `ooooo
    ///       `soooo    `\/ooo    `soooo
    /// ./oo    `\/ooo    `/oooo.   `/ooo
    /// `\/ooo.   `/oooo.   `/oooo.   ``
    ///   `\/ooo.    /oooo     /ooo`
    ///      `ooooo    ``    .oooo
    ///        `soooo.     .oooo`
    ///          `\/oooooooooo`
    ///             ``\/oo``
    /// ```
    pub fn ubuntu_gnome() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_GNOME,
            names: vec!["ubuntu gnome", "ubuntu-gnome"],
            colors: vec![Color::Blue, Color::Magenta, Color::White, Color::Cyan],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Magenta),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Kylin distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_kylin().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             .__=liiiiiii=__,
    ///         ._=liiliii|i|i|iiilii=_.
    ///       _=iiiii|ii|i|ii|i|inwwwzii=,
    ///     .=liiii|ii|ii|wwww|i3QWWWWziii=,
    ///    =lii|i|ii|i|QQQWWWWWm]QWWQD||iiii=
    ///   =lii|iiivwQm>3WWWWWQWQQwwQwcii|i|ii=
    ///  =lii|ii|nQWWWQ|)i|i%i|]TQWWWWm|ii|i|i=
    /// .li|i|i|mWWWQV|iiwmD|iiii|TWWWWm|i|iiii,
    /// =iiiwww|WQWk|iaWWWD|i|i|ii]QQWQk|ii|i|=
    /// iiiQWWWQzWW|ijQQWQmw|iiwWk|TTTTYi|i|iii
    /// iiIQWQWWtyQQ|i|WWWWWQWk||i|i|ii||i|ii|i
    /// <|i|TTT|mQQWz|i3D]C|nDW|iivWWWWk||ii|i>
    /// -|ii|i|iWWWQw|Tt|i3T|T|i|nQWQWDk|ii|ii`
    ///  <|i|iii|VWQWWQ|i|i|||iiwmWWQWD||ii|ii+
    ///   <|ii|i|i]W@tvQQQWQQQWWTTHT1|iii|i|>
    ///    <|i|ii|ii||vQWWWQWWW@vmWWWm|i|i|i>
    ///     -<|i|ii|ii|i|TTTTTT|]QQWWWC|ii>`
    ///       -<|i|ii|i|ii|i|i|ii3TTTt|i>`
    ///          ~<|ii|ii|iiiii|i|||i>~
    ///             -~~<|ii|i||i>~~`
    /// ```
    pub fn ubuntu_kylin() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_KYLIN,
            names: vec!["ubuntu kylin", "ubuntu-kylin"],
            colors: vec![Color::Red, Color::White, Color::Yellow],
            color_keys: Some(Color::Red),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Mate distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_mate().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             .:/+oossssoo+/:.`
    ///         `:+ssssssssssssssssss+:`
    ///       -+sssssssssssssssyssssssss+-
    ///     .osssssssssssssyyssmMmhssssssso.
    ///    /sssssssssydmNNNmmdsmMMMMNdysssss/
    ///  `+sssssssshNNdysssssssmMMMMNdyssssss+`
    ///  +sssssssyNNhsshmNNNNmsmMmhsydysssssss+
    /// -sssssyssNmsshNNhssssssyshhssmMysssssss-
    /// +ssssyMNdysshMdsssssssssshMdssNNsssssss+
    /// sssssyMMMMMmhsssssssssssssNMssdMysssssss
    /// sssssyMMMMMmhyssssssssssssNMssdMysssssss
    /// +ssssyMNdysshMdsssssssssshMdssNNsssssss+
    /// -sssssyssNmsshNNhssssssssdhssmMysssssss-
    ///  +sssssssyNNhsshmNNNNmsmNmhsymysssssss+
    ///   +sssssssshNNdysssssssmMMMMmhyssssss+
    ///    /sssssssssydmNNNNmdsmMMMMNdhsssss/
    ///     .osssssssssssssyyssmMmdysssssso.
    ///       -+sssssssssssssssyssssssss+-
    ///         `:+ssssssssssssssssss+:`
    ///             .:/+oossssoo+/:.
    /// ```
    pub fn ubuntu_mate() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_MATE,
            names: vec!["ubuntu mate", "ubuntu-mate"],
            colors: vec![Color::Green, Color::White],
            color_keys: Some(Color::Green),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns old logo of Ubuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              --+oossssssoo+--
    ///          .:+ssssssssssssssssss+:.
    ///        -+ssssssssssssssssssyyssss+-
    ///      .ossssssssssssssssssdMMMNysssso.
    ///    /ssssssssssshdmmNNmmyNMMMMhssssss/
    ///   +ssssssssshmydMMMMMMMNddddyssssssss+
    ///  /sssssssshNMMMyhhyyyyhmNMMMNhssssssss/
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    /// +sssshhhyNMMNyssssssssssssyNMMMysssssss+
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// +sssshhhyNMMNyssssssssssssyNMMMysssssss+
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    ///  /sssssssshNMMMyhhyyyyhdNMMMNhssssssss/
    ///   +sssssssssdmydMMMMMMMMddddyssssssss+
    ///    /ssssssssssshdmNNNNmyNMMMMhssssss/
    ///     .ossssssssssssssssssdMMMNysssso.
    ///      -+sssssssssssssssssyyyssss+-
    ///        `:+ssssssssssssssssss+:´
    ///            --+oossssssoo+--
    /// ```
    pub fn ubuntu_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_OLD,
            names: vec!["ubuntu_old", "ubuntu-linux_old"],
            colors: vec![Color::Red, Color::White],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Alter,
        }
    }
    /// returns small logo of Ubuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          _
    ///      ---(_)
    ///  _/  ---  \
    /// (_) |   |
    ///   \  --- _/
    ///      ---(_)
    /// ```
    pub fn ubuntu_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_SMALL,
            names: vec!["ubuntu_small", "ubuntu-linux-small"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Ubuntu Studio distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_studio().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               ..-::::::-.`
    ///          `.:+++++++++++ooo++:.`
    ///        ./+++++++++++++sMMMNdyo+/.
    ///      .++++++++++++++++oyhmMMMMms++.
    ///    `/+++++++++osyhddddhys+osdMMMh++/`
    ///   `+++++++++ydMMMMNNNMMMMNds+oyyo++++`
    ///   +++++++++dMMNhso++++oydNMMmo++++++++`
    ///  :+odmy+++ooysoohmNMMNmyoohMMNs+++++++:
    ///  ++dMMm+oNMd++yMMMmhhmMMNs+yMMNo+++++++
    /// `++NMMy+hMMd+oMMMs++++sMMN++NMMs+++++++.
    /// `++NMMy+hMMd+oMMMo++++sMMN++mMMs+++++++.
    ///  ++dMMd+oNMm++yMMNdhhdMMMs+yMMNo+++++++
    ///  :+odmy++oo+ss+ohNMMMMmho+yMMMs+++++++:
    ///   +++++++++hMMmhs+ooo+oshNMMms++++++++
    ///   `++++++++oymMMMMNmmNMMMMmy+oys+++++`
    ///    `/+++++++++oyhdmmmmdhso+sdMMMs++/
    ///      ./+++++++++++++++oyhdNMMMms++.
    ///        ./+++++++++++++hMMMNdyo+/.
    ///          `.:+++++++++++sso++:.
    ///               ..-::::::-..
    /// ```
    pub fn ubuntu_studio() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_STUDIO,
            names: vec!["ubuntu studio", "ubuntu-studio"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Sway distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_sway().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             .-/+oossssoo+\-.
    ///         ´:+ssssssssssssssssss+:`
    ///       -+ssssssssssssssssssyyssss+-
    ///     .ossssssssssssssssssdMMMNyyssso.
    ///    /ssssssssssshdmmNNmmyNMMMMhssssss\
    ///   +ssssssssshmydMMMMMMMNddddyssssssss+
    ///  /sssssssshNMMMyhhyyyyhmNMMMNhssssssss\
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    /// +sssyyyyyNMMNyssssssssssssyNMMMysssssss+
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// +sssyyyyyNMMNyssssssssssssyNMMMysssssss+
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    ///  \sssssssshNMMMyhhyyyyhdNMMMNhssssssss/
    ///   +sssssssssdmydMMMMMMMMddddyssssssss+
    ///    \ssssssssssshdmNNNNmyNMMMMhssssss/
    ///     .ossssssssssssssssssdMMMNyyssso.
    ///       -+sssssssssssssssssyysss+-
    ///         `:+ssssssssssssssssss+:`
    ///             .-\+oossssoo+/-.
    /// ```
    pub fn ubuntu_sway() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_SWAY,
            names: vec!["ubuntu sway", "ubuntu-sway"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Touch distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_touch().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///    ###############
    ///  ##               ##
    /// ##  ##         ##  ##
    /// ##  ##  #   #  ##  ##
    /// ##       ###       ##
    ///  ##               ##
    ///    ###############
    /// ```
    pub fn ubuntu_touch() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_TOUCH,
            names: vec!["ubuntu touch", "ubuntu-touch"],
            colors: vec![Color::Yellow, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Ubuntu Unity distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu_unity().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                .-/+ooosssssssooo+\-.
    ///             ,:+sssssssssssssssssssss+:.
    ///          -+ssssssssssssssssssss.....ssss+-
    ///        .ssssss,ss:cloooooo:ss.:loooo.ssssss.
    ///     .ssssssssloossooooooocss.ooooooooossssss.
    ///    .ssssss.;looooool:,''.ss:oooooooooocssssss.
    ///   +ssssss;loooool;.ssssssssssooooooooossssssss+
    ///  /ssssss;clool'sssssssssssssssoooooocssss,,sssss.
    /// .sssssssssssssssssssssssssssssssssssss.:oo,sssss.
    /// .sssss.;clol:,.ssssssssssssssssssssss.loooo'ssss.
    /// +ssss:ooooooooo,ssssssssssssssssssssss'oooolssss+
    /// osss'ooooooooooo.ssssssssssssssssssssssloooo.ssso
    /// osss'oooooooooolssssssssssssssssssssssscossssssso
    /// ossss,looooooc.sssssssssssssssssssssss.loooo.ssso
    /// +ssssss.,;;;'.ssssssssssssssssssssssss;oooocssss+
    /// .ssssssssssssssssssssssssssssssssssss,ooool.ssss.
    /// .sssssss.cooooc.sssssssssssss.,,,,.ss.cooo.sssss.
    ///  `sssssssooooo:.ssssssssssssoooooooc.ss:l.sssss'
    ///   +ssssss.coooooc,..ssssssscooooooooo.ssssssss+
    ///    `sssssss.:oosooooolc:.s.ooooooooooo'ssssss'
    ///     .sssssss.oss`:loooo;sss,oooooooocsssssss.
    ///      `sssssssss;oooo';:c'ssss`:ooo:'sssssss'
    ///        -+sssssssssssssssssssssssssssssss+-
    ///           `:+sssssssssssssssssssssss+:'
    ///                `-\+ooosssssssooo+/-'
    /// ```
    pub fn ubuntu_unity() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU_UNITY,
            names: vec!["ubuntu unity", "ubuntu-unity"],
            colors: vec![Color::Magenta, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns second old logo of Ubuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu2_old().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                          ./+o+-
    ///                  yyyyy- -yyyyyy+
    ///               ://+//////-yyyyyyo
    ///           .++ .:/++++++/-.+sss/`
    ///         .:++o:  /++++++++/:--:/-
    ///        o:+o+:++.`..```.-/oo+++++/
    ///       .:+o:+o/.          `+sssoo+/
    ///  .++/+:+oo+o:`             /sssooo.
    /// /+++//+:`oo+o               /::--:.
    /// +/+o+++`o++o               ++////.
    ///  .++.o+++oo+:`             /dddhhh.
    ///       .+.o+oo:.          `oddhhhh+
    ///        +.++o+o``-````.:ohdhhhhh+
    ///         `:o+++ `ohhhhhhhhyo++os:
    ///           .o:`.syhhhhhhh/.oo++o`
    ///               /osyyyyyyo++ooo+++/
    ///                   ````` +oo+++o:
    ///                          `oo++.
    /// ```
    pub fn ubuntu2_old() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU2_OLD,
            names: vec!["ubuntu2_old"],
            colors: vec![Color::Red, Color::White, Color::Yellow],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Alter,
        }
    }
    /// returns second small logo of Ubuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ubuntu2_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///        ..;,; .,;,.
    ///     .,lool: .ooooo,
    ///    ;oo;:    .coool.
    ///  ....         ''' ,l;
    /// :oooo,            'oo.
    /// looooc            :oo'
    ///  '::'             ,oo:
    ///    ,.,       .... co,
    ///     lo:;.   :oooo; .
    ///      ':ooo; cooooc
    ///         '''  ''''
    /// ```
    pub fn ubuntu2_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UBUNTU2_SMALL,
            names: vec!["ubuntu2_small", "ubuntu2-small"],
            colors: vec![Color::Red],
            color_keys: Some(Color::Red),
            color_title: Some(Color::Red),
            logo_type: LogoType::Small | LogoType::Alter,
        }
    }
    /// returns logo of Ultramarine distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::ultramarine().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///             .cd0NNNNNNNXOdc.
    ///         .:xKNNNNNNNNNNNNNNNNKd;.
    ///       ,dXNNNNNNNNNNNNNNNNNNNNNNNd,
    ///     'ONNNNNNNNNNNNNNNNNNNNNNNNNNNNO'
    ///   .xNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNk.
    ///  .0NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN0.
    /// .0NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN0.
    /// dNNNNNNNNNNNNWWWWWWWWNNNNNNNNNNNNNNNNNNd
    /// NNNNNNNNNNNNNWMMMMMMMMWWNNNNNNNNNNNNNNNN
    /// NNNNNNNNNNNNNNWMMMMMMMMMWWNNNNNNNNNNNNNN
    /// NNNNNNNNNNNNNNWMMMMMMMMMMMMWNNNNNNNNNNNN
    /// NNNNNNNNNNWWWMMMMMMMMMMMMMMMMWWWNNNNNNNX
    /// oNWWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWNNo
    ///  OWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWO
    ///  .OWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWO.
    ///    lNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNl
    ///     .dNWMMMMMMMMMMMMMMMMMMMMMMMMWNd.
    ///       .cKWMMMMMMMMMMMMMMMMMMMMWKc.
    ///          'oOXWWWMMMMMMMMWWWXOl.
    ///              ;lkXNNNNNNXkl'
    /// ```
    pub fn ultramarine() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ULTRAMARINE,
            names: vec!["Ultramarine", "Ultramarine Linux"],
            colors: vec![Color::Blue, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Univalent distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::univalent().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// UUUUUUU                   UUUUUUU
    /// UUUUUUU                   UUUUUUU
    /// UUUUUUU         A         UUUUUUU
    /// UUUUUUU        A|A        UUUUUUU
    /// UUUUUUU       A | A       UUUUUUU
    /// UUUUUUU      A  |  A      UUUUUUU
    /// UUUUUUU     A|  |  |A     UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    ///  UUUUUUU   A |  |  | A   UUUUUUU
    ///   UUUUUUU  A |  |  | A  UUUUUUU
    ///     UUUUUUUAAAAAAAAAAAUUUUUUU
    ///        UUUUUUUUUUUUUUUUUUU
    ///           UUUUUUUUUUUUU
    /// ```
    pub fn univalent() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UNIVALENT,
            names: vec!["Univalent"],
            colors: vec![Color::Cyan, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Univention distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::univention().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          ./osssssssssssssssssssssso+-
    ///        `ohhhhhhhhhhhhhhhhhhhhhhhhhhhhy:
    ///        shhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh-
    ///    `-//sssss/hhhhhhhhhhhhhh+s.hhhhhhhhh+
    ///  .ohhhysssss.hhhhhhhhhhhhhh.sss+hhhhhhh+
    /// .yhhhhysssss.hhhhhhhhhhhhhh.ssss:hhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhyssssss+yhhhhhhhhhhy/ssssssyhhhhh+
    /// +hhhhhh:sssssss:hhhhhhh+.ssssssssyhhhhy.
    /// +hhhhhhh+`sssssssssssssssshhsssssyhhho`
    /// +hhhhhhhhhs+ssssssssssss+hh+sssss/:-`
    /// -hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhho
    ///  :yhhhhhhhhhhhhhhhhhhhhhhhhhhhh+`
    ///    -+ossssssssssssssssssssss+:`
    /// ```
    pub fn univention() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UNIVENTION,
            names: vec!["Univention"],
            colors: vec![Color::Red, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of UOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::uos().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                       .......
    ///                   ..............
    ///               ......................
    ///            .............................
    /// uuuuuu    uuuuuu     ooooooooooo       ssssssssss
    /// u::::u    u::::u   oo:::::::::::oo   ss::::::::::s
    /// u::::u    u::::u  o:::::::::::::::oss:::::::::::::s
    /// u::::u    u::::u  o:::::ooooo:::::os::::::ssss:::::s
    /// u::::u    u::::u  o::::o     o::::o s:::::s  ssssss
    /// u::::u    u::::u  o::::o     o::::o   s::::::s
    /// u::::u    u::::u  o::::o     o::::o      s::::::s
    /// u:::::uuuu:::::u  o::::o     o::::ossssss   s:::::s
    /// u:::::::::::::::uuo:::::ooooo:::::os:::::ssss::::::s
    /// u:::::::::::::::uo:::::::::::::::os::::::::::::::s
    /// uu::::::::uu:::u oo:::::::::::oo  s:::::::::::ss
    ///     uuuuuuuu  uuuu   ooooooooooo     sssssssssss
    ///           .............................
    ///               .....................
    ///                   .............
    ///                       ......
    /// ```
    pub fn uos() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UOS,
            names: vec!["UOS"],
            colors: vec![Color::Red],
            color_keys: Some(Color::White),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of UrukOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::uruk_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///          :::::::::::::::    .
    ///         =#*============.   :#:
    ///        =##%+----------.   .###:
    ///       =####.              .####:
    ///      =####.                .####:
    ///     =###*.   .=--------.     ####:
    ///    =####.   .*#+======-       ####:
    ///   =###*.   .*###+====-         ####:
    ///  =###*.   .*#####+==-           ####:
    /// =###*.   .*#######+:             ####:
    /// =###*.   .*#######+:             ####:
    ///  =###*.   .*#####+==-           ####:
    ///   =###*.   .*###+====-         ####:
    ///    =####.   .*#+======-       ####:
    ///     =###*.   .=--------.    .####:
    ///      =####.                .####:
    ///       =####.              .####:
    ///        =###+--------------####:
    ///         =#+=================-:
    ///          :::::::::::::::::::.
    /// ```
    pub fn uruk_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_URUKOS,
            names: vec!["UrukOS"],
            colors: vec![
                Color::BrightBlue,
                Color::BrightBlue,
                Color::White,
                Color::BrightBlue,
                Color::Blue,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Uwuntu distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::uwuntu().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                   &&
    ///                                &&&&&&&&
    ///    ,                  *&&&&&&  &&&&&&&&(
    ///     &%%%%&&&&     &&&&&&&&&&&&  ,&&&&&
    ///      %%%%%%&&&&&   ,&&&&&&&&&&&&&,   %&&&&&&%%%%%.
    ///      &%%%%&&&&&&&#   &,       &&&&&&&&&&&&&%%%%%
    ///       &%%&&&&&&&&&(               &&&&&&&&&%%%%
    ///        &&&&&&&&&%                  *&&&&&&&&&%
    ///     &&&/  &&&&\&                    ,/*.**
    ///  %&&&&&&&&  &&&⟩.,                *.⟨
    ///  %&&&&&&&&  &&/..      /    \      ..\(&&&&&&
    ///    #&&&#%%%%.%%%(      \_/\_/      (%%%.%%%%/
    ///         /%%%%%%%&&*              ,&&&%%%%%%&
    ///            &&&&&&&&           &&&&&&&&&&&
    ///             (&&&&&    &&&&&&&&&&&
    ///             %%  &   &&&&&&&&&&&&  &&&&&&&
    ///            %%%        #&&&&&&#   &&&&&&&&&
    ///  %%%%%     %%                     #&&&&&(
    /// &%.      %%%
    ///   %%%%%%%
    /// ```
    pub fn uwuntu() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_UWUNTU,
            names: vec!["uwuntu"],
            colors: vec![
                Color::TrueColor {
                    r: 0xff,
                    g: 0xd7,
                    b: 0xff,
                },
                Color::TrueColor {
                    r: 0xff,
                    g: 0x5f,
                    b: 0xd7,
                },
                Color::TrueColor {
                    r: 0x5f,
                    g: 0x00,
                    b: 0x00,
                },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Vanilla distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::vanilla().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 .----:
    ///               .-------.
    ///              :---::----:
    ///             .----::-----.
    ///   .........  :----::-----: ..:::-::::..
    /// .-----------------::------------------:
    ///  ----::-----------::----------::::---:
    ///   -----:::--------::-------:::-------
    ///    :------::::--::...:::::---------:
    ///     .---------::..    ..:---------.
    ///       .::-----::..    .::----::.
    ///         .:------:.......:-------:
    ///        .--------::::::::-:::-------.
    ///       .-------::-----.:-----::------.
    ///      -----::------:   :------::-----
    ///     :--::--------:     .-------::---:
    ///    :----------::         .:----------
    ///     :--------:             :--------:
    /// ```
    pub fn vanilla() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VANILLA,
            names: vec!["vanilla", "vanilla-os", "vanilla-linux"],
            colors: vec![Color::Yellow],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Yellow),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Venom distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::venom().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// `-`
    ///  -yys+/-`
    ///   `oyyyyy: /osyyyyso+:.
    ///     /yyyyy+`+yyyyyyyyyys/.
    ///     .-yyyyys.:+//+oyyyyyyyo.
    ///   `oy/`oyyyyy/      ./syyyyy:
    ///   syyys`:yyyyyo`       :yyyyy:
    ///  /yyyyo  .syyyyy-       .yyyyy.
    ///  yyyyy.    +yyyyy/       /yyyy/
    /// `yyyyy      :yyyyys`     -yyyyo
    ///  yyyyy.      `syyyyy-    /yyyy/
    ///  /yyyyo        /yyyyy+  .yyyyy.
    ///   syyyys.       -yyyyys.:yyyy:
    ///   `oyyyyyo-`     `oyyyyy:.sy:
    ///     :syyyyyyso+/++`/yyyyyo``
    ///       -oyyyyyyyyyyy-.syyyys.
    ///          -/+osyyyyso.`+yyyyy/
    ///                        .-/+syo`
    ///                              `.
    /// ```
    pub fn venom() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VENOM,
            names: vec!["Venom"],
            colors: vec![Color::Black, Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Venom distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::venom_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///  ++**
    ///   *===**====+*
    ///    *====*   +===+
    ///  *==*+===*    *===*
    /// *===* *===+    *===*
    /// *===*  +===+   *===*
    /// *===*   +===*  *===*
    ///  *===*    *===+*==*
    ///    +===+   *===+=*
    ///       *+====**===*
    ///                **++
    /// ```
    pub fn venom_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VENOM_SMALL,
            names: vec!["Venom_small"],
            colors: vec![Color::Black, Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Vnux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::vnux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///               `
    ///            ^[XOx~.
    ///         ^_nwdbbkp0ti'
    ///         <vJCZw0LQ0Uj>
    ///           _j>!vC1,,
    ///      ,   ,CYOtO1(l;"
    /// `~-{r(1I ^/zmwJuc:I^
    /// '?)|U/}- ^fOCLLOw_,;
    ///  ,i,``. ",k%ooW@d"I,'
    ///   '    ;^u^<:^
    ///    ` .>>(@@@@nl[::
    ///     `!}?B%&WMMW&%}-}":
    ///     ^?jZWMMWWWWMMWofc;;`
    ///     <~x&&MWWWWWWWWp-l>[<
    ///  'ljmwn~tk8MWWWWM8OXr+]nC[
    /// !JZqwwdX:^C8#MMMM@XOdpdpq0<
    /// <wwwwmmpO10@%%%%8dnqmwmqqqJl
    /// ?QOZmqqqpbt[run/?!0pwqqQj-,
    ///  ^:l<{nUUv>      ^x00J("
    ///                    ^"
    /// ```
    pub fn vnux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VNUX,
            names: vec!["Vnux"],
            colors: vec![
                Color::TrueColor {
                    r: 0xff,
                    g: 0xff,
                    b: 0x00,
                },
                Color::TrueColor {
                    r: 0x80,
                    g: 0x80,
                    b: 0x80,
                },
                Color::TrueColor {
                    r: 0xff,
                    g: 0xff,
                    b: 0xff,
                },
                Color::Red,
                Color::White,
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Vzlinux distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::vzlinux().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              .::::::::.
    /// `/////`      :zzzzzzzz        ://///-
    ///  VVVVVu`         -zzz`       /VVVVV.
    ///  `dVVVVV        .zzz`       :VVVVV:
    ///   `VVVVVo       zzz        -VVVVV/
    ///    .VVVVV\     zzz/       .VVVVV+
    ///     -VVVVV:   zzzzzzzzz  .dVVVVs
    ///      \VVVVV-  `````````  VVVVVV
    ///       +VVVVV.           sVVVVu`
    ///        oVVVVd`         +VVVVd`
    ///         VVVVVV        /VVVVV.
    ///         `uVVVVs      -VVVVV-
    ///          `dVVVV+    .VVVVV/
    ///           .VVVVV\  `dVVVV+
    ///            -VVVVV-`uVVVVo
    ///             :VVVVVVVVVVV
    ///              \VVVVVVVVu
    ///               oVVVVVVd`
    ///                sVVVVV.
    ///                 ----.
    /// ```
    pub fn vzlinux() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VZLINUX,
            names: vec!["Vzlinux"],
            colors: vec![Color::Red, Color::White, Color::Yellow],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Void distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::void().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                 __.;=====;.__
    ///             _.=+==++=++=+=+===;.
    ///              -=+++=+===+=+=+++++=_
    ///         .     -=:``     `--==+=++==.
    ///        _vi,    `            --+=++++:
    ///       .uvnvi.       _._       -==+==+.
    ///      .vvnvnI`    .;==|==;.     :|=||=|.
    /// +QmQQmpvvnv; _yYsyQQWUUQQQm #QmQ#:QQQWUVQQm.
    ///  -QQWQWpvvowZ?.wQQQE==<QWWQ/QWQW.QQWW(: jQWQE
    ///   -QQQQmmU'  jQQQ@+=<QWQQ)mQQQ.mQQQC+;jWQQ@'
    ///    -WQ8YnI:   QWQQwgQQWV`mWQQ.jQWQQgyyWW@!
    ///      -1vvnvv.     `~+++`        ++|+++
    ///       +vnvnnv,                 `-|===
    ///        +vnvnvns.           .      :=-
    ///         -Invnvvnsi..___..=sv=.     `
    ///           +Invnvnvnnnnnnnnvvnn;.
    ///             ~|Invnvnvvnvvvnnv}+`
    ///                -~|{*l}*|~
    /// ```
    pub fn void() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VOID,
            names: vec!["void", "void-linux"],
            colors: vec![Color::Green, Color::Black],
            color_keys: Some(Color::White),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Void distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::void_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///     _______
    ///  _ \______ -
    /// | \  ___  \ |
    /// | | /   \ | |
    /// | | \___/ | |
    /// | \______ \_|
    ///  -_______\
    /// ```
    pub fn void_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_VOID_SMALL,
            names: vec!["void_small", "void-linux-small"],
            colors: vec![Color::Green],
            color_keys: Some(Color::White),
            color_title: Some(Color::Green),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of WiiLinuxNgx distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::wii_linux_ngx().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// '''''''            `~;:`            -''''''   ~kQ@@g\      ,EQ@@g/
    /// h@@@@@@'          o@@@@@9`         `@@@@@@D  `@@@@@@@=     @@@@@@@?
    /// '@@@@@@X         o@@@@@@@D         v@@@@@@:   R@@@@@@,     D@@@@@@_
    ///  t@@@@@@'       _@@@@@@@@@;       `Q@@@@@U     ;fmo/-       ;fmo/-
    ///  `Q@@@@@m       d@@@@@@@@@N       7@@@@@@'
    ///   L@@@@@@'     :@@@@@&@@@@@|     `Q@@@@@Z     :]]]]]v      :]]]]]v
    ///    Q@@@@@X     R@@@@Q`g@@@@Q     f@@@@@Q-     z@@@@@Q      v@@@@@Q
    ///    r@@@@@@~   ;@@@@@/ ;@@@@@L   `@@@@@@/      z@@@@@Q      v@@@@@Q
    ///     d@@@@@q   M@@@@#   H@@@@Q   ]@@@@@Q       z@@@@@Q      v@@@@@Q
    ///     ,@@@@@@, >@@@@@;   _@@@@@c `@@@@@@>       z@@@@@Q      v@@@@@Q
    ///      X@@@@@U Q@@@@R     Z@@@@Q`{@@@@@N        z@@@@@Q      v@@@@@Q
    ///      .@@@@@@S@@@@@:     -@@@@@e@@@@@@:        z@@@@@Q      v@@@@@Q
    ///       {@@@@@@@@@@U       t@@@@@@@@@@e         z@@@@@Q      v@@@@@Q
    ///       `Q@@@@@@@@@'       `Q@@@@@@@@@-         z@@@@@Q      v@@@@@Q
    ///        :@@@@@@@@|         ;@@@@@@@@=          z@@@@@Q      v@@@@@Q
    ///         '2#@@Q6:           ,eQ@@QZ~           /QQQQQg      \QQQQQN
    /// ```
    pub fn wii_linux_ngx() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_WII_LINUX_NGX,
            names: vec!["WiiLinuxNgx"],
            colors: vec![Color::Cyan, Color::White],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Windows 11
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::windows_11().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    ///
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// ```
    pub fn windows_11() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_WINDOWS_11,
            names: vec!["Windows 11", "Windows Server 2022"],
            colors: vec![Color::Blue, Color::Blue, Color::Blue, Color::Blue],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Normal,
        }
    }
    /// returns small logo of Windows 11
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::windows_11_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    /// lllllll  lllllll
    /// lllllll  lllllll
    /// lllllll  lllllll
    ///
    /// lllllll  lllllll
    /// lllllll  lllllll
    /// lllllll  lllllll
    /// ```
    pub fn windows_11_small() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_WINDOWS_11_SMALL,
            names: vec!["Windows 11_small", "Windows 11-small"],
            colors: vec![Color::Blue, Color::Blue, Color::Blue, Color::Blue],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::Cyan),
            logo_type: LogoType::Small,
        }
    }
    /// returns logo of Windows 8
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::windows_8().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                                 ..,
    ///                     ....,,:;+ccllll
    ///       ...,,+:;  cllllllllllllllllll
    /// ,cclllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    ///
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// `'ccllllllllll  lllllllllllllllllll
    ///        `' \*::  :ccllllllllllllllll
    ///                        ````''*::cll
    ///                                  ``
    /// ```
    pub fn windows_8() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_WINDOWS_8,
            names: vec![
                "Windows 8",
                "Windows 8.1",
                "Windows 10",
                "Windows Server 2012",
                "Windows Server 2012 R2",
                "Windows Server 2016",
                "Windows Server 2019",
            ],
            colors: vec![Color::Cyan, Color::Cyan, Color::Cyan, Color::Cyan],
            color_keys: Some(Color::Yellow),
            color_title: Some(Color::White),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Windows
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::windows().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         ,.=:!!t3Z3z.,
    ///        :tt:::tt333EE3
    ///        Et:::ztt33EEEL @Ee.,      ..,
    ///       ;tt:::tt333EE7 ;EEEEEEttttt33#
    ///      :Et:::zt333EEQ. EEEEEttttt33QL
    ///      it::::tt333EEF @EEEEEEttttt33F
    ///     ;3=*^```"*4EEV :EEEEEEttttt33@.
    ///     ,.=::::!t=., ` @EEEEEEtttz33QF
    ///    ;::::::::zt33)   "4EEEtttji3P*
    ///   :t::::::::tt33.:Z3z..  `` ,..g.
    ///   i::::::::zt33F AEEEtttt::::ztF
    ///  ;:::::::::t33V ;EEEttttt::::t3
    ///  E::::::::zt33L @EEEtttt::::z3F
    /// {3=*^```"*4E3) ;EEEtttt:::::tZ`
    ///              ` :EEEEtttt::::z7
    ///                  "VEzjt:;;z>*`
    /// ```
    pub fn windows() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_WINDOWS,
            names: vec![
                "Windows",
                "Windows 7",
                "Windows Server 2008",
                "Windows Server 2008 R2",
            ],
            colors: vec![Color::Red, Color::Green, Color::Blue, Color::Yellow],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Green),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Windows 95
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::windows_95().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                         ___
    ///                    .--=+++++=-:.
    /// .              _ *%@@@@@@@@@@@@@@*
    ///  *:+:.__ :+* @@@ @"_*&%@@%&&&*"@@@
    ///   "+.-#+ +%* " _ ++&&&%@@%&&&&&#@@
    /// "          , %@@ &&&&&%@@%&&&&&#@@
    ///    *  oo  *# " _ &&&&&%@@%&&&&&#@@
    /// "          , %@@ &&&&"@@@@#*"&&&@@
    /// .  *  oo  *# " _ %@@@@@@@@@@@@@@@@
    ///  *:+:.__ :=* %@@ @"**&%@@%&&&*"@@@
    ///   "+.-#+ +%* " _ &&&&&%@@%&&&&&#@@
    /// "          , %@@ &&&&&%@@%&&&&&#@@
    ///    *  oo  *# " _ &&&&&%@@%&&&&&#@@
    /// "          , %@@ &&*"%@@@@@@"*%&@@
    /// .  *  oo  *# " _ @@@@@@@@@@@@@@@@@
    ///  *:+:.__ :+# @@@ @%#=+""""""+==%#@
    ///   "+.-#+ +%* %+" "             ":@
    ///        " "
    /// ```
    pub fn windows_95() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_WINDOWS_95,
            names: vec!["Windows 95", "Windows 9x"],
            colors: vec![
                Color::Cyan,
                Color::Blue,
                Color::Yellow,
                Color::Green,
                Color::Red,
                Color::Black,
            ],
            color_keys: Some(Color::Cyan),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Xenia distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::xenia().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///       ,c.                       .c;
    ///     .KMMMk....             ....kMMMK.
    ///    .WMMMMMX.....         .....KMMMMMW.
    ///    XMMMMMMM0.....        ....OMMMMMMMN
    ///   dMMMMMMMMM;.... ..... ....,MMMMMMMMMd
    ///   WMMMMMMMMMl;okKKKKKKKKKOo;cMMMMMMMMMM
    ///  'MMMMMMMNXK0KKKKKKKKKKKKKKK0KXNMMMMMMM;
    ///  oMMMMMMMOxoKKKKKKKKKKKKKKKKKoxOMMMMMMMd
    ///  dMMMMMMMdxxxKKKKKKKKKKKKKKKxxxdNMMMMMMk
    ///  :MMMMX0xxxxxx0KKKKKKKK0KK0xxxxxx0XMMMMc
    ///   MMMOxxxxxxxxdxkdd0x0ddkxdxxxxxxxxOMMM
    ///  ;xxkxddxxxxdodxxxxdxdxxxxdodxxxxddxkxx;
    /// dxdKMMMWXo'.....'cdxxxdc'.....'lXWMMMXdxd
    /// cxdXMMMN,..........dxd'.........'XMMMNdxl
    ///  .xxWMMl...''....'.;k:.'....''...lMMWxx.
    /// ..:kXMMx..'....''..kMk..''....'..xMMXkc..
    ///  dMMMMMMd.....'...xMMMx...''....dMMMMMMx
    ///     kMMMMWOoc:coOkolllokOoc:coOWMMMMO
    ///          .MMMMMMMMl...lNMMMMMMM.
    ///             KMMMMMMXlKMMMMMMX
    ///                .MMMMMMMMM.
    /// ```
    pub fn xenia() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_XENIA,
            names: vec!["Xenia"],
            colors: vec![Color::Yellow, Color::Green, Color::Red],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Xferience distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::xferience().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            ``--:::::::-.`
    ///         .-/+++ooooooooo+++:-`
    ///      `-/+oooooooooooooooooo++:.
    ///     -/+oooooo/+ooooooooo+/ooo++:`
    ///   `/+oo++oo.   .+oooooo+.-: +:-o+-
    ///  `/+o/.  -o.    :oooooo+ ```:.+oo+-
    /// `:+oo-    -/`   :oooooo+ .`-`+oooo/.
    /// .+ooo+.    .`   `://///+-+..oooooo+:`
    /// -+ooo:`                ``.-+oooooo+/`
    /// -+oo/`                       :+oooo/.
    /// .+oo:            ..-/. .      -+oo+/`
    /// `/++-         -:::++::/.      -+oo+-
    ///  ./o:          `:///+-     `./ooo+:`
    ///   .++-         `` /-`   -:/+oooo+:`
    ///    .:+/:``          `-:ooooooo++-
    ///      ./+o+//:...../+oooooooo++:`
    ///        `:/++ooooooooooooo++/-`
    ///           `.-//++++++//:-.`
    ///                ``````
    /// ```
    pub fn xferience() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_XFERIENCE,
            names: vec!["Xferience"],
            colors: vec![Color::Cyan, Color::Cyan],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Xray_os distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::xray_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///                rrrrrraaaaaaaaaaaayyyy
    ///            xxrrrrrrrraaaaaaaaaaaayyyyyyyyy
    ///         xxxxxrrrrrrrraaaaaaaaaaaayyyyyyyyyyyyyyyyyyyyyy
    ///       xxxxxxxrrrrrrrraaaaa aaaaayyyyyyyyyyyyyyyyyyy
    ///     xxxxxxxxxrrrrrrrraaaa aaaaaaayyyyyyyyyyyyyyyyy
    ///   xxxxxxxxxxxrrrrrrrraa aaaaaaaaayyyyyyyyyyyyyy yy
    ///  xxxxxxxxxxxxrrrrrrrra aaaaaaaaaayyyyyyyyyyyyyyyyyy
    ///  xxxxxxxxxxxxrrrrrrrr aaaaaaaaaaayyyyyyyyyyyyyyyyyyy
    /// xxxxxxxxxxxxxrrrrrr raaaaaaaaaaaayyyyyyyyyyyyyyyyyyyy
    /// xxxxxxxxxxxxxrrrrr rraaaaaaaaaaaayyyyyyyyyyyyy yyyyyy
    /// xxxxxxxxxxxxxrrrrrrrraaaaaaaaaaaayyyyyyyyyy yyyyyyyyy
    /// xxxxxxxxxxxxxrrrrrrrraaaaaaaaaaaayyyyyyy yyyyyyyyyyyy
    /// xxxxxxxxxxxxxrrrrrrrraaaaaaaaaaaayyyy yyyyyyyyyyyyyy
    ///  xxxxxxxxxxxxrrrrrrrra aaaaaaaaaay yyyyyyyyyyyyyyyy
    ///   xxxxxxxxxxxrrrrrrr aaaaaaaaaaaayyyyyyyyyyyyyyyyy
    ///    xxxxxxxxxxrrrrrr raaaaaaaaaaaa yyyyyyyyyyyyyyy
    ///      xxxxxxxxrrrr rrraaaaaaaaa aayyyyyyyyyyyyyy
    ///        xxxxxxrrrrrrr aaaaaa  aaaayyyyyyyyyyyy
    ///           xxxrrrrrr raaa  aaaaaaayyyyyyyyy
    ///               rrrr rr  aaaaaaaaaayyyyyy
    ///                  r   aaaaaaaaaa
    /// ```
    pub fn xray_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_XRAY_OS,
            names: vec!["Xray_OS"],
            colors: vec![
                Color::TrueColor {
                    r: 0xff,
                    g: 0xff,
                    b: 0xff,
                },
                Color::TrueColor {
                    r: 0x00,
                    g: 0xff,
                    b: 0xff,
                },
                Color::TrueColor { r: 0, g: 0, b: 0 },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of YiffOS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::yiff_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///            NK
    ///             NxKXW
    ///             WOlcoXW
    ///             0olccloxNW
    ///             XxllllllloxOKNW
    ///             Nklllllllllllodk0XWW
    ///            N0dllllllllllllllodxOKNW
    ///          WKxlllllllllllllllllllooxOKN
    ///        WKdllllllllllllllooooooooooooo
    ///       Nxllllllllllllllloooooooooooooo
    ///     XXNOolllllllllllloooooooooooooooo
    ///   WX0xolllllllllllooooooooooooooooooo
    /// XWN0xollllllllllooooooooooooooooooooo
    ///   Kkdolllllllooooddddddoooooooooooddo
    ///        K00XNW      WX0xdooooooddddddd
    ///                       WXOxdoooooodddd
    ///                          WXOxdddddddd
    ///                                NX0ddd
    ///                                  WN0d
    /// ```
    pub fn yiff_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_YIFFOS,
            names: vec!["YiffOS"],
            colors: vec![
                Color::TrueColor {
                    r: 0x87,
                    g: 0x00,
                    b: 0xff,
                },
                Color::TrueColor {
                    r: 0x87,
                    g: 0x00,
                    b: 0xd7,
                },
            ],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of Zorin distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::zorin().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///         `osssssssssssssssssssso`
    ///        .osssssssssssssssssssssso.
    ///       .+oooooooooooooooooooooooo+.
    ///
    ///
    ///   `::::::::::::::::::::::.         .:`
    ///  `+ssssssssssssssssss+:.`     `.:+ssso`
    /// .ossssssssssssssso/.       `-+ossssssso.
    /// ssssssssssssso/-`      `-/osssssssssssss
    /// .ossssssso/-`      .-/ossssssssssssssso.
    ///  `+sss+:.      `.:+ssssssssssssssssss+`
    ///   `:.         .::::::::::::::::::::::`
    ///
    ///
    ///       .+oooooooooooooooooooooooo+.
    ///        -osssssssssssssssssssssso-
    ///         `osssssssssssssssssssso`
    /// ```
    pub fn zorin() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ZORIN,
            names: vec!["zorin", "zorin-linux", "zorinos", "zorinos-linux"],
            colors: vec![Color::Blue],
            color_keys: Some(Color::Blue),
            color_title: Some(Color::Blue),
            logo_type: LogoType::Normal,
        }
    }
    /// returns logo of z/OS distribution
    ///
    /// # Example
    ///
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::z_os().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```term
    ///              //  OOOOOOO  SSSSSSS
    ///             //  OO    OO SS
    ///     zzzzzz //  OO    OO SS
    ///       zz  //  OO    OO   SSSS
    ///     zz   //  OO    OO       SS
    ///   zz    //  OO    OO       SS
    /// zzzzzz //   OOOOOOO  SSSSSSS
    /// ```
    pub fn z_os() -> Self {
        Self {
            lines: FASTFETCH_DATATEXT_LOGO_ZOS,
            names: vec!["z/OS", "zos"],
            colors: vec![Color::Blue],
            color_keys: None,
            color_title: None,
            logo_type: LogoType::Normal,
        }
    }
}
