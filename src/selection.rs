use crate::AsciiLogo;
use strum_macros::EnumIter; // 0.17.1

/// Enum to selct logo
#[derive(Debug, EnumIter, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub enum LogoSelection {
    /// Unknown Logo
    /// ```text
    ///        ________
    ///    _jgN########Ngg_
    ///  _N##N@@""  ""9NN##Np_
    /// d###P            N####p
    /// "^^"              T####
    ///                   d###P
    ///                _g###@F
    ///             _gN##@P
    ///           gN###F"
    ///          d###F
    ///         0###F
    ///         0###F
    ///         0###F
    ///         "NN@'
    ///
    ///          ___
    ///         q###r
    ///          ""
    /// ``````
    #[default]
    Unknown,
    /// Adelie Logo
    /// ```text
    ///                   ,-^-___
    ///                 /\\\///
    /// refined.       /\\\\//
    /// reliable.     /\\\///
    /// ready.       /\\/////\
    ///         __///\\\\/////\
    ///      _//////\\\\\\\////
    ///    ///////\\\\\\\\\\//
    ///           //////\\\\\/
    ///           /////\\\\\/
    ///           /////\\\\/
    ///          /\\///\\\/
    ///          /\\\/\\/
    ///          /\\\\//
    ///         //////
    ///       /// \\\\\
    /// ```
    Adelie,
    /// AerOs Logo
    /// ```text
    ///                  ooo OOO OOO ooo
    ///              oOO                 OOo
    ///          oOO                         OOo
    ///       oOO                               OOo
    ///     oOO                                   OOo
    ///   oOO                                       OOo
    ///  oOO                                         OOo
    ///                                               OOo
    ///                                                OOo
    ///                                                OOo
    ///                                                OOo
    ///                                                OOo
    ///                                                OOo
    /// oOO                                           OOo
    ///  oOO                                         OOo
    ///   oOO                                       OOo
    ///     oOO                                   OOo
    ///       oO                                OOo
    ///          oOO                         OOo
    ///              oOO                 OOo
    ///                  ooo OOO OOO ooo
    /// ```
    AerOs,
    /// Afterglow Logo
    /// ```text
    ///                         .
    ///                .      .{!
    ///              .L!     J@||*
    ///            gJJJJL` g@FFS"
    ///         ,@FFFJF`_g@@LLP`
    ///       _@FFFFF`_@@@@@P`        .
    ///     J@@@LLF _@@@@@P`        .J!
    ///   g@@@@@" _@@@@@P`.       .L|||*
    /// g@@@@M"     "VP`.L!     <@JJJJ`
    ///  "@N"         :||||!  JFFFFS"
    ///            .{JJ||F`_gFFFF@'
    ///          .@FJJJF`,@LFFFF`
    ///        _@FFFFF   VLLLP`
    ///      J@@LL@"      `"
    ///       V@@"
    /// ```
    Afterglow,
    /// AIX Logo
    /// ```text
    ///            `:+ssssossossss+-`
    ///         .oys///oyhddddhyo///sy+.
    ///       /yo:+hNNNNNNNNNNNNNNNNh+:oy/
    ///     :h/:yNNNNNNNNNNNNNNNNNNNNNNy-+h:
    ///   `ys.yNNNNNNNNNNNNNNNNNNNNNNNNNNy.ys
    ///  `h+-mNNNNNNNNNNNNNNNNNNNNNNNNNNNNm-oh
    ///  h+-NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN.oy
    /// /d`mNNNNNNN/::mNNNd::m+:/dNNNo::dNNNd`m:
    /// h//NNNNNNN: . .NNNh  mNo  od. -dNNNNN:+y
    /// N.sNNNNNN+ -N/ -NNh  mNNd.   sNNNNNNNo-m
    /// N.sNNNNNs  +oo  /Nh  mNNs` ` /mNNNNNNo-m
    /// h//NNNNh  ossss` +h  md- .hm/ `sNNNNN:+y
    /// :d`mNNN+/yNNNNNd//y//h//oNNNNy//sNNNd`m-
    ///  yo-NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNm.ss
    ///  `h+-mNNNNNNNNNNNNNNNNNNNNNNNNNNNNm-oy
    ///    sy.yNNNNNNNNNNNNNNNNNNNNNNNNNNs.yo
    ///     :h+-yNNNNNNNNNNNNNNNNNNNNNNs-oh-
    ///       :ys:/yNNNNNNNNNNNNNNNmy/:sy:
    ///         .+ys///osyhhhhys+///sy+.
    ///             -/osssossossso/-
    /// ```
    Aix,
    /// Almalinux Logo
    /// ```text
    ///          'c:.
    ///         lkkkx, ..       ..   ,cc,
    ///         okkkk:ckkx'  .lxkkx.okkkkd
    ///         .:llcokkx'  :kkkxkko:xkkd,
    ///       .xkkkkdood:  ;kx,  .lkxlll;
    ///        xkkx.       xk'     xkkkkk:
    ///        'xkx.       xd      .....,.
    ///       .. :xkl'     :c      ..''..
    ///     .dkx'  .:ldl:'. '  ':lollldkkxo;
    ///   .''lkkko'                     ckkkx.
    /// 'xkkkd:kkd.       ..  ;'        :kkxo.
    /// ,xkkkd;kk'      ,d;    ld.   ':dkd::cc,
    ///  .,,.;xkko'.';lxo.      dx,  :kkk'xkkkkc
    ///      'dkkkkkxo:.        ;kx  .kkk:;xkkd.
    ///        .....   .;dk:.   lkk.  :;,
    ///              :kkkkkkkdoxkkx
    ///               ,c,,;;;:xkkd.
    ///                 ;kkkkl...
    ///                 ;kkkkl
    ///                  ,od;
    /// ```
    Almalinux,
    /// Alpine Logo
    /// ```text
    ///        .hddddddddddddddddddddddh.
    ///       :dddddddddddddddddddddddddd:
    ///      /dddddddddddddddddddddddddddd/
    ///     +dddddddddddddddddddddddddddddd+
    ///   `sdddddddddddddddddddddddddddddddds`
    ///  `ydddddddddddd++hdddddddddddddddddddy`
    /// .hddddddddddd+`  `+ddddh:-sdddddddddddh.
    /// hdddddddddd+`      `+y:    .sddddddddddh
    /// ddddddddh+`   `//`   `.`     -sddddddddd
    /// ddddddh+`   `/hddh/`   `:s-    -sddddddd
    /// ddddh+`   `/+/dddddh/`   `+s-    -sddddd
    /// ddd+`   `/o` :dddddddh/`   `oy-    .yddd
    /// hdddyo+ohddyosdddddddddho+oydddy++ohdddh
    /// .hddddddddddddddddddddddddddddddddddddh.
    ///  `yddddddddddddddddddddddddddddddddddy`
    ///   `sdddddddddddddddddddddddddddddddds`
    ///     +dddddddddddddddddddddddddddddd+
    ///      /dddddddddddddddddddddddddddd/
    ///       :dddddddddddddddddddddddddd:
    ///        .hddddddddddddddddddddddh.
    /// ```
    Alpine,
    /// Small Alpine Logo
    /// ```text
    ///    /\ /\
    ///   // \  \
    ///  //   \  \
    /// ///    \  \
    /// //      \  \
    ///          \
    /// ```
    AlpineSmall,
    /// Alternative Small Alpine Logo
    /// ```text
    ///    /\\ /\\
    ///   // \\  \\
    ///  ///  \\  \\
    /// ///    \\  \\
    /// //      \\  \\
    ///          \\
    /// ```
    Alpine2Small,
    /// Alter Logo
    /// ```text
    ///                       %,
    ///                     ^WWWw
    ///                    'wwwwww
    ///                   !wwwwwwww
    ///                  #`wwwwwwwww
    ///                 @wwwwwwwwwwww
    ///                wwwwwwwwwwwwwww
    ///               wwwwwwwwwwwwwwwww
    ///              wwwwwwwwwwwwwwwwwww
    ///             wwwwwwwwwwwwwwwwwwww,
    ///            w~1i.wwwwwwwwwwwwwwwww,
    ///          3~:~1lli.wwwwwwwwwwwwwwww.
    ///         :~~:~?ttttzwwwwwwwwwwwwwwww
    ///        #<~:~~~~?llllltO-.wwwwwwwwwww
    ///       #~:~~:~:~~?ltlltlttO-.wwwwwwwww
    ///      @~:~~:~:~:~~(zttlltltlOda.wwwwwww
    ///     @~:~~: ~:~~:~:(zltlltlO    a,wwwwww
    ///    8~~:~~:~~~~:~~~~_1ltltu          ,www
    ///   5~~:~~:~~:~~:~~:~~~_1ltq             N,,
    ///  g~:~~:~~~:~~:~~:~:~~~~1q                N,
    /// ```
    Alter,
    /// Amazon Logo
    /// ```text
    ///              `-/oydNNdyo:.`
    ///       `.:+shmMMMMMMMMMMMMMMmhs+:.`
    ///     -+hNNMMMMMMMMMMMMMMMMMMMMMMNNho-
    /// .``      -/+shmNNMMMMMMNNmhs+/-      ``.
    /// dNmhs+:.       `.:/oo/:.`       .:+shmNd
    /// dMMMMMMMNdhs+:..        ..:+shdNMMMMMMMd
    /// dMMMMMMMMMMMMMMNds    odNMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// dMMMMMMMMMMMMMMMMh    yMMMMMMMMMMMMMMMMd
    /// .:+ydNMMMMMMMMMMMh    yMMMMMMMMMMMNdy+:.
    ///      `.:+shNMMMMMh    yMMMMMNhs+:``
    ///             `-+shy    shs+:`
    /// ```
    Amazon,
    /// Amazon Linux Logo
    /// ```text
    ///   ,     #_
    ///   ~\_  ####_
    ///  ~~  \_#####\
    ///  ~~     \###|
    ///  ~~       \#/ ___
    ///   ~~       V~' '->
    ///    ~~~         /
    ///      ~~._.   _/
    ///         _/ _/
    ///       _/m/'
    /// ```
    AmazonLinux,
    /// AmogOS Logo
    /// ```text
    ///              ___________
    ///             /           \
    ///            /   ______    \
    ///           /   /      \    \
    ///           |  (        )    \
    ///          /    \______/     |
    ///          |                 |
    ///         /                   \
    ///         |                   |
    ///         |                   |
    ///        /                    |
    ///        |                    |
    ///        |     _______        |
    ///   ____/     /       \       |
    ///  /          |       |       |
    ///  |          /   ____/       |
    ///  \_________/   /            |
    ///                \         __/
    ///                 \_______/
    /// ```
    AmogOs,
    /// Anarchy Logo
    /// ```text
    ///                          ..
    ///                         ..
    ///                       :..
    ///                     :+++.
    ///               .:::+++++++::.
    ///           .:+######++++######+:.
    ///        .+#########+++++##########:.
    ///      .+##########+++++++##+#########+.
    ///     +###########+++++++++############:
    ///    +##########++++++#++++#+###########+
    ///   +###########+++++###++++#+###########+
    ///  :##########+#++++####++++#+############:
    ///  ###########+++++#####+++++#+###++######+
    /// .##########++++++#####++++++++++++#######.
    /// .##########+++++++++++++++++++###########.
    ///  #####++++++++++++++###++++++++#########+
    ///  :###++++++++++#########+++++++#########:
    ///   +######+++++##########++++++++#######+
    ///    +####+++++###########+++++++++#####+
    ///     :##++++++############++++++++++##:
    ///      .++++++#############+++++++++++.
    ///       :++++###############+++++++::
    ///      .++. .:+##############+++++++..
    ///      .:.      ..::++++++::..:+++++.
    ///      .                       .:+++.
    ///                                 .::
    ///                                    ..
    ///                                     ..
    /// ```
    Anarchy,
    /// Android Logo
    /// ```text
    ///          -o          o-
    ///           +hydNNNNdyh+
    ///         +mMMMMMMMMMMMMm+
    ///       `dMMm:NMMMMMMN:mMMd`
    ///       hMMMMMMMMMMMMMMMMMMh
    ///   ..  yyyyyyyyyyyyyyyyyyyy  ..
    /// .mMMm`MMMMMMMMMMMMMMMMMMMM`mMMm.
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// :MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM:
    /// -MMMM-MMMMMMMMMMMMMMMMMMMM-MMMM-
    ///  +yy+ MMMMMMMMMMMMMMMMMMMM +yy+
    ///       mMMMMMMMMMMMMMMMMMMm
    ///       `/++MMMMh++hMMMM++/`
    ///           MMMMo  oMMMM
    ///           MMMMo  oMMMM
    ///           oNMm-  -mMNs
    /// ```
    Android,
    /// Small Android Logo
    /// ```text
    ///   ;,           ,;
    ///    ';,.-----.,;'
    ///   ,'           ',
    ///  /    O     O    \
    /// |                 |
    /// '-----------------'
    /// ```
    AndroidSmall,
    /// Antergos Logo
    /// ```text
    ///               `.-/::/-``
    ///             .-/osssssssso/.
    ///            :osyysssssssyyys+-
    ///         `.+yyyysssssssssyyyyy+.
    ///        `/syyyyyssssssssssyyyyys-`
    ///       `/yhyyyyysss++ssosyyyyhhy/`
    ///      .ohhhyyyyso++/+oso+syy+shhhho.
    ///     .shhhhysoo++//+sss+++yyy+shhhhs.
    ///    -yhhhhs+++++++ossso+++yyys+ohhddy:
    ///   -yddhhyo+++++osyyss++++yyyyooyhdddy-
    ///  .yddddhso++osyyyyys+++++yyhhsoshddddy`
    /// `odddddhyosyhyyyyyy++++++yhhhyosddddddo
    /// .dmdddddhhhhhhhyyyo+++++shhhhhohddddmmh.
    /// ddmmdddddhhhhhhhso++++++yhhhhhhdddddmmdy
    /// dmmmdddddddhhhyso++++++shhhhhddddddmmmmh
    /// -dmmmdddddddhhyso++++oshhhhdddddddmmmmd-
    /// .smmmmddddddddhhhhhhhhhdddddddddmmmms.
    ///    `+ydmmmdddddddddddddddddddmmmmdy/.
    ///       `.:+ooyyddddddddddddyyso+:.`
    /// ```
    Antergos,
    /// Antix Logo
    /// ```text
    ///                     \
    ///          , - ~ ^ ~ - \        /
    ///      , '              \ ' ,  /
    ///    ,                   \   '/
    ///   ,                     \  / ,
    ///  ,___,                   \/   ,
    ///  /   |   _  _  _|_ o     /\   ,
    /// |,   |  / |/ |  |  |    /  \  ,
    ///  \,_/\_/  |  |_/|_/|_/_/    \,
    ///    ,                  /     ,\
    ///      ,               /  , '   \
    ///       ' - , _ _ _ ,  '
    /// ```
    Antix,
    /// Aosc OS Retro Logo
    /// ```text
    ///           .........
    ///      ...................
    ///    .....................################
    ///  ..............     ....################
    /// ..............       ...################
    /// .............         ..****************
    /// ............     .     .****************
    /// ...........     ...     ................
    /// ..........     .....     ...............
    /// .........     .......     ...
    ///  .......                   .
    ///   .....      .........    ...........
    ///   ....      .......       ...........
    ///   ...      .......        ...........
    ///   ................        ***********
    ///   ................        ###########
    ///   ****************
    ///   ################
    /// ```
    AoscOsRetro,
    /// Small Aosc OS Retro Logo
    /// ```text
    ///     _____   _____
    ///   -'     '-|     |
    ///  /     ___ |     |
    /// |     / _ \\|_____|
    /// '    / /_\\ \\
    ///  \\  / _____ \\___
    ///   |/_/  |   |   |
    ///   |     |   |___|
    ///   |_____|
    /// ```
    AoscOsRetroSmall,
    /// Aosc OS Logo
    /// ```text
    ///                 __
    ///              gpBBBBBBBBBP
    ///          _gBBBBBBBBBRP
    ///        4BBBBBBBBRP  ,_____
    ///             `"" _g@@@@@@@@@@@@@%g>
    ///             __@@@@@@@@@@@@@@@@P"  ___
    ///          _g@@@@@@@@@@@@@@@N"` _gN@@@@@N^
    ///      _w@@@@@@@@@@@@@@@@P" _g@@@@@@@P"
    ///   _g@@@@@@@@@@@@@@@N"`  VMNN@NNNM^`
    /// ^MMM@@@@@@@@@@@MP" ,ggppww__
    ///         `""""" _wNNNNNNNNNNNNNNNNNNN
    ///             _gBNNNNNNNNNNNNNNNNNP"
    ///         _wNNNNNNNNNNNNNNNNNNMP`
    ///      _gBNNNNNNNNNNNNNNNNNP"
    ///  _wNNNNNNNNNNNNNNNNNNNM^
    ///  ""Y^^MNNNNNNNNNNNNP`
    ///          `"""""""
    /// ```
    AoscOs,
    /// Old Aosc OS Logo
    /// ```text
    ///              .:+syhhhhys+:.
    ///          .ohNMMMMMMMMMMMMMMNho.
    ///       `+mMMMMMMMMMMmdmNMMMMMMMMm+`
    ///      +NMMMMMMMMMMMM/   `./smMMMMMN+
    ///    .mMMMMMMMMMMMMMMo        -yMMMMMm.
    ///   :NMMMMMMMMMMMMMMMs          .hMMMMN:
    ///  .NMMMMhmMMMMMMMMMMm+/-         oMMMMN.
    ///  dMMMMs  ./ymMMMMMMMMMMNy.       sMMMMd
    /// -MMMMN`      oMMMMMMMMMMMN:      `NMMMM-
    /// /MMMMh       NMMMMMMMMMMMMm       hMMMM/
    /// /MMMMh       NMMMMMMMMMMMMm       hMMMM/
    /// -MMMMN`      :MMMMMMMMMMMMy.     `NMMMM-
    ///  dMMMMs       .yNMMMMMMMMMMMNy/. sMMMMd
    ///  .NMMMMo         -/+sMMMMMMMMMMMmMMMMN.
    ///   :NMMMMh.          .MMMMMMMMMMMMMMMN:
    ///    .mMMMMMy-         NMMMMMMMMMMMMMm.
    ///      +NMMMMMms/.`    mMMMMMMMMMMMN+
    ///       `+mMMMMMMMMNmddMMMMMMMMMMm+`
    ///          .ohNMMMMMMMMMMMMMMNho.
    ///              .:+syhhhhys+:.
    /// ```
    AoscOsOld,
    /// Aperture Logo
    /// ```text
    ///               .,-:;//;:=,
    ///           . :H@@@MM@M#H/.,+%;,
    ///        ,/X+ +M@@M@MM%=,-%HMMM@X/,
    ///      -+@MM; SM@@MH+-,;XMMMM@MMMM@+-
    ///     ;@M@@M- XM@X;. -+XXXXXHHH@M@M#@/.
    ///   ,%MM@@MH ,@%=             .---=-=:=,.
    ///   =@#@@@MX.,                -%HXSS%%%:;
    ///  =-./@M@MS                   .;@MMMM@MM:
    ///  X@/ -SMM/                    . +MM@@@MS
    /// ,@M@H: :@:                    . =X#@@@@-
    /// ,@@@MMX, .                    /H- ;@M@M=
    /// .H@@@@M@+,                    %MM+..%#S.
    ///  /MMMM@MMH/.                  XM@MH; =;
    ///   /%+%SXHH@S=              , .H@@@@MX,
    ///    .=--------.           -%H.,@@@@@MX,
    ///    .%MM@@@HHHXXSSS%+- .:SMMX =M@@MM%.
    ///      =XMMM@MM@MM#H;,-+HMM@M+ /MMMX=
    ///        =%@M@M#@S-.=S@MM@@@M; %M%=
    ///          ,:+S+-,/H#MMMMMMM@= =,
    ///                =++%%%%+/:-.
    /// ```
    Aperture,
    /// Apricity Logo
    /// ```text
    ///                                     ./o-
    ///           ``...``              `:. -/:
    ///      `-+ymNMMMMMNmho-`      :sdNNm/
    ///    `+dMMMMMMMMMMMMMMMmo` sh:.:::-
    ///   /mMMMMMMMMMMMMMMMMMMMm/`sNd/
    ///  oMMMMMMMMMMMMMMMMMMMMMMMs -`
    /// :MMMMMMMMMMMMMMMMMMMMMMMMM/
    /// NMMMMMMMMMMMMMMMMMMMMMMMMMd
    /// MMMMMMMmdmMMMMMMMMMMMMMMMMd
    /// MMMMMMy` .mMMMMMMMMMMMmho:`
    /// MMMMMMNo/sMMMMMMMNdy+-.`-/
    /// MMMMMMMMMMMMNdy+:.`.:ohmm:
    /// MMMMMMMmhs+-.`.:+ymNMMMy.
    /// MMMMMM/`.-/ohmNMMMMMMy-
    /// MMMMMMNmNNMMMMMMMMmo.
    /// MMMMMMMMMMMMMMMms:`
    /// MMMMMMMMMMNds/.
    /// dhhyys+/-`
    /// ```
    Apricity,
    /// ArchBox Logo
    /// ```text
    ///               ...:+oh/:::..
    ///          ..-/oshhhhhh`   `::::-.
    ///      .:/ohhhhhhhhhhhh`        `-::::.
    ///  .+shhhhhhhhhhhhhhhhh`             `.::-.
    ///  /`-:+shhhhhhhhhhhhhh`            .-/+shh
    ///  /      .:/ohhhhhhhhh`       .:/ohhhhhhhh
    ///  /           `-:+shhh`  ..:+shhhhhhhhhhhh
    ///  /                 .:ohhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /                  `hhhhhhhhhhhhhhhhhhhh
    ///  /      .+o+        `hhhhhhhhhhhhhhhhhhhh
    ///  /     -hhhhh       `hhhhhhhhhhhhhhhhhhhh
    ///  /     ohhhhho      `hhhhhhhhhhhhhhhhhhhh
    ///  /:::+`hhhhoos`     `hhhhhhhhhhhhhhhhhs+`
    ///     `--/:`   /:     `hhhhhhhhhhhho/-
    ///              -/:.   `hhhhhhs+:-`
    ///                 ::::/ho/-`
    /// ```
    ArchBox,
    /// Archcraft Logo
    /// ```text
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢰⡆⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢠⣿⣿⡄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣾⣿⣿⣿⡀⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⣿⣷⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢼⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣤⣈⠻⢿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣮⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⡿⣿⣿⡟⠄⠄⠸⣿⣿⡿⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⠄⠄⣼⣿⣿⣿⣿⣿⡏⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠈⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⠄⢀⣼⣿⣿⣿⣿⣿⣿⡗⠄⠄⠄⢀⣠⣤⣀⠄⠄⠄⠸⣿⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⠄⢀⣾⣿⣿⣿⣿⣿⡏⠁⠄⠄⠄⢠⣿⣿⣿⣿⡇⠄⠄⠄⠄⢙⣿⣿⣻⠿⣿⣷⡀⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⠄⢀⣾⣿⣿⣿⣿⣿⣿⣷⣤⡀⠄⠄⠄⠻⣿⣿⡿⠃⠄⠄⠄⢀⣼⣿⣿⣿⣿⣦⣌⠙⠄⠄⠄⠄⠄
    /// ⠄⠄⠄⢠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⠏⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠄⠄⠄
    /// ⠄⠄⢠⣿⣿⣿⣿⣿⣿⣿⡿⠟⠋⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠙⠻⣿⣿⣿⣿⣿⣿⣿⣿⡄⠄⠄
    /// ⠄⣠⣿⣿⣿⣿⠿⠛⠋⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠉⠙⠻⢿⣿⣿⣿⣿⣆⠄
    /// ⡰⠟⠛⠉⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠉⠙⠛⠿⢆
    /// ```
    Archcraft,
    /// Alternative Archcraft Logo
    /// ```text
    ///                    -o\
    ///                   :ooo:
    ///                  .ooooo.
    ///                  ooooooo.
    ///                 +oooooooo.
    ///                -oooooooooo.
    ///               --:-+oooooooo.
    ///              yooo+=+sooooooo.
    ///             yoooooosooooooooo.
    ///            y+ooooooooooooooooo.
    ///           yoooooooooooooooooooo`
    ///          yoooooo+oo=  :oo++ooooo`
    ///         :oooooo.           +ooooo-
    ///        -ooooooo.   .::.    +ooosoo=
    ///       -oooooo`    .oooo`     +os-=o=
    ///      =ooooooo=:    `oo+    :=ooo=--`.
    ///     +ooooooooos.          .=sooooooo+-
    ///   .+osossos+-`              `-+osososs+.
    ///  :sss+=-:`                     `:-=+ssss:
    /// :=-:`                                `-=+:
    /// ```
    Archcraft2,
    /// Arch Logo
    /// ```text
    ///                   -`
    ///                  .o+`
    ///                 `ooo/
    ///                `+oooo:
    ///               `+oooooo:
    ///               -+oooooo+:
    ///             `/:-:++oooo+:
    ///            `/++++/+++++++:
    ///           `/++++++++++++++:
    ///          `/+++ooooooooooooo/`
    ///         ./ooosssso++osssssso+`
    ///        .oossssso-````/ossssss+`
    ///       -osssssso.      :ssssssso.
    ///      :osssssss/        osssso+++.
    ///     /ossssssss/        +ssssooo/-
    ///   `/ossssso+/:-        -:/+osssso+-
    ///  `+sso+:-`                 `.-/+oso:
    /// `++:.                           `-/+/
    /// .`                                 `/
    /// ```
    Arch,
    /// Alternative Arch Logo
    /// ```text
    ///                   ▄
    ///                  ▟█▙
    ///                 ▟███▙
    ///                ▟█████▙
    ///               ▟███████▙
    ///              ▂▔▀▜██████▙
    ///             ▟██▅▂▝▜█████▙
    ///            ▟█████████████▙
    ///           ▟███████████████▙
    ///          ▟█████████████████▙
    ///         ▟███████████████████▙
    ///        ▟█████████▛▀▀▜████████▙
    ///       ▟████████▛      ▜███████▙
    ///      ▟█████████        ████████▙
    ///     ▟██████████        █████▆▅▄▃▂
    ///    ▟██████████▛        ▜█████████▙
    ///   ▟██████▀▀▀              ▀▀██████▙
    ///  ▟███▀▘                       ▝▀███▙
    /// ▟▛▀                               ▀▜▙
    /// ```
    Arch2,
    /// Small Arch Logo
    /// ```
    /// use ascii_logos::AsciiLogo;
    ///
    /// AsciiLogo::arch_small().print_uncolored(None);
    /// ```
    /// will result in:
    ///
    /// ```text
    ///       /\
    ///      /  \
    ///     /    \
    ///    /      \
    ///   /   ,,   \
    ///  /   |  |   \
    /// /_-''    ''-_\
    /// ```
    ArchSmall,
    /// ARCHlabs Logo
    /// ```text
    ///                      'c'
    ///                     'kKk,
    ///                    .dKKKx.
    ///                   .oKXKXKd.
    ///                  .l0XXXXKKo.
    ///                  c0KXXXXKX0l.
    ///                 :0XKKOxxOKX0l.
    ///                :OXKOc. .c0XX0l.
    ///               :OK0o. ...'dKKX0l.
    ///              :OX0c  ;xOx''dKXX0l.
    ///             :0KKo..o0XXKd'.lKXX0l.
    ///            c0XKd..oKXXXXKd..oKKX0l.
    ///          .c0XKk;.l0K0OO0XKd..oKXXKo.
    ///         .l0XXXk:,dKx,.'l0XKo..kXXXKo.
    ///        .o0XXXX0d,:x;   .oKKx'.dXKXXKd.
    ///       .oKXXXXKK0c.;.    :00c'cOXXXXXKd.
    ///      .dKXXXXXXXXk,.     cKx''xKXXXXXXKx'
    ///     'xKXXXXK0kdl:.     .ok; .cdk0KKXXXKx'
    ///    'xKK0koc,..         'c
    /// ```
    ArchLabs,
    /// ArchStrike Logo
    /// ```text
    ///                    *
    ///                   **.
    ///                  ****
    ///                 ******
    ///                 *******
    ///               ** *******
    ///              **** *******
    ///             ****_____***/*
    ///            ***/*******//***
    ///           **/********///*/**
    ///          **/*******////***/**
    ///         **/****//////.,****/**
    ///        ***/*****/////////**/***
    ///       ****/****    /////***/****
    ///      ******/***  ////   **/******
    ///     ********/* ///      */********
    ///   ,******     // ______ /    ******,
    /// ```
    ArchStrike,
    /// Artix Logo
    /// ```text
    ///                    '
    ///                   'o'
    ///                  'ooo'
    ///                 'ooxoo'
    ///                'ooxxxoo'
    ///               'oookkxxoo'
    ///              'oiioxkkxxoo'
    ///             ':;:iiiioxxxoo'
    ///                `'.;::ioxxoo'
    ///           '-.      `':;jiooo'
    ///          'oooio-..     `'i:io'
    ///         'ooooxxxxoio:,.   `'-;'
    ///        'ooooxxxxxkkxoooIi:-.  `'
    ///       'ooooxxxxxkkkkxoiiiiiji'
    ///      'ooooxxxxxkxxoiiii:'`     .i'
    ///     'ooooxxxxxoi:::'`       .;ioxo'
    ///    'ooooxooi::'`         .:iiixkxxo'
    ///   'ooooi:'`                `'';ioxxo'
    ///  'i:'`                          '':io'
    /// '`                                   `'
    /// ```
    Artix,
    /// Small Artix Logo
    /// ```text
    ///       /\
    ///      /  \
    ///     /`'.,\
    ///    /     ',
    ///   /      ,`\
    ///  /   ,.'`.  \
    /// /.,'`     `'.\
    /// ```
    ArtixSmall,
    /// Alternative Small Artix Logo
    /// ```text
    ///             '
    ///            'A'
    ///           'ooo'
    ///          'ookxo'
    ///          `ookxxo'
    ///        '.   `ooko'
    ///       'ooo`.   `oo'
    ///      'ooxxxoo`.   `'
    ///     'ookxxxkooo.`   .
    ///    'ookxxkoo'`   .'oo'
    ///   'ooxoo'`     .:ooxxo'
    ///  'io'`             `'oo'
    /// '`                     `'
    /// ```
    Artix2Small,
    /// Arco Logo
    /// ```text
    ///                    /-
    ///                   ooo:
    ///                  yoooo/
    ///                 yooooooo
    ///                yooooooooo
    ///               yooooooooooo
    ///             .yooooooooooooo
    ///            .oooooooooooooooo
    ///           .oooooooarcoooooooo
    ///          .ooooooooo-oooooooooo
    ///         .ooooooooo-  oooooooooo
    ///        :ooooooooo.    :ooooooooo
    ///       :ooooooooo.      :ooooooooo
    ///      :oooarcooo         .oooarcooo
    ///     :ooooooooy           .ooooooooo
    ///    :ooooooooo   /ooooooooooooooooooo
    ///   :ooooooooo      .-ooooooooooooooooo.
    ///   ooooooooo-            -ooooooooooooo.
    ///  ooooooooo-                .-oooooooooo.
    /// ooooooooo.                    -ooooooooo
    /// ```
    Arco,
    /// Small Arco Logo
    /// ```text
    ///           A
    ///          ooo
    ///         ooooo
    ///        ooooooo
    ///       ooooooooo
    ///      ooooo ooooo
    ///     ooooo   ooooo
    ///    ooooo     ooooo
    ///   ooooo  <oooooooo>
    ///  ooooo      <oooooo>
    /// ooooo          <oooo>
    /// ```
    ArcoSmall,
    /// Arse Logo
    /// ```text
    ///                       ⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⠀⠀⠀⠀⠀⠀⣴⣶⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣄⠀⠀⠀⠀⣼⠟⠁⠀⠀⢀⣀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡀⠀⢀⣤⡀⠀⠀⠀⠉⢻⣷⡄⠀⠀⠁⠀⢀⣤⣾⡿⠟⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣷⣿⠏⠀⠀⠀⠀⠀⠀⠹⣿⡄⠀⠀⠀⠙⠉⠁⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⣿⡄⠀⠀⠀⠀⠀⠀⠀⢹⣿⠀⠀⠀⠀⠠⣶⣶⣶⡶
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣿⠀⠀⠀⠀⠀⠀⠀⠀⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⠀⠀⠀⠀⠀⠀⠀⢠⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⠂⠀⠀⠀⠀⠀⢀⣾⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣿⠇⠀⠀⠀⠀⠀⣠⣾⠟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⣿⣇⣀⣀⣀⣠⣴⣾⣿⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢸⣿⠀⠀⠀⠀⠀⣤⣤⣴⣶⣾⠿⠟⣿⡏⠙⠛⠛⠛⠋⠉⢀⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⣿⡄⠀⠀⠀⠀⠈⠉⠉⠀⠀⠀⠀⣿⡇⠀⠀⠀⠀⠀⠀⢸⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⠇⠀⠀⠀⠀⠀⠀⠘⠿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ```
    Arse,
    /// Arya Logo
    /// ```text
    ///                 `oyyy/-yyyyyy+
    ///                -syyyy/-yyyyyy+
    ///               .syyyyy/-yyyyyy+
    ///               :yyyyyy/-yyyyyy+
    ///            `/ :yyyyyy/-yyyyyy+
    ///           .+s :yyyyyy/-yyyyyy+
    ///          .oys :yyyyyy/-yyyyyy+
    ///         -oyys :yyyyyy/-yyyyyy+
    ///        :syyys :yyyyyy/-yyyyyy+
    ///       /syyyys :yyyyyy/-yyyyyy+
    ///      +yyyyyys :yyyyyy/-yyyyyy+
    ///    .oyyyyyyo. :yyyyyy/-yyyyyy+ ---------
    ///   .syyyyyy+`  :yyyyyy/-yyyyy+-+syyyyyyyy
    ///  -syyyyyy/    :yyyyyy/-yyys:.syyyyyyyyyy
    /// :syyyyyy/     :yyyyyy/-yyo.:syyyyyyyyyyy
    /// ```
    Arya,
    /// Asahi Logo
    /// ```text
    ///                    ##  **
    ///                 *####****.
    ///                   ###,
    ///                ...,/#,,,..
    ///           /*,,,,,,,,*,........,,
    ///         ,((((((//*,,,,,,,,,......
    ///        ((((((((((((((%............
    ///      ,(((((((((((((((@@(............
    ///     (((((((((((((((((@@@@/............
    ///   ,((((((((((((((((((@@@@@&*...........
    ///  ((((((((((((((((((((@@@@@@@&,...........
    /// (((((((((((((((((((((@@@&%&@@@%,..........
    ///  /(((((((((((((((((((@@@&%%&@@@@(........
    ///     ,((((((((((((((((@@@&&@@&/&@@@/..
    ///         /((((((((((((@@@@@@/.../&&
    ///            .(((((((((@@@@(....
    ///                /(((((@@#...
    ///                   .((&,
    /// ```
    Asahi,
    /// Alternative Asahi Logo
    /// ```text
    ///                  _wwM _ww
    ///                   MMMMMMM
    ///                    MM
    ///            _ww##############yy_
    ///           wMMMMM###########MMMMm
    ///         ,MMMMMMMMM######MMMMMMMM0_
    ///        wMMMMMMMMMMMMMMM0MMMMMMMMMMm
    ///      ,MMMMMMMMMMMMMMMMMMM0MMMMMMMMM0,
    ///     wMMMMMMMMMMMMMMMMMMMMM0MMMMMMMMMMb
    ///   _MMMMMMMMMMMMMMMMMMMMMMMMM0MMMMMMMMM0,
    ///  _MMMMMMMMMMMMMMMMMMMMMMMMMMMWMMMMMMMMMM_
    /// _MMMMMMMMMMMMMMMMMMMMMMMMM0MMMWMMMMMMMMMM_
    ///    ~MMMMMMMMMMMMMMMMMMMMMMMMMWMMMMM00~
    ///        ~MMMMMMMMMMMMMMMMWMMM0MMMMM~
    ///           ~MMMMMMMMMMMMMMM0MMM~~
    ///               ~MMMMMMMMM0MM~
    ///                   ~MMM@~
    ///                     M
    /// ```
    Asahi2,
    /// Aster Logo
    /// ```text
    ///                 ...''...
    ///            .;oOXWMWNXXXNMMN0d:.
    ///         .oXMWOo;..       ..:oO;
    ///       ;KMWx,       co,
    ///     'KMNl         dMMW.
    ///    oMMx          xMMMMk
    ///   xMM:          dMMMMMM;
    ///  cMMl          dMMMMMMMW
    ///  NMK          xMMMx::dXMx
    /// ,MMl         xMMN'     .o.
    /// cMM;        dMMW'
    /// ;MMc       oMMW,
    ///  WMK      dMMW,  ccccccc.
    ///  lMMl    oMMM;   ooooooo.
    ///   OMMc   ...
    ///    xMMx
    ///     ;XMN:
    ///       ,.
    /// ```
    Aster,
    /// AteroidOS Logo
    /// ```text
    ///                     ***
    ///                    *****
    ///                 **********
    ///               ***************
    ///            *///****////****////.
    ///          (/////// /////// ///////(
    ///       /(((((//*     //,     //((((((.
    ///     (((((((((((     (((        ((((((((
    ///  *(((((((((((((((((((((((        ((((((((
    ///     (((((#(((((((#(((((        ((#(((((
    ///      (#(#(#####(#(#,       ####(#(#
    ///          #########        ########
    ///            /########   ########
    ///               #######%#######
    ///                 (#%%%%%%%#
    ///                    %%%%%
    ///                     %%%
    /// ```
    AsteroidOs,
    /// astOS Logo
    /// ```text
    ///                 oQA#%UMn
    ///                 H       9
    ///                 G       #
    ///                 6       %
    ///                 ?#M#%KW3"
    ///                   // \\\
    ///                 //     \\\
    ///               //         \\\
    ///             //             \\\
    ///         n%@DK&ML       .0O3#@&M_
    ///         P       #       8       W
    ///         H       U       G       #
    ///         B       N       O       @
    ///         C&&#%HNAR       'WS3QMHB"
    ///           // \\\              \\\
    ///         //     \\\              \\\
    ///       //         \\\              \\\
    ///     //             \\\              \\\
    /// uURF##Bv       nKWB%ABc       aM@3R@D@b
    /// 8       M       @       O       #       %
    /// %       &       G       U       @       @
    /// &       @       #       %       %       #
    /// !HGN@MNCf       t&S9#%HQr       ?@G#6S@QP
    /// ```
    AstOs,
    /// Astra Logo
    /// ```text
    ///                   AA
    ///                  AaaA
    ///                 Aa/\\aA
    ///                Aa/aa\\aA
    ///               Aa/aAAa\\aA
    ///              aA/aaAAaa\\Aa
    ///             aA/aaAAAAaa\\Aa
    ///   aaaaaaAAAAa/aaAAAAAAaa\\aAAAAaaaaa
    /// aAAa-----aaaaaAAAAAAAAAAaaaaa-----aAAa
    ///   aAA\ aAAAAAAAAAAAAAAAAAAAAAAa /AAa
    ///     aAa\\aAAA\\AAAA\\AAAA\\AAA\\AAa/aAa
    ///       aAa\\aA\\\\AAA\\\\AAA\\\\AA\\\\/aAa
    ///        aAA\\aA\\\\AAA\\\\AAA\\\\Aa/AAa
    ///          aA\\aA\\\\AAA\\\\AAA\\\\/Aa
    ///          aA/AA\\\\\\AA\\\\\\AA\\\\\\Aa
    ///         aA/\\AAa\\\\\\Aa\\\\\\Aa\\\\\\Aa
    ///         aA/\\\\AAa\\\\/\\a\\\\\\Aa\\\\Aa
    ///        aA/a\\\\\\Aa\\/AA\\\\\\\\\\Aa\\\\Aa
    ///        aA/aA\\\\/aAa  aAa\\\\\\Aa\\Aa
    ///       aA/\\A\\/Aa        aA\\\\A\\\\Aa
    ///       A|/aaAa            aAaa\\|A
    ///      aAaa                    aaAa
    /// ```
    Astra,
    /// Athena Logo
    /// ```text
    ///     .          ..
    ///    :####:     ####.
    ///   .################
    ///  :##################
    /// .###################.
    /// ########     #######
    /// #######  #### #####
    /// :#######.      ####
    ///  #########  #   ##   #
    ///  #######   ##      ####
    /// ########  ####    #######
    /// ########  #####   ########
    /// ########  #######  #######
    ///  #######  ########  #######
    ///  ########  #########  ######
    ///   ########  #########  #####
    ///     #######  #########  ####
    ///      #######  #########  ##
    ///        #######  ######## ##
    ///           ###### ######## #
    ///                ### #######
    ///                      ######
    ///                         ####
    ///                           ##
    /// ```
    Athena,
    /// Azos Logo
    /// ```text
    ///   ////.                                           (((((
    /// ////////                                        @((((((((
    /// ////////                                        @((((((((
    /// ////////  ///////                      (((((((  @((((((((
    /// //////// /////////                    ((((((((( @((((((((
    /// //////// /////////                    ((((((((( @((((((((
    /// //////// /////////  //////    ((((((  ((((((((( @((((((((
    /// //////// ///////// ////////  (((((((( ((((((((( @((((((((
    /// //////// ///////// ////////  (((((((( ((((((((( @((((((((
    /// //////// ///////// ////////   ((((((( ((((((((( @((((((((
    /// //////// /////////   ///         (    ((((((((( @((((((((
    /// //////// /////////                    ((((((((( @((((((((
    /// //////// /////////                    &(((((((( @((((((((
    /// ////////  //////                        @((((   @((((((((
    /// ////////                                        @((((((((
    /// ////////                                        @((((((((
    ///  /////                                            (((((
    /// ```
    Azos,
    /// Bedrock Logo
    /// ```text
    /// --------------------------------------
    /// --------------------------------------
    /// --------------------------------------
    /// ---\\\\\\\\\\\\-----------------------
    /// ----\\\      \\\----------------------
    /// -----\\\      \\\---------------------
    /// ------\\\      \\\\\\\\\\\\\\\\\------
    /// -------\\\                    \\\-----
    /// --------\\\                    \\\----
    /// ---------\\\        ______      \\\---
    /// ----------\\\                   ///---
    /// -----------\\\                 ///----
    /// ------------\\\               ///-----
    /// -------------\\\////////////////------
    /// --------------------------------------
    /// --------------------------------------
    /// --------------------------------------
    /// ```
    Bedrock,
    /// BigLinux Logo
    /// ```text
    ///                                  ...
    ///                               :OWMMMNd.
    ///                             :NMMMMMMMMWc
    ///                   okkl.    kMMMMMW0xdOWMl
    ///   :             xMMMMMW.  kMMMMNc      lW.
    ///  :x             NMMMMMO  ,MMMM0.        'l
    ///  Xx              "lkk"   kMMMX      .okx,
    /// .MX      .cc;.    .xXKx. KMMM:    .OMMMMMl
    /// :MM'   'KMMMMWK:  0MMMMk xMMM.   lWMMMMMMM'
    /// cMMN:;xMMMMk::MMO oMMMMX .XMM. .KMMMWOOMMMd
    /// 'MMMMMMMMN,   NMMx OMMMMl .kM0OMMMMk.  ;MMd
    ///  xMMMMMMd    .MMMW  :NMMMd  .ckKKx'     KMc
    ///   dWMNd.     oMMMN    lkNMX,            oM.
    ///  ;.         ;MMMMx      "MM:.           cO
    ///  .X.       oMMMMW.                      l.
    ///   dMk:..;xWMMMMW,
    ///    kMMMMMMMMMMX.
    ///     :XMMMMMMK:
    ///       ':MM:"      Made in Brazil
    /// ```
    BigLinux,
    /// Bitrig Logo
    /// ```text
    ///    `hMMMMN+
    ///    -MMo-dMd`
    ///    oMN- oMN`
    ///    yMd  /NM:
    ///   .mMmyyhMMs
    ///   :NMMMhsmMh
    ///   +MNhNNoyMm-
    ///   hMd.-hMNMN:
    ///   mMmsssmMMMo
    ///  .MMdyyhNMMMd
    ///  oMN.`/dMddMN`
    ///  yMm/hNm+./MM/
    /// .dMMMmo.``.NMo
    /// :NMMMNmmmmmMMh
    /// /MN/-------oNN:
    /// hMd.       .dMh
    /// sm/         /ms
    /// ```
    Bitrig,
    /// Blackarch Logo
    /// ```text
    ///                    00
    ///                    11
    ///                   ====
    ///                   .//
    ///                  `o//:
    ///                 `+o//o:
    ///                `+oo//oo:
    ///                -+oo//oo+:
    ///              `/:-:+//ooo+:
    ///             `/+++++//+++++:
    ///            `/++++++//++++++:
    ///           `/+++oooo//ooooooo/`
    ///          ./ooosssso//osssssso+`
    ///         .oossssso-`//`/ossssss+`
    ///        -osssssso.  //  :ssssssso.
    ///       :osssssss/   //   osssso+++.
    ///      /ossssssss/   //   +ssssooo/-
    ///    `/ossssso+/:-   //   -:/+osssso+-
    ///   `+sso+:-`        //       `.-/+oso:
    ///  `++:.             //            `-/+/
    ///  .`                /                `/
    /// ```
    Blackarch,
    /// BlackMesa Logo
    /// ```text
    ///            .-;+XHHHHHHX+;-
    ///         ,:X@@X%/;=----=:\%X@@X:,
    ///       =@@%=.              .=+H@X:
    ///     -XMX:                      =XMX=
    ///    /@@:                          =H@+
    ///   %@X.                            .@
    ///  +@X,                               @%
    /// /@@,                                .@@\
    /// %@%                                  +@
    /// H@:                                  :@H
    /// H@:         :HHHHHHHHHHHHHHHHHHX,    =@H
    /// %@%         ;@M@@@@@@@@@@@@@@@@@H-   +@
    /// =@@,        :@@@@@@@@@@@@@@@@@@@@@= .@@:
    ///  =@X        :@@@@@@@@@@@@@@@@@@@@@@:%@%
    ///   @,      ;@@@@@@@@@@@@@@@@@M@@@@@@
    ///    +@@HHHHHHH@@@@@@@@@@@@@@@@@@@@@@@
    ///     =X@@@@@@@@@@@@@@@@@@@@@@@@@@@X=
    ///       :@@@@@@@@@@@@@@@@@@M@@@@:
    ///         \@@@@@@@@@@@@@@@@@@X/-
    ///            .-;+XXHHHHHX+;-.
    /// ```
    BlackMesa,
    /// BlackPanther Logo
    /// ```text
    ///                          ........
    ///                   .,»╔╗╗╬▄▄╫█▀▓▄▄╬╗╗g≈,.
    ///                ,j╗╬╣▓▓███████▌;»╙▀▀▀▀█▄▄╗j,
    ///             .≈╗╬▓██▀▀▀▀▀╠╙░░»»;:```>▄ ▐ ▓╫╗⌂,
    ///           .j╬▓█▀▒░░░░░░░░░»»»;:````      ╙▀█▌╬░,
    ///          ;╗▓█▄▄███████▀░░»»»»;```` ╓▄▄█▄▄φ  ██▌Ñ>.
    ///        .j╣█████▀▀░░░░░░░░»»╓▄▄¿``▄███████/▄████▓╬U.
    ///       .j╣▓██▀ÜÑ╦╦░░░░░░▐█@▄████⌐▐███████████████▓╬H.
    ///       «╫▓█▀░ÑÑ╩╦░░░░░░░░▀██████M"▀███████████████▓╫░
    ///      :]╣█▌ÑÑÑÑ▄▄██▀░░░░»»██████████████████████████Ñ~
    ///      »╫▓█╫ÑÑ▄███▀░░░░░»»▐██████████████████████████▌░
    ///     `j╣█▌Ñ╬████░░░░░░░»»▐████████████████████████▌▐█U`
    ///     `/╫█▌▄███▌░░░░░░░»»»;▀██████████████▀████████w▐█░`
    ///      ;╟█▌███▌░░░░░░░▄▄»»;:`▀▀████████▀Ü▄████████▌ ▐▌>`
    ///      `]▓████░░░░░░░░██⌂;:````╓▄▄µp╓▄▄██████████▀ ,█M`
    ///       "╠╣██▌░░░░░░░»██▌;````  ╙▀██████████████M  █▀"
    ///        "╟╣█░░░░░░░░»███⌂```      ▐▀████████▀░   █▌░`
    ///         "╩█▄░░░░░░»»▀███ ``           └└`     ,█▀"`
    ///          `░▀█▄░░░»»»»████@                  .▄█Ü`
    ///            `╙▀█▄@»»»;`▀███▌¿              ,▄▀Ñ"`
    ///              `"╨▀█▄▄▄░`▐█████▄,       ,▄▄▀▀░`
    ///                 `"╙╩▀▀▀▀████████▓▌▌▌▀▀▀╨"``
    ///                     ``""░╚╨╝╝╝╝╨╨░""``
    /// ```
    BlackPanther,
    /// Blag Logo
    /// ```text
    ///              d
    ///             ,MK:
    ///             xMMMX:
    ///            .NMMMMMX;
    ///            lMMMMMMMM0clodkO0KXWW:
    ///            KMMMMMMMMMMMMMMMMMMX'
    ///       .;d0NMMMMMMMMMMMMMMMMMMK.
    ///  .;dONMMMMMMMMMMMMMMMMMMMMMMx
    /// 'dKMMMMMMMMMMMMMMMMMMMMMMMMl
    ///    .:xKWMMMMMMMMMMMMMMMMMMM0.
    ///        .:xNMMMMMMMMMMMMMMMMMK.
    ///           lMMMMMMMMMMMMMMMMMMK.
    ///           ,MMMMMMMMWkOXWMMMMMM0
    ///           .NMMMMMNd.     `':ldko
    ///            OMMMK:
    ///            oWk,
    ///            ;:
    /// ```
    Blag,
    /// BlankOn Logo
    /// ```text
    ///         `./ohdNMMMMNmho+.`        .+oo:`
    ///       -smMMMMMMMMMMMMMMMMmy-`    `yyyyy+
    ///    `:dMMMMMMMMMMMMMMMMMMMMMMd/`  `yyyyys
    ///   .hMMMMMMMNmhso/++symNMMMMMMMh- `yyyyys
    ///  -mMMMMMMms-`         -omMMMMMMN-.yyyyys
    /// .mMMMMMMy.              .yMMMMMMm:yyyyys
    /// sMMMMMMy                 `sMMMMMMhyyyyys
    /// NMMMMMN:                  .NMMMMMNyyyyys
    /// MMMMMMm.                   NMMMMMNyyyyys
    /// hMMMMMM+                  /MMMMMMNyyyyys
    /// :NMMMMMN:                :mMMMMMM+yyyyys
    ///  oMMMMMMNs-            .sNMMMMMMs.yyyyys
    ///   +MMMMMMMNho:.`  `.:ohNMMMMMMNo `yyyyys
    ///    -hMMMMMMMMNNNmmNNNMMMMMMMMh-  `yyyyys
    ///      :yNMMMMMMMMMMMMMMMMMMNy:`   `yyyyys
    ///        .:sdNMMMMMMMMMMNds/.      `yyyyyo
    ///            `.:/++++/:.`           :oys+.
    /// ```
    BlankOn,
    /// BlueLight Logo
    /// ```text
    ///               oMMNMMMMMMMMMMMMMMMMMMMMMM
    ///               oMMMMMMMMMMMMMMMMMMMMMMMMM
    ///               oMMMMMMMMMMMMMMMMMMMMMMMMM
    ///               oMMMMMMMMMMMMMMMMMMMMMMMMM
    ///               -+++++++++++++++++++++++mM
    ///              ```````````````````````..dM
    ///            ```````````````````````....dM
    ///          ```````````````````````......dM
    ///        ```````````````````````........dM
    ///      ```````````````````````..........dM
    ///    ```````````````````````............dM
    /// .::::::::::::::::::::::-..............dM
    ///  `-+yyyyyyyyyyyyyyyyyyyo............+mMM
    ///      -+yyyyyyyyyyyyyyyyo..........+mMMMM
    ///         ./syyyyyyyyyyyyo........+mMMMMMM
    ///            ./oyyyyyyyyyo......+mMMMMMMMM
    ///               omdyyyyyyo....+mMMMMMMMMMM
    ///               oMMMmdhyyo..+mMMMMMMMMMMMM
    ///               oNNNNNNmdsomMMMMMMMMMMMMMM
    /// ```
    BlueLight,
    /// Bodhi Logo
    /// ```text
    /// |           ,,mmKKKKKKKKWm,,
    ///  '      ,aKKPLL**********|L*TKp,
    ///    t  aKPL**```          ```**L*Kp
    ///     IXELL,wwww,              ``*||Kp
    ///   ,#PL|KKKpPP@IPPTKmw,          `*||K
    ///  ,KLL*{KKKKKKPPbKPhpKKPKp        `||K
    ///  #PL  !KKKKKKPhKPPPKKEhKKKKp      `||K
    /// !HL*   1KKKKKKKphKbPKKKKKKKKp      `|IW
    /// bL     KKKKKKKKBQKhKbKKKKKKKK       |IN
    /// bL     !KKKKKKKKKKNKKKKKKKPP`       |Ib
    /// THL*     TKKKKKK##KKKN@KKKK^         |IM
    ///  K@L      *KKKKKKKKKKKEKE5          ||K
    ///  `NLL      `KKKKKKKKKK"```|L       ||#P
    ///   `K@LL       `"**"`        '.   :||#P
    ///     YpLL                      ' |LM`
    ///      `TppLL,                ,|||p'L
    ///         "KppLL++,.,    ,,|||#K*   '.
    ///            `"MKWpppppppp#KM"`        `h,
    /// ```
    Bodhi,
    /// Bonsai Logo
    /// ```text
    ///    ,####,
    ///    #######,  ,#####,
    ///    #####',#  '######
    ///     ''###'';,,,'###'
    ///           ,;  ''''
    ///          ;;;   ,#####,
    ///         ;;;'  ,,;;;###
    ///         ';;;;'''####'
    ///          ;;;
    ///       ,.;;';'',,,
    ///      '     '
    ///  #
    ///  #                        O
    ///  ##, ,##,',##, ,##  ,#,   ,
    ///  # # #  # #''# #,,  # #   #
    ///  '#' '##' #  #  ,,# '##;, #h,
    /// ```
    Bonsai,
    /// BSD Logo
    /// ```text
    ///              ,        ,
    ///             /(        )`
    ///             \ \___   / |
    ///             /- _  `-/  '
    ///            (/\/ \ \   /\
    ///            / /   | `    \
    ///            O O   ) /    |
    ///            `-^--'`<     '
    ///           (_.)  _  )   /
    ///            `.___/`    /
    ///              `-----' /
    /// <----.     __ / __   \
    /// <----|====O)))==) \) /====|
    /// <----'    `--' `.__,' \
    ///              |        |
    ///               \       /       /\
    ///          ______( (_  / \______/
    ///        ,'  ,-----'   |
    ///        `--{__________)
    /// ```
    Bsd,
    /// BunsenLabs Logo
    /// ```text
    ///         `++
    ///       -yMMs
    ///     `yMMMMN`
    ///    -NMMMMMMm.
    ///   :MMMMMMMMMN-
    ///  .NMMMMMMMMMMM/
    ///  yMMMMMMMMMMMMM/
    /// `MMMMMMNMMMMMMMN.
    /// -MMMMN+ /mMMMMMMy
    /// -MMMm`   `dMMMMMM
    /// `MMN.     .NMMMMM.
    ///  hMy       yMMMMM`
    ///  -Mo       +MMMMN
    ///   /o       +MMMMs
    ///            +MMMN`
    ///            hMMM:
    ///           `NMM/
    ///           +MN:
    ///           mh.
    ///          -/
    /// ```
    BunsenLabs,
    /// ChachyOS Logo
    /// ```text
    ///            .-------------------------:
    ///           .+=========================.
    ///          :++===++==================-       :++-
    ///         :*++====+++++=============-        .==:
    ///        -*+++=====+***++==========:
    ///       =*++++========------------:
    ///      =*+++++=====-                     ...
    ///    .+*+++++=-===:                    .=+++=:
    ///   :++++=====-==:                     -*****+
    ///  :++========-=.                      .=+**+.
    /// .+==========-.                          .
    ///  :+++++++====-                                .--==-.
    ///   :++==========.                             :+++++++:
    ///    .-===========.                            =*****+*+
    ///     .-===========:                           .+*****+:
    ///       -=======++++:::::::::::::::::::::::::-:  .---:
    ///        :======++++====+++******************=.
    ///         :=====+++==========++++++++++++++*-
    ///          .====++==============++++++++++*-
    ///           .===+==================+++++++:
    ///            .-=======================+++:
    ///              ..........................
    /// ```
    CachyOs,
    /// Small CachyOS Logo
    /// ```text
    ///    /''''''''''''/
    ///   /''''''''''''/
    ///  /''''''/
    /// /''''''/
    /// \......\
    ///  \......\
    ///   \.............../
    ///    \............./
    /// ```
    CachyOsSmall,
    /// Calculate Logo
    /// ```text
    ///                               ......
    ///                            ,,+++++++,.
    ///                          .,,,....,,,+**+,,.
    ///                        ............,++++,,,
    ///                       ...............
    ///                     ......,,,........
    ///                   .....+*#####+,,,*+.
    ///               .....,*###############,..,,,,,,..
    ///            ......,*#################*..,,,,,..,,,..
    ///          .,,....*####################+***+,,,,...,++,
    ///        .,,..,..*#####################*,
    ///      ,+,.+*..*#######################.
    ///    ,+,,+*+..,########################*
    /// .,++++++.  ..+##**###################+
    /// .....      ..+##***#################*.
    ///            .,.*#*****##############*.
    ///            ..,,*********#####****+.
    ///      .,++*****+++*****************+++++,.
    ///       ,++++++**+++++***********+++++++++,
    ///      .,,,,++++,..  .,,,,,.....,+++,.,,
    /// ```
    Calculate,
    /// Calinix Logo
    /// ```text
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣠⠤⠔⠒⠒⠋⠉⠉⠉⠉⠓⠒⠒⠦⠤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠤⠒⠉⣁⣠⣤⣶⣶⣿⣿⣿⣿⣿⣿⣿⣿⣶⣶⣤⣄⣈⠙⠲⢤⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠴⠋⢁⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⡈⠑⢦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⣠⠞⢁⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄⠈⠢⡀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⢀⠞⠁⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⠋⠉⠁⠀⠀⠀⠀⠈⠉⠙⠛⠿⣿⣿⣿⣿⣿⣿⠏⠀⠀⠀⠈⢢⡀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⡰⠃⣠⣾⣿⣿⣿⣿⣿⣿⡿⠛⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠻⢿⡿⠁⠀⠀⠀⠀⠀⠀⠙⣄⠀⠀⠀⠀
    /// ⠀⠀⠀⡼⠁⣴⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢆⠀⠀⠀
    /// ⠀⠀⡼⠀⣼⣿⣿⣿⣿⣿⣿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣆⠀⠀
    /// ⠀⣰⠁⣸⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠉⠻⣿⣿⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⡄⠀
    /// ⢀⡇⢠⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠛⢿⣿⣿⣿⣿⣿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢳⠀
    /// ⢸⠀⣸⣿⣿⣿⣿⣿⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿⣿⣿⣿⣿⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⡄
    /// ⣼⠀⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣿⣿⣿⣷⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇
    /// ⡇⠀⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢛⣿⣿⣿⣿⣿⣿⣿⡦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇
    /// ⢻⠀⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣶⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇
    /// ⢸⡀⢹⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣾⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⠃
    /// ⠀⣇⠘⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⣿⣿⣿⡿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡼⠀
    /// ⠀⠸⡄⢹⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠀⣠⣶⣿⣿⣿⣿⣿⣿⠟⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⠃⠀
    /// ⠀⠀⢳⡀⢻⣿⣿⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀⠈⠉⠉⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠏⠀⠀
    /// ⠀⠀⠀⠳⡀⠻⣿⣿⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣷⣄⡀⠀⠀⠀⠀⢠⠏⠀⠀⠀
    /// ⠀⠀⠀⠀⠙⣄⠙⢿⣿⣿⣿⣿⣿⣿⣷⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣴⣾⣿⣿⣿⣿⣿⣦⡀⠀⡰⠃⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠈⠢⡈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣤⣄⣀⡀⠀⠀⠀⠀⢀⣀⣠⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⣠⠞⠁⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠈⠢⡈⠙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠋⣡⠞⠁⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠓⢤⡈⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⣁⠴⠊⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠑⠢⢄⣉⠙⠛⠿⠿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠿⠛⠋⣉⡤⠖⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠓⠒⠢⠤⠤⠤⠤⠤⠤⠤⠤⠖⠒⠋⠉⠀
    /// ```
    Calinix,
    /// Small Calinix Logo
    /// ```text
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⣀⠤⠐⣂⣈⣩⣭⣭⣍⣀⣐⠀⠄⡀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⡀⠔⣨⣴⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣦⣅⠢⡀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠠⢊⣴⣾⣿⣿⣿⣿⠿⠟⠛⠛⠛⠛⠻⠿⣿⣿⣿⣿⠃⠀⠠⡀⠀⠀⠀
    /// ⠀⠀⡐⢡⣾⣿⣿⣿⠟⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠁⠀⠀⠀⠈⢆⠀⠀
    /// ⠀⡘⢰⣿⣿⣿⡟⠁⠀⠀⢀⣀⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢂⠀
    /// ⢠⢠⣿⣿⣿⡟⠀⠀⠀⠀⠀⠙⠿⣿⣿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⡀
    /// ⡄⢸⣿⣿⣿⠁⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁
    /// ⡇⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣹⣿⣿⣿⣷⠄⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠃⢸⣿⣿⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣿⣿⡿⠛⠁⠀⠀⠀⠀⠀⠀⠀⠀⡀
    /// ⠘⡘⣿⣿⣿⣧⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⠿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⠁
    /// ⠀⠡⠸⣿⣿⣿⣧⡀⠀⠀⠀⠉⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⢀⠀⠀⠀⠀⢀⠆⠀
    /// ⠀⠀⠡⡘⢿⣿⣿⣿⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣴⣿⣷⣦⡀⢀⠊⠀⠀
    /// ⠀⠀⠀⠈⠊⡻⢿⣿⣿⣿⣿⣶⣤⣤⣤⣤⣤⣤⣶⣿⣿⣿⣿⡿⢟⠕⠁⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠈⠢⢙⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⡩⠐⠁⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠈⠐⠂⠭⠉⠙⣛⣛⠋⠉⠭⠐⠂⠁⠀⠀⠀⠀
    /// ```
    CalinixSmall,
    /// Carbs Logo
    /// ```text
    ///              ..........
    ///           ..,;:ccccccc:;'..
    ///        ..,clllc:;;;;;:cllc,.
    ///       .,cllc,...     ..';;'.
    ///      .;lol;..           ..
    ///     .,lol;.
    ///     .coo:.
    ///    .'lol,.
    ///    .,lol,.
    ///    .,lol,.
    ///     'col;.
    ///     .:ooc'.
    ///     .'col:.
    ///      .'cllc'..          .''.
    ///       ..:lolc,'.......',cll,.
    ///         ..;cllllccccclllc;'.
    ///           ...',;;;;;;,,...
    ///                 .....
    /// ```
    Carbs,
    /// CBL-Mariner
    /// ```text
    ///                     .
    ///                   :-  .
    ///                 :==. .=:
    ///               :===:  -==:
    ///             :-===:  .====:
    ///           :-====-   -=====:
    ///          -======   :=======:
    ///         -======.  .=========:
    ///        -======:   -==========.
    ///       -======-    -===========.
    ///      :======-      :===========.
    ///     :=======.       .-==========.
    ///    :=======:          -==========.
    ///   :=======-            :==========.
    ///  :=======-              .-========-
    /// :--------.                :========-
    ///                     ..:::--=========-
    ///             ..::---================-=-
    /// ```
    CblMariner,
    /// Cel OS Logo
    /// ```text
    ///              `-:/++++/:-`
    ///           -/syyyyyyyyyyyyy+-
    ///         :ssssyyyyyyyyyyyyyyyy/
    ///       .osymmmmmmmmmmmmmmmNNNNNmmhy+
    ///      .sssshhhhhhhddddddddddddddds-
    ///     `osssssssyyyyyyyyyyyyyyyyyyhy`
    ///     :ssssssyyyyyyyyyyyyyyyyyyyyhh/
    /// sMMMMMMMMMMMMMMMMMMMMMMMhyyyyyyhho
    ///     :sssssssyyyyyyyyyyyyyyyyyyyhh/
    ///     `ssssssssyyyyyyyyyyyyyyyyyyhy.
    ///      -sssssyddddddddddddddddddddy
    ///       -sssshmmmmmmmmmmmmmmmmmmmyssss-
    ///        `/ssssyyyyyyyyyyyyyyyy+`
    ///          `:osyyyyyyyyyyyyys/`
    ///             `.:/+ooooo+:-`
    /// ```
    Celos,
    /// Center Logo
    /// ```text
    ///                 .
    ///                 o,
    ///         .       d,       .
    ///         ';'   ..d;..  .cl'
    ///           .:; 'oldO,.oo.
    ///           ..,:,xKXxoo;'.
    ///     ,;;;;;ldxkONMMMXxkxc;;;;;.
    ///     .....':oddXWMNOxlcl:......
    ///            .:dlxk0c;:. .
    ///           :d:.,xcld,.,:.
    ///         ;l,    .l;     ';'
    ///                .o;
    ///                 l,
    /// ```
    Center,
    /// CentOS Logo
    /// ```text
    ///                  ..
    ///                .PLTJ.
    ///               <><><><>
    ///      KKSSV' 4KKK LJ KKKL.'VSSKK
    ///      KKV' 4KKKKK LJ KKKKAL 'VKK
    ///      V' ' 'VKKKK LJ KKKKV' ' 'V
    ///      .4MA.' 'VKK LJ KKV' '.4Mb.
    ///    . KKKKKA.' 'V LJ V' '.4KKKKK .
    ///  .4D KKKKKKKA.'' LJ ''.4KKKKKKK FA.
    /// <QDD ++++++++++++  ++++++++++++ GFD>
    ///  'VD KKKKKKKK'.. LJ ..'KKKKKKKK FV
    ///    ' VKKKKK'. .4 LJ K. .'KKKKKV '
    ///       'VK'. .4KK LJ KKA. .'KV'
    ///      A. . .4KKKK LJ KKKKA. . .4
    ///      KKA. 'KKKKK LJ KKKKK' .4KK
    ///      KKSSA. VKKK LJ KKKV .4SSKK
    ///               <><><><>
    ///                'MKKM'
    ///                  ''
    /// ```
    CentOs,
    /// Small CentOS Logo
    /// ```text
    ///  ____^____
    ///  |\  |  /|
    ///  | \ | / |
    /// <---- ---->
    ///  | / | \ |
    ///  |/__|__\|
    ///      v
    /// ```
    CentOsSmall,
    /// Chakra Logo
    /// ```text
    ///      _ _ _        "kkkkkkkk.
    ///    ,kkkkkkkk.,    'kkkkkkkkk,
    ///    ,kkkkkkkkkkkk., 'kkkkkkkkk.
    ///   ,kkkkkkkkkkkkkkkk,'kkkkkkkk,
    ///  ,kkkkkkkkkkkkkkkkkkk'kkkkkkk.
    ///   "''"''',;::,,"''kkk''kkkkk;   __
    ///       ,kkkkkkkkkk, "k''kkkkk' ,kkkk
    ///     ,kkkkkkk' ., ' .: 'kkkk',kkkkkk
    ///   ,kkkkkkkk'.k'   ,  ,kkkk;kkkkkkkkk
    ///  ,kkkkkkkk';kk 'k  "'k',kkkkkkkkkkkk
    /// .kkkkkkkkk.kkkk.'kkkkkkkkkkkkkkkkkk'
    /// ;kkkkkkkk''kkkkkk;'kkkkkkkkkkkkk''
    /// 'kkkkkkk; 'kkkkkkkk.,""''"''""
    ///   ''kkkk;  'kkkkkkkkkk.,
    ///      ';'    'kkkkkkkkkkkk.,
    ///              ';kkkkkkkkkk'
    ///                ';kkkkkk'
    ///                   "''"
    /// ```
    Chakra,
    /// ChaletOS Logo
    /// ```text
    ///              `.//+osso+/:``
    ///          `/sdNNmhyssssydmNNdo:`
    ///        :hNmy+-`          .-+hNNs-
    ///      /mMh/`       `+:`       `+dMd:
    ///    .hMd-        -sNNMNo.  /yyy  /mMs`
    ///   -NM+       `/dMd/--omNh::dMM   `yMd`
    ///  .NN+      .sNNs:/dMNy:/hNmo/s     yMd`
    ///  hMs    `/hNd+-smMMMMMMd+:omNy-    `dMo
    /// :NM.  .omMy:/hNMMMMMMMMMMNy:/hMd+`  :Md`
    /// /Md` `sm+.omMMMMMMMMMMMMMMMMd/-sm+  .MN:
    /// /Md`      MMMMMMMMMMMMMMMMMMMN      .MN:
    /// :NN.      MMMMMMm....--NMMMMMN      -Mm.
    /// `dMo      MMMMMMd      mMMMMMN      hMs
    ///  -MN:     MMMMMMd      mMMMMMN     oMm`
    ///   :NM:    MMMMMMd      mMMMMMN    +Mm-
    ///    -mMy.  mmmmmmh      dmmmmmh  -hMh.
    ///      oNNs-                    :yMm/
    ///       .+mMdo:`            `:smMd/`
    ///          -ohNNmhsoo++osshmNNh+.
    ///             `./+syyhhyys+:``
    /// ```
    ChaletOs,
    /// Chapeau Logo
    /// ```text
    ///                .-/-.
    ///             ////////.
    ///           ////////y+//.
    ///         ////////mMN/////.
    ///       ////////mMN+////////.
    ///     ////////////////////////.
    ///   /////////+shhddhyo+////////.
    ///  ////////ymMNmdhhdmNNdo///////.
    /// ///////+mMms////////hNMh///////.
    /// ///////NMm+//////////sMMh///////
    /// //////oMMNmmmmmmmmmmmmMMm///////
    /// //////+MMmssssssssssssss+///////
    /// `//////yMMy////////////////////
    ///  `//////smMNhso++oydNm////////
    ///   `///////ohmNMMMNNdy+///////
    ///     `//////////++//////////
    ///        `////////////////.
    ///            -////////-
    /// ```
    Chapeau,
    /// Chimera Linux Logo
    /// ```text
    /// ddddddddddddddc  ,cc:
    /// ddddddddddddddc  ,cc:
    /// ddddddddddddddd  ,cc:
    /// ddddddddddddl:'  ,cc:
    /// dddddddddl'    ..;cc:
    /// dddddddo.   ,:cccccc:
    /// ddddddl   ,ccc:'''''
    /// dddddo.  ;ccc.          ............
    ///         .ccc.           cccccccccccc
    /// ......  .ccc.          .ccc'''''''''
    /// OOOOOk.  ;ccc.        .ccc;   ......
    /// OOOOOOd   'ccc:,....,:ccc'   coooooo
    /// OOOOOOOx.   ':cccccccc:'   .looooooo
    /// OOOOOOOOOd,     `'''`     .coooooooo
    /// OOOOOOOOOOOOdc,.    ..,coooooooooooo
    /// OOOOOOOOOOOOOOOO'  .oooooooooooooooo
    /// OOOOOOOOOOOOOOOO'  .oooooooooooooooo
    /// OOOOOOOOOOOOOOOO'  .oooooooooooooooo
    /// ```
    ChimeraLinux,
    /// Chonky Seal OS Logo
    /// ```text
    ///                   .-/-.
    ///             .:-=++****++=-:.
    ///         .:=+*##%%%%%%%%%%##*+=:.
    ///       :=*#%%%%%%%%%%%%%%%%%%%%#*=:
    ///     :=*#%%%%%%%%%%%%%%%%%%%%%%%%#*=.
    ///    -+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%#+-
    ///   =+#%%%%@@@@@@@%%%%%%%@@@@@@@%%%%%#+=
    ///  =+#@%%%%*+=-==*%%%%%%%#+====*%%%%%@#+=
    /// :+*%%%%@*       +@%%%@#       -@%%%%%*+:
    /// =+#%%%%%%#+====*###%%##*=--=+*%%%%%%%#+=
    /// +*%%%%%%%@@##%%%%*=::=#%%%##%@%%%%%%%%*+
    /// +*%%%%%%%@**@%%%%%@==@%%%%%@+#%%%%%%%%*+
    /// =+#%%%%%%@#*@%%%%%%**%%%%%@%+%%%%%%%%#+=
    /// :+*%%%%%%%@#*####**###*####*%@%%%%%%%*+:
    ///  =+#@%%%%%%@%%%%%%%@@%%%%%%%%%%%%%%@#+=
    ///   =+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#+=
    ///    -+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%*+-
    ///     .=*#%%%%%%%%%%%%%%%%%%%%%%%%#*=.
    ///       :=*##%%%%%%%%%%%%%%%%%%##*=:
    ///         .:=+*##%%%%%%%%%%##*+=:.
    ///             .:-=++****++=-:.
    /// ```
    ChonkySealOs,
    /// ChromeOS Logo
    /// ```text
    ///             .,:loool:,.
    ///         .,coooooooooooooc,.
    ///      .,lllllllllllllllllllll,.
    ///     ;ccccccccccccccccccccccccc;
    ///   'ccccccccccccccccccccccccccccc.
    ///  ,ooc::::::::okO0000OOkkkkkkkkkkk:
    /// .ooool;;;;:xK0kxxxxxk0XK0000000000.
    /// :oooool;,;OKdddddddddddKX000000000d
    /// lllllool;lNdllllllllllldNK000000000
    /// llllllllloMdcccccccccccoWK000000000
    /// ;cllllllllXXc:::::::::c0X000000000d
    /// .ccccllllllONkc;,,,;cxKK0000000000.
    ///  .cccccclllllxOOOOOOkxO0000000000;
    ///   .:cccccccclllllllloO0000000OOO,
    ///     ,:ccccccccclllcd0000OOOOOOl.
    ///       '::cccccccccdOOOOOOOkx:.
    ///         ..,::ccccxOOOkkko;.
    ///             ..,:dOkxl:.
    /// ```
    ChromOs,
    ///
    /// ```text
    /// ███████▌ ████████████████
    /// ███████▌ ████████████████
    /// ███████▌ ████████████████
    /// ███████▌
    /// ███████▌
    /// ███████▌
    /// ███████▌
    /// ███████▌
    /// █████████████████████████
    /// █████████████████████████
    /// █████████████████████████
    /// ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
    /// ```
    Cleanjaro,
    /// Small Cleanjaro Logo
    /// ```text
    /// █████ ██████████
    /// █████ ██████████
    /// █████
    /// █████
    /// █████
    /// ████████████████
    /// ████████████████
    /// ```
    CleanjaroSmall,
    /// Clear Linux Logo
    /// ```text
    ///           BBB
    ///        BBBBBBBBB
    ///      BBBBBBBBBBBBBBB
    ///    BBBBBBBBBBBBBBBBBBBB
    ///    BBBBBBBBBBB         BBB
    ///   BBBBBBBBYYYYY
    ///   BBBBBBBBYYYYYY
    ///   BBBBBBBBYYYYYYY
    ///   BBBBBBBBBYYYYYW
    ///  GGBBBBBBBYYYYYWWW
    ///  GGGBBBBBBBYYWWWWWWWW
    ///  GGGGGGBBBBBBWWWWWWWW
    ///  GGGGGGGGBBBBWWWWWWWW
    /// GGGGGGGGGGGBBBWWWWWWW
    /// GGGGGGGGGGGGGBWWWWWW
    /// GGGGGGGGWWWWWWWWWWW
    /// GGWWWWWWWWWWWWWWWW
    ///  WWWWWWWWWWWWWWWW
    ///       WWWWWWWWWW
    ///           WWW
    /// ```
    ClearLinux,
    /// ClearOS Logo
    /// ```text
    ///              `.--::::::--.`
    ///          .-:////////////////:-.
    ///       `-////////////////////////-`
    ///      -////////////////////////////-
    ///    `//////////////-..-//////////////`
    ///   ./////////////:      ://///////////.
    ///  `//////:..-////:      :////-..-//////`
    ///  ://////`    -///:.``.:///-`    ://///:
    /// `///////:.     -////////-`    `:///////`
    /// .//:--////:.     -////-`    `:////--://.
    /// ./:    .////:.     --`    `:////-    :/.
    /// `//-`    .////:.        `:////-    `-//`
    ///  :///-`    .////:.    `:////-    `-///:
    ///  `/////-`    -///:    :///-    `-/////`
    ///   `//////-   `///:    :///`   .//////`
    ///    `:////:   `///:    :///`   -////:`
    ///      .://:   `///:    :///`   -//:.
    ///        .::   `///:    :///`   -:.
    ///              `///:    :///`
    ///               `...    ...`
    /// ```
    ClearOs,
    /// Clover Logo
    /// ```text
    ///                `omo``omo`
    ///              `oNMMMNNMMMNo`
    ///            `oNMMMMMMMMMMMMNo`
    ///           oNMMMMMMMMMMMMMMMMNo
    ///           `sNMMMMMMMMMMMMMMNs`
    ///      `omo`  `sNMMMMMMMMMMNs`  `omo`
    ///    `oNMMMNo`  `sNMMMMMMNs`  `oNMMMNo`
    ///  `oNMMMMMMMNo`  `oNMMNs`  `oNMMMMMMMNo`
    /// oNMMMMMMMMMMMNo`  `sy`  `oNMMMMMMMMMMMNo
    /// `sNMMMMMMMMMMMMNo.oNNs.oNMMMMMMMMMMMMNs`
    /// `oNMMMMMMMMMMMMNs.oNNs.oNMMMMMMMMMMMMNo`
    /// oNMMMMMMMMMMMNs`  `sy`  `oNMMMMMMMMMMMNo
    ///  `oNMMMMMMMNs`  `oNMMNo`  `oNMMMMMMMNs`
    ///    `oNMMMNs`  `sNMMMMMMNs`  `oNMMMNs`
    ///      `oNs`  `sNMMMMMMMMMMNs`  `oNs`
    ///           `sNMMMMMMMMMMMMMMNs`
    ///           +NMMMMMMMMMMMMMMMMNo
    ///            `oNMMMMMMMMMMMMNo`
    ///              `oNMMMNNMMMNs`
    ///                `omo``oNs`
    /// ```
    Clover,
    /// Cobalt Logo
    /// ```text
    ///                           ///
    ///                   ,//////////////
    ///     ///////////////////////////////
    ///     ///////////////***********//////
    ///     ////***********************/////
    ///     /////***********************////
    ///    //////,,,,,,,,,,,,,,,,,,,,,,///
    ///  //////,,,,,,,,,,,,,,,,,,,,,,,,,/////
    ///  /////,,,,,,,,,,,,,,,,,,,,,,,,,,,,/////
    ///  *****,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*****
    ///  ******,,,,,,,,,,,,,,,,,,,,,,,,,,,,*****
    ///   *******,,,,,,,,,,,,,,,,,,,,,,,,,******
    ///     *******......................*******
    ///       ******....***********************
    ///         ****************************
    ///          *****
    /// ```
    Cobalt,
    /// Condres Logo
    /// ```text
    /// syyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy+.+.
    /// `oyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy+:++.
    /// /o+oyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy/oo++.
    /// /y+syyyyyyyyyyyyyyyyyyyyyyyyyyyyy+ooo++.
    /// /hy+oyyyhhhhhhhhhhhhhhyyyyyyyyy+oo+++++.
    /// /hhh+shhhhhdddddhhhhhhhyyyyyyy+oo++++++.
    /// /hhdd+oddddddddddddhhhhhyyyys+oo+++++++.
    /// /hhddd+odmmmdddddddhhhhyyyy+ooo++++++++.
    /// /hhdddmoodmmmdddddhhhhhyyy+oooo++++++++.
    /// /hdddmmms/dmdddddhhhhyyys+oooo+++++++++.
    /// /hddddmmmy/hdddhhhhyyyyo+oooo++++++++++:
    /// /hhdddmmmmy:yhhhhyyyyy++oooo+++++++++++:
    /// /hhddddddddy-syyyyyys+ooooo++++++++++++:
    /// /hhhddddddddy-+yyyy+/ooooo+++++++++++++:
    /// /hhhhhdddddhhy./yo:+oooooo+++++++++++++/
    /// /hhhhhhhhhhhhhy:-.+sooooo+++++++++++///:
    /// :sssssssssssso++`:/:--------.````````
    /// ```
    Condres,
    /// CRUX Logo
    /// ```text
    ///          odddd
    ///       oddxkkkxxdoo
    ///      ddcoddxxxdoool
    ///      xdclodod  olol
    ///      xoc  xdd  olol
    ///      xdc  k00Okdlol
    ///      xxdkOKKKOkdldd
    ///      xdcoxOkdlodldd
    ///      ddc:clllloooodo
    ///    odxxddxkO000kxooxdo
    ///   oxddx0NMMMMMMWW0odkkxo
    ///  oooxd0WMMMMMMMMMW0odxkx
    /// docldkXWMMMMMMMWWNOdolco
    /// xxdxkxxOKNWMMWN0xdoxo::c
    /// xOkkO0ooodOWWWXkdodOxc:l
    /// dkkkxkkkOKXNNNX0Oxxxc:cd
    ///  odxxdxxllodddooxxdc:ldo
    ///    lodddolcccccoxxoloo
    /// ```
    Crux,
    /// Small CRUX Logo
    /// ```text
    ///     ___
    ///    (.· |
    ///    (<> |
    ///   / __  \\
    ///  ( /  \\ /|
    /// _/\\ __)/_)
    /// \/-____\/
    /// ```
    CruxSmall,
    /// Crystal Logo
    /// ```text
    ///                   mysssym
    ///                 mysssym
    ///               mysssym
    ///             mysssym
    ///           mysssyd
    ///         mysssyd    N
    ///       mysssyd    mysym
    ///     mysssyd      dysssym
    ///   mysssyd          dysssym
    /// mysssyd              dysssym
    /// mysssyd              dysssym
    ///   mysssyd          dysssym
    ///     mysssyd      dysssym
    ///       mysym    dysssym
    ///         N    dysssym
    ///            dysssym
    ///          dysssym
    ///        dysssym
    ///      dysssym
    ///    dysssym
    /// ```
    Crystal,
    /// Cucumber Logo
    /// ```text
    ///            `.-://++++++//:-.`
    ///         `:/+//::--------:://+/:`
    ///       -++/:----..........----:/++-
    ///     .++:---...........-......---:++.
    ///    /+:---....-::/:/--//:::-....---:+/
    ///  `++:--.....:---::/--/::---:.....--:++`
    ///  /+:--.....--.--::::-/::--.--.....--:+/
    /// -o:--.......-:::://--/:::::-.......--:o-
    /// /+:--...-:-::---:::..:::---:--:-...--:+/
    /// o/:-...-:.:.-/:::......::/:.--.:-...-:/o
    /// o/--...::-:/::/:-......-::::::-/-...-:/o
    /// /+:--..-/:/:::--:::..:::--::////-..--:+/
    /// -o:--...----::/:::/--/:::::-----...--:o-
    ///  /+:--....://:::.:/--/:.::://:....--:+/
    ///  `++:--...-:::.--.:..:.--.:/:-...--:++`
    ///    /+:---....----:-..-:----....---:+/
    ///     .++:---..................---:++.
    ///       -/+/:----..........----:/+/-
    ///         `:/+//::--------:::/+/:`
    ///            `.-://++++++//:-.`
    /// ```
    Cucumber,
    /// CutefishOS Logo
    /// ```text
    ///                      ___ww___
    /// _              _wwMMM@M^^^^MMMMww_
    /// M0w_       _wMMM~~             ~~MMm_
    ///   ~MMy _ww0M~                      ~MMy
    ///     ~MMMM~                      o    "MM
    ///   jw0M~~MMMw_                      _wMM'
    /// wMM~      ~~MMmw__             __w0M~
    /// ~             ~~MM0MmwwwwwwwwwMMM~
    ///                     ~~~~^^~~~
    /// ```
    CutefishOs,
    /// CuteOS Logo
    /// ```text
    ///                        1ua
    ///                   MMM1ua
    ///  MMEE         MMMMM1uazE
    /// MM EEEE     M1MM1uazzEn EEEE  MME
    ///     EEEEE  MMM uazEno EEEE
    ///     EEEEEMMMMMMEno~; EE          E
    ///      EE MMMMMMMM~;;E  MMMMM      M
    ///      E MMMMMMMMM            E  E   
    ///       MMMMMMMMMMM
    ///            MMMMMMMMM EE
    ///                 MM1MMMM EEE
    ///                      MMMMM
    ///                           MMM
    ///                               M
    /// ```
    CuteOs,
    /// CyberOS Logo
    /// ```text
    ///              !MEEEEEEEEEEEP
    ///             .MMMMM000000Nr.
    ///             &MMMMMMMMMMMMMMMMMMM9
    ///            ~MMMMMMMMMMMMMMMMMMMC
    ///       "    MMMMMMMMMMMMMMMMMMs
    ///     iMMMM&&MMMMMMMMMMMMMMMM\\
    ///    BMMMMMMMMMMMMMMMMMMMMM"
    ///   9MMMMMMMMMMMMMMMMMMMMMMMf-
    ///         sMMMMMMMMMMMMMMMMMMMM3_
    ///          +ffffffffPMMMMMMMMMMMM0
    ///                     CMMMMMMMMMMM
    ///                       }MMMMMMMMM
    ///                         ~MMMMMMM
    ///                           "RMMMM
    ///                             .PMB
    /// ```
    CyberOs,
    /// Cycledream Logo
    /// ```text
    ///                 .:ox00000kdc,              
    ///       ;;    'cdO.            dxl,.         
    ///       00 'oO'                    0x;       
    ///       00ko                         ,Oc     
    ///       x00Okxx                        lO'   
    ///                     .x,               .0c  
    ///               ;c   .O00'   ::          '0:
    ///  .                 O0000.               o0.
    /// .0c        ,,;;:clO00000Olc:;;,,        .0l
    /// x0          ;00000000000000000c          00
    /// 00             0000000000000.            0O
    /// x0.        ...  00000000000. ...        '0:
    /// .0c            ,00000000000o            .o
    ///  o0.           O000;   '0000               
    ///   0k          .0     O     O'              
    ///    lO.                        ''.....      
    ///     .0o                          d000      
    ///        Oo'                     ,dO O0      
    ///          ;Oo;.             .;oO,   00      
    ///               xkdocc:ccodko                
    ///                   ;k0k,                  
    /// ```
    Cycledream,
    /// Dahlia Logo
    /// ```text
    ///                   .#.
    ///                 *%@@@%*
    ///         .,,,,,(&@@@@@@@&/,,,,,.
    ///        ,#@@@@@@@@@@@@@@@@@@@@@#.
    ///        ,#@@@@@@@&#///#&@@@@@@@#.
    ///      ,/%&@@@@@%/,    .,(%@@@@@&#/.
    ///    *#&@@@@@@#,.         .*#@@@@@@&#,
    ///  .&@@@@@@@@@(            .(@@@@@@@@@&&.
    /// #@@@@@@@@@@(               )@@@@@@@@@@@#
    ///  °@@@@@@@@@@(            .(@@@@@@@@@@@°
    ///    *%@@@@@@@(.           ,#@@@@@@@%*
    ///      ,(&@@@@@@%*.     ./%@@@@@@%(,
    ///        ,#@@@@@@@&(***(&@@@@@@@#.
    ///        ,#@@@@@@@@@@@@@@@@@@@@@#.
    ///         ,*****#&@@@@@@@&(*****,
    ///                ,/%@@@%/.
    ///                   ,#,
    /// ```
    Dahlia,
    /// DarkOS Logo
    /// ```text
    /// ⠀⠀⠀⠀  ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠢⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣶⠋⡆⢹⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡆⢀⣤⢛⠛⣠⣿⠀⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣶⣿⠟⣡⠊⣠⣾⣿⠃⣠⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣴⣯⣿⠀⠊⣤⣿⣿⣿⠃⣴⣧⣄⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣶⣿⣿⡟⣠⣶⣿⣿⣿⢋⣤⠿⠛⠉⢁⣭⣽⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    ///   ⠀⠀⠀⠀⠀⠀ ⠀⣠⠖⡭⢉⣿⣯⣿⣯⣿⣿⣿⣟⣧⠛⢉⣤⣶⣾⣿⣿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⣴⣫⠓⢱⣯⣿⢿⠋⠛⢛⠟⠯⠶⢟⣿⣯⣿⣿⣿⣿⣿⣿⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⢀⡮⢁⣴⣿⣿⣿⠖⣠⠐⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠛⠛⠛⢿⣶⣄⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⢀⣤⣷⣿⣿⠿⢛⣭⠒⠉⠀⠀⠀⣀⣀⣄⣤⣤⣴⣶⣶⣶⣿⣿⣿⣿⣿⠿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⢀⣶⠏⠟⠝⠉⢀⣤⣿⣿⣶⣾⣿⣿⣿⣿⣿⣿⣟⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⢴⣯⣤⣶⣿⣿⣿⣿⣿⡿⣿⣯⠉⠉⠉⠉⠀⠀⠀⠈⣿⡀⣟⣿⣿⢿⣿⣿⣿⣿⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠉⠛⣿⣧⠀⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⠃⣿⣿⣯⣿⣦⡀⠀⠉⠻⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠉⢿⣮⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⣿⠀⣯⠉⠉⠛⢿⣿⣷⣄⠀⠈⢻⣆⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠢⠀⠀⠀⠀⠀⠀⠀⢀⢡⠃⣾⣿⣿⣦⠀⠀⠀⠙⢿⣿⣤⠀⠙⣄⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⢋⡟⢠⣿⣿⣿⠋⢿⣄⠀⠀⠀⠈⡄⠙⣶⣈⡄⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⠚⢲⣿⠀⣾⣿⣿⠁⠀⠀⠉⢷⡀⠀⠀⣇⠀⠀⠈⠻⡀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢢⣀⣿⡏⠀⣿⡿⠀⠀⠀⠀⠀⠀⠙⣦⠀⢧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠿⣧⣾⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⣮⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    /// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠙⠛⠀⠀⠀⠀⠀⠀
    /// ```
    DarkOs,
    /// Debian Logo
    /// ```text
    ///        _,metgg.
    ///     ,gP.
    ///   ,gP"         """Y.".
    ///  ,P'               `.
    /// ',P       ,ggs.     `b:
    /// `d'     ,P"'   .    
    ///  P      d'     ,    P
    ///  :      .   -    ,d'
    ///  ;      Yb._   _,dP'
    ///  Y.    `.`"YP"'
    ///  `b      "-.__
    ///   `Y
    ///    `Y.
    ///      `b.
    ///        `Yb.
    ///           `"Yb._
    ///              `"""
    /// ```
    Debian,
    /// Small Debian Logo
    /// ```text
    ///   _____
    ///  /  __ \
    /// |  /    |
    /// |  \___-
    /// -_
    ///   --_
    /// ```
    DebianSmall,
    /// Deepin Logo
    /// ```text
    ///              ............
    ///          .';;;;;.       .,;,.
    ///       .,;;;;;;;.       ';;;;;;;.
    ///     .;::::::::'     .,::;;,''''',.
    ///    ,'.::::::::    .;;'.          ';
    ///   ;'  'cccccc,   ,' :: '..        .:
    ///  ,,    :ccccc.  ;: .c, '' :.       ,;
    /// .l.     cllll' ., .lc  :; .l'       l.
    /// .c       :lllc  ;cl:  .l' .ll.      :'
    /// .l        'looc. .   ,o:  'oo'      c,
    /// .o.         .:ool::coc'  .ooo'      o.
    ///  ::            .....   .;dddo      ;c
    ///   l:...            .';lddddo.     ,o
    ///    lxxxxxdoolllodxxxxxxxxxc      :l
    ///     ,dxxxxxxxxxxxxxxxxxxl.     'o,
    ///       ,dkkkkkkkkkkkkko;.    .;o;
    ///         .;okkkkkdl;.    .,cl:.
    ///             .,:cccccccc:,.
    /// ```
    Deepin,
    /// DesaOS Logo
    /// ```text
    /// ███████████████████████
    /// ███████████████████████
    /// ███████████████████████
    /// ███████████████████████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ████████               ███████
    /// ██████████████████████████████
    /// ██████████████████████████████
    /// ████████████████████████
    /// ████████████████████████
    /// ████████████████████████
    /// ```
    DesaOs,
    /// Devuan Logo
    /// ```text
    ///    ..,,;;;::;,..
    ///            `':ddd;:,.
    ///                  `'dPPd:,.
    ///                      `:bb`.
    ///                         'Pd`
    ///                          .`
    ///                          ;P
    ///                       .:P`
    ///                   .,:b;'
    ///              .,:dPb:'
    ///       .,:;dbPd'`
    ///  ,dbb:'`
    /// :b:'`
    ///  `bd:''`
    ///    `'''`
    /// ```
    Devuan,
    /// Small Devuan Logo
    /// ```text
    ///  ..:::.
    ///     ..-==-
    ///         .+#:
    ///          =@@
    ///       :+%@#:
    /// .:=+#@@%*:
    /// #@@@#=:
    /// ```
    DevuanSmall,
    /// DietPI Logo
    /// ```text
    ///   :=+******+-    -+******+=:
    ///  =#-::-::::-=#:-#=-::::-::-#=
    ///  :%-::--==-::-%%-::-==--::-%:
    ///   +#-:::::=+++@@+++=-::::-#=
    ///    :#+-::::=%@@@@@=::::-+#:
    ///      =@%##%@@@@@@@@%##%@=
    ///    .#@@@@@@@@@@@@@@@@@@@@#.
    ///    %@@@@@@@@@@@@@@@@@@@@@@%
    ///   -@@@@@@@@@@@@@@@@@@@@@@@@:
    /// .#@@@@@@@@@@%%%%%@@@@@@@@@@@#.
    /// #@@@+-=*#%%%%%%%%%%%%#+--#@@@#
    /// %@@%*.   .:=*%%%%*=:    .#@@@%
    /// :%@@@#+=-::-*%%%%+:::-=+%@@@%:
    ///  :@@@@%@%%%%@###%@%%%%@%@@@@.
    ///   +@@@@@@@@@%=*+%@%@@@@@@@@+
    ///    #@@@@@@@@@@@@@@@@@@@@@@#
    ///     -#@@@@@@@@@@@@@@@@@@#-
    ///        -*%@@@@@@@@@@%*-
    ///           .+%@@@@%+.
    /// ```
    DietPi,
    /// DracOS Logo
    /// ```text
    ///        `-:/-
    ///           -os:
    ///             -os/`
    ///               :sy+-`
    ///                `/yyyy+.
    ///                  `+yyyyo-
    ///                    `/yyyys:
    /// `:osssoooo++-        +yyyyyy/`
    ///    ./yyyyyyo         yo`:syyyy+.
    ///       -oyyy+         +-   :yyyyyo-
    ///         `:sy:        `.    `/yyyyys:
    ///            ./o/.`           .oyyso+oo:`
    ///               :+oo+//::::///:-.`
    /// ```
    DracOs,
    /// DragonFly Logo
    /// ```text
    /// ,--,           |           ,--,
    /// |   `-,       ,^,       ,-'   |
    ///  `,    `-,   (/ \)   ,-'    ,'
    ///    `-,    `-,/   \,-'    ,-'
    ///       `------(   )------'
    ///   ,----------(   )----------,
    ///  |        _,-(   )-,_        |
    ///   `-,__,-'   \   /   `-,__,-'
    ///               | |
    ///               | |
    ///               | |
    ///               | |
    ///               | |
    ///               | |
    ///               `|'
    /// ```
    DragonFly,
    /// Small DragonFly Logo
    /// ```text
    ///    ,_,
    /// ('-_|_-')
    ///  >--|--<
    /// (_-'|'-_)
    ///     |
    ///     |
    ///     |
    /// ```
    DragonFlySmall,
    /// Alternative DragonFly Logo
    /// ```text
    ///                         .-.
    ///                   ()I()
    ///              "==.__:-:__.=="
    ///             "==.__/~|~\__.=="
    ///             "==._(  Y  )_.=="
    ///  .-'~~""~=--...,__\/|\/__,...--=~""~~'-.
    /// (               ..=\\=/=..               )
    ///  `'-.        ,.-"`;/=\\;"-.,_        .-'`
    ///      `~"-=-~` .-~` |=| `~-. `~-=-"~`
    ///           .-~`    /|=|\    `~-.
    ///        .~`       / |=| \       `~.
    ///    .-~`        .'  |=|  `.        `~-.
    ///  (`     _,.-="`    |=|    `"=-.,_     `)
    ///   `~"~"`           |=|           `"~"~`
    ///                    /=\\
    ///                    \\=/
    ///                     ^
    /// ```
    DragonFlyOld,
    /// DraugerOS Logo
    /// ```text
    ///                   -``-
    ///                 `:+``+:`
    ///                `/++``++/.
    ///               .++/.  ./++.
    ///              :++/`    `/++:
    ///            `/++:        :++/`
    ///           ./+/-          -/+/.
    ///          -++/.            ./++-
    ///         :++:`              `:++:
    ///       `/++-                  -++/`
    ///      ./++.                    ./+/.
    ///     -++/`                      `/++-
    ///    :++:`                        `:++:
    ///  `/++-                            -++/`
    /// .:-.`..............................`.-:.
    /// `.-/++++++++++++++++++++++++++++++++/-.`
    /// ```
    DraugerOs,
    /// Droidian Logo
    /// ```text
    ///        _,metgg.
    ///    ,gP.
    ///  ,P'              `.
    /// ',P       ,ggs.     `b:
    /// `d'     ,P"'   .    
    ///  P      d'     ,    P
    ///  :      .   -    ,d'
    ///  ;      Yb._   _,dP'
    ///  Y.    `.`"YP"'
    ///  `b      "-.__
    ///   `Y
    ///   `Y.
    ///      `b.
    ///        `Yb.
    ///           `"Yb._
    /// ```
    Droidian,
    /// Elbrus Logo
    /// ```text
    /// ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
    /// ██▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ██   ███████   ███████   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ███████   ███████
    /// ██   ██                  ██
    /// ██   ██▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄██
    /// ██   ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ███████████████████████████
    /// ```
    Elbrus,
    /// Elementary Logo
    /// ```text
    ///          eeeeeeeeeeeeeeeee
    ///       eeeeeeeeeeeeeeeeeeeeeee
    ///     eeeee  eeeeeeeeeeee   eeeee
    ///   eeee   eeeee       eee     eeee
    ///  eeee   eeee          eee     eeee
    /// eee    eee            eee       eee
    /// eee   eee            eee        eee
    /// ee    eee           eeee       eeee
    /// ee    eee         eeeee      eeeeee
    /// ee    eee       eeeee      eeeee ee
    /// eee   eeee   eeeeee      eeeee  eee
    /// eee    eeeeeeeeee     eeeeee    eee
    ///  eeeeeeeeeeeeeeeeeeeeeeee    eeeee
    ///   eeeeeeee eeeeeeeeeeee      eeee
    ///     eeeee                 eeeee
    ///       eeeeeee         eeeeeee
    ///          eeeeeeeeeeeeeeeee
    /// ```
    Elementary,
    /// Small Elementary Logo
    /// ```text
    ///   _______
    ///  / ____  \\
    /// /  |  /  /\\
    /// |__\\ /  / |
    /// \\   /__/  /
    ///  \\_______/
    /// ```
    ElementarySmall,
    /// ```text
    ///              *@,,&(%%%..%*.
    ///          (@&%/##############((/*,
    ///       @@&#########*..../########%*..
    ///     @&#%%%%%.              ,.,%%%%%%.
    ///   /%(%%%%.                      (%%%%#.
    ///  /*%%##,.                       .,%%###,
    ///  ,####.   ,*#%#/,(/               /####,
    /// ((###/   ,,##########(/(#          %####,
    /// %#(((.   ../(((((((((((((((#/*..   *.(((/
    /// %#///.        ***.*/////////////
    /// ##////*              ***.*/////.
    ///  ((*****                   ***
    ///   ,*****..
    ///    ..******..                 *%/****.
    ///      .,,*******,,,../##(%&&#******,.
    ///         ,*,,,,,,,,,,,,,,,,,,,,,..
    ///             *///,,,,,,,,,,..
    /// ```
    Elive,
    /// EncryptOS Logo
    /// ```text
    ///      *******
    ///    ***       **.
    ///    **         **
    ///    **         **
    ///
    ///  *****************
    /// ,,,,,,,,,,,,,,,,***
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,     ,,,,,,,
    /// ,,,,,,,,,,,,,,,,,,,
    ///     ,,,,,,,,,,,,.
    /// ```
    EncryptOs,
    /// Endeavour Logo
    /// ```text
    ///                      ./o.
    ///                    ./sssso-
    ///                  `:osssssss+-
    ///                `:+sssssssssso/.
    ///              `-/ossssssssssssso/.
    ///            `-/+sssssssssssssssso+:`
    ///          `-:/+sssssssssssssssssso+/.
    ///        `.://osssssssssssssssssssso++-
    ///       .://+ssssssssssssssssssssssso++:
    ///     .:///ossssssssssssssssssssssssso++:
    ///   `:////ssssssssssssssssssssssssssso+++.
    /// `-////+ssssssssssssssssssssssssssso++++-
    ///  `..-+oosssssssssssssssssssssssso+++++/`
    ///    ./++++++++++++++++++++++++++++++/:.
    ///   `:::::::::::::::::::::::::------
    /// ```
    Endeavour,
    /// Small Endeavour Logo
    /// ```text
    ///            /o.
    ///          :sssso-
    ///        :ossssssso:
    ///      /ssssssssssso+
    ///   -+ssssssssssssssso+
    ///  //osssssssssssssso+-
    ///   `+++++++++++++++-`
    /// ```
    EndeavourSmall,
    /// Endless Logo
    /// ```text
    ///            `:+yhmNMMMMNmhy+:`
    ///         -odMMNhso//////oshNMMdo-
    ///       /dMMh+.              .+hMMd/
    ///     /mMNo`                    `oNMm:
    ///   `yMMo`                        `oMMy`
    ///  `dMN-                            -NMd`
    ///  hMN.                              .NMh
    /// /MM/                  -os`          /MM/
    /// dMm    `smNmmhs/- `:sNMd+   ``       mMd
    /// MMy    oMd--:+yMMMMMNo.:ohmMMMNy`    yMM
    /// MMy    -NNyyhmMNh+oNMMMMMy:.  dMo    yMM
    /// dMm     `/++/-``/yNNh+/sdNMNddMm-    mMd
    /// /MM/          `dNy:       `-::-     /MM/
    ///  hMN.                              .NMh
    ///  `dMN-                            -NMd`
    ///   `yMMo`                        `oMMy`
    ///     /mMNo`                    `oNMm/
    ///       /dMMh+.              .+hMMd/
    ///         -odMMNhso//////oshNMMdo-
    ///            `:+yhmNMMMMNmhy+:`
    /// ```
    Endless,
    /// Enso Logo
    /// ```text
    ///                 .:--==--:.                     
    ///             :=*#############*+-.               
    ///          .+##################*##*:             
    ///        .*##########+==-==++*####*##-           
    ///       =########=:           .-+**#***.         
    ///      *#######-                  ++*#**.        
    ///     +######+                     -*+#**        
    ///    :######*                       .*+**=       
    ///    *######:                        --#*#       
    ///    #######                          +++#.      
    ///    #######.                         ++=*.      
    ///    *######+                        .-+*+       
    ///    :#######-                       -:*+:       
    ///     =#######*.                    :.*+-        
    ///      +########*-                  :*=-         
    ///       =###########+=:            =+=:          
    ///        .+#############.       .-==:            
    ///          .=###########=   ..:--:.              
    ///             .-+######+                 
    /// ```
    Enso,
    /// EshanizedOS Logo
    /// ```text
    ///                  .:-==++++++++++++++++++++++-.
    ///               .:=+##############################+
    ///            .=*###################################-
    ///          :+######################################:
    ///        :+######################################*-
    ///      .+#############*+--:::::::::::::::::::::.
    ///     :*##########*=:
    ///    -###########=
    ///   :##########+.
    ///   *#########=
    ///  =*********+             .::::::::::::::::.
    ///  **********.           -*###################+:
    /// :**********           =#######################=
    /// -*********+           +************************
    /// :*********+           :***********************:
    /// .**********            .=******************+-
    ///  +*********-
    ///  .**********.
    ///   =**********:
    ///    +**********=.
    ///     =***********=:
    ///      -*************+-.
    ///       :+****************+++++++++++++++++++++==:
    ///         :+**************************************+.
    ///            -+************************************-
    ///              .-+*********************************.
    ///                 :-==+*************************=.
    /// ```
    EshanizedOs,
    /// EuroLinux Logo
    /// ```text
    ///                 __
    ///          -wwwWWWWWWWWWwww-
    ///         -WWWWWWWWWWWWWWWWWWw-
    ///           \WWWWWWWWWWWWWWWWWWW-
    ///   _Ww      `WWWWWWWWWWWWWWWWWWWw
    ///  -WEWww                -WWWWWWWWW-
    /// _WWUWWWW-                _WWWWWWWW
    /// _WWRWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW-
    /// wWWOWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWLWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWw
    /// WWWIWWWWWWWWWWWWWWWWWWWWWWWWWWWWww-
    /// wWWNWWWWw
    ///  WWUWWWWWWw
    ///  wWXWWWWWWWWww
    ///    wWWWWWWWWWWWWWWWw
    ///     wWWWWWWWWWWWWWWWw
    ///        WWWWWWWWWWWWWw
    ///            wWWWWWWWw
    /// ```
    EuroLinux,
    /// Evolinx Logo
    /// ```text
    /// #-----------------------------------------------------------------------#
    /// |#SSSSSSSS\*SS\****SS\**SSSSSS\**SS\*******SSSSSS\*SS\***SS\*SS\***SS\*#|
    /// |#SS**_____|SS*|***SS*|SS**__SS\*SS*|******\_SS**_|SSS\**SS*|SS*|**SS*|#|
    /// |#SS*|******SS*|***SS*|SS*/**SS*|SS*|********SS*|**SSSS\*SS*|\SS\*SS**|#|
    /// |#SSSSS\****\SS\**SS**|SS*|**SS*|SS*|********SS*|**SS*SS\SS*|*\SSSS**/*#|
    /// |#SS**__|****\SS\SS**/*SS*|**SS*|SS*|********SS*|**SS*\SSSS*|*SS**SS<**#|
    /// |#SS*|********\SSS**/**SS*|**SS*|SS*|********SS*|**SS*|\SSS*|SS**/\SS\*#|
    /// |#SSSSSSSS\****\S**/****SSSSSS**|SSSSSSSS\*SSSSSS\*SS*|*\SS*|SS*/**SS*|#|
    /// |#\________|****\_/*****\______/*\________|\______|\__|**\__|\__|**\__|#|
    /// #-----------------------------------------------------------------------#
    /// ```
    Evolinx,
    /// EvolutionOS Logo
    /// ```text
    ///                .':ldxxxxdo:,.
    ///            .lXMMMMMMMMMMMMMMMMNo'
    ///          dWMMMMMMMMMMMMMMMMMMMMMMWk
    ///       .OMMMMWWWWWWWWWWWWWWWWWWWMMMMM0;
    ///      kMMMMMXxxxxkkkkkkkkkkkkkkkKWMMMMMK.
    ///    .kMMMMMMXddd0KKKKKKKKKKKKKKKNMMMMMMMN.
    ///    KMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMX.
    ///   cMMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMMo
    ///   KMMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMMN
    ///   XMMMMMMMMXdddXWOkkkkkkkKWKOOOXMMMMMMMMMW
    ///   XMMMMMMMMXdddXWOkkkkkkkKWKOOOXMMMMMMMMMW
    ///   0MMMMMMMMXdddXMWWWWWWWWWMWWWWWMMMMMMMMMN
    ///   cMMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMMMd
    ///   .kMMMMMMMXdddXMMMMMMMMMMMMMMMMMMMMMMMM0.
    ///    .kMMMMMMXxxxNW0OOOOOOOOOOOOOKMMMMMMMK.
    ///      oMMMMMXxxxNW0OOOOOOOOOOOOOKWMMMMMk.
    ///       '0MMMWNNNWMWWWWWWWWWWWWWWWMMMMX;
    ///          cWMMMMMMMMMMMMMMMMMMMMMMWd
    ///             :KWMMMMMMMMMMMMMMWXo.
    ///                 .cdO0KK00xl'
    ///                      ..
    /// ```
    EvolutionOs,
    /// Small EvolutionOS Logo
    /// ```text
    ///       ,coddoc'
    ///    'cddddddddddc'
    ///  'dddOWWXXXXXXKddo.
    /// .ddddOMXddddddddddd.
    /// oddddOMXk00OkOOddddo
    /// .ddddOMXkOOOxOkdddd.
    ///  .dddOWWXXXXXXKddd'
    ///    'dddddddddddd'
    ///       'cddddd,
    /// ```
    EvolutionOsSmall,
    /// Alternative EvolutionOS Logo
    /// ```text
    ///     dddddddddddddddddddddddd
    /// .dddd''''''''''''''''''''''dddd.
    /// dd:   dddddddddddddddddddd;   dd:
    /// dd:   ldl:''''''''''''''''    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl: ddddddd;  ddddd;   dd:
    /// dd:   ldl: '''''''   '''''    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl:                    dd:
    /// dd:   ldl: ddddddddddddddd;   dd:
    /// dddd:.'''  '''''''''''''''  dddd:
    ///    dddddddddddddddddddddddddd;;'
    ///     '''''''''''''''''''''''''
    /// ```
    EvolutionOsOld,
    /// Exherbo Logo
    /// ```text
    ///  ,
    /// OXo.
    /// NXdX0:    .cok0KXNNXXK0ko:.
    /// KX  '0XdKMMK;.xMMMk, .0MMMMMXx;  ...
    /// 'NO..xWkMMx   kMMM    cMMMMMX,NMWOxOXd.
    ///   cNMk  NK    .oXM.   OMMMMO. 0MMNo  kW.
    ///   lMc   o:       .,   .oKNk;   ;NMMWlxW'
    ///  ;Mc    ..   .,,'    .0Mg;WMN'dWMMMMMMO
    ///  XX        ,WMMMMW.  cMcfliWMKlo.   .kMk
    /// .Mo        .WMGDMW.   XMWO0MMk        oMl
    /// ,M:         ,XMMWx::,''oOK0x;          NM.
    /// 'Ml      ,kNKOxxxxxkkO0XXKOd:.         oMk
    ///  NK    .0Nxc:::::::::::::::fkKNk,      .MW
    ///  ,Mo  .NXc::qXWXb::::::::::oo::lNK.    .MW
    ///   ;Wo oMd:::oNMNP::::::::oWMMMx:c0M;   lMO
    ///    'NO;W0c:::::::::::::::dMMMMO::lMk  .WM'
    ///      xWONXdc::::::::::::::oOOo::lXN. ,WMd
    ///       'KWWNXXK0Okxxo,:::::::,lkKNo  xMMO
    ///         :XMNxl,';:lodxkOO000Oxc. .oWMMo
    ///           'dXMMXkl;,.        .,o0MMNo'
    ///              ':d0XWMMMMWNNNNMMMNOl'
    ///                    ':okKXWNKkl'
    /// ```
    Exherbo,
    /// Exodia Predator Logo
    /// ```text
    /// -                                  :
    /// +:                                :+
    /// ++.                              .++
    /// +++             :  .             +++
    /// +++=           .+  +            =+++
    /// ++++-          ++  +=          -++++
    /// ++++++-       -++  ++-       -++++++
    /// ++++++++:    .+++  +++.    :++++++++
    /// ++++++++++:  ++++  ++++  :++++++++++
    /// +++++++++++==++++  ++++=++++++=+++++
    /// +++++.:++++++++++  ++++++++++:.+++++
    /// +++++. .+++++++++  +++++++++. .+++++
    /// +++++:   ++++++++  ++++++++   :+++++
    /// ++++++-  =+++++++  +++++++=  -++++++
    ///  :+++++= =+++++++  +++++++= =+++++:
    ///    :+++= =+++++++  +++++++= =+++:
    ///      -+= =+++++++  +++++++= ++-
    ///        : =++++++-  -++++++= :
    ///          =++++-      -++++=
    ///          =++=          =++=
    ///          =++            ++=
    ///          =+.            .+=
    ///          =-              -=
    ///          :                :
    /// ```
    ExodiaPredator,
    /// Fedora Logo
    /// ```text
    ///              .',;::::;,'.
    ///          .';:cccccccccccc:;,.
    ///       .;cccccccccccccccccccccc;.
    ///     .:cccccccccccccccccccccccccc:.
    ///   .;ccccccccccccc;.:dddl:.;ccccccc;.
    ///  .:ccccccccccccc;OWMKOOXMWd;ccccccc:.
    /// .:ccccccccccccc;KMMc;cc;xMMc;ccccccc:.
    /// ,cccccccccccccc;MMM.;cc;;WW:;cccccccc,
    /// :cccccccccccccc;MMM.;cccccccccccccccc:
    /// :ccccccc;oxOOOo;MMM000k.;cccccccccccc:
    /// cccccc;0MMKxdd:;MMMkddc.;cccccccccccc;
    /// ccccc;XMO';cccc;MMM.;cccccccccccccccc'
    /// ccccc;MMo;ccccc;MMW.;ccccccccccccccc;
    /// ccccc;0MNc.ccc.xMMd;ccccccccccccccc;
    /// cccccc;dNMWXXXWM0:;cccccccccccccc:,
    /// cccccccc;.:odl:.;cccccccccccccc:,.
    /// ccccccccccccccccccccccccccccc:'.
    /// :ccccccccccccccccccccccc:;,..
    ///  ':cccccccccccccccc::;,.
    /// ```
    Fedora,
    /// Small Fedora Logo
    /// ```text
    ///         ,'''''.
    ///        |   ,.  |
    ///        |  |  '_'
    ///   ,....|  |..
    /// .'  ,_;|   ..'
    /// |  |   |  |
    /// |  ',_,'  |
    ///  '.     ,'
    ///    '''''
    /// ```
    FedoraSmall,
    /// Alternative Fedora Logo
    /// ```text
    ///           /:-------------:\
    ///        :-------------------::
    ///      :-----------/shhOHbmp---:\
    ///    /-----------omMMMNNNMMD  ---:
    ///   :-----------sMMMMNMNMP.    ---:
    ///  :-----------:MMMdP-------    ---\
    /// ,------------:MMMd--------    ---:
    /// :------------:MMMd-------    .---:
    /// :----    oNMMMMMMMMMNho     .----:
    /// :--     .+shhhMMMmhhy++   .------/
    /// :-    -------:MMMd--------------:
    /// :-   --------/MMMd-------------;
    /// :-    ------/hMMMy------------:
    /// :-- :dMNdhhdNMMNo------------;
    /// :---:sdNMMMMNds:------------:
    /// :------:://:-------------::
    /// :---------------------://
    /// ```
    FedoraOld,
    /// Fedora-Silverblue Logo
    /// ```text
    ///     .;ooooooooooooooooooooooooooo.
    ///   ,dddddddddddddddddddddddddddddd';
    ///  lddddddddddddddddddddddddddddd';;;
    /// ddddd,XXX.ddddd,XXX.dddd',XXX.;;;;;
    /// dddddXXxXXdddddXXxXXddd',XXxXX;;;;;
    /// ddddd'XXX'ddddd'XXX'dd'XXXXXX';;;;;
    /// dddddd;X;ddddddd;X:d'XXX;;;;;;;;;;;
    /// dddddd;X;ddddddd;X:XXX;;;;;;;;;;;;;
    /// dddddd;X;dddddd';XXX,,,,,,XXX.;;;;;
    /// dddddd;X;dddd'XXXXXXXXXXXXXxXX;;;;;
    /// dddddd;X;dd'XXX;;;;;;;;;;;XXX;;;;;;
    /// dddddd;X;'XXX;;;;;;;;;;;;;;;;;;;;;;
    /// dddddd;XXXXX,,,,,,,,,,,,,;XXX:;;;;;
    /// dddddd:XXXXXXXXXXXXXXXXXXXXxXX;;;;;
    /// ddddd';;;;;;;;;;;;;;;;;;;'XXX';;;;'
    /// ddd';;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    /// o';;;;;;;;;;;;;;;;;;;;;;;;;;;;'
    /// ```
    FedoraSilverblue,
    /// Fedora-Kinoite Logo
    /// ```text
    ///  ,clll:.          .,::::::::::::'
    /// :ooooooo        .;::::::::::::::
    /// looooooo       ,:::::::::::::::'
    /// looooooo      .::::::::::::::::
    /// looooooo      ;:::::::::::::::.
    /// looooooo     .::::::::::::::::
    /// looooool;;;;,::::::::::::::::
    /// looool::,   .::::::::::::::
    /// looooc::     ;::
    /// looooc::;.  .::;
    /// loooool:::::::::::.
    /// looooooo.    .::::::'
    /// looooooo       .::::::,;,..
    /// looooooo          :::;' ';:;.
    /// looooooo          :::     :::
    /// cooooooo          .::'   '::.
    ///  .ooooc             ::, ,::
    ///                       ''''
    /// ```
    FedoraKinoite,
    /// Fedora-Sericea Logo
    /// ```text
    ///               :oooo,  .','
    ///       .';;;.;oooooooolooooo'
    ///      coooooooooooooooooooooooolc'
    ///   .':oooooooooooollooooooooooooool
    /// .oooooooooooooooolloooooooooooolool
    /// ooooooooooooooooollooooooooooolloo'
    ///  ooooloooooooooolllooooooooollloo
    ///  .ooooolllooooolllooooooooolllool
    ///  .ooooooollloolllloooolllllooooo:
    ///   'oooooooolllllllllllloooooooo'
    ///      .c,.oollllloooooooo.
    ///            'll;
    ///            'll.
    ///            lll
    ///            lll
    ///           ;ll,
    ///           .l:
    /// ```
    FedoraSericea,
    /// Fedora-CoreOS Logo
    /// ```text
    ///                 .....
    ///           .';:cccccccc:;'.
    ///         ':ccccclclllllllllcc:.
    ///      .;cccccccclllllllllllllllc,
    ///     ;clllcccccllllllllllllllllllc,
    ///   .cllclcccccllllllllllllllllllllc:
    ///   ccclclcccccllllkWMMNKkllllllllllc:
    ///  :ccclclcccclllloWMMMMMMWOlllllllllc,
    /// .ccllllllcccclllOMMMMMMMMM0lllllllllc
    /// .lllllclccccllllKMMMMMMMMMMollllllllc.
    /// .lllllllccccclllKMMMMMMMMN0lllllllllc.
    /// .cclllllcccclllldxkkxxdollllllllllclc
    ///  :cccllllllcccclllccllllcclccccccccc;
    ///  .ccclllllllcccccccclllccccclccccccc
    ///   .cllllllllllclcccclccclccllllcllc
    ///     :cllllllllccclcllllllllllllcc;
    ///      .cccccccccccccclcccccccccc:.
    ///        .;cccclccccccllllllccc,.
    ///           .';ccccclllccc:;..
    ///                 .....
    /// ```
    FedoraCoreos,
    /// FemboyOS Logo
    /// ```text
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// MMMMWKkxkKWMMMMMMMMMMMMMMMMMMMMWKkxkKWMM
    /// MMMMXo. .;xKWMMMMMMMMMMMMMMMMMMXo. .oXMM
    /// MMWXx,..'..oXMMMMMMMMMMMMMMMMWKx,  .lXMM
    /// MMNo. .cOc.,xKWMMMMMMMMMMMMWXx;.....cXMM
    /// MMXl..;kKl. .oXMMMMMMMMMMWKx;..,ok:.'o0W
    /// WKx,.cKWNk;..lXMMMMMMMMWKx;..,o0NXl. .oN
    /// No. .lXMMWKc.,dKWMMMMMMNo..;d0NWMNx,..lX
    /// Nk:,:kNMMMNk:,ckNMMMMMMNxcxXWMMMMMN0ockN
    /// MWNNNWMMMMMWNNNWMMMMMMMMWWWMMMMMMMMMWWWM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// MMMMMMMNXKXNWMMMMMMMMMMMWNKOKWMMMMMMMMMM
    /// MMMMMMWKdccxXMMMMMMMMMMW0o'.oXMMMMMMMMMM
    /// MMMMMMMNO:.'o0NKkkkkkOXXo. .lXMMMMMMMMMM
    /// MMMMMMMMNx,..;o;.    .:o,..;kNMMMMMMMMMM
    /// MMMMMMMMMNO:     ...     .cKWMMMMMMMMMMM
    /// MMMMMMMMMMNx,. .;dk:.   .;kNMMMMMMMMMMMM
    /// MMMMMMMMMMMN0ocxXWNkl:,:xXWMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMWNWMMMWWNNNWMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// ```
    FemboyOs,
    /// Feren Logo
    /// ```text
    ///  `----------`
    ///  :+ooooooooo+.
    /// -o+oooooooooo+-
    /// ..`/+++++++++++/...`````````````````
    ///    .++++++++++++++++++++++++++/////-
    ///     ++++++++++++++++++++++++++++++++//:`
    ///     -++++++++++++++++++++++++++++++/-`
    ///      ++++++++++++++++++++++++++++:.
    ///      -++++++++++++++++++++++++/.
    ///       +++++++++++++++++++++/-`
    ///       -++++++++++++++++++//-`
    ///         .:+++++++++++++//////-
    ///            .:++++++++//////////-
    ///              `-++++++---:::://///.
    ///            `.:///+++.             `
    ///           `.........
    /// ```
    Feren,
    /// Finnix
    /// ```text
    ///             ,,:;;;;:,,
    ///         ,;*%S########S%*;,
    ///       ;?#################S?:
    ///     :%######################?:
    ///    +##########################;
    ///   +############################;
    ///  :#############.**,#############,
    ///  *###########+      +###########+
    ///  ?##########  Finnix  ##########*
    ///  *###########,      ,###########+
    ///  :#############%..%#############,
    ///   *############################+
    ///    *##########################+
    ///     ;S######################%:
    ///      ,+%##################%;
    ///         :+?S##########S?+:
    ///             ,:;++++;:,
    /// ```
    Finnix,
    /// Floflis Logo
    /// ```text
    ///               ,▄▄▄▌▓▓███▓▓▌▄▄▄,
    ///          ,▄▒▓███████████████████▓▄▄
    ///        ▄▓███████████████████████████▌
    ///       ▓███████████████████████████████
    ///    ,  ╙▓████████████████████████████▀   ▄
    ///   ╓█▓▄   ╙▀▓████████████████████▀▀`  ,▄██▓
    ///  ╓█████▌▄,    '▀▀▀▀▓▓▓▓▓▓▀▀Å╙`    ▄▄▓█████▌
    ///  ██████████▓▌▄  ,             ▄▓███████████▄
    /// ╢████████████▓  ║████▓▓███▌  ╣█████████████▓
    /// ▓█████████████  ▐█████████▀  ▓██████████████
    /// ▓█████████████  ▐█████████▄  ███████████████
    /// ▀████████████▌  ║█████████▌  ▀█████████████▌
    ///  ████████████M  ▓██████████  ▐█████████████⌐
    ///  ▀██████████▌  ▐███████████▌  ▀███████████▌
    ///    ╙▓█████▓   ▓██████████████▄  ▀███████▀
    ///      ╝▓██▀  ╓▓████████████████▓   ▀▓██▀
    ///           ,▄████████████████████▌,
    ///           ╝▀████████████████████▓▀'
    ///              `╙▀▀▓▓███████▓▀▀╩'
    /// ```
    Floflis,
    /// FreeBSD Logo
    /// ```text
    /// ```                        `
    ///   ` `.....---.......--.```   -/
    ///   +o   .--`         /y:`      +.
    ///    yo`:.            :o      `+-
    ///     y/               -/`   -o/
    ///    .-                  ::/sy+:.
    ///    /                     `--  /
    ///   `:                          :`
    ///   `:                          :`
    ///    /                          /
    ///    .-                        -.
    ///     --                      -.
    ///      `:`                  `:`
    ///        .--             `--.
    ///           .---.....----.
    /// ```
    Freebsd,
    /// Small FreeBSD Logo
    /// ```text
    /// ```                        `
    /// /\,-'''''-,/\
    /// \_)       (_/
    /// |           |
    /// |           |
    ///  ;         ;
    ///   '-_____-'
    /// ```
    FreebsdSmall,
    /// FreeMINT Logo
    /// ```text
    ///           ##
    ///           ##         #########
    ///                     ####      ##
    ///             ####  ####        ##
    /// ####        ####  ##        ##
    ///         ####    ####      ##  ##
    ///         ####  ####  ##  ##  ##
    ///             ####  ######
    ///         ######  ##  ##  ####
    ///       ####    ################
    ///     ####        ##  ####
    ///     ##            ####  ######
    ///     ##      ##    ####  ####
    ///     ##    ##  ##    ##  ##  ####
    ///       ####  ##          ##  ##
    /// ```
    FreeMint,
    /// Frugalware Logo
    /// ```text
    ///           `++/::-.`
    ///          /o+++++++++/::-.`
    ///         `o+++++++++++++++o++/::-.`
    ///         /+++++++++++++++++++++++oo++/:-.``
    ///        .o+ooooooooooooooooooosssssssso++oo++/:-`
    ///        ++osoooooooooooosssssssssssssyyo+++++++o:
    ///       -o+ssoooooooooooosssssssssssssyyo+++++++s`
    ///       o++ssoooooo++++++++++++++sssyyyyo++++++o:
    ///      :o++ssoooooo/-------------+syyyyyo+++++oo
    ///     `o+++ssoooooo/-----+++++ooosyyyyyyo++++os:
    ///     /o+++ssoooooo/-----ooooooosyyyyyyyo+oooss
    ///    .o++++ssooooos/------------syyyyyyhsosssy-
    ///    ++++++ssooooss/-----+++++ooyyhhhhhdssssso
    ///   -s+++++syssssss/-----yyhhhhhhhhhhhddssssy.
    ///   sooooooyhyyyyyh/-----hhhhhhhhhhhddddyssy+
    ///  :yooooooyhyyyhhhyyyyyyhhhhhhhhhhdddddyssy`
    ///  yoooooooyhyyhhhhhhhhhhhhhhhhhhhddddddysy/
    /// -ysooooooydhhhhhhhhhhhddddddddddddddddssy
    ///  .-:/+osssyyyysyyyyyyyyyyyyyyyyyyyyyyssy:
    ///        ``.-/+oosysssssssssssssssssssssss
    ///                ``.:/+osyysssssssssssssh.
    ///                         `-:/+osyyssssyo
    ///                                 .-:+++`
    /// ```
    Frugalware,
    /// Funtoo Logo
    /// ```text
    ///    .dKXXd                         .
    ///   :XXl;:.                      .OXo
    /// .'OXO''  .''''''''''''''''''''':XNd..'oco.lco,
    /// xXXXXXX, cXXXNNNXXXXNNXXXXXXXXNNNNKOOK; d0O .k
    ///   kXX  xXo  KNNN0  KNN.       'xXNo   :c; 'cc.
    ///   kXX  xNo  KNNN0  KNN. :xxxx. 'NNo
    ///   kXX  xNo  loooc  KNN. oNNNN. 'NNo
    ///   kXX  xN0:.       KNN' oNNNX' ,XNk
    ///   kXX  xNNXNNNNNNNNXNNNNNNNNXNNOxXNX0Xl
    ///   ...  ......................... .;cc;.
    /// ```
    Funtoo,
    /// GalliumOS Logo
    /// ```text
    /// sooooooooooooooooooooooooooooooooooooo+:
    /// yyooooooooooooooooooooooooooooooooo+/:::
    /// yyysoooooooooooooooooooooooooooo+/::::::
    /// yyyyyoooooooooooooooooooooooo+/:::::::::
    /// yyyyyysoooooooooooooooooo++/::::::::::::
    /// yyyyyyysoooooooooooooo++/:::::::::::::::
    /// yyyyyyyyysoooooosydddys+/:::::::::::::::
    /// yyyyyyyyyysooosmMMMMMMMNd+::::::::::::::
    /// yyyyyyyyyyyyosMMMMMMMMMMMN/:::::::::::::
    /// yyyyyyyyyyyyydMMMMMMMMMMMMo//:::::::::::
    /// yyyyyyyyyyyyyhMMMMMMMMMMMm--//::::::::::
    /// yyyyyyyyyyyyyyhmMMMMMMMNy:..-://::::::::
    /// yyyyyyyyyyyyyyyyyhhyys+:......://:::::::
    /// yyyyyyyyyyyyyyys+:--...........-///:::::
    /// yyyyyyyyyyyys+:--................://::::
    /// yyyyyyyyyo+:-.....................-//:::
    /// yyyyyyo+:-..........................://:
    /// yyyo+:-..............................-//
    /// o/:-...................................:
    /// ```
    GalliumOs,
    /// Garuda Logo
    /// ```text
    ///                    .%;888:8898898:
    ///                  x;XxXB%89b8:b8%b88:
    ///               .8Xxd                8X:.
    ///             .8Xx;                    8x:.
    ///           .tt8x          .d            x88;
    ///        .@8x8;          .db:              xx@;
    ///      ,tSXX°          .bbbbbbbbbbbbbbbbbbbB8x@;
    ///    .SXxx            bBBBBBBBBBBBBBBBBBBBbSBX8;
    ///  ,888S                                     pd!
    /// 8X88/                                       q
    /// 8X88/
    /// GBB.
    ///  x%88        d888@8@X@X@X88X@@XX@@X@8@X.
    ///    dxXd    dB8b8b8B8B08bB88b998888b88x.
    ///     dxx8o                      .@@;.
    ///       dx88                   .t@x.
    ///         d:SS@8ba89aa67a853Sxxad.
    ///           .d988999889889899dd.
    /// ```
    Garuda,
    /// Garuda Dragon Logo
    /// ```text
    ///                 .:--=========--:..
    ///            .:-=+++++===-----=======-:.
    ///          :=++++-:..            ..:-===-:.
    ///        .+++=:.                      .-=---.
    ///      . :-:                            :---:
    ///       :=           .                     :---:
    ///        .=-:        :-.                    .---:
    ///          :-===--::. .::::.                  ---:
    ///       .::--====-===+=-----=-:                ---:
    ///  ::  :-++++====--=-:=====--=--:              .---
    /// +**=:+++===++++++==- -===== -==-.             ---:
    /// *****+==++==--::::::======== . ===-.          :---
    /// ****==++-::.:::::-.::--======-.-===           .---
    /// ***+=+-::::--:.     -==:-==--=======-::. .    :---
    /// =**++-::::==       -+===-==: :::.:-=-==---    :--:
    /// .*+*-:::-+=        ...::====  .::  .=-:--.     ..
    ///  -*++:::=+-:             .:=-   -- .:  .  ...
    ///   =*++:.++-+:   .:.         =-.: :-:    ::   :.
    ///    -*++-=+=-+=-=++===-.     .====: .::::.
    ///     :++++++-:::--.-:===       --.
    ///      .=+++++- .=  +.=:=                .::
    ///        :=+++++:. .: : .              :----
    ///          .-++++=-:..            .:--===-.
    ///             .-=+++++===-----=======-:.
    /// ```
    GarudaDragon,
    /// Small Garuda Logo
    /// ```text
    ///      .----.
    ///    .'   ,  '.
    ///  .'    '-----|
    /// '.   -----,
    ///   '.____.'
    /// ```
    GarudaSmall,
    /// Gentoo Logo
    /// ```text
    ///          -/oyddmdhs+:.
    ///      -odNMMMMMMMMNNmhy+-`
    ///    -yNMMMMMMMMMMMNNNmmdhy+-
    ///  `omMMMMMMMMMMMMNmdmmmmddhhy/`
    ///  omMMMMMMMMMMMNhhyyyohmdddhhhdo`
    /// .ydMMMMMMMMMMdhs++so/smdddhhhhdm+`
    ///  oyhdmNMMMMMMMNdyooydmddddhhhhyhNd.
    ///   :oyhhdNNMMMMMMMNNNmmdddhhhhhyymMh
    ///     .:+sydNMMMMMNNNmmmdddhhhhhhmMmy
    ///        /mMMMMMMNNNmmmdddhhhhhmMNhs:
    ///     `oNMMMMMMMNNNmmmddddhhdmMNhs+`
    ///   `sNMMMMMMMMNNNmmmdddddmNMmhs/.
    ///  /NMMMMMMMMNNNNmmmdddmNMNdso:`
    /// +MMMMMMMNNNNNmmmmdmNMNdso/-
    /// yMMNNNNNNNmmmmmNNMmhs+/-`
    /// /hMMNNNNNNNNMNdhs++/-`
    /// `/ohdmmddhys+++/:.`
    ///   `-//////:--.
    /// ```
    Gentoo,
    /// Small Gentoo Logo
    /// ```text
    ///  _-----_
    /// (       \
    /// \    0   \
    ///  \        )
    ///  /      _/
    /// (     _-
    /// \____-
    /// ```
    GentooSmall,
    /// GhostBSD Logo
    /// ```text
    ///            ,gggggg.
    ///         ,agg9*   .g)
    ///       .agg* ._.,gg*
    ///     ,gga*  (ggg*'
    ///    ,ga*       ,ga*
    ///   ,ga'     .ag*
    ///  ,ga'   .agga'
    ///  9g' .agg'g*,a
    ///  'gggg*',gga'
    ///       .gg*'
    ///     .gga*
    ///   .gga*
    ///  (ga*
    /// ```
    GhostBsd,
    /// Glaucus Logo
    /// ```text
    ///              ,,        ,d88P
    ///            ,d8P    ,ad8888*
    ///          ,888P    d88888*     ,,ad8888P*
    ///     d   d888P   a88888P*  ,ad8888888*
    ///   .d8  d8888:  d888888* ,d888888P*
    ///  .888; 88888b d8888888b8888888P
    ///  d8888J888888a88888888888888P*    ,d
    ///  88888888888888888888888888P   ,,d8*
    ///  888888888888888888888888888888888*
    ///  *8888888888888888888888888888888*
    ///   Y888888888P* `*``*888888888888*
    ///    *^888^*            *Y888P**
    /// ```
    Glaucus,
    /// gNewSense Logo
    /// ```text
    ///                      ..,,,,..
    ///                .oocchhhhhhhhhhccoo.
    ///         .ochhlllllllc hhhhhh ollllllhhco.
    ///     ochlllllllllll hhhllllllhhh lllllllllllhco
    ///  .cllllllllllllll hlllllo  +hllh llllllllllllllc.
    /// ollllllllllhco''  hlllllo  +hllh  ``ochllllllllllo
    /// hllllllllc'       hllllllllllllh       `cllllllllh
    /// ollllllh          +llllllllllll+          hllllllo
    ///  `cllllh.           ohllllllho           .hllllc'
    ///     ochllc.            ++++            .cllhco
    ///        `+occooo+.                .+ooocco+'
    ///               `+oo++++      ++++oo+'
    /// ```
    GNewSense,
    /// Gnome Logo
    /// ```text
    ///                                ,@@@@@@@@,
    ///                  @@@@@@      @@@@@@@@@@@@
    ///         ,@@.    @@@@@@@    *@@@@@@@@@@@@
    ///        @@@@@%   @@@@@@(    @@@@@@@@@@@&
    ///        @@@@@@    @@@@*     @@@@@@@@@#
    /// @@@@*   @@@@,              *@@@@@%
    /// @@@@@.
    ///  @@@@#         @@@@@@@@@@@@@@@@
    ///          ,@@@@@@@@@@@@@@@@@@@@@@@,
    ///       ,@@@@@@@@@@@@@@@@@@@@@@@@@@&
    ///     .@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///     @@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///    @@@@@@@@@@@@@@@@@@@@@@@@(
    ///    @@@@@@@@@@@@@@@@@@@@%
    ///     @@@@@@@@@@@@@@@@
    ///      @@@@@@@@@@@@*        @@@@@@@@/
    ///       &@@@@@@@@@@        @@@@@@@@@*
    ///         @@@@@@@@@@@,    @@@@@@@@@*
    ///           ,@@@@@@@@@@@@@@@@@@@@&
    ///               &@@@@@@@@@@@@@@
    ///                      ...
    /// ```
    Gnome,
    /// GNU Logo
    /// ```text
    ///     _-`````-,           ,- '- .
    ///   .'   .- - |          | - -.  `.
    ///  /.'  /                     `.   \
    /// :/   :      _...   ..._      ``   :
    /// ::   :     /._ .`:'_.._\.    ||   :
    /// ::    `._ ./  ,`  :    \ . _.''   .
    /// `:.      /   |  -.  \-. \\_      /
    ///   \:._ _/  .'   .@)  \@) ` `\ ,.'
    ///      _/,--'       .- .\,-.`--`.
    ///        ,'/''     (( \ `  )
    ///         /'/'  \    `-'  (
    ///          '/''  `._,-----'
    ///           ''/'    .,---'
    ///            ''/'      ;:
    ///              ''/''  ''/
    ///                ''/''/''
    ///                  '/'/'
    ///                   `;
    /// ```
    Gnu,
    /// Gobo Logo
    /// ```text
    ///   _____       _
    ///  / ____|     | |
    /// | |  __  ___ | |__   ___
    /// | | |_ |/ _ \| '_ \ / _ \
    /// | |__| | (_) | |_) | (_) |
    ///  \_____|\___/|_.__/ \___/
    /// ```
    GoboLinux,
    /// GrapheneOS Logo
    /// ```text
    ///                         B?
    ///                         G~
    ///                         G~&
    ///                       G!^:^?#
    ///                      &^.:::.J
    ///     &PG&          #G5JJ7~^~?JY5B&          #PG
    ///      B5JJPGJ77YG5JYP#   &&   &B5JYPGJ7?YG5JYP#
    ///         &Y..::.:P&               &?..::.:G
    ///          #!::::?                  B~::::J
    ///            B~J#                     B!?#
    ///             !P                       75
    ///             !P                       75
    ///             !5                       7Y
    ///          &Y~:^!P                  &J~:^!P
    ///          P..::.:B                 Y..::.:#
    ///       #PYJJ~^^!JJYP#          &B5YJ?~^^!JJYG#
    ///     &YYG#   &&   #PYJ5G5??JGGYJ5G&   &&   #PYP
    ///                      B^.::..7&
    ///                       J::::^G
    ///                        #Y^G&
    ///                         B~
    ///                         G!
    ///                         #
    /// ```
    GrapheneOs,
    /// Grombyang Logo
    /// ```text
    ///             eeeeeeeeeeee
    ///          eeeeeeeeeeeeeeeee
    ///       eeeeeeeeeeeeeeeeeeeeeee
    ///     eeeee       .o+       eeee
    ///   eeee         `ooo/         eeee
    ///  eeee         `+oooo:         eeee
    /// eee          `+oooooo:          eee
    /// eee          -+oooooo+:         eee
    /// ee         `/:oooooooo+:         ee
    /// ee        `/+   +++    +:        ee
    /// ee              +o+\             ee
    /// eee             +o+\            eee
    /// eee        //  \\ooo/  \\\      eee
    ///  eee      //++++oooo++++\\\    eee
    ///   eeee    ::::++oooo+:::::   eeee
    ///     eeeee   Grombyang OS   eeee
    ///       eeeeeeeeeeeeeeeeeeeeeee
    ///          eeeeeeeeeeeeeeeee
    /// ```
    Grombyang,
    /// Guix Logo
    /// ```text
    ///  ..                             `.
    ///  `--..```..`           `..```..--`
    ///    .-:///-:::.       `-:::///:-.
    ///       ````.:::`     `:::.````
    ///            -//:`    -::-
    ///             ://:   -::-
    ///             `///- .:::`
    ///              -+++-:::.
    ///               :+/:::-
    ///               `-....`
    /// ```
    Guix,
    /// Small Guix Logo
    /// ```text
    /// |.__          __.|
    /// |__ \\        / __|
    ///    \\ \\      / /
    ///     \\ \\    / /
    ///      \\ \\  / /
    ///       \\ \\/ /
    ///        \\__/
    /// ```
    GuixSmall,
    /// Haiku Logo
    /// ```text
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            MMMM       .ciO| /YMMMMM*"
    ///            MMMM   .cOMMMMM|/MMMMM/`
    ///  ,         ,iMM|/MMMMMMMMMMMMMMM*
    ///   `*.__,-cMMMMMMMMMMMMMMMMM/`.MMM
    ///            MMMMMMMMM/`:MMM/  MMMM
    ///            MMMM              MMMM
    ///            MMMM              MMMM
    ///            """"              """"
    /// ```
    Haiku,
    /// Small Haiku Logo
    /// ```text
    ///        ,^,
    ///       /   \\
    /// *--_ ;     ; _--*
    /// \\   '"     "'   /
    ///  '.           .'
    /// .-'"         "'-.
    ///  '-.__.   .__.-'
    ///        |_|
    /// ```
    HaikuSmall,
    /// HamoniKR Logo
    /// ```text
    ///                      cO0Ox.
    ///                   .ldddddddo.
    ///                 .lddddddddddo
    ///               'lddddddddddddc
    ///             ,oddddddddddddd;
    ///           'ldddddddddddddo.
    ///         .oddddddddddddddc.
    ///       ,dddddddddddddddo.
    ///     ,ccoooooooocoddooo:
    ///   ,cooooooooooooooooop                  c000x.
    /// .cooooooooooooooopcllll              .cddddddo.
    /// coooooooooooooop' .qlll.           .ddoooooooo;
    /// cooooooooooc;        'qlllp.     .ddoooooooooo;
    /// .cooooooc;             'lllbc...coooooooooooo;
    ///   .cooc'                .llllcoooooooooooooo.
    ///                          .coooooooooooooop:
    ///                        .coooooooooooooop'
    ///                       .cooooooooooooop.
    ///                     .cooooooooooooop.
    ///                    .coooooooooooop.
    ///                   .cooooooooooop.
    ///                   .cooooooooop.
    ///                    .cooooop'
    /// ```
    HamoniKr,
    /// HarDClanZ Logo
    /// ```text
    ///           ........::::....
    ///         ::################::..
    ///       :########################:.
    ///      :######**###################:
    ///     :###&&&&^############ &#######:
    ///    :#&&&&&.:##############:^&o`:###:
    ///   :#&&&&.:#################:.&&&`###:
    ///  :##&^:######################:^&&::##:
    ///  :#############################:&:##::
    ///  :##########@@###########@@#####:.###:
    /// :#########@@o@@#########@@o@@########:
    /// :#######:@@o0o@@@@###@@@@o0o@@######: :
    ///  :######:@@@o@@@@@@V@@@@@@o@@@######:
    ///    :#####:@@@@@@@@@@@@@@@@@@@:####;
    ///     :####:.@@@@@@@@@@@@@@@@:#####:
    ///     `:####:.@@@@@@@@@@@@@@:#####:
    ///       ``:##:.@@@@@@@@@@@@^## # :
    ///          : ##  \@@@;@@@/ :: # :
    ///                   VVV
    /// ```
    HarDcLanZ,
    /// HardendBSD Logo
    /// ```text
    /// ```                        `
    ///   ` `.....---.......--.```   -/
    ///   +o   .--`         /y:`      +.
    ///    yo`:.            :o      `+-
    ///     y/               -/`   -o/
    ///    .-                  ::/sy+:.
    ///    /                     `--  /
    ///   `:                          :`
    ///   `:                          :`
    ///    /                          /
    ///    .-                        -.
    ///     --                      -.
    ///      `:`                  `:`
    ///        .--             `--.
    ///           .---.....----.
    /// ```
    HardenedBsd,
    /// Hash Logo
    /// ```text
    ///       +   ######   +
    ///     ###   ######   ###
    ///   #####   ######   #####
    ///  ######   ######   ######
    ///
    /// ####### '"###### '"########
    /// #######   ######   ########
    /// #######   ######   ########
    ///
    ///  ###### '"###### '"######
    ///   #####   ######   #####
    ///     ###   ######   ###
    ///       ~   ######   ~
    /// ```
    Hash,
    /// Huayra Logo
    /// ```text
    ///                      `
    ///             .       .       `
    ///        ``    -      .      .
    ///         `.`   -` `. -  `` .`
    ///           ..`-`-` + -  / .`     ```
    ///           .--.+--`+:- :/.` .-``.`
    ///             -+/so::h:.d-`./:`.`
    ///               :hNhyMomy:os-...-.  ````
    ///                .dhsshNmNhoo+:-``.```
    ///                 `ohy:-NMds+::-.``
    ///             ````.hNN+`mMNho/:-....````
    ///        `````     `../dmNhoo+/:..``
    ///     ````            .dh++o/:....`
    /// .+s/`                `/s-.-.:.`` ````
    /// ::`                    `::`..`
    ///                           .` `..
    ///                                 ``
    /// ```
    Huayra,
    /// Hybrid Logo
    /// ```text
    ///    /                          #
    /// ////&                      #####
    /// /////                      ######
    /// /////      //////////      ######
    /// ///// //////////////////// ######
    /// ////////////////////////// ######
    /// /////////              /// ######
    /// ///////                  / ######
    /// //////                     ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      ######
    /// /////                      #########
    /// ////&                       ########
    /// ```
    Hybrid,
    /// HydroOS Logo
    /// ```text
    ///  _    _           _            ____   _____
    /// | |  | |         | |          / __ \ / ____|
    /// | |__| |_   _  __| |_ __ ___ | |  | | (___
    /// |  __  | | | |/ _` | '__/ _ \| |  | |\___ \
    /// | |  | | |_| | (_| | | | (_) | |__| |____) |
    /// |_|  |_|\__, |\__,_|_|  \___/ \____/|_____/
    ///          __/ |
    ///         |___/
    /// ```
    HydroOs,
    /// Hyperbola Logo
    /// ```text
    ///                      WW
    ///                      KX              W
    ///                     WO0W          NX0O
    ///                     NOO0NW  WNXK0OOKW
    ///                     W0OOOOOOOOOOOOKN
    ///                      N0OOOOOOO0KXW
    ///                        WNXXXNW
    ///                  NXK00000KN
    ///              WNK0OOOOOOOOOO0W
    ///            NK0OOOOOOOOOOOOOO0W
    ///          X0OOOOOOO00KK00OOOOOK
    ///        X0OOOO0KNWW      WX0OO0W
    ///      X0OO0XNW              KOOW
    ///    N00KNW                   KOW
    ///  NKXN                       W0W
    /// WW                           W
    /// ```
    Hyperbola,
    /// Small Hyperbola Logo
    /// ```text
    ///     |`__.`/
    ///     \____/
    ///     .--.
    ///    /    \\
    ///   /  ___ \\
    ///  / .`   `.\\
    /// /.`      `.\\
    /// ```
    HyperbolaSmall,
    /// Iglunix Logo
    /// ```text
    ///      |
    ///      |        |
    ///               |
    /// |    ________
    /// |  /\   |    \
    ///   /  \  |     \  |
    ///  /    \        \ |
    /// /      \________\
    /// \      /        /
    ///  \    /        /
    ///   \  /        /
    ///    \/________/
    /// ```
    Iglunix,
    /// InstantOS Logo
    /// ```text
    ///      'cx0XWWMMWNKOd:'.
    ///   .;kNMMMMMMMMMMMMMWNKd'
    ///  'kNMMMMMMWNNNWMMMMMMMMXo.
    /// ,0MMMMMW0o;'..,:dKWMMMMMWx.
    /// OMMMMMXl.        .xNMMMMMNo
    /// WMMMMNl           .kWWMMMMO'
    /// MMMMMX;            oNWMMMMK,
    /// NMMMMWo           .OWMMMMMK,
    /// kWMMMMNd.        ,kWMMMMMMK,
    /// 'kWMMMMWXxl:;;:okNMMMMMMMMK,
    ///  .oXMMMMMMMWWWMMMMMMMMMMMMK,
    ///    'oKWMMMMMMMMMMMMMMMMMMMK,
    ///      .;lxOKXXXXXXXXXXXXXXXO;......
    ///           ................,d0000000kd:.
    ///                           .kMMMMMMMMMW0;
    ///                           .kMMMMMMMMMMMX
    ///                           .xMMMMMMMMMMMW
    ///                            cXMMMMMMMMMM0
    ///                             :0WMMMMMMNx,
    ///                              .o0NMWNOc.
    /// ```
    InstantOs,
    /// Interix Logo
    /// ```text
    ///                    ..
    ///                   75G!
    ///                 ^?PG&&J.
    ///               :!5GPP&&&B!
    ///              :YPPPPP&&&&&Y:
    ///             !5PPPPPP&&&&&&B!
    ///           :?PPPPPPPP&&&&&&&&Y~
    ///          !5PPPPPPPPP###&&&&&&B7
    ///        :?PPPP5555555B####&&&&&&5:
    ///       ~5PPPP555YJ7!~7?5B###&&&&&B?.
    ///    .:JPPPP5555Y?^....:^?G####&&&&&5:
    ///    75PPP555555Y7:....:^!5#####&&&&&B7.
    ///  :JPPPP555555YY?~::::^~7YPGBB###&&&&&5^
    /// 75GGPPPPPP555555YJ?77??YYYYYY55PPGGB#&B?
    /// ~!!7JY5PGGBBBBBBBBGGGGGGGBGGGGGP5YJ?7~~~
    ///        .::^~7?JYPGBB#BGPYJ?7!7^:.
    ///                  ..:^...
    /// ```
    Interix,
    /// Irix Logo
    /// ```text
    ///            ./ohmNd/  +dNmho/-
    ///      `:+ydNMMMMMMMM.-MMMMMMMMMdyo:.
    ///    `hMMMMMMNhs/sMMM-:MMM+/shNMMMMMMh`
    ///    -NMMMMMmo-` /MMM-/MMM- `-omMMMMMN.
    ///  `.`-+hNMMMMMNhyMMM-/MMMshmMMMMMmy+...`
    /// +mMNds:-:sdNMMMMMMMyyMMMMMMMNdo:.:sdMMm+
    /// dMMMMMMmy+.-/ymNMMMMMMMMNmy/-.+hmMMMMMMd
    /// oMMMMmMMMMNds:.+MMMmmMMN/.-odNMMMMmMMMM+
    /// .MMMM-/ymMMMMMmNMMy..hMMNmMMMMMmy/-MMMM.
    ///  hMMM/ `/dMMMMMMMN////NMMMMMMMd/. /MMMh
    ///  /MMMdhmMMMmyyMMMMMMMMMMMMhymMMMmhdMMM:
    ///  `mMMMMNho//sdMMMMM//NMMMMms//ohNMMMMd
    ///   `/so/:+ymMMMNMMMM` mMMMMMMMmh+::+o/`
    ///      `yNMMNho-yMMMM` NMMMm.+hNMMNh`
    ///      -MMMMd:  oMMMM. NMMMh  :hMMMM-
    ///       -yNMMMmooMMMM- NMMMyomMMMNy-
    ///         .omMMMMMMMM-`NMMMMMMMmo.
    ///           `:hMMMMMM. NMMMMMh/`
    ///              .odNm+  /dNms.
    /// ```
    Irix,
    /// Ironclad Logo
    /// ```text
    ///                  &#BGPPPPPG#&
    ///               B5?77!!?YJJ7!7YBB&
    ///            &G5YJ77!7JYYYYYBPJ&PY#
    ///          #PYYYYYY?!?YYYYY7?7JP5JJ
    ///         B?YYYYYY7!!7JYYYYJ!!?JJJ5
    ///  &&    B7?J?77?7!!!!!77777!7Y5YYBBPGGG&
    /// G77?YBB!!!!!!!!!!!!!JYJ??7JYJJY# PYPPG&
    /// J777JB?!7JJ???!!!7?JYYYYYPJ!7JB
    /// GYYG #JJJJJ??7!!!JYYY5PGB&GB&
    ///    #Y!?GB5YYJY5PG###&
    ///    GJJP
    /// ```
    Ironclad,
    /// Itc Logo
    /// ```text
    /// ....................-==============+...
    /// ....................-==============:...
    /// ...:===========-....-==============:...
    /// ...-===========:....-==============-...
    /// ....*==========+........-::********-...
    /// ....*===========+.:*====**==*+-.-......
    /// ....:============*+-..--:+**====*---...
    /// ......::--........................::...
    /// ..+-:+-.+::*:+::+:-++::++-.:-.*.:++:++.
    /// ..:-:-++++:-::--:+::-::.:++-++:++--:-:.
    /// ```
    Itc,
    /// Janus Linux Logo
    /// ```text
    ///                'l:
    ///         loooooo
    ///           loooo coooool
    ///  looooooooooooooooooool
    ///   looooooooooooooooo
    ///          lool   cooo
    ///         coooooooloooooooo
    ///      clooooo  ;lood  cloooo
    ///   :loooocooo cloo      loooo
    ///  loooo  :ooooool       loooo
    /// looo    cooooo        cooooo
    /// looooooooooooo      ;loooooo looooooc
    /// looooooooo loo   cloooooool    looooc
    ///  cooo       cooooooooooo       looolooooool
    ///             cooo:     coooooooooooooooooool
    ///                        loooooooooooolc:   loooc;
    ///                              cooo:    loooooooooooc
    ///                             ;oool         looooooo:
    ///                            coool          olc,
    ///                           looooc   ,,
    ///                         coooooc    loc
    ///                        :oooool,    coool:, looool:,
    ///                        looool:      ooooooooooooooo:
    ///                        cooolc        .ooooooooooool
    /// ```
    JanusLinux,
    /// Kaisen Logo
    /// ```text
    ///                   `:+oyyho.
    ///              `+:`sdddddd/
    ///         `+` :ho oyo++ohds-`
    ///        .ho :dd.  .: `sddddddhhyso+/-
    ///        ody.ddd-:yd- +hysssyhddddddddho`
    ///        yddddddhddd` ` `--`   -+hddddddh.
    ///        hddy-+dddddy+ohh/..+sddddy/:::+ys
    ///       :ddd/sdddddddddd- oddddddd       `
    ///      `yddddddddddddddd/ /ddddddd/
    /// :.  :ydddddddddddddddddo..sddddddy/`
    /// odhdddddddo- `ddddh+-``....-+hdddddds.
    /// -ddddddhd:   /dddo  -ydddddddhdddddddd-
    ///  /hdy:o - `:sddds   .`./hdddddddddddddo
    ///   `/-  `+hddyosy+       :dddddddy-.-od/
    ///       :sydds           -hddddddd`    /
    ///        .+shd-      `:ohddddddddd`
    ///                 `:+ooooooooooooo:
    /// ```
    Kaisen,
    /// Kali Logo
    /// ```text
    /// ..............
    ///             ..,;:ccc,.
    ///           ......''';lxO.
    /// .....''''..........,:ld;
    ///            .';;;:::;,,.x,
    ///       ..'''.            0Xxoc:,.  ...
    ///   ....                ,ONkc;,;cokOdc',.
    ///  .                   OMo           ':ddo.
    ///                     dMc               :OO;
    ///                     0M.                 .:o.
    ///                     ;Wd
    ///                      ;XO,
    ///                        ,d0Odlc;,..
    ///                            ..',;:cdOOd::,.
    ///                                     .:d;.':;.
    ///                                        'd,  .'
    ///                                          ;l   ..
    ///                                           .o
    ///                                             c
    ///                                             .'
    ///                                              .
    /// ```
    Kali,
    /// Small Kali Logo
    /// ```text
    ///      -#. #
    ///       @###
    ///   -######
    ///  @#########
    /// =##.  .#####
    /// ##      ## ##
    /// ##       ## #
    /// ##       @###
    /// ##.        ###
    ///  ##%       ##-
    ///   -##%    -*
    ///    :*##+
    ///      :*#*
    ///        -#
    ///         @
    ///         :
    /// ```
    KaliSmall,
    /// KaOS Logo
    /// ```text
    ///                      ..
    ///   .....         ..OSSAAAAAAA..
    ///  .KKKKSS.     .SSAAAAAAAAAAA.
    /// .KKKKKSO.    .SAAAAAAAAAA...
    /// KKKKKKS.   .OAAAAAAAA.
    /// KKKKKKS.  .OAAAAAA.
    /// KKKKKKS. .SSAA..
    /// .KKKKKS..OAAAAAAAAAAAA........
    ///  DKKKKO.=AA=========A===AASSSO..
    ///   AKKKS.==========AASSSSAAAAAASS.
    ///   .=KKO..========ASS.....SSSSASSSS.
    ///     .KK.       .ASS..O.. =SSSSAOSS:
    ///      .OK.      .ASSSSSSSO...=A.SSA.
    ///        .K      ..SSSASSSS.. ..SSA.
    ///                  .SSS.AAKAKSSKA.
    ///                     .SSS....S..
    /// ```
    KaOs,
    /// KernelOS Logo
    /// ```text
    ///               .''''....''''.
    ///          .''...            ...''.
    ///        ''..                    ..''
    ///      '..                         ..''
    ///    .'.      .,,'..       .',,,'    ..'.
    ///   .'.       .,,,,'    .,,,,,'        .'.
    ///  .'.        .,,,,'  .,,,,,.           .'.
    ///  '..        .,,,,'',,,,;.             .''
    /// .'.         .,,,,,,,;;.               ..'
    /// .'.         .,,,,;;;;;,               ..'.
    ///  '..        .,;;;,';;;;;,             ..'
    ///  .'.        .;;;;'  .;;::::           .,.
    ///   '..       .;;;;'    .::::::.       .,'
    ///    '..      .;;;;'      .::::::.    .,'
    ///     .'..                          .',.
    ///       ',..                      .','
    ///          .,,...              ..',,.
    ///            ..,,''........',,,.
    ///                   ......
    /// ```
    KernelOs,
    /// KDE-Neon Logo
    /// ```text
    ///              `..---+/---..`
    ///          `---.``   ``   `.---.`
    ///       .--.`        ``        `-:-.
    ///     `:/:     `.----//----.`     :/-
    ///    .:.    `---`          `--.`    .:`
    ///   .:`   `--`                .:-    `:.
    ///  `/    `:.      `.-::-.`      -:`   `/`
    ///  /.    /.     `:++++++++:`     .:    .:
    /// `/    .:     `+++++++++++/      /`   `+`
    /// /+`   --     .++++++++++++`     :.   .+:
    /// `/    .:     `+++++++++++/      /`   `+`
    ///  /`    /.     `:++++++++:`     .:    .:
    ///  ./    `:.      `.:::-.`      -:`   `/`
    ///   .:`   `--`                .:-    `:.
    ///    .:.    `---`          `--.`    .:`
    ///     `:/:     `.----//----.`     :/-
    ///       .-:.`        ``        `-:-.
    ///          `---.``   ``   `.---.`
    ///              `..---+/---..`
    /// ```
    KdeNeon,
    /// Kibojoe Logo
    /// ```text
    ///            ./+oooooo+/.
    ///            -/+ooooo+/:.`
    ///           `yyyo+++/++osss.
    ///          +NMNyssssssssssss.
    ///        .dMMMMNsssssssssssyNs`
    ///       +MMMMMMMmssssssssssshMNo`
    ///     `hMMMMMNNNMdsssssssssssdMMN/
    ///    .syyyssssssyNNmmmmdssssshMMMMd:
    ///   -NMmhyssssssssyhhhhyssyhmMMMMMMMy`
    ///  -NMMMMMNNmdhyyyyyyyhdmNMMMMMMMMMMMN+
    /// `NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.
    /// ods+/:-----://+oyydmNMMMMMMMMMMMMMMMMMN-
    /// `                     .-:+osyhhdmmNNNmdo
    /// ```
    Kibojoe,
    /// Kiss Linux Logo
    /// ```text
    ///     ___     
    ///    (.· |     
    ///    (<> |     
    ///   / __  \    
    ///  ( /  \ /|   
    /// _/\ __)/_)   
    /// \/-____\/   
    /// ```
    KissLinux,
    /// Kogaion Logo
    /// ```text
    ///             ;;      ,;
    ///            ;;;     ,;;
    ///          ,;;;;     ;;;;
    ///       ,;;;;;;;;    ;;;;
    ///      ;;;;;;;;;;;   ;;;;;
    ///     ,;;;;;;;;;;;;  ';;;;;,
    ///     ;;;;;;;;;;;;;;, ';;;;;;;
    ///     ;;;;;;;;;;;;;;;;;, ';;;;;
    /// ;    ';;;;;;;;;;;;;;;;;;, ;;;
    /// ;;;,  ';;;;;;;;;;;;;;;;;;;,;;
    /// ;;;;;,  ';;;;;;;;;;;;;;;;;;,
    /// ;;;;;;;;,  ';;;;;;;;;;;;;;;;,
    /// ;;;;;;;;;;;;, ';;;;;;;;;;;;;;
    /// ';;;;;;;;;;;;; ';;;;;;;;;;;;;
    ///  ';;;;;;;;;;;;;, ';;;;;;;;;;;
    ///   ';;;;;;;;;;;;;  ;;;;;;;;;;
    ///     ';;;;;;;;;;;; ;;;;;;;;
    ///         ';;;;;;;; ;;;;;;
    ///            ';;;;; ;;;;
    ///              ';;; ;;
    /// ```
    Kogaion,
    /// Korora Logo
    /// ~~~text
    ///                 ____________
    ///              _add55555555554:
    ///            _w?'``````````')k:
    ///           _Z'`            ]k:
    ///           m(`             )k:
    ///      _.ss`m[`,            ]e:
    ///    .uY"^``Xc`?Ss.         d(`
    ///   jF'`    `@.  `Sc      .jr`
    ///  jr`       `?n_ `;   _a2"`
    /// .m:          `~M`1k`5?!``
    /// :#:             `)e```
    /// :m:             ,#'`
    /// :#:           .s2'`
    /// :m,________.aa7^`
    /// :#baaaaaaas!J'`
    ///  ```````````
    /// ~~~
    Korora,
    /// KrassOS Logo
    /// ```text
    ///                   **@@@@@@@@@@@*
    ///              ,@@@@%(((((((((((((%@@@@,
    ///           #@@&(((((((((((((((((((((((&@@%
    ///         @@&(((((((((((((((((((((((((((((@@@
    ///       @@&(((((((((((((((((((((((((((((((((&@@
    ///     .@@(((((((((((((((((((((((((((((((((((((@@.
    ///     @@(((((((((((((((((((((((((((((((((((((((@@
    ///    @@#(((((((((((((((((((((((((((((%@@@@@@@#(#@@
    ///   .@@((((((((((((((((#%@@@@@@@@@&%#((((%@&((((@@.
    ///   .@@(((((((/(&@@@@@@%(/((((((((((((((@@/(((((@@.
    ///   .@@(///////////////////////////////@(///////@@
    ///    %@#/////////////////////////////(#////////%@%
    ///     @@(///////////////////////////%/////////(@@
    ///      @@#***********************************%@@
    ///       *@@********************************/@@/
    ///         ,@@#***************************%@@*
    ///            @@@&********************/@@@@
    ///                &@@@@&(//***//(&@@@@&
    ///                   **@@@@@@@@@@@*
    /// ```
    KrassOs,
    /// KSLinux Logo
    /// ```text
    /// K   K U   U RRRR   ooo
    /// K  K  U   U R   R o   o
    /// KKK   U   U RRRR  o   o
    /// K  K  U   U R  R  o   o
    /// K   K  UUU  R   R  ooo
    ///
    ///  SSS   AAA  W   W  AAA
    /// S     A   A W   W A   A
    ///  SSS  AAAAA W W W AAAAA
    ///     S A   A WW WW A   A
    ///  SSS  A   A W   W A   A
    /// ```
    KsLinux,
    /// Kubuntu Logo
    /// ```text
    ///            `.:/ossyyyysso/:.
    ///         .:oyyyyyyyyyyyyyyyyyyo:`
    ///       -oyyyyyyyodMMyyyyyyyysyyyyo-
    ///     -syyyyyyyyyydMMyoyyyydmMMyyyyys-
    ///    oyyysdMysyyyydMMMMMMMMMMMMMyyyyyyyo
    ///  `oyyyydMMMMysyysoooooodMMMMyyyyyyyyyo`
    ///  oyyyyyydMMMMyyyyyyyyyyyysdMMysssssyyyo
    /// -yyyyyyyydMysyyyyyyyyyyyyyysdMMMMMysyyy-
    /// oyyyysoodMyyyyyyyyyyyyyyyyyyydMMMMysyyyo
    /// yyysdMMMMMyyyyyyyyyyyyyyyyyyysosyyyyyyyy
    /// yyysdMMMMMyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// oyyyyysosdyyyyyyyyyyyyyyyyyyydMMMMysyyyo
    /// -yyyyyyyydMysyyyyyyyyyyyyyysdMMMMMysyyy-
    ///  oyyyyyydMMMysyyyyyyyyyyysdMMyoyyyoyyyo
    ///  `oyyyydMMMysyyyoooooodMMMMyoyyyyyyyyo
    ///    oyyysyyoyyyysdMMMMMMMMMMMyyyyyyyyo
    ///     -syyyyyyyyydMMMysyyydMMMysyyyys-
    ///       -oyyyyyyydMMyyyyyyysosyyyyo-
    ///         ./oyyyyyyyyyyyyyyyyyyo/.
    ///            `.:/oosyyyysso/:.`
    /// ```
    Kubuntu,
    /// Langitketujuh Logo
    /// ```text
    /// . '7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7
    /// L7.   '7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7
    /// L7L7L      7L7L7L7L7L7L7L7L7L7L7L7L7L7
    /// L7L7L7                          L7L7L7
    /// L7L7L7           'L7L7L7L7L7L7L7L7L7L7
    /// L7L7L7               'L7L7L7L7L7L7L7L7
    /// L7L7L7                   'L7L7L7L7L7L7
    /// L7L7L7                          L7L7L7
    /// L7L7L7L7L7L7L7L7L7L7LL7L7L7.    '7L7L7
    /// L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L.   'L7
    /// L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7L7.  '
    /// ```
    Langitketujuh,
    /// Laxeros Logo
    /// ```text
    ///                     /.
    ///                  `://:-
    ///                 `//////:
    ///                .////////:`
    ///               -//////////:`
    ///              -/////////////`
    ///             :///////////////.
    ///           `://////.```-//////-
    ///          `://///:`     .//////-
    ///         `//////:        `//////:
    ///        .//////-          `://///:`
    ///       -//////-            `://///:`
    ///      -//////.               ://////`
    ///     ://////`                 -//////.
    ///    `/////:`                   ./////:
    ///     .-::-`                     .:::-`
    ///
    /// .:://////////////////////////////////::.
    /// ////////////////////////////////////////
    /// .:////////////////////////////////////:.
    /// ```
    Laxeros,
    /// LEDE Logo
    /// ```text
    ///     _________
    ///    /        /\
    ///   /  LE    /  \
    ///  /    DE  /    \
    /// /________/  LE  \
    /// \        \   DE /
    ///  \    LE  \    /
    ///   \  DE    \  /
    ///    \________\/
    /// ```
    Lede,
    /// LibreELEC Logo
    /// ```text
    ///           :+ooo/.      ./ooo+:
    ///         :+ooooooo/.  ./ooooooo+:
    ///       :+ooooooooooo::ooooooooooo+:
    ///     :+ooooooooooo+-  -+ooooooooooo+:
    ///   :+ooooooooooo+-  --  -+ooooooooooo+:
    /// .+ooooooooooo+-  :+oo+:  -+ooooooooooo+-
    /// -+ooooooooo+-  :+oooooo+:  -+oooooooooo-
    ///   :+ooooo+-  :+oooooooooo+:  -+oooooo:
    ///     :+o+-  :+oooooooooooooo+:  -+oo:
    ///      ./   :oooooooooooooooooo:   /.
    ///    ./oo+:  -+oooooooooooooo+-  :+oo/.
    ///  ./oooooo+:  -+oooooooooo+-  :+oooooo/.
    /// -oooooooooo+:  -+oooooo+-  :+oooooooooo-
    /// .+ooooooooooo+:  -+oo+-  :+ooooooooooo+.
    ///   -+ooooooooooo+:  ..  :+ooooooooooo+-
    ///     -+ooooooooooo+:  :+ooooooooooo+-
    ///       -+oooooooooo+::+oooooooooo+-
    ///         -+oooooo+:    :+oooooo+-
    ///           -+oo+:        :+oo+-
    ///             ..            ..
    /// ```
    LibreElec,
    /// Linspire Logo
    /// ```text
    ///                                                    __^
    ///                                                 __/    \\
    ///    MMy        dMy                            __/        \\
    ///   dMMy        MMy                            MM          \\
    ///   MMMy            ,,        dMMMMn                        \\
    ///  dMMy        dMM dMMMMMMy  dMM  MM dMMMMMy  dMM MM.nMMM dMMMMMM
    /// MMM          MMy MMy  MMy dMM      MMy  MMy MMy MMy    dy   dMy
    /// MMM         dMM dMM   MMy  dMMMMy dMM  dMM dMM dMM    dMMMMMMM
    ///  dMMy       MMy MMy  MMy     dMMy MM  MMy MMy  MMy    dMM
    /// dMMy       dMM dMM  dMM dMM  MMy dMMMMMy dMM  dMM     MMy   MM
    /// MMMMMMMMMM MMy MMy  MMy dMMMyyy MMy     MMy  MMy      dMMMMMMy
    ///                                 dy
    /// ```
    Linspire,
    /// Linux Logo
    /// ```text
    ///         #####
    ///        #######
    ///        ##O#O##
    ///        #######
    ///      ###########
    ///     #############
    ///    ###############
    ///    ################
    ///   #################
    /// #####################
    /// #####################
    ///   #################
    /// ```
    Linux,
    /// Small Linux Logo
    /// ```text
    ///     ___
    ///    (.. \
    ///    (<> |
    ///   //  \ \
    ///  ( |  | /|
    /// _/\ __)/_)
    /// \/-____\/
    /// ```
    LinuxSmall,
    /// Linux Lite Logo
    /// ```text
    ///           ,xXc
    ///       .l0MMMMMO
    ///    .kNMMMMMWMMMN,
    ///    KMMMMMMKMMMMMMo
    ///   'MMMMMMNKMMMMMM:
    ///   kMMMMMMOMMMMMMO
    ///  .MMMMMMX0MMMMMW.
    ///  oMMMMMMxWMMMMM:
    ///  WMMMMMNkMMMMMO
    /// :MMMMMMOXMMMMW
    /// .0MMMMMxMMMMM;
    /// :;cKMMWxMMMMO
    /// 'MMWMMXOMMMMl
    ///  kMMMMKOMMMMMX:
    ///  .WMMMMKOWMMM0c
    ///   lMMMMMWO0MNd:'
    ///    oollXMKXoxl;.
    ///      ':. .: .'
    ///               ..
    ///                 .
    /// ```
    LinuxLite,
    /// Linux Lite Small
    /// ```text
    ///    /\\
    ///   /  \\
    ///  / / /
    /// > / /
    /// \\ \\ \\
    ///  \\_\\_\\
    ///     \\
    /// ```
    LinuxLiteSmall,
    /// Live Raizo Logo
    /// ```text
    ///              `......`
    ///         -+shmNMMMMMMNmhs/.
    ///      :smMMMMMmmhyyhmmMMMMMmo-
    ///    -hMMMMd+:. `----` .:odMMMMh-
    ///  `hMMMN+. .odNMMMMMMNdo. .yMMMMs`
    ///  hMMMd. -dMMMMmdhhdNMMMNh` .mMMMh
    /// oMMMm` :MMMNs.:sddy:-sMMMN- `NMMM+
    /// mMMMs  dMMMo sMMMMMMd yMMMd  sMMMm
    /// ----`  .---` oNMMMMMh `---.  .----
    ///               .sMMy:
    ///                /MM/
    ///               +dMMms.
    ///              hMMMMMMN
    ///             `dMMMMMMm:
    ///       .+ss+sMNysMMoomMd+ss+.
    ///      +MMMMMMN` +MM/  hMMMMMNs
    ///      sMMMMMMm-hNMMMd-hMMMMMMd
    ///       :yddh+`hMMMMMMN :yddy/`
    ///              .hMMMMd:
    ///                `..`
    /// ```
    LiveRaizo,
    /// LMDE Logo
    /// ```text
    ///           `.-::---..
    ///       .:++++ooooosssoo:.
    ///     .+o++::.      `.:oos+.
    ///    :oo:.`             -+oo:
    ///  `+o/`    .::::::-.    .++-`
    /// `/s/    .yyyyyyyyyyo:   +o-`
    /// `so     .ss       ohyo` :s-:
    /// `s/     .ss  h  m  myy/ /s``
    /// `s:     `oo  s  m  Myy+-o:`
    /// `oo      :+sdoohyoydyso/.
    ///  :o.      .:////////++:
    ///  `/++        -:::::-
    ///   `++-
    ///    `/+-
    ///      .+/.
    ///        .:+-.
    ///           `--.``
    /// ```
    Lmde,
    /// LainOS Logo
    /// ```text
    ///                     /==\
    ///                     \==/
    ///                · · · · · · ·
    ///             · · · · · · · · · ·
    ///            · · · .-======-.· · · ·
    ///         .::. ·.-============-.· .::.
    ///      .:==:· .:===:'. ·· .':===:. ·:==:.
    ///   .:===: · :===. ·  .--.  · .===: · :===:.
    ///  :===:· · :===. · .:====:. · .===: · ·:===:
    /// (===:· · :===- ·  :======:  · -===: · ·:===)
    ///  :===:· · :===. · ':====:' · .===: · ·:===:
    ///   ':===: · :===. ·  '--'  · .===: · :===:'
    ///      ':==:· ':===:.' ·· '.:===:' ·:==:'
    ///         '::' · '===-.  .-===' · '::'
    ///   /==\     · · · :===  ===: · · ·     /==\
    ///   \==/      · · ·:=== ·===:· · ·      \==/
    ///          .-.   · :===· ===: ·   .-.
    ///          .===.   .===  ===.   .===.
    ///            .========    ========.
    ///              '''''        '''''
    /// ```
    LainOs,
    /// Lunar Logo
    /// ```text
    /// `-.                                 `-.
    ///   -ohys/-`                    `:+shy/`
    ///      -omNNdyo/`          :+shmNNy/`
    ///                    -
    ///                  /mMmo
    ///                  hMMMN`
    ///                  .NMMs
    ///       -:+oooo+//: /MN. -///oooo+/-`
    ///      /:.`          /           `.:/`
    ///           __
    ///          |  |   _ _ ___ ___ ___
    ///          |  |__| | |   | .'|  _|
    ///          |_____|___|_|_|__,|_|
    /// ```
    Lunar,
    /// MacOS Logo
    /// ```text
    ///                      ..'
    ///                  ,xNMM.
    ///                .OMMMMo
    ///                lMM"
    ///      .;loddo:.  .olloddol;.
    ///    cKMMMMMMMMMMNWMMMMMMMMMM0:
    ///  .KMMMMMMMMMMMMMMMMMMMMMMMWd.
    ///  XMMMMMMMMMMMMMMMMMMMMMMMX.
    /// ;MMMMMMMMMMMMMMMMMMMMMMMM:
    /// :MMMMMMMMMMMMMMMMMMMMMMMM:
    /// .MMMMMMMMMMMMMMMMMMMMMMMMX.
    ///  kMMMMMMMMMMMMMMMMMMMMMMMMWd.
    ///  'XMMMMMMMMMMMMMMMMMMMMMMMMMMk
    ///   'XMMMMMMMMMMMMMMMMMMMMMMMMK.
    ///     kMMMMMMMMMMMMMMMMMMMMMMd
    ///      ;KMMMMMMMWXXWMMMMMMMk.
    ///        "cooc*"    "*coo'"
    /// ```
    MacOs,
    /// Small MacOS Logo
    /// ```text
    ///         .:'
    ///     __ :'__
    ///  .'`__`-'__``.
    /// :__________.-'
    /// :_________:
    ///  :_________`-;
    ///   `.__.-.__.'
    /// ```
    MacOsSmall,
    /// MacOS2 Logo
    /// ```text
    ///                      ..'
    ///                  ,xN  .
    ///                .O    o
    ///                l  "
    ///      .;loddo:.  .olloddol;.
    ///    cK          NW          0:
    ///  .K                       Wd.
    ///  X                       X.
    /// ;                        :
    /// :                        :
    /// .                        X.
    ///  k                        Wd.
    ///  'X                          k
    ///   'X                        K.
    ///     k                      d
    ///      ;K       WXXW       k.
    ///        "cooc*"    "*coo'"
    /// ```
    MacOs2,
    /// Small MacOS2 Logo
    /// ```text
    ///         .:'
    ///     __ :'__
    ///  .'`  `-'  ``.
    /// :          .-'
    /// :         :
    ///  :         `-;
    ///   `.__.-.__.'
    /// ```
    MacOs2Small,
    /// MainsailOS Logo
    /// ```text
    ///                            -
    ///                           *%:
    ///                         :%%%#
    ///                        =%%%%%-
    ///                       *%%%%%%#
    ///                     :#%%%%%%%#.
    ///                    -%%%%%%%%+
    ///                   *%%%%%%%%-    :
    ///                 .#%%%%%%%#.    *%=
    ///                -%%%%%%%%+    :#%%%*
    ///               +%%%%%%%%-    =%%%%%%#.
    ///             .#%%%%%%%#.    *%%%%%%%%:
    ///            -%%%%%%%%*    :#%%%%%%%#.
    ///           +%%%%%%%%-    =%%%%%%%%+    :%*.
    ///         .#%%%%%%%#:    *%%%%%%%%-    +%%%%*:
    ///        :%%%%%%%%*    :#%%%%%%%#.   .*%%%%%%%*
    ///       +%%%%%%%%=    -%%%%%%%%+    :%%%%%%%%*
    ///     .#%%%%%%%%:    *%%%%%%%%-    =%%%%%%%%=
    /// ```
    MainsailOs,
    /// Small MainsailOS Logo
    /// ```text
    ///           -:
    ///          +%*
    ///        .#%%+
    ///       -%%%: +=
    ///      +%%#..#%%-
    ///    .#%%+ -%%%- +=
    ///   -%%%- +%%#..#%%+
    /// ```
    MainsailOsSmall,
    /// Mageia Logo
    /// ```text
    ///         .°°.
    ///          °°   .°°.
    ///          .°°°. °°
    ///          .   .
    ///           °°° .°°°.
    ///       .°°°.   '___'
    ///      .'___'        .
    ///    :dkxc;'.  ..,cxkd;
    ///  .dkk. kkkkkkkkkk .kkd.
    /// .dkk.  ';cloolc;.  .kkd
    /// ckk.                .kk;
    /// xO:                  cOd
    /// xO:                  lOd
    /// lOO.                .OO:
    /// .k00.              .00x
    ///  .k00;            ;00O.
    ///   .lO0Kc;,,,,,,;c0KOc.
    ///      ;d00KKKKKK00d;
    ///         .,KKKK,.
    /// ```
    Mageia,
    /// Small Mageia Logo
    /// ```text
    ///    *
    ///     *
    ///    **
    ///  /\\__/\\
    /// /      \\
    /// \\      /
    ///  \\____/
    /// ```
    MageiaSmall,
    /// MagpieOS Logo
    /// ```text
    ///         ;00000     :000Ol
    ///      .x00kk00:    O0kk00k;
    ///     l00:   :00.  o0k   :O0k.
    ///   .k0k.     xdddddk'    .d00;
    ///   k0k.      .dddddl       o00,
    ///  o00.        ':cc:.        d0O
    /// .00l                       ,00.
    /// l00.                       d0x
    /// k0O                     .:k0o
    /// O0k                 ;dO0000d.
    /// k0O               .O0Oxxxxk00:
    /// o00.              k0Oddddddocc
    /// '00l              x0Odddddo;..
    ///  x00.             .x00kxxd:..
    ///  .O0x               .:oxxxOkl.
    ///   .x0d                     ,xx,
    ///     .:o.          .xd       ckd
    ///        ..          dxl     .xx;
    ///                     :xxolldxd'
    ///                       ;oxdl.
    /// ```
    MagpieOs,
    /// Mandriva Logo
    /// ```text
    ///                         ``
    ///                        `-.
    ///       `               .---
    ///     -/               -::--`
    ///   `++    `----...```-:::::.
    ///  `os.      .::::::::::::::-```     `  `
    ///  +s+         .::::::::::::::::---...--`
    /// -ss:          `-::::::::::::::::-.``.``
    /// /ss-           .::::::::::::-.``   `
    /// +ss:          .::::::::::::-
    /// /sso         .::::::-::::::-
    /// .sss/       -:::-.`   .:::::
    ///  /sss+.    ..`  `--`    .:::
    ///   -ossso+/:://+/-`        .:`
    ///     -/+ooo+/-.              `
    /// ```
    Mandriva,
    /// Manjaro Logo
    /// ```text
    /// ██████████████████  ████████
    /// ██████████████████  ████████
    /// ██████████████████  ████████
    /// ██████████████████  ████████
    /// ████████            ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ████████  ████████  ████████
    /// ```
    Manjaro,
    /// Small Manjaro Logo
    /// ```text
    /// ||||||||| ||||
    /// ||||||||| ||||
    /// ||||      ||||
    /// |||| |||| ||||
    /// |||| |||| ||||
    /// |||| |||| ||||
    /// |||| |||| ||||
    ManjaroSmall,
    /// MassOS Logo
    /// ```text
    /// -+++/+++osyyhdmNNMMMMNdy/
    /// /MMMMMMMMMMMMMMMMMMMMMMMMm.
    /// /MMMMMMMMMMMMMMMMMMMMMMMMMm
    /// /MMMMMMMMMMMMMMMMMMMMMMMMMM:
    /// :ddddddddhddddddddhdddddddd/
    /// /NNNNNNNm:NNNNNNNN-NNNNNNNNo
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// /MMMMMMMN:MMMMMMMM-MMMMMMMMs
    /// :MMMMMMMN:MMMMMMMM-MMMMMMMMs
    ///  dMMMMMMN:MMMMMMMM-MMMMMMMMs
    ///  `yNMMMMN:MMMMMMMM-MMMMMMMMs
    ///    `:+oss.ssssssss.ssssssss/
    /// ```
    MassOs,
    /// MatuusOS Logo
    /// ```text
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░▒▓▓████▓▒▒░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░▒▓████████████▓░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░▓████████████████▒░░░░░░░░░░░░
    /// ░░░░░░░░░░░▓██████████████████▒░░░░░░░░░░░
    /// ░░░░░░░░░░▓████▒▓███████▓▓█████░░░░░░░░░░░
    /// ░░░░░░░░░░██████▓░▓████▒░██████▓░░░░░░░░░░
    /// ░░░░░░░░░▒███████▓░▒▓▒░░████████░░░░░░░░░░
    /// ░░░░░░░░░▒█████████▒░░░█████████░░░░░░░░░░
    /// ░░░░░░░░░░██████████▓▒██████████░░░░░░░░░░
    /// ░░░░░░░░░░▓████████████████████▒░░░░░░░░░░
    /// ░░░░░░░░░░░███████████████████▓░░░░░░░░░░░
    /// ░░░░░░░░░░░░█████████████████▓░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░▓██████████████▒░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░▒▓████████▓░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
    /// ```
    MatuusOs,
    /// MaUI Logo
    /// ```text
    ///              `.-://////:--`
    ///          .:/oooooooooooooooo+:.
    ///       `:+ooooooooooooooooooooooo:`
    ///     `:oooooooooooooooooooooooooooo/`
    ///     ..```-oooooo/-`` `:oooooo+:.` `--
    ///   :.      +oo+-`       /ooo/`       -/
    ///  -o.     `o+-          +o/`         -o:
    /// `oo`     ::`  :o/     `+.  .+o`     /oo.
    /// /o+      .  -+oo-     `   /oo/     `ooo/
    /// +o-        /ooo+`       .+ooo.     :ooo+
    /// ++       .+oooo:       -oooo+     `oooo+
    /// :.      .oooooo`      :ooooo-     :oooo:
    /// `      .oooooo:      :ooooo+     `ooo+-`
    ///       .+oooooo`     -oooooo:     `o/-
    ///       +oooooo:     .ooooooo.
    ///      /ooooooo`     /ooooooo/       ..
    ///     `:oooooooo/:::/ooooooooo+:--:/:`
    ///       `:+oooooooooooooooooooooo+:`
    ///          .:+oooooooooooooooo+:.
    ///              `.-://////:-.`
    /// ```
    MaUi,
    /// Meowix Logo
    /// ```text
    ///          #%            &*
    ///         ##%%          &&**
    ///        ##  %%        &&  **
    ///       ##    %%      &&    **
    ///      ##      %%    &&      **
    ///     ##        %%  &&        **
    ///    ##          %%&&          **
    ///   ##            %%            **
    ///  ##                            **
    /// ##                              **
    /// ```
    Meowix,
    /// Mer Logo
    /// ```text
    ///                          dMs
    ///                          .-`
    ///                        `y`-o+`
    ///                         ``NMMy
    ///                       .--`:++.
    ///                     .hNNNNs
    ///                     /MMMMMN
    ///                     `ommmd/ +/
    ///                       ````  +/
    ///                      `:+sssso/-`
    ///   .-::. `-::-`     `smNMNmdmNMNd/      .://-`
    /// .ymNMNNdmNMMNm+`  -dMMh:.....+dMMs   `sNNMMNo
    /// dMN+::NMMy::hMM+  mMMo `ohhy/ `dMM+  yMMy::-
    /// MMm   yMM-  :MMs  NMN` `:::::--sMMh  dMM`
    /// MMm   yMM-  -MMs  mMM+ `ymmdsymMMMs  dMM`
    /// NNd   sNN-  -NNs  -mMNs-.--..:dMMh`  dNN
    /// ---   .--`  `--.   .smMMmdddmMNdo`   .--
    ///                      ./ohddds+:`
    ///                      +h- `.:-.
    ///                      ./`.dMMMN+
    ///                         +MMMMMd
    ///                         `+dmmy-
    ///                       ``` .+`
    ///                      .dMNo-y.
    ///                      `hmm/
    ///                          .:`
    ///                          dMs
    /// ```
    Mer,
    /// Mint Logo
    /// ```text
    ///              ...-:::::-...
    ///           .-MMMMMMMMMMMMMMM-.
    ///       .-MMMM`..-:::::::-..`MMMM-.
    ///     .:MMMM.:MMMMMMMMMMMMMMM:.MMMM:.
    ///    -MMM-M---MMMMMMMMMMMMMMMMMMM.MMM-
    ///  `:MMM:MM`  :MMMM:....::-...-MMMM:MMM:`
    ///  :MMM:MMM`  :MM:`  ``    ``  `:MMM:MMM:
    /// .MMM.MMMM`  :MM.  -MM.  .MM-  `MMMM.MMM.
    /// :MMM:MMMM`  :MM.  -MM-  .MM:  `MMMM-MMM:
    /// :MMM:MMMM`  :MM.  -MM-  .MM:  `MMMM:MMM:
    /// :MMM:MMMM`  :MM.  -MM-  .MM:  `MMMM-MMM:
    /// .MMM.MMMM`  :MM:--:MM:--:MM:  `MMMM.MMM.
    ///  :MMM:MMM-  `-MMMMMMMMMMMM-`  -MMM-MMM:
    ///   :MMM:MMM:`                `:MMM:MMM:
    ///    .MMM.MMMM:--------------:MMMM.MMM.
    ///      '-MMMM.-MMMMMMMMMMMMMMM-.MMMM-'
    ///        '.-MMMM``--:::::--``MMMM-.'
    ///             '-MMMMMMMMMMMMM-'
    ///                ``-:::::-``
    /// ```
    Mint,
    /// Small Mint Logo
    /// ```text
    ///  __________
    /// |_          \
    ///   | | _____ |
    ///   | | | | | |
    ///   | | | | | |
    ///   | \_____/ |
    ///   \_________/
    /// ```
    MintSmall,
    /// Old Mint Logo
    /// ```text
    /// MMMMMMMMMMMMMMMMMMMMMMMMMmds+.
    /// MMm----::-://////////////oymNMd+`
    /// MMd      /++                -sNMd:
    /// MMNso/`  dMM    `.::-. .-::.` .hMN:
    /// ddddMMh  dMM   :hNMNMNhNMNMNh: `NMm
    ///     NMm  dMM  .NMN/-+MMM+-/NMN` dMM
    ///     NMm  dMM  -MMm  `MMM   dMM. dMM
    ///     NMm  dMM  -MMm  `MMM   dMM. dMM
    ///     NMm  dMM  .mmd  `mmm   yMM. dMM
    ///     NMm  dMM`  ..`   ...   ydm. dMM
    ///     hMM- +MMd/-------...-:sdds  dMM
    ///     -NMm- :hNMNNNmdddddddddy/`  dMM
    ///     -dMNs-``-::::-------.``    dMM
    ///     `/dMNmy+/:-------------:/yMMM
    ///       ./ydNMMMMMMMMMMMMMMMMMMMMM
    ///           .MMMMMMMMMMMMMMMMMMM
    /// ```
    MintOld,
    /// Minix Logo
    /// ```text
    ///    -sdhyo+:-`                -/syymm:
    ///    sdyooymmNNy.     ``    .smNmmdysNd
    ///    odyoso+syNNmysoyhhdhsoomNmm+/osdm/
    ///     :hhy+-/syNNmddhddddddmNMNo:sdNd:
    ///      `smNNdNmmNmddddddddddmmmmmmmy`
    ///    `ohhhhdddddmmNNdmddNmNNmdddddmdh-
    ///    odNNNmdyo/:/-/hNddNy-`..-+ydNNNmd:
    ///  `+mNho:`   smmd/ sNNh :dmms`   -+ymmo.
    /// -od/       -mmmmo -NN+ +mmmm-       yms:
    /// +sms -.`    :so:  .NN+  :os/     .-`mNh:
    /// .-hyh+:////-     -sNNd:`    .--://ohNs-
    ///  `:hNNNNNNNMMd/sNMmhsdMMh/ymmNNNmmNNy/
    ///   -+sNNNNMMNNNsmNMo: :NNmymNNNNMMMms:
    ///     //oydNMMMMydMMNysNMMmsMMMMMNyo/`
    ///        ../-yNMMy--/::/-.sMMmos+.`
    ///            -+oyhNsooo+omy/```
    ///               `::ohdmds-`
    /// ```
    Minix,
    /// Miracle Linux Logo
    /// ```text
    ///             ,A
    ///           .###
    ///      .#' .####   .#.
    ///    r##:  :####   ####.
    ///   +###;  :####  ######C
    ///   :####:  #### ,######".#.
    /// .# :####. :### #####'.#####.
    /// ##: `####. ### ###'.########+.
    /// #### `####::## ##'.#######+'
    ///  ####+.`###i## #',####:'
    ///  `+###MI`##### 'l###:'
    ///    `+####+`### ;#E'
    ///       `+###:## #'
    ///          `:### '
    ///            '##
    ///             ';
    /// ```
    MiracleLinux,
    /// MOS Logo
    /// ```text
    ///   :--==========================--:
    /// .-=================================.
    /// -==================================-
    /// ====================================
    /// =======-....:==========:....:=======
    /// =======:      -======-.     .=======
    /// =======:       :====-       .=======
    /// =======:        :==:        .=======
    /// =======:         ..         .=======
    /// =======:    .:        .:    .=======
    /// =======:    .=-      :=:    .=======
    /// =======:    .===.  .-==:    .=======
    /// =======:    .==========:    .=======
    /// =======:    :==========:    :=======
    /// ====================================
    /// -===================================
    /// .==================================:
    ///   :--===========================-:.
    /// ```
    Mos,
    /// Msys2 Logo
    /// ```text
    ///                  ...
    ///               5GB###GJ.             !YPGGGG
    ///               7@@@@@@@B.          :G@@@@@@@
    ///               7@@@@@@@@Y         ~&@@@@@@@@YJYY5YY?L
    ///              !@@@@@@@@@@^       ^&@@@@@@@#PP555555PBY
    ///             ~&@@@@@@@@@@?      ^&@@@@@@#5YY5YYYYYYYY#7
    ///            ^&@@@@@@@@@@@B     :#@@@@@@@G5BBYGPYYYYYY#J
    ///           ^#@@@&J#@@@@@@@~   .B@@@@@@@@@@@P ?#YYYYYPB.
    ///          :#@@@@7 G@@@@@@@J   P@@@#!&@@@@@@G.GGYYYYGB^
    ///         :#@@@@J  Y@@@@@@@B  5@@@&:.&@@@@@@&BBYYY5B5.
    ///        :#@@@@Y   !@@@@@@@@!Y@@@&~ .#@@@@@@GYYYYYBP  JP~
    ///       :#@@@@P    :&@@@@@@@@@@@&~   B@@@@@#5YYYYYPGPGPGG
    ///      ^#@@@@G.     P@@@@@@@@@@@!    P@@@@GYYYYYYYYYYYYBY
    ///     ^#@@@@B:      ^@@@@@@@@@@7     !@@@#GGGGGGGPPPP5GB:
    ///    !&@@@@B:        Y@@@@@@@@?       P@@@@@@@@@&?  ^PY:
    ///   7&@@@@5.          P@@@@@@?         P@@@@@@@@@B
    ///  Y@@@&P!             5@@@@7           7G@@@@@&P~
    /// .JJ?~:                ^JY~              ^!5J!^:
    ///                              :@P5#B. #G  7&^ :@P5#B.
    ///                              !&P^.   ?@~ #P  !&P^.  
    ///                               .?BG!   #G5@~   .?BG!
    ///                                :.B@.  7@@5     :.B@.
    ///                              !PYY5Y   :&@^   !PYY5Y
    ///                                       ~@Y
    ///                                       !5:
    /// ```
    Msys2,
    /// MX Logo
    /// ```text
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNMMMMMMMMM
    /// MMMMMMMMMMNs..yMMMMMMMMMMMMMm: +NMMMMMMM
    /// MMMMMMMMMN+    :mMMMMMMMMMNo` -dMMMMMMMM
    /// MMMMMMMMMMMs.   `oNMMMMMMh- `sNMMMMMMMMM
    /// MMMMMMMMMMMMN/    -hMMMN+  :dMMMMMMMMMMM
    /// MMMMMMMMMMMMMMh-    +ms. .sMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMN+`   `  +NMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMNMMd:    .dMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMm/-hMd-     `sNMMMMMMMMMMMMM
    /// MMMMMMMMMMNo`   -` :h/    -dMMMMMMMMMMMM
    /// MMMMMMMMMd:       /NMMh-   `+NMMMMMMMMMM
    /// MMMMMMMNo`         :mMMN+`   `-hMMMMMMMM
    /// MMMMMMh.            `oNMMd:    `/mMMMMMM
    /// MMMMm/                -hMd-      `sNMMMM
    /// MMNs`                   -          :dMMM
    /// Mm:                                 `oMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// ```
    Mx,
    /// Small MX Logo
    /// ```text
    ///     \\\\  /
    ///      \\\\/
    ///       \\\\
    ///    /\\/ \\\\
    ///   /  \\  /\\
    ///  /    \\/  \\
    /// /__________\\
    /// ```
    MxSmall,
    /// MX2 Logo
    /// ```text
    /// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /// @@@@@@@@@@@@%*+--:------=+*%@@@@@@@@@@@@
    /// @@@@@@@@@#=. .-+#%@@@@@%#*+--=#@@@@@@@@@
    /// @@@@@@@+. .=%@@@@@@@@@@@@@@@@*-:+@@@@@@@
    /// @@@@@*.  *@@@@@@@@@@@@@@@@@@@@@%-.*@@@@@
    /// @@@@-  -@@@@@@@@@@@@@@@@@@@@@@@#:  -@@@@
    /// @@@:  -@@@@@@@=.*@@@@@@@@@@@@%-   = :@@@
    /// @@=  .@@@@@@@@%- :%@@@@@@@@@+   -%@# =@@
    /// @%   +@@@@@@@@@@#. =@@@@@@*.  .*@@@@. %@
    /// @+   *@@@@@@*..*@@+  *@@%-   =@@@@@@- +@
    /// @=   *@@@@%-    -%@@- :=   -%@@@@@@@: +@
    /// @+   :@@@=        +@@=   .#@@@@@@@@%  *@
    /// @%    +*.          .:     *@@#: +@@:  @@
    /// @@+                   :%@- :-    ::  +@@
    /// @@@-                  .=@@=         -@@@
    /// @@+.                     .           +@@
    /// %=..:.................::...........:..-%
    /// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /// ```
    Mx2,
    /// Namib Logo
    /// ```text
    ///           .:+shysyhhhhysyhs+:.
    ///        -/yyys              syyy/-
    ///      -shy                      yhs-
    ///    -yhs                          shy-
    ///   +hy                              yh+
    ///  +ds                                sd+
    /// /ys                  so              sy/
    /// sh                 smMMNdyo           hs
    /// yo               ymMMMMNNMMNho        oy
    /// N             ydMMMNNMMMMMMMMMmy       N
    /// N         shmMMMMNNMMMMMMMMMMMMMNy     N
    /// yo  ooshmNMMMNNNNMMMMMMMMMMMMMMMMMms  oy
    /// sd yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy ds
    /// /ys                                  sy/
    ///  +ds                                sd+
    ///   +hy                              yh+
    ///    -yhs                          shy-
    ///      -shy                      yhs-
    ///        -/yyys              syyy/-
    ///           .:+shysyhyhhysyhs+:.
    /// ```
    Namib,
    /// Nekos Logo
    /// ```text
    ///                         @@@@@
    ///                      @@@@@@@@@.
    ///                   @@@@@@@@    @@@
    ///                @@@@@@@@@@@     @@.
    ///               @@@@@@@@@@@@@      .
    ///              @@@@@@@@@@@@@@@@@   ,
    ///            @@@@@@@@@@@@@@@@@@@
    ///           @@@@@///@@@@@@@///@@@
    ///           @@@@/***@@@@@@@**//@@@@
    ///        @@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///           @@@@@@@@@@@@@@@@@@@@@@@
    ///          @@@/   /@@@@@@@@@/   /@@@
    ///       @@@@@@     @@@██@@@@     @@@@@@
    ///       @@@@@@/   /@██████@@/   /@@@@@@
    ///        @@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///                  ##########%%%%
    ///                  ##########%%  %%
    ///          @     @@@#######@@%%%
    ///       @@@      @@@@####@@@@   %
    ///    @@@        @@@@@@@#@@@@@@@
    ///    @@@        @@@@@@@@@@@@@@@
    ///    @@@@      @@@@@@@@@@@@@@@@@
    ///       @@@@@@@@@@@@@@@@@@@@@@@@
    /// ```
    Nekos,
    /// Neptune Logo
    /// ```text
    ///             ./+sydddddddys/-.
    ///         .+ymNNdyooo/:+oooymNNmy/`
    ///      `/hNNh/.`             `-+dNNy:`
    ///     /mMd/.          .++.:oy/   .+mMd-
    ///   `sMN/             oMMmdy+.     `oNNo
    ///  `hMd.           `/ymy/.           :NMo
    ///  oMN-          `/dMd:               /MM-
    /// `mMy          -dMN+`                 mMs
    /// .MMo         -NMM/                   yMs
    ///  dMh         mMMMo:`                `NMo
    ///  /MM/        /ymMMMm-               sMN.
    ///   +Mm:         .hMMd`              oMN/
    ///    +mNs.      `yNd/`             -dMm-
    ///     .yMNs:    `/.`            `/yNNo`
    ///       .odNNy+-`           .:ohNNd/.
    ///          -+ymNNmdyyyyyyydmNNmy+.
    ///              `-//sssssss//.
    /// ```
    Neptune,
    /// NetRunner Logo
    /// ```text
    ///            .:oydmMMMMMMmdyo:`
    ///         -smMMMMMMMMMMMMMMMMMMds-
    ///       +mMMMMMMMMMMMMMMMMMMMMMMMMd+
    ///     /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMm/
    ///   `hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMy`
    ///  .mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd`
    ///  dMMMMMMMMMMMMMMMMMMMMMMNdhmMMMMMMMMMMh
    /// +MMMMMMMMMMMMMNmhyo+/-.   -MMMMMMMMMMMM/
    /// mMMMMMMMMd+:.`           `mMMMMMMMMMMMMd
    /// MMMMMMMMMMMdy/.          yMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMNh+`     +MMMMMMMMMMMMMMM
    /// mMMMMMMMMMMMMMMMMMs    -NMMMMMMMMMMMMMMd
    /// +MMMMMMMMMMMMMMMMMN.  `mMMMMMMMMMMMMMMM/
    ///  dMMMMMMMMMMMMMMMMMy  hMMMMMMMMMMMMMMMh
    ///  `dMMMMMMMMMMMMMMMMM-+MMMMMMMMMMMMMMMd`
    ///   `hMMMMMMMMMMMMMMMMmMMMMMMMMMMMMMMMy
    ///     /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMm:
    ///       +dMMMMMMMMMMMMMMMMMMMMMMMMd/
    ///         -odMMMMMMMMMMMMMMMMMMdo-
    ///            `:+ydmNMMMMNmhy+-`
    /// ```
    NetRunner,
    /// Nitrux Logo
    /// ```text
    /// `:/.
    /// `/yo
    /// `/yo
    /// `/yo      .+:.
    /// `/yo      .sys+:.`
    /// `/yo       `-/sys+:.`
    /// `/yo           ./sss+:.`
    /// `/yo              .:oss+:-`
    /// `/yo                 ./o///:-`
    /// `/yo              `.-:///////:`
    /// `/yo           `.://///++//-``
    /// `/yo       `.-:////++++/-`
    /// `/yo    `-://///++o+/-`
    /// `/yo `-/+o+++ooo+/-`
    /// `/s+:+oooossso/.`
    /// `//+sssssso:.
    /// `+syyyy+:`
    /// :+s+-
    /// ```
    Nitrux,
    /// NixOS Logo
    /// ```text
    ///           ▗▄▄▄       ▗▄▄▄▄    ▄▄▄▖
    ///           ▜███▙       ▜███▙  ▟███▛
    ///            ▜███▙       ▜███▙▟███▛
    ///             ▜███▙       ▜██████▛
    ///      ▟█████████████████▙ ▜████▛     ▟▙
    ///     ▟███████████████████▙ ▜███▙    ▟██▙
    ///            ▄▄▄▄▖           ▜███▙  ▟███▛
    ///           ▟███▛             ▜██▛ ▟███▛
    ///          ▟███▛               ▜▛ ▟███▛
    /// ▟███████████▛                  ▟██████████▙
    /// ▜██████████▛                  ▟███████████▛
    ///       ▟███▛ ▟▙               ▟███▛
    ///      ▟███▛ ▟██▙             ▟███▛
    ///     ▟███▛  ▜███▙           ▝▀▀▀▀
    ///     ▜██▛    ▜███▙ ▜██████████████████▛
    ///      ▜▛     ▟████▙ ▜████████████████▛
    ///            ▟██████▙       ▜███▙
    ///           ▟███▛▜███▙       ▜███▙
    ///          ▟███▛  ▜███▙       ▜███▙
    ///          ▝▀▀▀    ▀▀▀▀▘       ▀▀▀▘
    /// ```
    NixOs,
    /// Small NixOS Logo
    /// ```text
    ///   ▗▄   ▗▄ ▄▖
    ///  ▄▄🬸█▄▄▄🬸█▛ ▃
    ///    ▟▛    ▜▃▟🬕
    /// 🬋🬋🬫█      █🬛🬋🬋
    ///  🬷▛🮃▙    ▟▛
    ///  🮃 ▟█🬴▀▀▀█🬴▀▀
    ///   ▝▀ ▀▘   ▀▘
    /// ```
    NixOsSmall,
    /// Alternative NixOS Logo
    /// ```text
    ///               ____       _______        ____
    ///              /####\      \######\      /####\
    ///              ######\      \######\    /#####/
    ///              \######\      \######\  /#####/
    ///               \######\      \######\/#####/    /\
    ///                \######\      \###########/    /##\
    ///         ________\######\______\#########/    /####\
    ///        /#######################\#######/    /######
    ///       /#########################\######\   /######/
    ///      /###########################\######\ /######/
    ///      ¯¯¯¯¯¯¯¯¯¯¯¯/######/¯¯¯¯¯¯¯¯¯\######/######/
    ///                 /######/           \####/######/________
    ///   _____________/######/             \##/################\
    ///  /###################/               \/##################\
    ///  \##################/\               /###################/
    ///   \################/##\             /######/¯¯¯¯¯¯¯¯¯¯¯¯¯
    ///    ¯¯¯¯¯¯¯¯/######/####\           /######/
    ///           /######/######\_________/######/____________
    ///          /######/ \######\###########################/
    ///         /######/   \######\#########################/
    ///         ######/    /#######\#######################/
    ///         \####/    /#########\¯¯¯¯¯¯\######\¯¯¯¯¯¯¯¯
    ///          \##/    /###########\      \######\
    ///           \/    /#####/\######\      \######\
    ///                /#####/  \######\      \######\
    ///               /#####/    \######\      \######
    ///               \####/      \######\      \####/
    ///                ¯¯¯¯        ¯¯¯¯¯¯¯       ¯¯¯¯
    /// ```
    NixOsOld,
    /// Alternative Small NixOS Logo
    /// ```text
    ///   \\  \\ //
    ///  ==\\__\\/ //
    ///    //   \\//
    /// ==//     //==
    ///  //\\___//
    /// // /\\  \\==
    ///   // \\  \\
    /// ```
    NixOsOldSmall,
    /// NetBSD Logo
    /// ```text
    ///                      `-/oshdmNMNdhyo+:-`
    /// y/s+:-``    `.-:+oydNMMMMNhs/-``
    /// -m+NMMMMMMMMMMMMMMMMMMMNdhmNMMMmdhs+/-`
    ///  -m+NMMMMMMMMMMMMMMMMMMMMmy+:`
    ///   -N/dMMMMMMMMMMMMMMMds:`
    ///    -N/hMMMMMMMMMmho:`
    ///     -N/-:/++/:.`
    ///      :M+
    ///       :Mo
    ///        :Ms
    ///         :Ms
    ///          :Ms
    ///           :Ms
    ///            :Ms
    ///             :Ms
    ///              :Ms
    ///               :Ms
    /// ```
    NetBsd,
    /// Nobara Logo
    /// ```text
    /// ⢀⣤⣴⣶⣶⣶⣦⣤⡀⠀⣀⣠⣤⣴⣶⣶⣶⣶⣶⣶⣶⣶⣤⣤⣀⡀
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⡀
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠋⠉⠁⠀⠀⠉⠉⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⢀⣀⣀⡀⠀⠀⠀⠈⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡏⠀⠀⠀⢠⣾⣿⣿⣿⣿⣷⡄⠀⠀⠀⠻⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⣀⣀⣬⣽⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠈⠻⢿⣿⣿⡿⠟⠁⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⣤⣄⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
    /// ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠛⠉⠉⠛⠛⢿⣿⣿⠀⠀⠀⠀⠀⠸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿
    /// ⠘⢿⣿⣿⣿⣿⣿⣿⣿⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠈⢿⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿⣿⣿⣿⣿⣿⠟⠁
    ///   ⠈⠙⠛⠛⠛⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠛⠛⠛⠉⠁
    /// ```
    Nobara,
    /// NomadBSD Logo
    /// ```text
    ///          _======__
    ///      (===============\\
    ///    (===================\\
    ///    _              _---__
    ///   (=               ====-
    ///  (=                ======
    ///  (==                ======
    ///  (==                ======
    ///  (==\\ \\=-_      _=/ /====/
    ///   (==\\ \\========/ /====/  /====-_
    ///    (==\\ \\=====/ /==/   /===--
    /// /================/  /===-
    /// \\===========/
    /// ```
    NomadBsd,
    /// Nurunner Logo
    /// ```text
    ///                   ,xc
    ///                 ;00cxXl
    ///               ;K0,   .xNo.
    ///             :KO'       .lXx.
    ///           cXk.    ;xl     cXk.
    ///         cXk.    ;k:.,xo.    cXk.
    ///      .lXx.    :x::0MNl,dd.    :KO,
    ///    .xNx.    cx;:KMMMMMNo'dx.    ;KK;
    ///  .dNl.    cd,cXMMMMMMMMMWd,ox'    'OK:
    /// ;WK.    'K,.KMMMMMMMMMMMMMWc.Kx     lMO
    ///  'OK:    'dl'xWMMMMMMMMMM0::x:    'OK:
    ///    .kNo    .xo'xWMMMMMM0;:O:    ;KK;
    ///      .dXd.   .do,oNMMO;ck:    ;00,
    ///         oNd.   .dx,;'cO;    ;K0,
    ///           oNx.    okk;    ;K0,
    ///             lXx.        :KO'
    ///               cKk'    cXk.
    ///                 ;00:lXx.
    ///                   ,kd.
    /// ```
    Nurunner,
    /// NuTyx Logo
    /// ```text
    ///                                       .
    ///                                     .
    ///                                  ...
    ///                                ...
    ///             ....     .........--.
    ///        ..-++-----....--++++++---.
    ///     .-++++++-.   .-++++++++++++-----..
    ///   .--...  .++..-+++--.....-++++++++++--..
    ///  .     .-+-. .**-            ....  ..-+----..
    ///      .+++.  .*+.         +            -++-----.
    ///    .+++++-  ++.         .*+.     .....-+++-----.
    ///   -+++-++. .+.          .-+***++***++--++++.  .
    ///  -+-. --   -.          -*- ......        ..--.
    /// .-. .+-    .          -+.
    /// .  .+-                +.
    ///    --                 --
    ///   -+----.              .-
    ///   -++-.+.                .
    ///  .++. --
    ///   +.  ----.
    ///   .  .+. ..
    ///       -  .
    ///       .
    /// ```
    NuTyx,
    /// Obarun Logo
    /// ```text
    ///                  ,;::::;
    ///              ;cooolc;,
    ///           ,coool;
    ///         ,loool,
    ///        loooo;
    ///      :ooool
    ///     cooooc            ,:ccc;
    ///    looooc           :oooooool
    ///   cooooo          ;oooooooooo,
    ///  :ooooo;         :ooooooooooo
    ///  oooooo          oooooooooooc
    /// :oooooo         :ooooooooool
    /// loooooo         ;oooooooool
    /// looooooc        .coooooooc
    /// cooooooo:           ,;co;
    /// ,ooooooool;       ,:loc
    ///  cooooooooooooloooooc
    ///   ;ooooooooooooool;
    ///     ;looooooolc;
    /// ```
    Obarun,
    /// OBRevenge Logo
    /// ```text
    ///         __   __
    ///      _@@@@   @@@g_
    ///    _@@@@@@   @@@@@@
    ///   _@@@@@@M   W@@@@@@_
    ///  j@@@@P        ^W@@@@
    ///  @@@@L____  _____Q@@@@
    /// Q@@@@@@@@@@j@@@@@@@@@@
    /// @@@@@    T@j@    T@@@@@
    /// @@@@@ ___Q@J@    _@@@@@
    /// @@@@@fMMM@@j@jggg@@@@@@
    /// @@@@@    j@j@^MW@P @@@@
    /// Q@@@@@ggg@@f@   @@@@@@L
    /// ^@@@@WWMMP  ^    Q@@@@
    ///  @@@@@_         _@@@@l
    ///   W@@@@@g_____g@@@@@P
    ///    @@@@@@@@@@@@@@@@l
    ///     ^W@@@@@@@@@@@P
    ///        ^TMMMMTll
    /// ```
    ObRevenge,
    /// OmniOS Logo
    /// ```text
    ///   ____   __  __  _   _  _
    ///  / __ \ |  \/  || \ | || |
    /// | |  | ||      ||  \| || |
    /// | |__| || |\/| || , `_||_|  ____
    ///  \____/ |_|  |_||_|\/ __ \ / ___|
    ///                    | |  | ||(__
    ///        community   | |__| | ___)|
    ///             edition \____/ |____/
    /// ```
    OmniOs,
    /// Open-Kylin Logo
    /// ```text
    ///
    ///              /KKK]
    ///             KKKKKKK`   ]KKKK\
    ///            KKKKK/  /KKKKKKKKK\
    ///           KKKK/ ,KKKKKKKKKKKK^
    ///   ,]KKK  =KKK` /KKKKKKOOOOOO`
    /// ,KKKKKK  =KK  /`    [\OOOOOOO\
    ///  \KKKKK  =K            ,OOOOOOO`
    ///  ,KKKKK  =^              \OOOOOO
    ///   ,KKKK   ^               OOOOOO^
    ///    *KKK^                  =OOOOO^
    ///     OOKK^                 OOOOOO^
    ///     \OOOK\               /OOOOOO`
    ///      OOOOOO]           ,OOOOOOO^
    ///      ,OOOOOOOO\]   ,[OOOOOOOOO/
    ///        \OOOOOOOOOOOOOOOOOOOOO`
    ///          [OOOOOOOOOOOOOOOO/`
    ///             ,[OOOOOOOOO]
    /// ```
    OpenKylin,
    /// OpenBSD Logo
    /// ```text
    ///                                      _
    ///                                     (_)
    ///               |    .
    ///           .   |L  /|   .          _
    ///       _ . |\ _| \--+._/| .       (_)
    ///      / ||\| Y J  )   / |/| ./
    ///     J  |)'( |        ` F`.'/        _
    ///   -<|  F         __     .-<        (_)
    ///     | /       .-'. `.  /-. L___
    ///     J \\      <    \  | | O\\|.-'  _
    ///   _J \\  .-    \\/ O | | \\  |F    (_)
    ///  '-F  -<_.     \\   .-'  `-' L__
    /// __J  _   _.     >-'  )._.   |-'
    ///  `-|.'   /_.          \_|   F
    ///   /.-   .                _.<
    ///  /'    /.'             .'  `\\
    ///   /L  /'   |/      _.-'-\\
    ///  /'J       ___.---'\|
    ///    |\  .--' V  | `. `
    ///    |/`. `-.     `._)
    ///       / .-.\\
    ///       \\ (  `\\
    ///        `.\\
    /// ```
    OpenBsd,
    /// Small OpenBSD Logo
    /// ```text
    ///       _____
    ///     \\-     -/
    ///  \\_/         \\
    ///  |        O O |
    ///  |_  <   )  3 )
    ///  / \\         /
    ///     /-_____-\\
    /// ```
    OpenBsdSmall,
    /// OpenEuler Logo
    /// ```text
    ///                  `.cc.`
    ///              ``.cccccccc..`
    ///           `.cccccccccccccccc.`
    ///       ``.cccccccccccccccccccccc.``
    ///    `..cccccccccccccccccccccccccccc..`
    /// `.ccccccccccccccc/++/ccccccccccccccccc.`
    /// .cccccccccccccccmNMMNdo+oso+ccccccccccc.
    /// .cccccccccc/++odms+//+mMMMMm/:+syso/cccc
    /// .cccccccccyNNMMMs:::/::+o+/:cdMMMMMmcccc
    /// .ccccccc:+NmdyyhNNmNNNd:ccccc:oyyyo:cccc
    /// .ccc:ohdmMs:cccc+mNMNmyccccccccccccccccc
    /// .cc/NMMMMMo////:c:///:cccccccccccccccccc
    /// .cc:syysyNMNNNMNyccccccccccccccccccccccc
    /// .cccccccc+MMMMMNyc:/+++/cccccccccccccccc
    /// .cccccccccohhhs/comMMMMNhccccccccccccccc
    /// .ccccccccccccccc:MMMMMMMM/cccccccccccccc
    /// .ccccccccccccccccsNNNNNd+cccccccccccccc.
    /// `..cccccccccccccccc/+/:cccccccccccccc..`
    ///    ``.cccccccccccccccccccccccccccc.``
    ///        `.cccccccccccccccccccccc.`
    ///           ``.cccccccccccccc.``
    ///               `.cccccccc.`
    ///                  `....`
    /// ```
    OpenEuler,
    /// OpenIndiana Logo
    /// ```text
    ///                          .sy/
    ///                          .yh+
    ///
    ///            -+syyyo+-      /+.
    ///          +ddo/---/sdh/    ym-
    ///        `hm+        `sms   ym-```````.-.
    ///        sm+           sm/  ym-         +s
    ///        hm.           /mo  ym-         /h
    ///        omo           ym:  ym-       `os`
    ///         smo`       .ym+   ym-     .os-
    ///      ``  :ymy+///oyms-    ym-  .+s+.
    ///    ..`     `:+oo+/-`      -//oyo-
    ///  -:`                   .:oys/.
    /// +-               `./oyys/.
    /// h+`      `.-:+oyyyo/-`
    /// `/ossssysso+/-.`
    /// ```
    OpenIndiana,
    /// OpenMamba Logo
    /// ```text
    ///                  `````
    ///            .-/+ooooooooo+/:-`
    ///         ./ooooooooooooooooooo+:.
    ///       -+oooooooooooooooooooooooo+-
    ///     .+ooooooooo+/:---::/+ooooooooo+.
    ///    :oooooooo/-`          `-/oos´oooo.s´
    ///   :ooooooo/`                `sNdsooosNds
    ///  -ooooooo-                   :dmyooo:dmy
    ///  +oooooo:                      :oooooo-
    /// .ooooooo                        .://:`
    /// :oooooo+                        ./+o+:`
    /// -ooooooo`                      `oooooo+
    /// `ooooooo:                      /oooooo+
    ///  -ooooooo:                    :ooooooo.
    ///   :ooooooo+.                .+ooooooo:
    ///    :oooooooo+-`          `-+oooooooo:
    ///     .+ooooooooo+/::::://oooooooooo+.
    ///       -+oooooooooooooooooooooooo+-
    ///         .:ooooooooooooooooooo+:.
    ///            `-:/ooooooooo+/:.`
    ///                  ``````
    /// ```
    OpenMamba,
    /// OpenStage Logo
    /// ```text
    ///                  /(/
    ///               .(((((((,
    ///              /(((((((((/
    ///            .(((((/,/(((((,
    ///           *(((((*   ,(((((/
    ///           (((((*      .*/((
    ///          *((((/  (//(/*
    ///          /((((*  ((((((((((,
    ///       .  /((((*  (((((((((((((.
    ///      ((. *((((/        ,((((((((
    ///    ,(((/  (((((/     **   ,((((((*
    ///   /(((((. .(((((/   //(((*  *(((((/
    ///  .(((((,    ((/   .(((((/.   .(((((,
    ///  /((((*        ,(((((((/      ,(((((
    ///  /(((((((((((((((((((/.  /(((((((((/
    ///  /(((((((((((((((((,   /(((((((((((/
    ///      */(((((//*.      */((/(/(/*
    /// ```
    OpenStage,
    /// OpenSuse Logo
    /// ```text
    ///            .;ldkO0000Okdl;.
    ///        .;d00xl:^''''''^:ok00d;.
    ///      .d00l'                'o00d.
    ///    .d0Kd'  Okxol:;,.          :O0d
    ///   .OKKKK0kOKKKKKKKKKKOxo:,      lKO.
    ///  ,0KKKKKKKKKKKKKKKK0P^,,,^dx:    ;00,
    /// .OKKKKKKKKKKKKKKKKk'.oOPPb.'0k.   cKO.
    /// :KKKKKKKKKKKKKKKKK: kKx..dd lKd   'OK:
    /// dKKKKKKKKKKKOx0KKKd ^0KKKO' kKKc   dKd
    /// dKKKKKKKKKKKK;.;oOKx,..^..;kKKK0.  dKd
    /// :KKKKKKKKKKKK0o;...^cdxxOK0O/^^'  .0K:
    ///  kKKKKKKKKKKKKKKK0x;,,......,;od  lKk
    ///  '0KKKKKKKKKKKKKKKKKKKKK00KKOo^  c00'
    ///   'kKKKOxddxkOO00000Okxoc;''   .dKk'
    ///     l0Ko.                    .c00l'
    ///      'l0Kk:.              .;xK0l'
    ///         'lkK0xl:;,,,,;:ldO0kl'
    ///             '^:ldxkkkkxdl:^'
    /// ```
    OpenSuse,
    /// Small OpenSuse Logo
    /// ```text
    ///   _______
    /// __|   __ \
    ///      / .\ \
    ///      \__/ |
    ///    _______|
    ///    \_______
    /// __________/
    /// ```
    OpenSuseSmall,
    /// OpenSuseMicroOS Logo
    /// ```text
    ///              ⣀⣠⣴⣶⣶⣿⣿⣿⣿⣶⣶⣦⣄⣀
    ///           ⢀⣴⣾⣿⠿⠛⠉⠉    ⠉⠉⠛⠿⣿⣷⣦⡀
    ///          ⣴⣿⡿⠋              ⠙⢿⣿⣦
    ///         ⣾⣿⡟     ⣠⣴⣶⣿⣿⣶⣦⣄     ⢻⣿⣷
    /// ⣠⣤⣤⣤⣤⣤⣤⣼⣿⣿     ⣼⣿⡟⠉  ⠉⢻⣿⣧     ⣿⣿⣧⣤⣤⣤⣤⣤⣤⣄
    /// ⠙⠛⠛⠛⠛⠛⠛⢻⣿⣿     ⢻⣿⣧⡀  ⢀⣼⣿⡟     ⣿⣿⡟⠛⠛⠛⠛⠛⠛⠋
    ///         ⢿⣿⣇     ⠙⠿⣿⣿⣿⣿⠿⠋     ⣸⣿⡿
    ///         ⠈⢻⣿⣧⣀              ⣀⣾⣿⡟⠁
    ///           ⠙⠻⣿⣷⣦⣄⣀      ⣀⣠⣴⣾⣿⠟⠋
    ///              ⠉⠛⠿⢿⣿⣿⣿⣿⣿⣿⡿⠿⠛⠉
    /// ```
    OpenSuseMicroOs,
    /// OpenSuse Leap Logo
    /// ```text
    ///                  .-++:.
    ///                ./oooooo/-
    ///             `:oooooooooooo:.
    ///           -+oooooooooooooooo+-`
    ///        ./oooooooooooooooooooooo/-
    ///       :oooooooooooooooooooooooooo:
    ///     `  `-+oooooooooooooooooooo/-   `
    ///  `:oo/-   .:ooooooooooooooo+:`  `-+oo/.
    /// `/oooooo:.   -/oooooooooo/.   ./oooooo/.
    ///   `:+ooooo+-`  `:+oooo+-   `:oooooo+:`
    ///      .:oooooo/.   .::`   -+oooooo/.
    ///         -/oooooo:.    ./oooooo+-
    ///           `:+ooooo+-:+oooooo:`
    ///              ./oooooooooo/.
    ///                 -/oooo+:`
    ///                   `:/.
    /// ```
    OpenSuseLeap,
    /// OpenSuse Tumbleweed Logo
    /// ```text
    ///                                      ......
    ///      .,cdxxxoc,.               .:kKMMMNWMMMNk:.
    ///     cKMMN0OOOKWMMXo. A        ;0MWk:'      ':OMMk.
    ///   ;WMK;'       'lKMMNM,     :NMK'             'OMW;
    ///  cMW;             WMMMN   ,XMK'                 oMM.
    /// .MMc             ''^*~l. xMN:                    KM0
    /// 'MM.                   .NMO                      oMM
    /// .MM,                 .kMMl                       xMN
    ///  KM0               .kMM0' .dl>~,.               .WMd
    ///  'XM0.           ,OMMK'    OMMM7'              .XMK
    ///    *WMO:.    .;xNMMk'       NNNMKl.          .xWMx
    ///      ^ONMMNXMMMKx;          V  'xNMWKkxllox0NMWk'
    ///          '''''                    ':dOOXXKOxl'
    /// ```
    OpenSuseTumbleweed,
    /// Open Mandriva Logo
    /// ```text
    ///                  ``````
    ///             `-:/+++++++//:-.`
    ///          .:+++oooo+/:.``   ``
    ///       `:+ooooooo+:.  `-:/++++++/:.`
    ///      -+oooooooo:` `-++o+/::::://+o+/-
    ///    `/ooooooooo-  -+oo/.`        `-/oo+.
    ///   `+ooooooooo.  :os/`              .+so:
    ///   +sssssssss/  :ss/                 `+ss-
    ///  :ssssssssss`  sss`                  .sso
    ///  ossssssssss  `yyo                    sys
    /// `sssssssssss` `yys                   `yys
    /// `sssssssssss:  +yy/                  +yy:
    ///  oyyyyyyyyyys. `oyy/`              `+yy+
    ///  :yyyyyyyyyyyo. `+yhs:.         `./shy/
    ///   oyyyyyyyyyyys:` .oyhys+:----/+syhy+. `
    ///   `syyyyyyyyyyyyo-` .:osyhhhhhyys+:``.:`
    ///    `oyyyyyyyyyyyyys+-`` `.----.```./oo.
    ///      /yhhhhhhhhhhhhhhyso+//://+osyhy/`
    ///       `/yhhhhhhhhhhhhhhhhhhhhhhhhy/`
    ///         `:oyhhhhhhhhhhhhhhhhhhyo:`
    ///             .:+syhhhhhhhhys+:-`
    ///                 ``....``
    /// ```
    OpenMandriva,
    /// OpenWrt Logo
    /// ```text
    ///  _______
    /// |       |.-----.-----.-----.
    /// |   -   ||  _  |  -__|     |
    /// |_______||   __|_____|__|__|
    ///          |__|
    ///  ________        __
    /// |  |  |  |.----.|  |_
    /// |  |  |  ||   _||   _|
    /// |________||__|  |____|
    /// ```
    OpenWrt,
    /// OpnSense
    /// ```text
    ///     .'''''''''''''''''''''''''''''''''''
    ///   oocc:::::::::::::::::::::::::::::::cox
    ///  ;00;                                o0O
    ///  .,:'  .;;;;;;;;;;;;;;;;;;;;;;;;;;   ;:,
    ///   .',;;cxOOOOOOOOOOOOOOOOOOOOOOOkd:;;,..
    ///      .,cll:'                 ':llc,.
    ///     ,;;:okxdxd:           :dxdxko:;;,
    ///    .xxxx0XNNK0O.         .O0KNNX0xxxx.
    ///        ,cc:,.               .,:cc,
    ///  ........;ccc:;.         .;:ccc;........
    ///  ccccccccccccccc         ccccccccccccccc
    ///  ........;ccc:;.         .;:ccc;........
    ///        ,cc:,.               .,:cc,
    ///    .xxxx0XNNK0O.         .O0KNNX0xxxx.
    ///     ,;;:okxdxd:           :dxdxko:;;,
    ///      .,cll:'                 ':llc,.
    ///   .,,;,ckOOOOOOOOOOOOOOOOOOOOOOOOx;,;,'.
    ///  .:l'  ...........................   ;:;
    ///  lOk'                                cdd
    ///  ;lccccccccccccccccccccccccccccccccccc:.
    /// ```
    OpnSense,
    /// Oracle Logo
    /// ```text
    ///       `-/+++++++++++++++++/-.`
    ///    `/syyyyyyyyyyyyyyyyyyyyyyys/.
    ///   :yyyyo/-...............-/oyyyy/
    ///  /yyys-                     .oyyy+
    /// .yyyy`                       `syyy-
    /// :yyyo                         /yyy/
    /// .yyyy`                       `syyy-
    ///  /yyys.                     .oyyyo
    ///   /yyyyo:-...............-:oyyyy/`
    ///    `/syyyyyyyyyyyyyyyyyyyyyyys+.
    ///      `.:/+ooooooooooooooo+/:.`
    /// ```
    Oracle,
    /// Orchid Logo
    /// ```text
    ///                   .==.
    ///                 .-#@@#-.
    ///               .-##@@@@##-.
    ///             .-##@@@@@@@@##-.
    ///            :*@@@@@####@@@@@*:
    ///          ..:*@@@@==--==@@@@*:..
    ///       .-*%%#==#@@#====#@@#==#%%*-.
    ///     .-#@@@@@##==#@@++@@##==#@@@@@#-.
    ///   .-#@@@@@#@@@#++#====#++#@@@#@@@@@#-.
    /// .-#@@@@@#-==**###+:--:+###**==-#@@@@@#-.
    /// .-#@@@@@#-==**###+:--:+###**==-#@@@@@#-.
    ///   .-#@@@@@#@@@#++#====#++#@@@#@@@@@#-.
    ///     .-#@@@@@##==#@@++@@##==#@@@@@#-.
    ///       .-*%%#==#@@#====#@@#==#%%*-.
    ///          ..:*@@@@==--==@@@@*:..
    ///            :*@@@@@####@@@@@*:
    ///             .-##@@@@@@@@##-.
    ///               .-##@@@@##-.
    ///                 .-#@@#-.
    ///                   .==.
    /// ```
    Orchid,
    /// Small Orchid Logo
    /// ```text
    ///             :##:
    ///           -#@@@@#-
    ///          #@@=..=@@#
    ///          +@@-  -@@+
    ///     -#@@*..*@..@*..*@@#-
    ///   :#@@*+%@= .  . =@%+*@@#:
    /// +@@@:    :-.    .-:   :@@@+
    ///   :#@@*+%@= .  . =@%+*@@#:
    ///     -#@@*..*@..@*..*@@#-
    ///          +@@-  -@@+
    ///          #@@=..=@@#
    ///           -#@@@@#-
    ///             :##:
    /// ```
    OrchidSmall,
    /// OS Elbrus Logo
    /// ```text
    /// ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
    /// ██▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ██   ███████   ███████   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ██   ██   ██   ██
    /// ██   ██   ███████   ███████
    /// ██   ██                  ██
    /// ██   ██▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄██
    /// ██   ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██
    /// ██                       ██
    /// ███████████████████████████
    /// ```
    OsElbrus,
    /// OSMC Logo
    /// ```text
    ///             -+shdmNNNNmdhs+-
    ///         .+hMNho/:..``..:/ohNMh+.
    ///       :hMdo.                .odMh:
    ///     -dMy-                      -yMd-
    ///    sMd-                          -dMs
    ///   hMy       +.            .+       yMh
    ///  yMy        dMs.        .sMd        yMy
    /// :Mm         dMNMs`    `sMNMd        `mM:
    /// yM+         dM//mNs``sNm//Md         +My
    /// mM-         dM:  +NNNN+  :Md         -Mm
    /// mM-         dM: `oNN+    :Md         -Mm
    /// yM+         dM/+NNo`     :Md         +My
    /// :Mm`        dMMNs`       :Md        `mM:
    ///  yMy        dMs`         -ms        yMy
    ///   hMy       +.                     yMh
    ///    sMd-                          -dMs
    ///     -dMy-                      -yMd-
    ///       :hMdo.                .odMh:
    ///         .+hMNho/:..``..:/ohNMh+.
    ///             -+shdmNNNNmdhs+-
    /// ```
    OpenSourceMediaCenter,
    /// PacBSD
    /// ```text
    ///       :+sMs.
    ///   `:ddNMd-                         -o--`
    ///  -sMMMMh:                          `+N+``
    ///  yMMMMMs`     .....-/-...           `mNh/
    ///  yMMMMMmh+-`:sdmmmmmmMmmmmddy+-``./ddNMMm
    ///  yNMMNMMMMNdyyNNMMMMMMMMMMMMMMMhyshNmMMMm
    ///  :yMMMMMMMMMNdooNMMMMMMMMMMMMMMMMNmy:mMMd
    ///   +MMMMMMMMMmy:sNMMMMMMMMMMMMMMMMMMMmshs-
    ///   :hNMMMMMMN+-+MMMMMMMMMMMMMMMMMMMMMMMs.
    ///  .omysmNNhy/+yNMMMMMMMMMMNMMMMMMMMMNdNNy-
    ///  /hMM:::::/hNMMMMMMMMMMMm/-yNMMMMMMN.mMNh`
    /// .hMMMMdhdMMMMMMMMMMMMMMmo  `sMMMMMMN mMMm-
    /// :dMMMMMMMMMMMMMMMMMMMMMdo+  oMMMMMMN`smMNo`
    /// /dMMMMMMMMMMMMMMMMMMMMMNd/` :yMMMMMN:-hMMM.
    /// :dMMMMMMMMMMMMMMMMMMMMMNh`  oMMMMMMNo/dMNN`
    /// :hMMMMMMMMMMMMMMMMMMMMMMNs--sMMMMMMMNNmy++`
    ///  sNMMMMMMMMMMMMMMMMMMMMMMMmmNMMMMMMNho::o.
    ///  :yMMMMMMMMMMMMMNho+sydNNNNNNNmysso/` -//
    ///   /dMMMMMMMMMMMMMs-  ````````..``
    ///    .oMMMMMMMMMMMMNs`               ./y:`
    ///      +dNMMNMMMMMMMmy`          ``./ys.
    ///       `/hMMMMMMMMMMMNo-``    `.+yy+-`
    ///         `-/hmNMNMMMMMMmmddddhhy/-`
    ///             `-+oooyMMMdsoo+/:.
    /// ```
    PacBsd,
    /// Panwah Logo
    /// ```text
    ///          HHH
    ///         HAAAH                             HHH
    ///         HAAAAH                           HAAAH
    ///        HAAAAAAH                         HAAAAH
    ///        HAAAAAAH                       HAAAAAH
    ///       HAAAAAAAAHWWWWWWWWWWWWWWWW      HAAAAAH
    ///       HAAAAAAAAHWWWWWWWWWWWWWWWWWWWW HAAAAAH
    ///       HAAWWWWWWWWWWWWWWWWWWWWWWWWWWWWWAAAAAH
    ///      WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWAH
    ///     WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    ///   WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    ///  WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
    /// WWWWWWWAAAWWWW   WWWWWWWWWWWWWWWWWWWWWWWWWWW
    ///   WWWWAAAWWWWW    WWWWWWW    WWWWWWWWWWWWWWW
    ///    WWAAAWWWWWWWWWWWWWWWWW   WWWWWAAAWWWWWWWW
    ///     AAAWWWWWOOOOOOOOOOOWWWWWWWWWWWAAAWWWWWW
    ///           OOOOGGGGGGGOOOOWWWWWWWWWWAAAWWWW
    ///            OOOGGGGGGGOOOWWWWWWWWWWWWAAAW
    ///              OOOOOOOOO
    /// ```
    Panwah,
    /// Parabola Logo
    /// ```text
    ///                           `.-.    `.
    ///                    `.`  `:++.   `-+o+.
    ///              `` `:+/. `:+/.   `-+oooo+
    ///         ``-::-.:+/. `:+/.   `-+oooooo+
    ///     `.-:///-  ..`   .-.   `-+oooooooo-
    ///  `..-..`                 `+ooooooooo:
    /// ``                        :oooooooo/
    ///                           `ooooooo:
    ///                           `oooooo:
    ///                           -oooo+.
    ///                           +ooo/`
    ///                          -ooo-
    ///                         `+o/.
    ///                         /+-
    ///                        //`
    ///                       -.
    /// ```
    Parabola,
    /// Small Parabola Logo
    /// ```text
    ///   __ __ __  _
    /// .`_//_//_/ / `.
    ///           /  .`
    ///          / .`
    ///         /.`
    ///        /`
    /// ```
    ParabolaSmall,
    /// Parch Logo
    /// ```text
    ///             ,:lodddd.
    ///           .:clooood.
    ///         ;clllooooc
    ///       ;cclllllloo
    ///      .cccccllllll
    ///    .   ,cccclllll
    ///   ':::;; ccccclll;
    ///  .:::cccccccccccll;
    ///  ;::::ccccllllllcll:
    /// .;::::cccclllloool::;
    /// ;;;::::cccclllolc::::;.
    /// ;;;::::cccclllccc:::::;.
    /// ;;;::::cccclccccc::::::;.
    /// ;;;;::::::llcccccc:::::'
    /// ;;;;:; ,clllccccccc::
    /// .;;  .cllllllcccccc::;::::'
    ///     .'''''''''',:lddoooolll
    ///    '.....'''',cdddooooollll
    ///   ........':oddddoooolllllc
    ///    ....';ldddddooooolllllc:
    ///      ,cdddddddooooollllccc
    ///       :ddddddoooolllllccc
    ///         ;ddooooolllllcc.
    ///            :ooollllc.
    ///                c'
    /// ```
    Parch,
    /// Pardus Logo
    /// ```text
    ///  .smNdy+-    `.:/osyyso+:.`    -+ydmNs.
    /// /Md- -/ymMdmNNdhso/::/oshdNNmdMmy/. :dM/
    /// mN.     oMdyy- -y          `-dMo     .Nm
    /// .mN+`  sMy hN+ -:             yMs  `+Nm.
    ///  `yMMddMs.dy `+`               sMddMMy`
    ///    +MMMo  .`  .                 oMMM+
    ///    `NM/    `````.`    `.`````    +MN`
    ///    yM+   `.-:yhomy    ymohy:-.`   +My
    ///    yM:          yo    oy          :My
    ///    +Ms         .N`    `N.      +h sM+
    ///    `MN      -   -::::::-   : :o:+`NM`
    ///     yM/    sh   -dMMMMd-   ho  +y+My
    ///     .dNhsohMh-//: /mm/ ://-yMyoshNd`
    ///       `-ommNMm+:/. oo ./:+mMNmmo:`
    ///      `/o+.-somNh- :yy: -hNmos-.+o/`
    ///     ./` .s/`s+sMdd+``+ddMs+s`/s. `/.
    ///         : -y.  -hNmddmNy.  .y- :
    ///          -+       `..`       +-
    /// ```
    Pardus,
    /// Parrot Logo
    /// ```text
    ///   `:oho/-`
    /// `mMMMMMMMMMMMNmmdhy-
    ///  dMMMMMMMMMMMMMMMMMMs`
    ///  +MMsohNMMMMMMMMMMMMMm/
    ///  .My   .+dMMMMMMMMMMMMMh.
    ///   +       :NMMMMMMMMMMMMNo
    ///            `yMMMMMMMMMMMMMm:
    ///              /NMMMMMMMMMMMMMy`
    ///               .hMMMMMMMMMMMMMN+
    ///                   ``-NMMMMMMMMMd-
    ///                      /MMMMMMMMMMMs`
    ///                       mMMMMMMMsyNMN/
    ///                       +MMMMMMMo  :sNh.
    ///                       `NMMMMMMm     -o/
    ///                        oMMMMMMM.
    ///                        `NMMMMMM+
    ///                         +MMd/NMh
    ///                          mMm -mN`
    ///                          /MM  `h:
    ///                           dM`   .
    ///                           :M-
    ///                            d:
    ///                            -+
    ///                             -
    /// ```
    Parrot,
    /// Parsix Logo
    /// ```text
    ///                  -/+/:.
    ///                .syssssys.
    ///        .--.    ssssssssso   ..--.
    ///      :++++++:  +ssssssss+ ./++/+++:
    ///     /+++++++++..yssooooy`-+///////o-
    ///     /++++++++++.+soooos::+////////+-
    ///      :+++++////o-oooooo-+/////////-
    ///       `-/++//++-.-----.-:+/////:-
    ///   -://::---:/:.--.````.--.:::---::::::.
    /// -/:::::::://:.:-`      `-:`:/:::::::--/-
    /// /::::::::::/---.        .-.-/://///::::/
    /// -/:::::::::/:`:-.      .-:`:///////////-
    ///  `-::::--.-://.---....---`:+/:---::::-`
    ///        -/+///+o/-.----..:oo+++o+.
    ///      -+/////+++o:syyyyy.o+++++++++:
    ///     .+////+++++-+sssssy+.++++++++++\
    ///     .+:/++++++..yssssssy-`+++++++++:
    ///      :/+++++-  +sssssssss  -++++++-
    ///        `--`    +sssssssso    `--`
    ///                 +sssssy+`
    ///                  `.::-`
    /// ```
    Parsix,
    /// PCBSD Logo
    /// ```text
    ///                        ..
    ///                         s.
    ///                         +y
    ///                         yN
    ///                        -MN  `.
    ///                       :NMs `m
    ///                     .yMMm` `No
    ///             `-/+++sdMMMNs+-`+Ms
    ///         `:oo+-` .yMMMMy` `-+oNMh
    ///       -oo-     +NMMMM/       oMMh-
    ///     .s+` `    oMMMMM/     -  oMMMhy.
    ///    +s`- ::   :MMMMMd     -o `mMMMy`s+
    ///   y+  h .Ny+oNMMMMMN/    sh+NMMMMo  +y
    ///  s+ .ds  -NMMMMMMMMMMNdhdNMMMMMMh`   +s
    /// -h .NM`   `hMMMMMMMMMMMMMMNMMNy:      h-
    /// y- hMN`     hMMmMMMMMMMMMNsdMNs.      -y
    /// m` mMMy`    oMMNoNMMMMMMo`  sMMMo     `m
    /// m` :NMMMdyydMMMMo+MdMMMs     sMMMd`   `m
    /// h-  `+ymMMMMMMMM--M+hMMN/    +MMMMy   -h
    /// :y     `.sMMMMM/ oMM+.yMMNddNMMMMMm   y:
    ///  y:   `s  dMMN- .MMMM/ :MMMMMMMMMMh  :y
    ///  `h:  `mdmMMM/  yMMMMs  sMMMMMMMMN- :h`
    ///    so  -NMMMN   /mmd+  `dMMMMMMMm- os
    ///     :y: `yMMM`       `+NMMMMMMNo`:y:
    ///       /s+`.omy      /NMMMMMNh/.+s:
    ///         .+oo:-.     /mdhs+::oo+.
    ///             -/o+++++++++++/-
    /// ```
    Pcbsd,
    /// PCLinuxOS Logo
    /// ```text
    ///             mhhhyyyyhhhdN
    ///         dyssyhhhhhhhhhhhssyhN
    ///      Nysyhhyo/:-.....-/oyhhhssd
    ///    Nsshhy+.              `/shhysm
    ///   dohhy/                    -shhsy
    ///  dohhs`                       /hhys
    /// N+hho   +ssssss+-   .+syhys+   /hhsy
    /// ohhh`   ymmo++hmm+`smmy/::+y`   shh+
    /// +hho    ymm-  /mmy+mms          :hhod
    /// /hh+    ymmhhdmmh.smm/          .hhsh
    /// +hhs    ymm+::-`  /mmy`    `    /hh+m
    /// yyhh-   ymm-       /dmdyosyd`  `yhh+
    ///  ohhy`  ://`         -/+++/-   ohhom
    ///  N+hhy-                      `shhoh
    ///    sshho.                  `+hhyom
    ///     dsyhhs/.            `:ohhhoy
    ///       dysyhhhso///://+syhhhssh
    ///          dhyssyhhhhhhyssyyhN
    ///               mddhdhdmN
    /// ```
    PcLinuxOs,
    /// PearOS Logo
    /// ```text
    ///                   .+yh
    ///                  sMMMo
    ///                 sMMN+
    ///                 +o:
    ///            ./oyyys+.
    ///          :dMMMMMMMMMm/
    ///         :MMMMMMMMMMMMMy
    ///         yMMMMMMMMMMMMMN
    ///         mMMMMMMMMMMMMs`
    ///        yMMMMMMMMMMMMo
    ///      -mMMMMMMMMMMMMM`
    ///     oMMMMMMMMMMMMMMM`
    ///    oMMMMMMMMMMMMMMMMy
    ///   .MMMMMMMMMMMMMMMMMMy`
    ///   +MMMMMMMMMMMMMMMMMMMMy/`
    ///   /MMMMMMMMMMMMMMMMMMMMMMMNds
    ///   `mMMMMMMMMMMMMMMMMMMMMMMMM/
    ///    .mMMMMMMMMMMMMMMMMMMMMMM+
    ///     `oNMMMMMMMMMMMMMMMMMMd-
    ///       `+hMMMMMMMMMMMMMms-
    ///           -/osyhhyso:.
    /// ```
    PearOs,
    /// Pengwin Logo
    /// ```text
    ///               ...`
    ///               `-///:-`
    ///                 .+ssys/
    ///                  +yyyyyo    
    ///                  -yyyyyy:
    ///     `.:/+ooo+/:` -yyyyyy+
    ///   `:oyyyyyys+:-.`syyyyyy:
    ///  .syyyyyyo-`   .oyyyyyyo
    /// `syyyyyy   `-+yyyyyyy/`
    /// /yyyyyy+ -/osyyyyyyo/.
    /// +yyyyyy-  `.-:::-.`
    /// .yyyyyy-
    ///  :yyyyyo
    ///   .+ooo+
    ///     `.::/:.
    /// ```
    Pengwin,
    /// Pentoo Logo
    /// ```text
    ///            `:oydNNMMMMNNdyo:`
    ///         :yNMMMMMMMMMMMMMMMMNy:
    ///       :dMMMMMMMMMMMMMMMMMMMMMMd:
    ///      oMMMMMMMho/-....-/ohMMMMMMMo
    ///     oMMMMMMy.            .yMMMMMMo
    ///    .MMMMMMo                oMMMMMM.
    ///    +MMMMMm                  mMMMMM+
    ///    oMMMMMh                  hMMMMMo
    ///  //hMMMMMm//`          `////mMMMMMh//
    /// MMMMMMMMMMM/      /o/`  .smMMMMMMMMMMM
    /// MMMMMMMMMMm      `NMN:    .yMMMMMMMMMM
    /// MMMMMMMMMMMh:.              dMMMMMMMMM
    /// MMMMMMMMMMMMMy.            -NMMMMMMMMM
    /// MMMMMMMMMMMd:`           -yNMMMMMMMMMM
    /// MMMMMMMMMMh`          ./hNMMMMMMMMMMMM
    /// MMMMMMMMMMs        .:ymMMMMMMMMMMMMMMM
    /// MMMMMMMMMMNs:..-/ohNMMMMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    ///  MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    /// ```
    Pentoo,
    /// Peppermint Logo
    /// ```text
    ///              PPPPPPPPPPPPPP
    ///          PPPPMMMMMMMPPPPPPPPPPP
    ///        PPPPMMMMMMMMMMPPPPPPPPMMPP
    ///      PPPPPPPPMMMMMMMPPPPPPPPMMMMMPP
    ///    PPPPPPPPPPPPMMMMMMPPPPPPPMMMMMMMPP
    ///   PPPPPPPPPPPPMMMMMMMPPPPMPMMMMMMMMMPP
    ///  PPMMMMPPPPPPPPPPMMMPPPPPMMMMMMMPMMPPPP
    ///  PMMMMMMMMMMPPPPPPMMPPPPPMMMMMMPPPPPPPP
    /// PMMMMMMMMMMMMPPPPPMMPPMPMMPMMPPPPPPPPPPP
    /// PMMMMMMMMMMMMMMMMPPMPMMMPPPPPPPPPPPPPPPP
    /// PMMMPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPMMMMMP
    /// PPPPPPPPPPPPPPPPMMMPMPMMMMMMMMMMMMMMMMPP
    /// PPPPPPPPPPPMMPMMPPPPMMPPPPPMMMMMMMMMMMPP
    ///  PPPPPPPPMMMMMMPPPPPMMPPPPPPMMMMMMMMMPP
    ///  PPPPMMPMMMMMMMPPPPPPMMPPPPPPPPPPMMMMPP
    ///   PPMMMMMMMMMPMPPPPMMMMMMPPPPPPPPPPPPP
    ///    PPMMMMMMMPPPPPPPMMMMMMPPPPPPPPPPPP
    ///      PPMMMMPPPPPPPPPMMMMMMMPPPPPPPP
    ///        PPMMPPPPPPPPMMMMMMMMMMPPPP
    ///          PPPPPPPPPPMMMMMMMMPPPP
    ///              PPPPPPPPPPPPPP
    /// ```
    Peppermint,
    /// Peropesis Logo
    /// ```text
    /// ####  #### ####   ###   ####  ####  #### #  ####
    /// #   # #    #   # #   #  #   # #     #    #  #
    /// ####  ###  #### #     # ####  ###     #  #    #
    /// #     #    #  #  #   #  #     #        # #     #
    /// #     #### #   #  ###   #     #### ####  # ####
    /// ```
    Peropesis,
    /// PhyOS Logo
    /// ```text
    /// .^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^.^^^^^.
    ///  :777777777777777777777777777777^~7777:
    ///   .~~~~~~~~~~~~~~~~~~~~~^~7777!:!777!.
    ///     ~7!!!!!!!!!!!!!!!!!^:!777~^!777~
    ///      ^77777!!!!!!!!!7!^^7777^^7777^
    ///       ^7777~.~~~~^.  .~7777^~7777:
    ///        :!777~^!777~. !777!:~777!:
    ///         .!777!:~777!:~77~:!777!.
    ///           ~777!^~7777:^~^!777~
    ///            ^7777^^7777^^7777^
    ///             :7777~^!7777777:
    ///              .!777!:!7777!.
    ///               .~777!:~77~.
    ///                 ~7777^~~
    ///                  ^7777.
    ///                   :77:
    ///                    ..
    /// ```
    PhyOs,
    /// PikaOS Logo
    /// ```text
    ///                   '',,,               ,,,d,
    ///               ',,                           ,,'
    ///            ',                                   ,.
    ///          .,                                       '
    ///         .                                           .
    ///       '                                               .
    ///     ..                   oddddkdlc:;,..                ..
    ///    .                        ............lllc,            .
    ///   .                       ....................:           .
    ///  .                        .  ....................
    ///       '.                     ..........'o........d0XX0.
    ///      ....lllllllcOOOcllllll............cxlxc...;okkkx.
    ///      ..................................';lc'...lo.
    ///     .'''''''''''''.....................,;,.......
    ///     ',,,,,,,,,,,,,,,,''...............dkkkd......
    ///     ',,,,,,,,,,,,,,,,,,,'............;okkkd;....
    ///     .,,,,,,,,,,,,,,,,,,,,,............;cll;.....
    ///       ,,,,,,,,,,,,,,,,,,,,'....................:d,
    ///        ,,,,,,,,,,,,,,,,,,,....................oxxx:
    ///         .,,,,,,,,,,,,,,,,,'..................oxxxxx.  .
    ///          .,,,,,,,,,,,,,,'..........        ,oxxxxxxx .
    ///         .;,,,,,,,,,,,,'..                'cxxxxxxxxx,
    ///          :dl:,'',,'....               .;lxxxxxxxxxd;
    ///           ,codol:;'.           ...,;cldxxxxxxxxxoc.
    ///             .:cxxxxxdlccccc:ccldxxxxxxxxxxxxxx::.
    ///               .'::dxxxxxxxxxxxxxxxxxxxxxxxd::'.
    ///                  ..,::cdxxxxxxxxxxxxxdc::,..
    ///                       ...,;:::::::;,...
    /// ```
    PikaOs,
    /// Pisi Logo
    /// ```text
    ///    \Fv/!-                      `:?lzC
    ///    Q!::=zFx!  `;v6WBCicl;`  ,vCC\!::#.
    ///   ,%:::,'` +#%@@FQ@@.   ,cF%i``-',::a?
    ///   +m:,'```}3,/@@Q\@@       "af- `-'"7f
    ///   =o'.` /m'   :Q@:Qg         ,kl  `.|o
    ///   :k` '+      'Narm           >d,  ii
    ///    #`!p.        `C ,            'd+ %'
    ///    !0m                           `6Kv
    ///    =a                              m+
    ///   !A     !\L|:            :|L\!     :
    ///  .8`     Q''%Q#'        '#Q%''Q     `0-
    ///  :6      E|.6QQu        uQQ6.|E      p:
    ///   i{      \jts9?        ?9stj\      u\
    ///    |a`            -''.            `e>
    ///     ,m+     '^ !`s@@@@a'"`+`     >e'
    ///       !3|`|=>>r-  'U%:  '>>>=:`\3!
    ///        'xopE|      `'     `ledoz-
    ///     `;=>>+``^llci/|==|/iclc;`'>>>>:
    ///    `^`+~          ````          !!-^
    /// ```
    Pisi,
    /// PNM Linux Logo
    /// ```text
    ///                ``.---..` `--`
    ///             ``.---........-:.-::`
    ///            ./::-........--::.````
    ///           .:://:::----::::-..
    ///           ..--:::::--::::++-.`
    ///   `-:-`   .-ohy+::-:::/sdmdd:.   `-:-
    ///    .-:::...sNNmdsyo/+sy+NNmd+.`-:::-.
    ///      `.-:-./dN()yyooosd()mdy-.::-.`
    ///       `.-...-+hNdyyyyyydmy:......`
    ///  ``..--.....-yNNmhsssshmmdo.........```
    /// `-:://:.....hNNNNNmddmNNNmds.....//::--`
    ///   ```.:-...oNNNNNNNNNNNNNNmd/...:-.```
    ///       .....hNNNNNNNNNNNNNNmds....`
    ///       --...hNNNNNNNNNNNNNNmdo.....
    ///       .:.../NNNNNNNNNNNNNNdd:....`
    ///        `-...+mNNNNNNNNNNNmh:...-.
    ///      .:+o+/:-:+oo+///++o+/:-:/+ooo/:.
    ///        +oo/:o-            +oooooso.`
    ///        .`   `             `/  .-//-
    /// ```
    PnmLinux,
    /// Pop Logo
    /// ```text
    ///              /////////////
    ///          /////////////////////
    ///       ///////*767////////////////
    ///     //////7676767676*//////////////
    ///    /////76767//7676767//////////////
    ///   /////767676///*76767///////////////
    ///  ///////767676///76767.///7676*///////
    /// /////////767676//76767///767676////////
    /// //////////76767676767////76767/////////
    /// ///////////76767676//////7676//////////
    /// ////////////,7676,///////767///////////
    /// /////////////*7676///////76////////////
    /// ///////////////7676////////////////////
    ///  ///////////////7676///767////////////
    ///   //////////////////////'////////////
    ///    //////.7676767676767676767,//////
    ///     /////767676767676767676767/////
    ///       ///////////////////////////
    ///          /////////////////////
    ///              /////////////
    /// ```
    Pop,
    /// Small Pop Logo
    /// ```text
    /// ______
    /// \   _ \        __
    ///  \ \ \ \      / /
    ///   \ \_\ \    / /
    ///    \  ___\  /_/
    ///     \ \    _
    ///    __\_\__(_)_
    ///   (___________)`
    /// ```
    PopSmall,
    /// Porteus Logo
    /// ```text
    ///              `.-:::-.`
    ///          -+ydmNNNNNNNmdy+-
    ///       .+dNmdhs+//////+shdmdo.
    ///     .smmy+-`             ./sdy:
    ///   `omdo.    `.-/+osssso+/-` `+dy.
    ///  `yms.   `:shmNmdhsoo++osyyo-``oh.
    ///  hm/   .odNmds/.`    ``.....:::-+s
    /// /m:  `+dNmy:`   `./oyhhhhyyooo++so
    /// ys  `yNmy-    .+hmmho:-.`     ```
    /// s:  yNm+`   .smNd+.
    /// `` /Nm:    +dNd+`
    ///    yN+   `smNy.
    ///    dm    oNNy`
    ///    hy   -mNm.
    ///    +y   oNNo
    ///    `y`  sNN:
    ///     `:  +NN:
    ///      `  .mNo
    ///          /mm`
    ///           /my`
    ///            .sy`
    ///              .+:
    ///                 `
    /// ```
    Porteus,
    /// PostMarketOS Logo
    /// ```text
    ///                  /\
    ///                 /  \
    ///                /    \
    ///               /      \
    ///              /        \
    ///             /          \
    ///             \           \
    ///           /\ \____       \
    ///          /  \____ \       \
    ///         /       /  \       \
    ///        /       /    \    ___\
    ///       /       /      \  / ____
    ///      /       /        \/ /    \
    ///     /       / __________/      \
    ///    /        \ \                 \
    ///   /          \ \                 \
    ///  /           / /                  \
    /// /___________/ /____________________\
    /// ```
    PostMarketOs,
    /// Small PostMarketOS Logo
    /// ```text
    ///         /\
    ///        /  \
    ///       /    \
    ///       \__   \
    ///     /\__ \  _\
    ///    /   /  \/ __
    ///   /   / ____/  \
    ///  /    \ \       \
    /// /_____/ /________\
    /// ```
    PostMarketOsSmall,
    /// Proxmox Logo
    /// ```text
    ///          .://:`              `://:.
    ///        `hMMMMMMd/          /dMMMMMMh`
    ///         `sMMMMMMMd:      :mMMMMMMMs`
    /// `-/+oo+/:`.yMMMMMMMh-  -hMMMMMMMy.`:/+oo+/-`
    /// `:oooooooo/`-hMMMMMMMyyMMMMMMMh-`/oooooooo:`
    ///   `/oooooooo:`:mMMMMMMMMMMMMm:`:oooooooo/`
    ///     ./ooooooo+- +NMMMMMMMMN+ -+ooooooo/.
    ///       .+ooooooo+-`oNMMMMNo`-+ooooooo+.
    ///         -+ooooooo/.`sMMs`./ooooooo+-
    ///           :oooooooo/`..`/oooooooo:
    ///           :oooooooo/`..`/oooooooo:
    ///         -+ooooooo/.`sMMs`./ooooooo+-
    ///       .+ooooooo+-`oNMMMMNo`-+ooooooo+.
    ///     ./ooooooo+- +NMMMMMMMMN+ -+ooooooo/.
    ///   `/oooooooo:`:mMMMMMMMMMMMMm:`:oooooooo/`
    /// `:oooooooo/`-hMMMMMMMyyMMMMMMMh-`/oooooooo:`
    /// `-/+oo+/:`.yMMMMMMMh-  -hMMMMMMMy.`:/+oo+/-`
    ///         `sMMMMMMMm:      :dMMMMMMMs`
    ///        `hMMMMMMd/          /dMMMMMMh`
    ///          `://:`              `://:`
    /// ```
    Proxmox,
    /// PuffOS Logo
    /// ```text
    ///               _,..._,m,
    ///             ,/'      '"";
    ///            /             ".
    ///          ,'mmmMMMMmm.      \
    ///        _/-"^^^^^"""%#%mm,   ;
    ///  ,m,_,'              "###)  ;,
    /// (###%                 \#/  ;##mm.
    ///  ^#/  __        ___    ;  (######)
    ///   ;  //.\\     //.\\   ;   \####/
    ///  _; (#\"//     \\"/#)  ;  ,/
    /// @##\ \##/   =   `"=" ,;mm/
    /// `\##>.____,...,____,<####@
    /// ```
    PuffOs,
    /// Puppy Logo
    /// ```text
    ///            `-/osyyyysosyhhhhhyys+-
    ///   -ohmNNmh+/hMMMMMMMMNNNNd+dMMMMNM+
    ///  yMMMMNNmmddo/NMMMNNNNNNNNNo+NNNNNy
    /// .NNNNNNmmmddds:MMNNNNNNNNNNNh:mNNN/
    /// -NNNdyyyhdmmmd`dNNNNNmmmmNNmdd/os/
    /// .Nm+shddyooo+/smNNNNmmmmNh.   :mmd.
    ///  NNNNy:`   ./hmmmmmmmNNNN:     hNMh
    ///  NMN-    -++- +NNNNNNNNNNm+..-sMMMM-
    /// .MMo    oNNNNo hNNNNNNNNmhdNNNMMMMM+
    /// .MMs    /NNNN/ dNmhs+:-`  yMMMMMMMM+
    ///  mMM+     .. `sNN+.      hMMMMhhMMM-
    ///  +MMMmo:...:sNMMMMMms:` hMMMMm.hMMy
    ///   yMMMMMMMMMMMNdMMMMMM::/+o+//dMMd`
    ///    sMMMMMMMMMMN+:oyyo:sMMMNNMMMNy`
    ///     :mMMMMMMMMMMMmddNMMMMMMMMmh/
    ///       /dMMMMMMMMMMMMMMMMMMNdy/`
    ///         .+hNMMMMMMMMMNmdhs/.
    ///             .:/+ooo+/:-.
    /// ```
    Puppy,
    /// PureOS Logo
    /// ```text
    /// dmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmd
    /// dNm//////////////////////////////////mNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNd                                  dNd
    /// dNm//////////////////////////////////mNd
    /// dmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmd
    /// ```
    PureOs,
    /// Small PureOS Logo
    /// ```text
    ///  _____________
    /// |  _________  |
    /// | |         | |
    /// | |         | |
    /// | |_________| |
    /// |_____________|
    /// ```
    PureOsSmall,
    /// Q4DOS Logo
    /// ```text
    ///            .:*****  :=====.
    ///         .:********  :========.
    ///       .***********  :===========.
    ///     .:************  :============-
    ///    .**************  :==============
    ///   :***************  :===============
    ///  :**************.    :===============
    /// .*************:        .=============.
    /// *************.          .============:
    ///
    /// :############.          :==:
    /// :##############.      :======:
    ///  :################  .==========:
    ///   :###############   .===========:
    ///    :##############     .===========:
    ///     :#############       .=========:
    ///       :###########         .=====:
    ///         .#########           .=:
    ///             .#####
    /// ```
    Q4Dos,
    /// Qubes Logo
    /// ```text
    ///                `..--..`
    ///             `.----------.`
    ///         `..----------------..`
    ///      `.------------------------.``
    ///  `..-------------....-------------..`
    /// .::----------..``    ``..----------:+:
    /// :////:----..`            `..---:/ossso
    /// :///////:`                  `/osssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :///////:                    /ssssssso
    /// :////////-`                .:sssssssso
    /// :///////////-.`        `-/osssssssssso
    /// `//////////////:-```.:+ssssssssssssso-
    ///   .-://////////////sssssssssssssso/-`
    ///      `.:///////////sssssssssssssso:.
    ///          .-:///////ssssssssssssssssss/`
    ///             `.:////ssss+/+ssssssssssss.
    ///                 `--//-    `-/osssso/.
    /// ```
    Qubes,
    /// Qubyt Logo
    /// ```text
    ///     ########################(ooo
    ///     ########################(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo                  ###(ooo
    /// ###(ooo           ##o    ((((ooo
    /// ###(ooo          o((###   oooooo
    /// ###(ooo           oo((###o
    /// ###(ooo             ooo((###
    /// ################(oo    oo((((o
    /// (((((((((((((((((ooo     ooooo
    ///   oooooooooooooooooo        o
    /// ```
    Qubyt,
    /// Quibian Logo
    /// ```text
    ///             `.--::::::::--.`
    ///         `.-:::-..``   ``..-::-.`
    ///       .::::-`   .+:``       `.-::.`
    ///     .::::.`    -::::::-`       `.::.
    ///   `-:::-`    -:::::::::--..``     .::`
    ///  `::::-     .oy:::::::---.```.:    `::`
    ///  -::::  `.-:::::::::::-.```         `::
    /// .::::.`-:::::::::::::.               `:.
    /// -::::.:::::::::::::::                 -:
    /// ::::::::::::::::::::`                 `:
    /// :::::::::::::::::::-                  `:
    /// :::::::::::::::::::                   --
    /// .:::::::::::::::::`                  `:`
    /// `:::::::::::::::::                   -`
    ///  .:::::::::::::::-                  -`
    ///   `::::::::::::::-                `.`
    ///     .::::::::::::-               ``
    ///       `.--:::::-.
    /// ```
    Quibian,
    /// Radix Logo
    /// ```text
    ///                 .:oyhdmNo
    ///              `/yhyoosdms`
    ///             -o+/ohmmho-
    ///            ..`.:/:-`
    ///      `.--:::-.``
    ///   .+ydNMMMMMMNmhs:`
    /// `omMMMMMMMMMMMMMMNh-
    /// oNMMMNmddhhyyhhhddmy.
    /// mMMMMNmmddhhysoo+/:-`
    /// yMMMMMMMMMMMMMMMMNNh.
    /// -dmmmmmNNMMMMMMMMMMs`
    ///  -+oossyhmMMMMMMMMd-
    ///  `sNMMMMMMMMMMMMMm:
    ///   `yMMMMMMNmdhhhh:
    ///    `sNMMMMMNmmho.
    ///     `+mMMMMMMMy.
    ///       .yNMMMm+`
    ///        `:yd+.
    /// ```
    Radix,
    /// Raspbian Logo
    /// ```text
    ///    `.::///+:/-.        --///+//-:`
    ///  `+oooooooooooo:   `+oooooooooooo:
    ///   /oooo++//ooooo:  ooooo+//+ooooo.
    ///   `+ooooooo:-:oo-  +o+::/ooooooo:
    ///    `:oooooooo+``    `.oooooooo+-
    ///      `:++ooo/.        :+ooo+/.`
    ///         ...`  `.----.` ``..
    ///      .::::-``:::::::::.`-:::-`
    ///     -:::-`   .:::::::-`  `-:::-
    ///    `::.  `.--.`  `` `.---.``.::`
    ///        .::::::::`  -::::::::` `
    ///  .::` .:::::::::- `::::::::::``::.
    /// -:::` ::::::::::.  ::::::::::.`:::-
    /// ::::  -::::::::.   `-::::::::  ::::
    /// -::-   .-:::-.``....``.-::-.   -::-
    ///  .. ``       .::::::::.     `..`..
    ///    -:::-`   -::::::::::`  .:::::`
    ///    :::::::` -::::::::::` :::::::.
    ///    .:::::::  -::::::::. ::::::::
    ///     `-:::::`   ..--.`   ::::::.
    ///       `...`  `...--..`  `...`
    ///             .::::::::::
    ///              `.-::::-`
    /// ```
    Raspbian,
    /// Small Raspbian Logo
    /// ```text
    ///    .~~.   .~~.
    ///   '. \ ' ' / .'
    ///    .~ .~~~..~.
    ///   : .~.'~'.~. :
    ///  ~ (   ) (   ) ~
    /// ( : '~'.~.'~' : )
    ///  ~ .~ (   ) ~. ~
    ///   (  : '~' :  )
    ///    '~ .~~~. ~'
    ///        '~'
    /// ```
    RaspbianSmall,
    /// RavynOS Logo
    /// ```text
    ///                 ..oooo..
    ///            .oo.
    ///         odo
    ///       oo
    ///     ..
    ///    d********b
    ///   d*            °****?b
    ///   *                     °
    ///  d**                     .oob
    ///  *°                     o
    ///                       o
    ///                     oP
    ///                     *
    ///                       ?P
    ///                        P
    ///                        P
    ///                       ?*
    ///                        *°
    ///                       d*°
    ///                        °
    /// ```
    RavynOs,
    /// Reborn Logo
    /// ```text
    ///           .======================.          
    ///          .##*********%%*********#%:         
    ///         :%#**********%%**********#%-        
    ///        -%************%%************%=       
    ///       +%******%%#####%%#####%%******%+      
    ///      *%%#****%#+=====%%=====+#%****#%%*     
    ///     *%*##%%#%#====+++%%+++====#%#%%##*##.   
    ///   .##*****#%%%#*++%######%*+*#%%%#*****#%.  
    ///  :%#*****#%*=+*#%%*++++++*%%#*+=*%#*****#%:
    /// -%#*****#%+====*%*++++++++*%#====+%#******%-
    /// -%#*****#%+====*%*++++++++*%#====+%#******%=
    ///  :%#*****#%*=+*#%%*++++++*%%#*+=*%#*****#%-
    ///   .##*****#%%%#*+*%######%*+*#%%%#*****#%:  
    ///    .##**#%%#%#====+++%%+++====#%#%%##*##.   
    ///      *%%#****%#+=====%%=====+#%****#%%*     
    ///       +%******%%#####%%#####%%******%*      
    ///        -%************%%************%=       
    ///         :%#**********%%**********#%-        
    ///          :%#*********%%*********#%:         
    ///           .======================.   
    /// ```
    Reborn,
    /// Small Reborn
    /// ```text
    ///    _______   
    ///   /\_____/\  
    ///  / /\___/\ \
    /// /_/_/   \_\_\
    /// \ \ \___/ / /
    ///  \ \/___\/ /
    ///   \/_____\/  
    /// ```
    RebornSmall,
    /// RedCore Logo
    /// ```text
    ///                  RRRRRRRRR
    ///                RRRRRRRRRRRRR
    ///         RRRRRRRRRR      RRRRR
    ///    RRRRRRRRRRRRRRRRRRRRRRRRRRR
    ///  RRRRRRR  RRR         RRR RRRRRRRR
    /// RRRRR    RR                 RRRRRRRRR
    /// RRRR    RR     RRRRRRRR      RR RRRRRR
    /// RRRR   R    RRRRRRRRRRRRRR   RR   RRRRR
    /// RRRR   R  RRRRRRRRRRRRRRRRRR  R   RRRRR
    /// RRRR     RRRRRRRRRRRRRRRRRRR  R   RRRR
    ///  RRR     RRRRRRRRRRRRRRRRRRRR R   RRRR
    ///   RRR    RRRRRRRRRRRRRRRRRRRR    RRRR
    ///     RR   RRRRRRRRRRRRRRRRRRR    RRR
    ///      RR   RRRRRRRRRRRRRRRRR    RRR
    ///        RR   RRRRRRRRRRRRRR   RR
    ///          R       RRRR      RR
    /// ```
    RedCore,
    /// Red Hat Enterprise Linux Logo
    /// ```text
    ///            .MMM..:MMMMMMM
    ///           MMMMMMMMMMMMMMMMMM
    ///           MMMMMMMMMMMMMMMMMMMM.
    ///          MMMMMMMMMMMMMMMMMMMMMM
    ///         ,MMMMMMMMMMMMMMMMMMMMMM:
    ///         MMMMMMMMMMMMMMMMMMMMMMMM
    ///   .MMMM'  MMMMMMMMMMMMMMMMMMMMMM
    ///  MMMMMM    `MMMMMMMMMMMMMMMMMMMM.
    /// MMMMMMMM      MMMMMMMMMMMMMMMMMM .
    /// MMMMMMMMM.       `MMMMMMMMMMMMM' MM.
    /// MMMMMMMMMMM.                     MMMM
    /// `MMMMMMMMMMMMM.                 ,MMMMM.
    ///  `MMMMMMMMMMMMMMMMM.          ,MMMMMMMM.
    ///     MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    ///       MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:
    ///          MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    ///             `MMMMMMMMMMMMMMMMMMMMMMMM:
    ///                 ``MMMMMMMMMMMMMMMMM'
    /// ```
    RedHatEnterpriseLinux,
    /// Old Red Hat Enterprise Linux Logo
    /// ```text
    ///              `.-..........`
    ///             `////////::.`-/.
    ///             -: ....-////////.
    ///             //:-::///////////`
    ///      `--::: `-://////////////:
    ///      //////-    ``.-:///////// .`
    ///      `://////:-.`    :///////::///:`
    ///        .-/////////:---/////////////:
    ///           .-://////////////////////.
    ///          yMN+`.-::///////////////-`
    ///       .-`:NMMNMs`  `..-------..`
    ///        MN+/mMMMMMhoooyysshsss
    /// MMM    MMMMMMMMMMMMMMyyddMMM+
    ///  MMMM   MMMMMMMMMMMMMNdyNMMh`     hyhMMM
    ///   MMMMMMMMMMMMMMMMyoNNNMMM+.   MMMMMMMM
    ///    MMNMMMNNMMMMMNM+ mhsMNyyyyMNMMMMsMM
    /// ```
    RedHatEnterpriseLinuxOld,
    /// Redstar OS Logo
    /// ```text
    ///                     ..
    ///                   .oK0l
    ///                  :0KKKKd.
    ///                .xKO0KKKKd
    ///               ,Od' .d0000l
    ///              .c;.   .'''...           ..'.
    /// .,:cloddxxxkkkkOOOOkkkkkkkkxxxxxxxxxkkkx:
    /// ;kOOOOOOOkxOkc'...',;;;;,,,'',;;:cllc:,.
    ///  .okkkkd,.lko  .......',;:cllc:;,,'''''.
    ///    .cdo. :xd' cd:.  ..';'',,,'',,;;;,'.
    ///       . .ddl.;doooc'..;oc;'..';::;,'.
    ///         coo;.oooolllllllcccc:'.  .
    ///        .ool''lllllccccccc:::::;.
    ///        ;lll. .':cccc:::::::;;;;'
    ///        :lcc:'',..';::::;;;;;;;,,.
    ///        :cccc::::;...';;;;;,,,,,,.
    ///        ,::::::;;;,'.  ..',,,,'''.
    ///         ........          ......
    /// ```
    RedstarOs,
    /// Refracted Devuan Logo
    /// ```text
    ///                              A
    ///                             VW
    ///                            VVW\\
    ///                          .yWWW\\
    ///  ,;,,u,;yy;;v;uyyyyyyy  ,WWWWW^
    ///     *WWWWWWWWWWWWWWWW/  VWWWWw      ,
    ///         ^*%WWWWWWVWWX  WWWW**    ,yy
    ///         ,    "**WWW/' **'   ,yy/WWW*`
    ///        &WWWWwy    `*`  <,ywWW%VWWW*
    ///      yWWWWWWWWWW*    .,   "**WW%W
    ///    ,&WWWWWM*"`  ,y/  &WWWww   ^*
    ///   XWWX*^   ,yWWWW09 .WWWWWWWWwy,
    ///  *`        &WWWWWM  WWWWWWWWWWWWWww,
    ///            (WWWWW` /#####WWW***********
    ///            ^WWWW
    ///             VWW
    ///             Wh.
    ///             V/
    RefractedDevuan,
    /// Regata Logo
    /// ```text
    ///             ddhso+++++osydd
    ///         dho/.`hh.:/+/:.hhh`:+yd
    ///       do-hhhhhh/sssssss+`hhhhh./yd
    ///     h/`hhhhhhh-sssssssss:hhhhhhhh-yd
    ///   do`hhhhhhhhh`ossssssso.hhhhhhhhhh/d
    ///  d/hhhhhhhhhhhh`/ossso/.hhhhhhhhhhhh.h
    ///  /hhhhhhhhhhhh`-/osyso/-`hhhhhhhhhhhh.h
    /// shh-/ooo+-hhh:syyso+osyys/`hhh`+oo`hhh/
    /// h`ohhhhhhho`+yyo.hhhhh.+yyo`.sssssss.h`h
    /// s:hhhhhhhhhoyys`hhhhhhh.oyy/ossssssso-hs
    /// s.yhhhhhhhy/yys`hhhhhhh.oyy/ossssssso-hs
    /// hh./syyys+. +yy+.hhhhh.+yyo`.ossssso/h`h
    /// shhh``.`hhh`/syyso++oyys/`hhh`+++-`hh:h
    /// d/hhhhhhhhhhhh`-/osyso+-`hhhhhhhhhhhh.h
    ///  d/hhhhhhhhhhhh`/ossso/.hhhhhhhhhhhh.h
    ///   do`hhhhhhhhh`ossssssso.hhhhhhhhhh:h
    ///     h/`hhhhhhh-sssssssss:hhhhhhhh-yd
    ///       h+.hhhhhh+sssssss+hhhhhh`/yd
    ///         dho:.hhh.:+++/.hhh`-+yd
    ///             ddhso+++++osyhd
    /// ```
    Regata,
    /// Regolith Logo
    /// ```text
    ///                  ``....```
    ///             `.:/++++++/::-.`
    ///           -/+++++++:.`
    ///         -++++++++:`
    ///       `/++++++++-
    ///      `/++++++++.                    -/+/
    ///      /++++++++/             ``   .:+++:.
    ///     -+++++++++/          ./++++:+++/-`
    ///     :+++++++++/         `+++++++/-`
    ///     :++++++++++`      .-/+++++++`
    ///    `:++++++++++/``.-/++++:-:::-`      `
    ///  `:+++++++++++++++++/:.`            ./`
    /// :++/-:+++++++++/:-..              -/+.
    /// +++++++++/::-...:/+++/-..````..-/+++.
    /// `......``.::/+++++++++++++++++++++/.
    ///          -/+++++++++++++++++++++/.
    ///            .:/+++++++++++++++/-`
    ///               `.-:://////:-.
    /// ```
    Regolith,
    /// RhaymOS Logo
    /// ```text
    ///                     ###
    ///                    #####
    ///
    ///               #######        /########
    ///            #############   ###########
    ///    ,###########      ####  ####(..
    ///   ####   ####       ####*  ##########
    ///   ####    #####     #####         (####
    ///   ####      ###########    ###########
    ///   ####       #########     ##########
    ///
    ///   ###################################
    ///  #####################################
    /// #######################################
    /// ```
    RhaymOs,
    /// Rocky Linux Logo
    /// ```text
    ///           __wgliliiligw_,
    ///        _williiiiiiliilililw,
    ///      _%iiiiiilililiiiiiiiiiii_
    ///    .Qliiiililiiiiiiililililiilm.
    ///   _iiiiiliiiiiililiiiiiiiiiiliil,
    ///  .lililiiilililiiiilililililiiiii,
    /// _liiiiiiliiiiiiiliiiiiF{iiiiiilili,
    /// jliililiiilililiiili@`  ~ililiiiiiL
    /// iiiliiiiliiiiiiili>`      ~liililii
    /// liliiiliiilililii`         -9liiiil
    /// iiiiiliiliiiiii~             "4lili
    /// 4ililiiiiilil~|      -w,       )4lf
    /// -liiiiililiF'       _liig,       )'
    ///  )iiiliii@`       _QIililig,
    ///   )iiii>`       .Qliliiiililw
    ///    )<>~       .mliiiiiliiiiiil,
    ///             _gllilililiililii~
    ///            giliiiiiiiiiiiiT`
    ///           -^~ililili@~~'
    /// ```
    RockyLinux,
    /// Small Rocky Linux Logo
    /// ```text
    ///     `-/+++++++++/-.`
    ///  `-+++++++++++++++++-`
    /// .+++++++++++++++++++++.
    /// -+++++++++++++++++++++++.
    /// +++++++++++++++/-/+++++++
    /// +++++++++++++/.   ./+++++
    /// +++++++++++:.       ./+++
    /// +++++++++:`   `:/:`   .:/
    /// -++++++:`   .:+++++:`
    ///  .+++-`   ./+++++++++:`
    ///   `-`   ./+++++++++++-
    ///        -+++++++++:-.`
    /// ```
    RockyLinuxSmall,
    /// Rosa Linux Logo
    /// ```text
    ///            ROSAROSAROSAROSAR
    ///         ROSA               AROS
    ///       ROS   SAROSAROSAROSAR   AROS
    ///     RO   ROSAROSAROSAROSAROSAR   RO
    ///   ARO  AROSAROSAROSARO      AROS  ROS
    ///  ARO  ROSAROS         OSAR   ROSA  ROS
    ///  RO  AROSA   ROSAROSAROSA    ROSAR  RO
    /// RO  ROSAR  ROSAROSAROSAR  R  ROSARO  RO
    /// RO  ROSA  AROSAROSAROSA  AR  ROSARO  AR
    /// RO AROS  ROSAROSAROSA   ROS  AROSARO AR
    /// RO AROS  ROSAROSARO   ROSARO  ROSARO AR
    /// RO  ROS  AROSAROS   ROSAROSA AROSAR  AR
    /// RO  ROSA  ROS     ROSAROSAR  ROSARO  RO
    ///  RO  ROS     AROSAROSAROSA  ROSARO  AR
    ///  ARO  ROSA   ROSAROSAROS   AROSAR  ARO
    ///   ARO  OROSA      R      ROSAROS  ROS
    ///     RO   AROSAROS   AROSAROSAR   RO
    ///      AROS   AROSAROSAROSARO   AROS
    ///         ROSA               SARO
    ///            ROSAROSAROSAROSAR
    /// ```
    RosaLinux,
    /// Sabayon Logo
    /// ```text
    ///             ...........
    ///          ..             ..
    ///       ..                   ..
    ///     ..           o           ..
    ///   ..            :W'            ..
    ///  ..             .d.             ..
    /// :.             .KNO              .:
    /// :.             cNNN.             .:
    /// :              dXXX,              :
    /// :   .          dXXX,       .cd,   :
    /// :   'kc ..     dKKK.    ,ll;:'    :
    /// :     .xkkxc;..dkkkc',cxkkl       :
    /// :.     .,cdddddddddddddo:.       .:
    ///  ..         :lllllll:           ..
    ///    ..         ',,,,,          ..
    ///      ..                     ..
    ///         ..               ..
    ///           ...............
    /// ```
    Sabayon,
    /// Sabotage Logo
    /// ```text
    ///  .|'''.|      |     '||''|.    ..|''||
    ///  ||..  '     |||     ||   ||  .|'    ||
    ///   ''|||.    |  ||    ||'''|.  ||      ||
    /// .     '||  .''''|.   ||    || '|.     ||
    /// |'....|'  .|.  .||. .||...|'   ''|...|'
    ///
    /// |''||''|     |      ..|'''.|  '||''''|
    ///    ||       |||    .|'     '   ||  .
    ///    ||      |  ||   ||    ....  ||''|
    ///    ||     .''''|.  '|.    ||   ||
    ///   .||.   .|.  .||.  ''|...'|  .||.....|
    /// ```
    Sabotage,
    /// Sailfish Logo
    /// ```text
    ///               _a@b
    ///            _#b (b
    ///          _@@   @_         _,
    ///        _#^@ _#*^^*gg,aa@^^
    ///        #- @@^  _a@^^
    ///        @_  *g#b
    ///        ^@_   ^@_
    ///          ^@_   @
    ///           @(b (b
    ///          #b(b#^
    ///        _@_#@^
    ///     _a@a*^
    /// ,a@*^
    /// ```
    Sailfish,
    /// SalentOS Logo
    /// ```text
    ///                  ``..``
    ///         .-:+oshdNMMMMMMNdhyo+:-.`
    ///   -oydmMMMMMMMMMMMMMMMMMMMMMMMMMMNdhs/
    ///  +hdddmNMMMMMMMMMMMMMMMMMMMMMMMMNmdddh+`
    /// `MMMMMNmdddddmMMMMMMMMMMMMmdddddmNMMMMM-
    ///  mMMMMMMMMMMMNddddhyyhhdddNMMMMMMMMMMMM`
    ///  dMMMMMMMMMMMMMMMMMooMMMMMMMMMMMMMMMMMN`
    ///  yMMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMMd
    ///  +MMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMMy
    ///  :MMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMMo
    ///  .MMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMM/
    ///  `NMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMM-
    ///   mMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMN`
    ///   hMMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMm
    ///   /MMMMMMMMMMMMMMMMhhMMMMMMMMMMMMMMMMy
    ///    .+hMMMMMMMMMMMMMhhMMMMMMMMMMMMMms:
    ///       `:smMMMMMMMMMhhMMMMMMMMMNh+.
    ///           .+hMMMMMMhhMMMMMMdo:
    ///              `:smMMyyMMNy/`
    ///                  .- `:.
    /// ```
    SalentOs,
    /// SalientOS Logo
    /// ```text
    ///               00xxxx0
    ///           00xxxxxx0
    ///        0xxxxxxxxx         000000
    ///      0xxxxxxxxxx        xxxxxxxxxx0
    ///    0xxxxxxxxxxx0       xxxxxxxxxxxxx0
    ///   0xxxxxxxxxxxx0      0xxxxxxxxxxxxxx0
    ///  0xxxxxxxxxxxxx0      0xxxxxxxxxxxxxxx0
    /// 0xxxxxxxxxxxxxxx       xxxxxxxxxxxxxxxx0
    /// xxxxxxxxxxxxxxxx0      0xxxxxxxxxxxxxxxx
    /// xxxxxxxxxxxxxxxxx       0xxxxxxxxxxxxxxx
    /// xxxxxxxxxxxxxxxxxx       xxxxxxxxxxxxxxx
    /// xxxxxxxxxxxxxxxxxx0       xxxxxxxxxxxxxx
    /// 0xxxxxxxxxxxxxxxxxx0      0xxxxxxxxxxxx0
    ///  0xxxxxxxxxxxxxxxxxx       xxxxxxxxxxx0
    ///   0xxxxxxxxxxxxxxxxx       xxxxxxxxxx0
    ///    0xxxxxxxxxxxxxxxx       xxxxxxxxx0
    ///      0xxxxxxxxxxxx0       0xxxxxxx0
    ///         0xxxxxxx0         xxxxxx0
    ///                         0xxx00
    ///                       x00
    /// ```
    SalientOs,
    /// Salix Logo
    /// ```text
    ///               __s_aaaaaaaaauuoXSSSSSSSS:
    ///           ._xSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///         _aSSSSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///       _xSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///      <XSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS:
    ///     -"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
    ///   .ssssssssssssssssssssssssssssssssssss
    ///   {SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSl
    ///   oSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS;
    ///  :XSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS;
    ///  {SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
    ///  -"""""""""""""""""""""""""""""""""""'
    ///  <assssssssssssssssssssssssssssssss>
    ///  nSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS}
    ///  nSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS}`
    ///  XSSSSSSSSSSSSSSSSSSSSSSSSSSSS"`
    ///  SSSSSSSSSSSSSSSSSSSSSSSSS!"`
    /// -""""""""""""""""""""""`
    /// ```
    Salix,
    /// SambaBox Logo
    /// ```text
    ///                     #
    ///                *////#####
    ///            /////////#########(
    ///       .((((((/////    ,####(#(((((
    ///   /#######(((*             (#(((((((((.
    /// //((#(#(#,        ((##(        ,((((((//
    /// //////        #(##########(       //////
    /// //////    ((#(#(#(#(##########(/////////
    /// /////(    (((((((#########(##((((((/////
    /// /(((#(                             ((((/
    /// ####(#                             ((###
    /// #########(((/////////(((((((((,    (#(#(
    /// ########(   /////////(((((((*      #####
    /// ####///,        *////(((         (((((((
    /// .///////////                .//(((((((((
    ///      ///////////,       *(/////((((*
    ///          ,/(((((((((##########/.
    ///              .((((((#######
    ///                   ((##*
    /// ```
    SambaBox,
    /// Sasanqua Logo
    /// ```text
    ///                      __,_
    ///              _╕⌐≡µ,√*    º≡,
    ///             ñ     "'       ░
    ///            ╞)         _,   ▒     __
    ///      _,,,_ _Ñ╜^≡µ   ≡'  1µ╕º^el    "%µ
    ///      ∩'     K      Yµ&          1l     ╞)
    ///   ▒     √"        ^Ü          1"     `1µ
    ///   Γ    ║h                  _¿▒∞√;,     ^≡,
    ///    K    ^u_              ⌐*      ╙¥     ╓Ñ
    ///   ⌠        º≡u,,                  ║I    Å
    ///   Ü     _∩"                       ║µ_¿╝"
    ///    Yu_ ▒               ╙º≡_       ║l1µ
    ///       ║l          ,        Y∞µ___≡ª  Γl
    ///        ╓hⁿ╖I     1l         Ñ        ╓Ñ
    ///        Ñ   ¥,___≡1l        ╓Ñ      ¿╕ª
    ///        Ü         ╙L     ¿¿∩ª     ╓P
    ///         ª≡,__      *ⁿ┤ⁿÑⁿ^µ     √ª
    ///               ⁿ≡,,__√╝*    "ⁿⁿ*"
    /// ```
    Sasanqua,
    /// Scientific Logo
    /// ```text
    ///                  =/;;/-
    ///                 +:    //
    ///                /;      /;
    ///               -X        H.
    /// .//;;;:;;-,   X=        :+   .-;:=;:;#;.
    /// M-       ,=;;;#:,      ,:#;;:=,       ,@
    /// :#           :#.=/++++/=.=           #=
    ///  ,#;         #/:+/;,,/++:+/         ;+.
    ///    ,+/.    ,;@+,        ,#H;,    ,/+,
    ///       ;+;;/= @.  .H##X   -X :///+;
    ///       ;+=;;;.@,  .XM@.  =X.//;=#/.
    ///    ,;:      :@#=        =H:     .+#-
    ///  ,#=         #;-///==///-//         =#,
    /// ;+           :#-;;;:;;;;-X-           +:
    /// @-      .-;;;;M-        =M/;;;-.      -X
    ///  :;;::;;-.    #-        :+    ,-;;-;:==
    ///               ,X        H.
    ///                ;/      #=
    ///                 //    +;
    ///                  '////'
    /// ```
    Scientific,
    /// SEMC Logo
    /// ```text
    ///             /\
    ///      ______/  \
    ///     /      |()| E M C
    ///    |   (-- |  |
    ///     \   \  |  |
    /// .----)   | |__|
    /// |_______/ / "  \
    ///               "
    ///             "
    /// ```
    Semc,
    /// Septor Logo
    /// ```text
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssssssssssssssssssssssssssssssssss
    /// ssssssssss;okOOOOOOOOOOOOOOko;ssssssssss
    /// sssssssssoNWWWWWWWWWWWWWWWWWWNosssssssss
    /// ssssssss:WWWWWWWWWWWWWWWWWWWWWW:ssssssss
    /// sssssssslWWWWWksssssssssslddddd:ssssssss
    /// sssssssscWWWWWNKKKKKKKKKKKKOx:ssssssssss
    /// yysssssssOWWWWWWWWWWWWWWWWWWWWxsssssssyy
    /// yyyyyyyyyy:kKNNNNNNNNNNNNWWWWWW:yyyyyyyy
    /// yyyyyyyysccccc;yyyyyyyyyykWWWWW:yyyyyyyy
    /// yyyyyyyy:WWWWWWNNNNNNNNNNWWWWWW;yyyyyyyy
    /// yyyyyyyy.dWWWWWWWWWWWWWWWWWWWNdyyyyyyyyy
    /// yyyyyyyyyysdO0KKKKKKKKKKKK0Od;yyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// ```
    Septor,
    /// Serene Logo
    /// ```text
    ///               __---''''''---__
    ///           .                      .
    ///         :                          :
    ///       -                       _______----_-
    ///      s               __----'''     __----
    ///  __h_            _-'           _-'     h
    ///  '-._''--.._    ;           _-'         y
    ///   :  ''-._  '-._/        _-'             :
    ///   y       ':_       _--''                y
    ///   m    .--'' '-._.;'                     m
    ///   m   :        :                         m
    ///   y    '.._     '-__                     y
    ///   :        '--._    '''----___           :
    ///    y            '--._         ''-- _    y
    ///     h                '--._          :  h
    ///      s                  __';         vs
    ///       -         __..--''             -
    ///         :_..--''                   :
    ///           .                     _ .
    ///             `''---______---''-``
    /// ```
    Serene,
    /// SharkLinux Logo
    /// ```text
    ///                              `:shd/
    ///                          `:yNMMMMs
    ///                       `-smMMMMMMN.
    ///                     .+dNMMMMMMMMs
    ///                   .smNNMMMMMMMMm`
    ///                 .sNNNNNNNMMMMMM/
    ///               `omNNNNNNNMMMMMMm
    ///              /dNNNNNNNNMMMMMMM+
    ///            .yNNNNNNNNNMMMMMMMN`
    ///           +mNNNNNNNNNMMMMMMMMh
    ///         .hNNNNNNNNNNMMMMMMMMMs
    ///        +mMNNNNNNNNMMMMMMMMMMMs
    ///      .hNMMNNNNMMMMMMMMMMMMMMMd
    ///    .oNNNNNNNNNNMMMMMMMMMMMMMMMo
    /// `:+syyssoo++++ooooossssssssssso:
    /// ```
    SharkLinux,
    /// ShastraOS Logo
    /// ```text
    ///                ..,;;,'.
    ///             ':oo.     ;o:
    ///           :o,           ol
    ///         .oo        ..';co:
    ///         ooo',;:looo;
    ///     .;lddl
    ///   cx   .xl     .c:'
    ///  dd     xx     xx ,d;
    /// .xd     cx.    xx   dd.
    ///  cx:    .xo    xx    ,x:
    ///   'xl    xx    cx'    .xl
    ///     xd,  xx    .xd     dx.
    ///      .xo:xx     xx    .xx
    ///         'c      xx:.'lx:
    ///            ..,;cxxxo
    ///   .';:codxxl   lxo
    /// cd.           'xo
    /// :o,         'ld
    ///  .oc'...';lo
    /// ```
    ShastraOs,
    /// Siduction Logo
    /// ```text
    ///                 _aass,
    ///                jQh: =w
    ///                QWmwawQW
    ///                )QQQQ@(   ..
    ///          _a_a.   ~??^  syDY?Sa,
    ///        _mW>-<c       jWmi  imm.
    ///        ]QQwayQE       4QQmgwmQQ`
    ///         ?WWQWP'       -9QQQQQ@'._aas,
    ///  _a%is.        .adYYs,. -"?!` aQB*~^3c
    /// _Qh;.nm       .QWc. {QL      ]QQp;..vmQ/
    /// "QQmmQ@       -QQQggmQP      ]QQWmggmQQ(
    ///  -???"         "WQQQY`  __,  ?QQQQQQW!
    ///         _yZ!?q,   -   .yWY!!Sw, "???^
    ///        .QQa_=qQ       mQm>..vmm
    ///         QQWQQP       QQQgmQQ@
    ///          "???"   _aa, -9WWQQWY`
    ///                _mB>~)a  -~~
    ///                mQms_vmQ.
    ///                ]WQQQQQP
    ///                 -?T??"
    /// ```
    Siduction,
    /// SkiffOS Logo
    /// ```text
    ///              ,@@@@@@@@@@@w,_
    ///   ====~~~,,.A@@@@@@@@@@@@@@@@@W,_
    ///   `||||||||||||||L{"@@@@@@@@@B"
    ///    `|||||||||||||||||||||L{"D
    ///      @@@@@@@@@@@@@@@@@@@@@_||||}==,
    ///       *@@@@@@@@@@@@@@@@@@@@@@@@@p||||==,
    ///         `'||LLL{{""@B@@@@@@@@@@@@@@@p||
    ///             `~=|||||||||||L"@@@@@@@@@@@
    ///                    ````'"""""""'""""""""
    /// ```
    SkiffOs,
    /// Slitaz Logo
    /// ```text
    ///         @    @(               @
    ///       @@   @@                  @    @/
    ///      @@   @@                   @@   @@
    ///     @@  %@@                     @@   @@
    ///    @@  %@@@       @@@@@.       @@@@  @@
    ///   @@@    @@@@    @@@@@@@    &@@@    @@@
    ///    @@@@@@@ %@@@@@@@@@@@@ &@@@% @@@@@@@/
    ///        ,@@@@@@@@@@@@@@@@@@@@@@@@@
    ///   .@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/
    /// @@@@@@.  @@@@@@@@@@@@@@@@@@@@@  /@@@@@@
    /// @@    @@@@@  @@@@@@@@@@@@,  @@@@@   @@@
    /// @@ @@@@.    @@@@@@@@@@@@@%    #@@@@ @@.
    /// @@ ,@@      @@@@@@@@@@@@@      @@@  @@
    /// @   @@.     @@@@@@@@@@@@@     @@@  *@
    /// @    @@     @@@@@@@@@@@@      @@   @
    ///       @      @@@@@@@@@.     #@
    ///        @      ,@@@@@       @
    /// ```
    Slitaz,
    /// Slackel Logo
    /// ```text
    ///               _aawmmmmmwwaaaaas,,,_.
    ///            .ammmmm###mmmmmmm###BQmm##mws
    ///          .am###mmBmBmBmBmBmBmmmmm#mmmm#2
    ///         <q###mmBmBmBmBmBmBmBmBmBmBmmBmZ`
    ///        um#mmmBmBm##U##mmBmBmBmWmmBmWm#(
    ///      .wm#mmBBmm#Y~   ~XmBmBmWmmmmmBm#e
    ///     .dm#mmWmm#Z'      ]#mBmBmmBZ!""""`
    ///    .dm#mmBmm#2`       ]mmmBmBm#2
    ///    jm#mmWmm#2`        dmmBmBmB#(
    ///   )m##mBmmWZ`        )##mBmBmmZ
    ///  :dmmmBmBm#'        .d#mBmBmWZ(
    ///  j#mmBmBmme         jmmmBmBm#2
    /// _m#mBmWmmm'        )mmmBmBmmZ`
    /// ]##mBmmm#2        <m#mBmBmB#^
    /// dmmmBmWm#C       <m#mBmBmB#(
    /// ZmmBmBmmmh.    _jm#mmBmBm#(
    /// XBmBmBmBmm6s_aum##mmBmBm&^
    /// 3mBmBmBmmm#mmmmmmBmBm#2'
    /// +ZmBmBmWmBmBmWmmBmBm##!
    ///  )ZmBmBmmmBmBmmBmB##!`
    ///   -4U#mBmWmBmBm##2"
    ///     -!!XU##US*?"-
    /// ```
    Slackel,
    /// Slackware Logo
    /// ```text
    ///                   :::::::::
    ///             :::::::::::::::::::
    ///          :::::::::::::::::::::::::
    ///        ::::::::cllcccccllllllll::::::
    ///     :::::::::lc               dc:::::::
    ///    ::::::::cl   clllccllll    oc:::::::::
    ///   :::::::::o   lc::::::::co   oc::::::::::
    ///  ::::::::::o    cccclc:::::clcc::::::::::::
    ///  :::::::::::lc        cclccclc:::::::::::::
    /// ::::::::::::::lcclcc          lc::::::::::::
    /// ::::::::::cclcc:::::lccclc     oc:::::::::::
    /// ::::::::::o    l::::::::::l    lc:::::::::::
    ///  :::::cll:o     clcllcccll     o:::::::::::
    ///  :::::occ:o                  clc:::::::::::
    ///   ::::ocl:ccslclccclclccclclc:::::::::::::
    ///    :::oclcccccccccccccllllllllllllll:::::
    ///     ::lcc1lcccccccccccccccccccccccco::::
    ///       ::::::::::::::::::::::::::::::::
    ///         ::::::::::::::::::::::::::::
    ///            ::::::::::::::::::::::
    ///                 ::::::::::::
    /// ```
    Slackware,
    /// Small Slackware Logo
    /// ```text
    ///    ________
    ///   /  ______|
    ///   | |______
    ///   \______  \
    ///    ______| |
    /// | |________/
    /// |____________
    /// ```
    SlackwareSmall,
    /// SmartOS Logo
    /// ```text
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyys             oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyy  oyyyyyyyyyyyyyyyy
    /// yyyys  yyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyy                         syyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  syyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  oyyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  oyyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy  syyyyyyyyy  syyyy
    /// yyyyyyyyyyyyyyyy              yyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    /// ```
    SmartOs,
    /// Soda Logo
    /// ```text
    ///                 @&&&&        *'*,
    ///               @@@@@@@&&&  **     '*,
    ///              @@@@@@@@@@@&&&&
    ///            @&@@@@@@@@@@@@@@@&&&
    ///           ******@@@@@@@@@@@@@@@&&&&
    ///         ************@@@@@@@@@@@@@@
    ///       *****************@@@@@@@@@
    ///      **********************@@@
    ///    @@@@@********************
    ///   @@@@@@@@@***************
    /// @@@@@@@@@@@@@@@*********
    /// @@@@@@@@@@@@@@@@@@****
    ///    @@@@@@@@@@@@@@@@@@
    ///        @@@@@@@@@@@@
    ///           @@@@@@@
    /// ```
    Soda,
    /// Source Mage Logo
    /// ```text
    ///        :ymNMNho.
    /// .+sdmNMMMMMMMMMMy`
    /// .-::/yMMMMMMMMMMMm-
    ///       sMMMMMMMMMMMm/
    ///      /NMMMMMMMMMMMMMm:
    ///     .MMMMMMMMMMMMMMMMM:
    ///     `MMMMMMMMMMMMMMMMMN.
    ///      NMMMMMMMMMMMMMMMMMd
    ///      mMMMMMMMMMMMMMMMMMMo
    ///      hhMMMMMMMMMMMMMMMMMM.
    ///      .`/MMMMMMMMMMMMMMMMMs
    ///         :mMMMMMMMMMMMMMMMN`
    ///          `sMMMMMMMMMMMMMMM+
    ///            /NMMMMMMMMMMMMMN`
    ///              oMMMMMMMMMMMMM+
    ///           ./sd.-hMMMMMMMMmmN`
    ///       ./+oyyyh- `MMMMMMMMMmNh
    ///                  sMMMMMMMMMmmo
    ///                  `NMMMMMMMMMd:
    ///                   -dMMMMMMMMMo
    ///                     -shmNMMms.
    /// ```
    SourceMage,
    /// Solaris Logo
    /// ```text
    ///                  `-     `
    ///           `--    `+-    .:
    ///            .+:  `++:  -/+-     .
    ///     `.::`  -++/``:::`./+/  `.-/.
    ///       `++/-`.`          ` /++:`
    ///   ``   ./:`                .: `..`.-
    /// ``./+/:-                     -+++:-
    ///     -/+`                      :.
    /// ```
    Solaris,
    /// Small Solaris Logo
    /// ```text
    ///        .   .;   .
    ///    .   :;  ::  ;:   .
    ///    .;. ..      .. .;.
    /// ..  ..             ..  ..
    ///  .;,                 ,;.
    /// ```
    SolarisSmall,
    /// Solus Logo
    /// ```text
    ///             -```````````
    ///           `-+/------------.`
    ///        .---:mNo---------------.
    ///      .-----yMMMy:---------------.
    ///    `------oMMMMMm/----------------`
    ///   .------/MMMMMMMN+----------------.
    ///  .------/NMMMMMMMMm-+/--------------.
    /// `------/NMMMMMMMMMN-:mh/-------------`
    /// .-----/NMMMMMMMMMMM:-+MMd//oso/:-----.
    /// -----/NMMMMMMMMMMMM+--mMMMh::smMmyo:--
    /// ----+NMMMMMMMMMMMMMo--yMMMMNo-:yMMMMd/.
    /// .--oMMMMMMMMMMMMMMMy--yMMMMMMh:-yMMMy-`
    /// `-sMMMMMMMMMMMMMMMMh--dMMMMMMMd:/Ny+y.
    /// `-/+osyhhdmmNNMMMMMm-/MMMMMMMmh+/ohm+
    ///   .------------:://+-/++++++oshddys:
    ///    -hhhhyyyyyyyyyyyhhhhddddhysssso-
    ///     `:ossssssyysssssssssssssssso:`
    ///       `:+ssssssssssssssssssss+-
    ///          `-/+ssssssssssso+/-`
    ///               `.-----..`
    /// ```
    Solus,
    /// Sparky Logo
    /// ```text
    ///            .            `-:-`
    ///           .o`       .-///-`
    ///          `oo`    .:/++:.
    ///          os+`  -/+++:` ``.........```
    ///         /ys+`./+++/-.-::::::----......``
    ///        `syyo`++o+--::::-::/+++/-``
    ///        -yyy+.+o+`:/:-:sdmmmmmmmmdy+-`
    /// ::-`   :yyy/-oo.-+/`ymho++++++oyhdmdy/`
    /// `/yy+-`.syyo`+o..o--h..osyhhddhs+//osyy/`
    ///   -ydhs+-oyy/.+o.-: ` `  :/::+ydhy+```-os-
    ///    .sdddy::syo--/:.     `.:dy+-ohhho    ./:
    ///      :yddds/:+oo+//:-`- /+ +hy+.shhy:     ``
    ///       `:ydmmdysooooooo-.ss`/yss--oyyo
    ///         `./ossyyyyo+:-/oo:.osso- .oys
    ///        ``..-------::////.-oooo/   :so
    ///     `...----::::::::--.`/oooo:    .o:
    ///            ```````     ++o+:`     `:`
    ///                      ./+/-`        `
    ///                    `-:-.
    ///                    ``
    /// ```
    Sparky,
    /// Star Logo
    /// ```text
    ///                    ./
    ///                   `yy-
    ///                  `y.`y`
    ///     ``           s-  .y            `
    ///     +h//:..`    +/    /o    ``..:/so
    ///      /o``.-::/:/+      o/://::-.`+o`
    ///       :s`     `.        .`     `s/
    ///        .y.                    .s-
    ///         `y-                  :s`
    ///       .-//.                  /+:.
    ///    .:/:.                       .:/:.
    /// -+o:.                             .:+:.
    /// -///++///:::`              .-::::///+so-
    ///        ``..o/              d-....```
    ///            s.     `/.      d
    ///            h    .+o-+o-    h.
    ///            h  -o/`   `/o:  s:
    ///           -s/o:`       `:o/+/
    ///           /s-             -yo
    /// ```
    Star,
    /// Stock Linux Logo
    /// ```text
    ///               #G5J5G#
    ///           &BPYJJJJJJJYPB&
    ///       &#G5JJJJJJY5YJJJJJJ5G#&
    ///    #G5YJJJJJY5G#& &#G5YJJJJJY5G#
    /// BPYJJJJJJJ5B&         &BPYJJJJJJYPB
    /// JJJJJJJJJJY5G#&           &BPYJJJJJ
    /// JJJJJJJJJJJJJJY5G#           &JJJJJ
    /// PYJJJJJJJJJJJJJJJJYPB&        GYJJJ
    ///   &BPYJJJJJJJJJJJJJJJJ5PB&      &BP
    ///       #G5YJJJJJJJJJJJJJJJY5G#
    /// PB&      &BP5JJJJJJJJJJJJJJJJYPB&
    /// JJJYG        &BPYJJJJJJJJJJJJJJJJYP
    /// JJJJJ&           #G5YJJJJJJJJJJJJJJ
    /// JJJJJYPB&           &#G5YJJJJJJJJJJ
    /// BPYJJJJJJYPB&         &B5JJJJJJJYPB
    ///    #G5YJJJJJY5G#& &#G5YJJJJJY5G#
    ///       &#G5JJJJJJY5YJJJJJJ5G#&
    ///           &BPYJJJJJJJYPB&
    ///               #G5J5G#
    /// ```
    StockLinux,
    /// SteamOS Logo
    /// ```text
    ///               .,,,,.
    ///         .,'onNMMMMMNNnn',.
    ///      .'oNMANKMMMMMMMMMMMNNn'.
    ///    .'ANMMMMMMMXKNNWWWPFFWNNMNn.
    ///   ;NNMMMMMMMMMMNWW'' ,.., 'WMMM,
    ///  ;NMMMMV+##+VNWWW' .+;'':+, 'WMW,
    /// ,VNNWP+######+WW,  +:    :+, +MMM,
    /// '+#############,   +.    ,+' +NMMM
    ///   '*#########*'     '*,,*' .+NMMMM.
    ///      `'*###*'          ,.,;###+WNM,
    ///          .,;;,      .;##########+W
    /// ,',.         ';  ,+##############'
    ///  '###+. :,. .,; ,###############'
    ///   '####.. `'' .,###############'
    ///     '#####+++################'
    ///       '*##################*'
    ///          ''*##########*''
    ///               ''''''
    /// ```
    SteamOs,
    /// Sulin Logo
    /// ```text
    ///                          /\          /\
    ///                         ( \\\\        // )
    ///                          \ \\\\      // /
    ///                           \_\\\\||||//_/
    ///                            \/ _  _ \
    ///                           \/|(O)(O)|
    ///                          \/ |      |
    ///      ___________________\/  \      /
    ///     //                //     |____|
    ///    //                ||     /      \
    ///   //|                \|     \ 0  0 /
    ///  // \       )         V    / \____/
    /// //   \     /        (     /
    ///       \   /_________|  |_/
    ///       /  /\   /     |  ||
    ///      /  / /  /      \  ||
    ///      | |  | |        | ||
    ///      | |  | |        | ||
    ///      |_|  |_|        |_||
    ///      \_\  \_\        \_\\
    /// ```
    Sulin,
    /// Suse Logo
    /// ```text
    ///  kKKKKKd'  Okxol:;,.
    /// kKKKKKKK0kOKKKKKKKKKKOxo:,
    /// KKKKKKKKKKKKKKKKKKK0P^,,,^dx:
    /// KKKKKKKKKKKKKKKKKKk'.oOPPb.'0k.
    /// KKKKKKKKKKKKKKKKKK: kKx..dd lKd
    /// KKKKKKKKKKKKOx0KKKd ^0KKKO' kKKc
    /// KKKKKKKKKKKKK;.;oOKx,..^..;kKKK0.
    /// KKKKKKKKKKKKK0o;...^cdxxOK0O/^^'
    /// KKKKKKKKKKKKKKKKK0x;,,......,;od
    /// kKKKKKKKKKKKKKKKKKKKKKKK00KKOo^
    ///  kKKKKKOxddxkOO00000Okxoc;''
    /// ```
    Suse,
    /// Swagarch Logo
    /// ```text
    ///         .;ldkOKXXNNNNXXK0Oxoc,.
    ///    ,lkXMMNK0OkkxkkOKWMMMMMMMMMM;
    ///  'K0xo  ..,;:c:.     `'lKMMMMM0
    ///      .lONMMMMMM'         `lNMk'
    ///     ;WMMMMMMMMMO.              ....::...
    ///     OMMMMMMMMMMMMKl.       .,;;;;;ccccccc,
    ///     `0MMMMMMMMMMMMMM0:         .. .ccccccc.
    ///       'kWMMMMMMMMMMMMMNo.   .,:'  .ccccccc.
    ///         `c0MMMMMMMMMMMMMN,,:c;    :cccccc:
    ///  ckl.      `lXMMMMMMMMMXocccc:.. ;ccccccc.
    /// dMMMMXd,     `OMMMMMMWkccc;:''` ,ccccccc:
    /// XMMMMMMMWKkxxOWMMMMMNoccc;     .cccccccc.
    ///  `':ldxO0KXXXXXK0Okdocccc.     :cccccccc.
    ///                     :ccc:'     `cccccccc:,
    ///                                    ''
    /// ```
    Swagarch,
    /// T2 Logo
    /// ```text
    /// TTTTTTTTTT
    ///     tt   222
    ///     tt  2   2
    ///     tt     2
    ///     tt    2
    ///     tt  22222
    /// ```
    T2,
    /// Tails Logo
    /// ```text
    ///       ``
    ///   ./yhNh
    /// syy/Nshh         `:o/
    /// N:dsNshh  █   `ohNMMd
    /// N-/+Nshh      `yMMMMd
    /// N-yhMshh       yMMMMd
    /// N-s:hshh  █    yMMMMd so//.
    /// N-oyNsyh       yMMMMd d  Mms.
    /// N:hohhhd:.     yMMMMd  syMMM+
    /// Nsyh+-..+y+-   yMMMMd   :mMM+
    /// +hy-      -ss/`yMMMM     `+d+
    ///   :sy/.     ./yNMMMMm      ``
    ///     .+ys- `:+hNMMMMMMy/`
    ///       `hNmmMMMMMMMMMMMMdo.
    ///        dMMMMMMMMMMMMMMMMMNh:
    ///        +hMMMMMMMMMMMMMMMMMmy.
    ///          -oNMMMMMMMMMMmy+.`
    ///            `:yNMMMds/.`
    ///               .//`
    /// ```
    Tails,
    /// Tatra Logo
    /// ```text
    ///          . .:. .         .:.
    ///         .^^.!.:::.    .^!?J?^
    ///       .:^.^!!!~:~^.  .7??77!~~^.
    ///      .~^.!??77?!.^~:  ..:^^7JJJ7~~^.
    ///      .^~.^7???7~.~~.   .7??????????!
    ///       .:^:^^~^^:!^ ^:  .......^!:...
    ///     .!7~.::.!.::. ~BG~    :^  ^~:
    ///     :!!~   ~.    ?BBBB!  ^?J!.  .!~.
    ///      :!. .JBY. .Y#BBBY?~!???J7. :^^.
    ///      .. :5#B#P~P#BBP?7?55J?J7:
    ///        ^P#BBBBBBBB5?7J5555J!.....
    ///       !BBBBBBGBBGJ77::Y555J?77777^
    ///      ?BBBBG5JJ5PJ?!:  .?Y??????77?~.
    ///    .YBGPYJ??????Y?^^^^~7?????????7?!.
    ///    .^^:..::::::::.....:::::::::::..:.
    /// ```
    Tatra,
    /// TeArch Logo
    /// ```text
    ///           @@@@@@@@@@@@@@
    ///       @@@@@@@@@              @@@@@@
    ///      @@@@@                     @@@@@
    ///      @@                           @@
    ///       @%                         @@
    ///        @                         @
    ///        @@@@@@@@@@@@@@@@@@@@@@@@ @@
    ///        .@@@@@@@@@@@@/@@@@@@@@@@@@
    ///        @@@@@@@@@@@@///@@@@@@@@@@@@
    ///       @@@@@@@@@@@@@((((@@@@@@@@@@@@
    ///      @@@@@@@@@@@#(((((((#@@@@@@@@@@@
    ///     @@@@@@@@@@@#//////////@@@@@@@@@@&
    ///     @@@@@@@@@@////@@@@@////@@@@@@@@@@
    ///     @@@@@@@@//////@@@@@/////@@@@@@@@@
    ///     @@@@@@@//@@@@@@@@@@@@@@@//@@@@@@@
    ///  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    /// @@     .@@@@@@@@@@@@@@@@@@@@@@@@@      @
    ///  @@@@@@           @@@.           @@@@@@@
    ///    @@@@@@@&@@@@@@@#  #@@@@@@@@@@@@@@@@
    ///       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    ///           @@@@@@@@@@@@@@@@@@@@@
    /// ```
    TeArch,
    /// TileOS Logo
    /// ```text
    ///                       ,.
    ///                   ##########
    ///              ###################.
    ///          ############################
    ///      ####################################
    ///     ######################################
    /// ,(/     *############################(     .%
    /// (((((((     (####################      %%####
    /// (((((((((((/     ############      %#########
    /// ((((((((((((((((      ##/     (##############
    /// ((((((((((((((((((((      ###################
    /// (((((((((((((((((((((   ####################
    /// (((((((((((((((((((((   ####################
    /// (((((((((((((((((((((   ####################
    /// (((((((((((((((((((((   ####################
    ///  ((((((((((((((((((((   ###################%
    ///      ((((((((((((((((   ################/
    ///          ((((((((((((   ###########%
    ///               (((((((   #######*
    ///                   (((   ##%*
    /// ```
    TileOs,
    /// TorizonCore Logo
    /// ```text
    ///                           `.::-.
    ///                        `-://////:-.
    ///                      `:////////////:.
    ///                 `.-.`  `-:///////-.     `
    ///              `.://///:.`  `-::-.     `-://:.`
    ///            .:///////////:.        `-:////////:.`
    ///            `.://///////:-`       .:////////////:`
    ///     `.-/:-`   `.:///:-`    `-+o/.  `.://////-.     `.`
    ///  `.:///////:-`   `.`    `-+sssssso/.  `.--.     `-:///:-`
    /// -/////////////:       `+sssssssssssso-       `-://///////:-`
    ///   .-///////:.`    ``    -/sssssssso/.    `   .:///////////-.
    ///      .-::.`    `-://:-.    -/sso/.    `.:/:-.  `.://///-.
    ///             `-:////////:-.    `    `.:////////-.  `.-.
    /// o-          -:///////////:`       .:////////////:`        -o
    /// hdho-         `-://///:.`    `..`   `-://////:.`       -ohdh
    /// /ydddho-         `--.`    `.:////:.`   `-::.`       -ohdddy/
    ///   ./ydddho:`           `.://////////:.          `:ohdddy/.
    /// `    `/shddho:`        `.://////////:.       `:ohddds/`    `
    /// ::-`    `/shddhs:`        `.:////:.`      `:shddhs/`    `-::
    /// :///:-`    `:shddhs/`        `..`      `/shddhs:`    `-:///-
    ///  `.:///:-`    `:ohddhs/`            `/shddho:`    `-:///:.`
    ///     `.:///:-.     -ohdddy/.      ./ydddho-     .-:///-.`
    ///        `.:///:-.     -+hdddy+//+ydddh+-     .-://:-.
    ///           `.-///:-.     -+yddddddy+-     .-://:-.
    ///               .-://:-.     ./++/.     .-///:-.
    ///                  .-:///:.`        `.:///:-`
    ///                     `-:///:.````.:///:-`
    ///                        `-:////////:-`
    ///                           `-::::-`
    /// ```
    TorizonCore,
    /// Trisquel Logo
    /// ```text
    ///                          ,oo.
    ///                       ,oY"""Yb
    ///     ,oo.       ,'   ,   Yb
    ///  ,oo.    :      b   Y.
    /// ,"'      "Yo.   'b.   ,b  d
    /// '  .db  'o   'YY  d'
    /// '  q'    'b  'o._   _.o'
    /// .,_    _,d  ,Y'
    ///  'aaa' .'
    ///      """"     d"'
    ///              d'   .db.
    ///                ."   'a.
    ///                b      .
    ///              '. 'b,,.  
    ///               '.       .'
    ///                'ao._.oa'
    ///                   'aa'
    /// ```
    Trisquel,
    /// Tuxedo OS Logo
    /// ```text
    ///  #############################+
    /// .%#######################%@@@+
    ///             .           :#@%-
    ///            .#@%%%-     =@@#.
    ///              *@%@@=   +@@+
    ///               =@@%@+.#@%-
    ///                -@@%@@@#:
    ///                 .#@%%@-
    ///                 :#@@%@@=
    ///                =@@+=@@%@+
    ///              .#@@=  -@@%@#
    ///             -%@#:    :%@%@%:
    ///            *@@+       .#%%%%-
    ///          :%@%-              .
    ///         +@@@%#%%%%%%%%%%%%%%%%%%%%%%%.
    ///        +######################*******
    /// ```
    TuxedoOs,
    /// Twister Logo
    /// ```text
    /// ...............................,:ccllllcc:....
    /// ........................,;cldxkO0KXNNNWNNNX0x:
    /// ..................,:ldkOOKK0OkkkkkOKWMMMMMMMNo
    /// .............,;coxO0Oxo::'.........,oXMMMMMM0'
    /// .........,:ldkOOkc;'................;OMMMMW0:.
    /// .......;ldkOxol:'..................,dNMMX0''..
    /// ....;:oxkxl:'....................,lOWWXx:.,'..
    /// ..,ldxkkd:'.............:c....':lkNMNOc,cdl'..
    /// .;oxxkkkc'..........:clc;'.';lkKNNKk;;ck00l...
    /// .lxxxkkkxoooooddxkOOko'..cok0KOd':::o0NXd;'...
    /// .:odxxkkOOOOOOOkdoi'..:ddxdoc:::od0NWW0c'.....
    /// ...':;gggggg;::''.......::::x0XWMMMNO.::'.....
    /// ..............;......,;od0KNWMWNK0O:::do'.....
    /// ...............'cclgggggggxdll":::'XKoo,......
    /// .................',,,,,,,::::;ooNWMWOc'.......
    /// ..................,:loxxxO0KXNNXK0ko:'........
    /// ..................';::;oTcoggcoF":::'.........
    /// ..................':o,.:::::::::,p'...........
    /// ..................;'ccdxkOOOdxlf'.............
    /// .................,l;;XgggggXP:'...............
    /// ................;lco;::::::'..................
    /// ...............';ggggol'`.....................
    /// .............':oo:''..........................
    /// ```
    Twister,
    /// Ubuntu Logo
    ///
    /// ```text
    ///                             ....
    ///               .',:clooo:  .:looooo:.
    ///            .;looooooooc  .oooooooooo'
    ///         .;looooool:,''.  :ooooooooooc
    ///        ;looool;.         'oooooooooo,
    ///       ;clool'             .cooooooc.  ,,
    ///          ...                ......  .:oo,
    ///   .;clol:,.                        .loooo'
    ///  :ooooooooo,                        'ooool
    /// 'ooooooooooo.                        loooo.
    /// 'ooooooooool                         coooo.
    ///  ,loooooooc.                        .loooo.
    ///    .,;;;'.                          ;ooooc
    ///        ...                         ,ooool.
    ///     .cooooc.              ..',,'.  .cooo.
    ///       ;ooooo:.           ;oooooooc.  :l.
    ///        .coooooc,..      coooooooooo.
    ///          .:ooooooolc:. .ooooooooooo'
    ///            .':loooooo;  ,oooooooooc
    ///                ..';::c'  .;loooo:'
    ///                              .
    ///                       .
    /// ```
    Ubuntu,
    /// Ubuntu Budgie Logo
    /// ```text
    ///            ./oydmMMMMMMmdyo/.
    ///         :smMMMMMMMMMMMhs+:++yhs:
    ///      `omMMMMMMMMMMMN+`        `odo`
    ///     /NMMMMMMMMMMMMN-            `sN/
    ///   `hMMMMmhhmMMMMMMh               sMh`
    ///  .mMmo-     /yMMMMm`              `MMm.
    ///  mN/       yMMMMMMMd-              MMMm
    /// oN-        oMMMMMMMMMms+//+o+:    :MMMMo
    /// m/          +NMMMMMMMMMMMMMMMMm. :NMMMMm
    /// M`           .NMMMMMMMMMMMMMMMNodMMMMMMM
    /// M-            sMMMMMMMMMMMMMMMMMMMMMMMMM
    /// mm`           mMMMMMMMMMNdhhdNMMMMMMMMMm
    /// oMm/        .dMMMMMMMMh:      :dMMMMMMMo
    ///  mMMNyo/:/sdMMMMMMMMM+          sMMMMMm
    ///  .mMMMMMMMMMMMMMMMMMs           `NMMMm.
    ///   `hMMMMMMMMMMM.oo+.            `MMMh`
    ///     /NMMMMMMMMMo                sMN/
    ///      `omMMMMMMMMy.            :dmo`
    ///         :smMMMMMMMh+-`   `.:ohs:
    ///            ./oydmMMMMMMdhyo/.
    /// ```
    UbuntuBudgie,
    /// Ubuntu Cinnamon Logo
    /// ```text
    ///             .-/+oooooooo+/-.
    ///         `:+oooooooooooooooooo+:`
    ///       -+oooooooooooooooooooooooo+-
    ///     .ooooooooooooooooooo:ohNdoooooo.
    ///    /oooooooooooo:/+oo++:/ohNdooooooo/
    ///   +oooooooooo:osNdhyyhdNNh+:+oooooooo+
    ///  /ooooooooo/dN/ooooooooo/sNNoooooooooo/
    /// .ooooooooooMd:oooooooooooo:yMyooooooooo.
    /// +ooooo:+o/Mdoooooo:sm/oo/oooyMooooooooo+
    /// ooo:sdMdosMoooooooNMd//dMd+o:soooooooooo
    /// oooo+ymdosMoooo+mMm+/hMMMMMh+hsooooooooo
    /// +oooooo::/Nm:/hMNo:yMMMMMMMMMM+oooooooo+
    /// .ooooooooo/NNMNy:oNMMMMMMMMMMoooooooooo.
    /// /oooooooooo:yh:+mMMMMMMMMMMd/ooooooooo/
    ///   +oooooooooo+/hmMMMMMMNds//ooooooooo+
    ///    /oooooooooooo+:////:o/ymMdooooooo/
    ///     .oooooooooooooooooooo/sdhoooooo.
    ///       -+oooooooooooooooooooooooo+-
    ///         `:+oooooooooooooooooo+:`
    ///             .-/+oooooooo+/-.
    /// ```
    UbuntuCinnamon,
    /// Ubuntu Gnome Logo
    /// ```text
    ///           ./o.
    ///         .oooooooo
    ///       .oooo```soooo
    ///     .oooo`     `soooo
    ///    .ooo`   .o.   `\/ooo.
    ///    :ooo   :oooo.   `\/ooo.
    ///     sooo    `ooooo    \/oooo
    ///      \/ooo    `soooo    `ooooo
    ///       `soooo    `\/ooo    `soooo
    /// ./oo    `\/ooo    `/oooo.   `/ooo
    /// `\/ooo.   `/oooo.   `/oooo.   ``
    ///   `\/ooo.    /oooo     /ooo`
    ///      `ooooo    ``    .oooo
    ///        `soooo.     .oooo`
    ///          `\/oooooooooo`
    ///             ``\/oo``
    /// ```
    UbuntuGnome,
    /// Ubuntu Kylin Logo
    /// ```text
    ///             .__=liiiiiii=__,
    ///         ._=liiliii|i|i|iiilii=_.
    ///       _=iiiii|ii|i|ii|i|inwwwzii=,
    ///     .=liiii|ii|ii|wwww|i3QWWWWziii=,
    ///    =lii|i|ii|i|QQQWWWWWm]QWWQD||iiii=
    ///   =lii|iiivwQm>3WWWWWQWQQwwQwcii|i|ii=
    ///  =lii|ii|nQWWWQ|)i|i%i|]TQWWWWm|ii|i|i=
    /// .li|i|i|mWWWQV|iiwmD|iiii|TWWWWm|i|iiii,
    /// =iiiwww|WQWk|iaWWWD|i|i|ii]QQWQk|ii|i|=
    /// iiiQWWWQzWW|ijQQWQmw|iiwWk|TTTTYi|i|iii
    /// iiIQWQWWtyQQ|i|WWWWWQWk||i|i|ii||i|ii|i
    /// <|i|TTT|mQQWz|i3D]C|nDW|iivWWWWk||ii|i>
    /// -|ii|i|iWWWQw|Tt|i3T|T|i|nQWQWDk|ii|ii`
    ///  <|i|iii|VWQWWQ|i|i|||iiwmWWQWD||ii|ii+
    ///   <|ii|i|i]W@tvQQQWQQQWWTTHT1|iii|i|>
    ///    <|i|ii|ii||vQWWWQWWW@vmWWWm|i|i|i>
    ///     -<|i|ii|ii|i|TTTTTT|]QQWWWC|ii>`
    ///       -<|i|ii|i|ii|i|i|ii3TTTt|i>`
    ///          ~<|ii|ii|iiiii|i|||i>~
    ///             -~~<|ii|i||i>~~`
    /// ```
    UbuntuKylin,
    /// Ubuntu Mate Logo
    /// ```text
    ///             .:/+oossssoo+/:.`
    ///         `:+ssssssssssssssssss+:`
    ///       -+sssssssssssssssyssssssss+-
    ///     .osssssssssssssyyssmMmhssssssso.
    ///    /sssssssssydmNNNmmdsmMMMMNdysssss/
    ///  `+sssssssshNNdysssssssmMMMMNdyssssss+`
    ///  +sssssssyNNhsshmNNNNmsmMmhsydysssssss+
    /// -sssssyssNmsshNNhssssssyshhssmMysssssss-
    /// +ssssyMNdysshMdsssssssssshMdssNNsssssss+
    /// sssssyMMMMMmhsssssssssssssNMssdMysssssss
    /// sssssyMMMMMmhyssssssssssssNMssdMysssssss
    /// +ssssyMNdysshMdsssssssssshMdssNNsssssss+
    /// -sssssyssNmsshNNhssssssssdhssmMysssssss-
    ///  +sssssssyNNhsshmNNNNmsmNmhsymysssssss+
    ///   +sssssssshNNdysssssssmMMMMmhyssssss+
    ///    /sssssssssydmNNNNmdsmMMMMNdhsssss/
    ///     .osssssssssssssyyssmMmdysssssso.
    ///       -+sssssssssssssssyssssssss+-
    ///         `:+ssssssssssssssssss+:`
    ///             .:/+oossssoo+/:.
    /// ```
    UbuntuMate,
    /// Old Ubuntu Logo
    /// ```text
    ///              --+oossssssoo+--
    ///          .:+ssssssssssssssssss+:.
    ///        -+ssssssssssssssssssyyssss+-
    ///      .ossssssssssssssssssdMMMNysssso.
    ///    /ssssssssssshdmmNNmmyNMMMMhssssss/
    ///   +ssssssssshmydMMMMMMMNddddyssssssss+
    ///  /sssssssshNMMMyhhyyyyhmNMMMNhssssssss/
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    /// +sssshhhyNMMNyssssssssssssyNMMMysssssss+
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// +sssshhhyNMMNyssssssssssssyNMMMysssssss+
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    ///  /sssssssshNMMMyhhyyyyhdNMMMNhssssssss/
    ///   +sssssssssdmydMMMMMMMMddddyssssssss+
    ///    /ssssssssssshdmNNNNmyNMMMMhssssss/
    ///     .ossssssssssssssssssdMMMNysssso.
    ///      -+sssssssssssssssssyyyssss+-
    ///        `:+ssssssssssssssssss+:´
    ///            --+oossssssoo+--
    /// ```
    UbuntuOld,
    /// Small Ubuntu Logo
    /// ```text
    ///          _
    ///      ---(_)
    ///  _/  ---  \
    /// (_) |   |
    ///   \  --- _/
    ///      ---(_)
    /// ```
    UbuntuSmall,
    /// Ubuntu Studio Logo
    /// ```text
    ///               ..-::::::-.`
    ///          `.:+++++++++++ooo++:.`
    ///        ./+++++++++++++sMMMNdyo+/.
    ///      .++++++++++++++++oyhmMMMMms++.
    ///    `/+++++++++osyhddddhys+osdMMMh++/`
    ///   `+++++++++ydMMMMNNNMMMMNds+oyyo++++`
    ///   +++++++++dMMNhso++++oydNMMmo++++++++`
    ///  :+odmy+++ooysoohmNMMNmyoohMMNs+++++++:
    ///  ++dMMm+oNMd++yMMMmhhmMMNs+yMMNo+++++++
    /// `++NMMy+hMMd+oMMMs++++sMMN++NMMs+++++++.
    /// `++NMMy+hMMd+oMMMo++++sMMN++mMMs+++++++.
    ///  ++dMMd+oNMm++yMMNdhhdMMMs+yMMNo+++++++
    ///  :+odmy++oo+ss+ohNMMMMmho+yMMMs+++++++:
    ///   +++++++++hMMmhs+ooo+oshNMMms++++++++
    ///   `++++++++oymMMMMNmmNMMMMmy+oys+++++`
    ///    `/+++++++++oyhdmmmmdhso+sdMMMs++/
    ///      ./+++++++++++++++oyhdNMMMms++.
    ///        ./+++++++++++++hMMMNdyo+/.
    ///          `.:+++++++++++sso++:.
    ///               ..-::::::-..
    /// ```
    UbuntuStudio,
    /// Ubuntu Sway Logo
    /// ```text
    ///             .-/+oossssoo+\-.
    ///         ´:+ssssssssssssssssss+:`
    ///       -+ssssssssssssssssssyyssss+-
    ///     .ossssssssssssssssssdMMMNyyssso.
    ///    /ssssssssssshdmmNNmmyNMMMMhssssss\
    ///   +ssssssssshmydMMMMMMMNddddyssssssss+
    ///  /sssssssshNMMMyhhyyyyhmNMMMNhssssssss\
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    /// +sssyyyyyNMMNyssssssssssssyNMMMysssssss+
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// ossyNMMMNyMMhsssssssssssssshmmmhssssssso
    /// +sssyyyyyNMMNyssssssssssssyNMMMysssssss+
    /// .ssssssssdMMMNhsssssssssshNMMMdssssssss.
    ///  \sssssssshNMMMyhhyyyyhdNMMMNhssssssss/
    ///   +sssssssssdmydMMMMMMMMddddyssssssss+
    ///    \ssssssssssshdmNNNNmyNMMMMhssssss/
    ///     .ossssssssssssssssssdMMMNyyssso.
    ///       -+sssssssssssssssssyysss+-
    ///         `:+ssssssssssssssssss+:`
    ///             .-\+oossssoo+/-.
    /// ```
    UbuntuSway,
    /// Ubuntu Touch Logo
    /// ```text
    ///    ###############
    ///  ##               ##
    /// ##  ##         ##  ##
    /// ##  ##  #   #  ##  ##
    /// ##       ###       ##
    ///  ##               ##
    ///    ###############
    /// ```
    UbuntuTouch,
    /// Ubuntu Unity Logo
    /// ```text
    ///                .-/+ooosssssssooo+\-.
    ///             ,:+sssssssssssssssssssss+:.
    ///          -+ssssssssssssssssssss.....ssss+-
    ///        .ssssss,ss:cloooooo:ss.:loooo.ssssss.
    ///     .ssssssssloossooooooocss.ooooooooossssss.
    ///    .ssssss.;looooool:,''.ss:oooooooooocssssss.
    ///   +ssssss;loooool;.ssssssssssooooooooossssssss+
    ///  /ssssss;clool'sssssssssssssssoooooocssss,,sssss.
    /// .sssssssssssssssssssssssssssssssssssss.:oo,sssss.
    /// .sssss.;clol:,.ssssssssssssssssssssss.loooo'ssss.
    /// +ssss:ooooooooo,ssssssssssssssssssssss'oooolssss+
    /// osss'ooooooooooo.ssssssssssssssssssssssloooo.ssso
    /// osss'oooooooooolssssssssssssssssssssssscossssssso
    /// ossss,looooooc.sssssssssssssssssssssss.loooo.ssso
    /// +ssssss.,;;;'.ssssssssssssssssssssssss;oooocssss+
    /// .ssssssssssssssssssssssssssssssssssss,ooool.ssss.
    /// .sssssss.cooooc.sssssssssssss.,,,,.ss.cooo.sssss.
    ///  `sssssssooooo:.ssssssssssssoooooooc.ss:l.sssss'
    ///   +ssssss.coooooc,..ssssssscooooooooo.ssssssss+
    ///    `sssssss.:oosooooolc:.s.ooooooooooo'ssssss'
    ///     .sssssss.oss`:loooo;sss,oooooooocsssssss.
    ///      `sssssssss;oooo';:c'ssss`:ooo:'sssssss'
    ///        -+sssssssssssssssssssssssssssssss+-
    ///           `:+sssssssssssssssssssssss+:'
    ///                `-\+ooosssssssooo+/-'
    /// ```
    UbuntuUnity,
    /// Second Old Ubuntu Logo
    /// ```text
    ///                          ./+o+-
    ///                  yyyyy- -yyyyyy+
    ///               ://+//////-yyyyyyo
    ///           .++ .:/++++++/-.+sss/`
    ///         .:++o:  /++++++++/:--:/-
    ///        o:+o+:++.`..```.-/oo+++++/
    ///       .:+o:+o/.          `+sssoo+/
    ///  .++/+:+oo+o:`             /sssooo.
    /// /+++//+:`oo+o               /::--:.
    /// +/+o+++`o++o               ++////.
    ///  .++.o+++oo+:`             /dddhhh.
    ///       .+.o+oo:.          `oddhhhh+
    ///        +.++o+o``-````.:ohdhhhhh+
    ///         `:o+++ `ohhhhhhhhyo++os:
    ///           .o:`.syhhhhhhh/.oo++o`
    ///               /osyyyyyyo++ooo+++/
    ///                   ````` +oo+++o:
    ///                          `oo++.
    /// ```
    Ubuntu2Old,
    /// Second Small Ubuntu Logo
    /// ```text
    ///        ..;,; .,;,.
    ///     .,lool: .ooooo,
    ///    ;oo;:    .coool.
    ///  ....         ''' ,l;
    /// :oooo,            'oo.
    /// looooc            :oo'
    ///  '::'             ,oo:
    ///    ,.,       .... co,
    ///     lo:;.   :oooo; .
    ///      ':ooo; cooooc
    ///         '''  ''''
    /// ```
    Ubuntu2Small,
    /// Ultramarine Logo
    /// ```text
    ///             .cd0NNNNNNNXOdc.
    ///         .:xKNNNNNNNNNNNNNNNNKd;.
    ///       ,dXNNNNNNNNNNNNNNNNNNNNNNNd,
    ///     'ONNNNNNNNNNNNNNNNNNNNNNNNNNNNO'
    ///   .xNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNk.
    ///  .0NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN0.
    /// .0NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN0.
    /// dNNNNNNNNNNNNWWWWWWWWNNNNNNNNNNNNNNNNNNd
    /// NNNNNNNNNNNNNWMMMMMMMMWWNNNNNNNNNNNNNNNN
    /// NNNNNNNNNNNNNNWMMMMMMMMMWWNNNNNNNNNNNNNN
    /// NNNNNNNNNNNNNNWMMMMMMMMMMMMWNNNNNNNNNNNN
    /// NNNNNNNNNNWWWMMMMMMMMMMMMMMMMWWWNNNNNNNX
    /// oNWWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWNNo
    ///  OWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWO
    ///  .OWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWO.
    ///    lNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNl
    ///     .dNWMMMMMMMMMMMMMMMMMMMMMMMMWNd.
    ///       .cKWMMMMMMMMMMMMMMMMMMMMWKc.
    ///          'oOXWWWMMMMMMMMWWWXOl.
    ///              ;lkXNNNNNNXkl'
    /// ```
    Ultramarine,
    /// Univalent Logo
    /// ```text
    /// UUUUUUU                   UUUUUUU
    /// UUUUUUU                   UUUUUUU
    /// UUUUUUU         A         UUUUUUU
    /// UUUUUUU        A|A        UUUUUUU
    /// UUUUUUU       A | A       UUUUUUU
    /// UUUUUUU      A  |  A      UUUUUUU
    /// UUUUUUU     A|  |  |A     UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    /// UUUUUUU    A |  |  | A    UUUUUUU
    ///  UUUUUUU   A |  |  | A   UUUUUUU
    ///   UUUUUUU  A |  |  | A  UUUUUUU
    ///     UUUUUUUAAAAAAAAAAAUUUUUUU
    ///        UUUUUUUUUUUUUUUUUUU
    ///           UUUUUUUUUUUUU
    /// ```
    Univalent,
    /// Univention Logo
    /// ```text
    ///          ./osssssssssssssssssssssso+-
    ///        `ohhhhhhhhhhhhhhhhhhhhhhhhhhhhy:
    ///        shhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh-
    ///    `-//sssss/hhhhhhhhhhhhhh+s.hhhhhhhhh+
    ///  .ohhhysssss.hhhhhhhhhhhhhh.sss+hhhhhhh+
    /// .yhhhhysssss.hhhhhhhhhhhhhh.ssss:hhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhysssss.hhhhhhhhhhhhhh.sssssyhhhhh+
    /// +hhhhhyssssss+yhhhhhhhhhhy/ssssssyhhhhh+
    /// +hhhhhh:sssssss:hhhhhhh+.ssssssssyhhhhy.
    /// +hhhhhhh+`sssssssssssssssshhsssssyhhho`
    /// +hhhhhhhhhs+ssssssssssss+hh+sssss/:-`
    /// -hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhho
    ///  :yhhhhhhhhhhhhhhhhhhhhhhhhhhhh+`
    ///    -+ossssssssssssssssssssss+:`
    /// ```
    Univention,
    /// UOS Logo
    /// ```text
    ///                       .......
    ///                   ..............
    ///               ......................
    ///            .............................
    /// uuuuuu    uuuuuu     ooooooooooo       ssssssssss
    /// u::::u    u::::u   oo:::::::::::oo   ss::::::::::s
    /// u::::u    u::::u  o:::::::::::::::oss:::::::::::::s
    /// u::::u    u::::u  o:::::ooooo:::::os::::::ssss:::::s
    /// u::::u    u::::u  o::::o     o::::o s:::::s  ssssss
    /// u::::u    u::::u  o::::o     o::::o   s::::::s
    /// u::::u    u::::u  o::::o     o::::o      s::::::s
    /// u:::::uuuu:::::u  o::::o     o::::ossssss   s:::::s
    /// u:::::::::::::::uuo:::::ooooo:::::os:::::ssss::::::s
    /// u:::::::::::::::uo:::::::::::::::os::::::::::::::s
    /// uu::::::::uu:::u oo:::::::::::oo  s:::::::::::ss
    ///     uuuuuuuu  uuuu   ooooooooooo     sssssssssss
    ///           .............................
    ///               .....................
    ///                   .............
    ///                       ......
    /// ```
    Uos,
    /// UrukOS Logo
    /// ```text
    ///          :::::::::::::::    .
    ///         =#*============.   :#:
    ///        =##%+----------.   .###:
    ///       =####.              .####:
    ///      =####.                .####:
    ///     =###*.   .=--------.     ####:
    ///    =####.   .*#+======-       ####:
    ///   =###*.   .*###+====-         ####:
    ///  =###*.   .*#####+==-           ####:
    /// =###*.   .*#######+:             ####:
    /// =###*.   .*#######+:             ####:
    ///  =###*.   .*#####+==-           ####:
    ///   =###*.   .*###+====-         ####:
    ///    =####.   .*#+======-       ####:
    ///     =###*.   .=--------.    .####:
    ///      =####.                .####:
    ///       =####.              .####:
    ///        =###+--------------####:
    ///         =#+=================-:
    ///          :::::::::::::::::::.
    /// ```
    UrukOs,
    /// Uwuntu Logo
    /// ```text
    ///                                   &&
    ///                                &&&&&&&&
    ///    ,                  *&&&&&&  &&&&&&&&(
    ///     &%%%%&&&&     &&&&&&&&&&&&  ,&&&&&
    ///      %%%%%%&&&&&   ,&&&&&&&&&&&&&,   %&&&&&&%%%%%.
    ///      &%%%%&&&&&&&#   &,       &&&&&&&&&&&&&%%%%%
    ///       &%%&&&&&&&&&(               &&&&&&&&&%%%%
    ///        &&&&&&&&&%                  *&&&&&&&&&%
    ///     &&&/  &&&&\&                    ,/*.**
    ///  %&&&&&&&&  &&&⟩.,                *.⟨
    ///  %&&&&&&&&  &&/..      /    \      ..\(&&&&&&
    ///    #&&&#%%%%.%%%(      \_/\_/      (%%%.%%%%/
    ///         /%%%%%%%&&*              ,&&&%%%%%%&
    ///            &&&&&&&&           &&&&&&&&&&&
    ///             (&&&&&    &&&&&&&&&&&
    ///             %%  &   &&&&&&&&&&&&  &&&&&&&
    ///            %%%        #&&&&&&#   &&&&&&&&&
    ///  %%%%%     %%                     #&&&&&(
    /// &%.      %%%
    ///   %%%%%%%
    /// ```
    Uwuntu,
    /// Vanilla Logo
    /// ```text
    ///                 .----:
    ///               .-------.
    ///              :---::----:
    ///             .----::-----.
    ///   .........  :----::-----: ..:::-::::..
    /// .-----------------::------------------:
    ///  ----::-----------::----------::::---:
    ///   -----:::--------::-------:::-------
    ///    :------::::--::...:::::---------:
    ///     .---------::..    ..:---------.
    ///       .::-----::..    .::----::.
    ///         .:------:.......:-------:
    ///        .--------::::::::-:::-------.
    ///       .-------::-----.:-----::------.
    ///      -----::------:   :------::-----
    ///     :--::--------:     .-------::---:
    ///    :----------::         .:----------
    ///     :--------:             :--------:
    /// ```
    Vanilla,
    /// Venom Logo
    /// ```text
    /// `-`
    ///  -yys+/-`
    ///   `oyyyyy: /osyyyyso+:.
    ///     /yyyyy+`+yyyyyyyyyys/.
    ///     .-yyyyys.:+//+oyyyyyyyo.
    ///   `oy/`oyyyyy/      ./syyyyy:
    ///   syyys`:yyyyyo`       :yyyyy:
    ///  /yyyyo  .syyyyy-       .yyyyy.
    ///  yyyyy.    +yyyyy/       /yyyy/
    /// `yyyyy      :yyyyys`     -yyyyo
    ///  yyyyy.      `syyyyy-    /yyyy/
    ///  /yyyyo        /yyyyy+  .yyyyy.
    ///   syyyys.       -yyyyys.:yyyy:
    ///   `oyyyyyo-`     `oyyyyy:.sy:
    ///     :syyyyyyso+/++`/yyyyyo``
    ///       -oyyyyyyyyyyy-.syyyys.
    ///          -/+osyyyyso.`+yyyyy/
    ///                        .-/+syo`
    ///                              `.
    /// ```
    Venom,
    /// Small Venom Logo
    /// ```text
    ///  ++**
    ///   *===**====+*
    ///    *====*   +===+
    ///  *==*+===*    *===*
    /// *===* *===+    *===*
    /// *===*  +===+   *===*
    /// *===*   +===*  *===*
    ///  *===*    *===+*==*
    ///    +===+   *===+=*
    ///       *+====**===*
    ///                **++
    /// ```
    VenomSmall,
    /// Vnux Logo
    /// ```text
    ///               `
    ///            ^[XOx~.
    ///         ^_nwdbbkp0ti'
    ///         <vJCZw0LQ0Uj>
    ///           _j>!vC1,,
    ///      ,   ,CYOtO1(l;"
    /// `~-{r(1I ^/zmwJuc:I^
    /// '?)|U/}- ^fOCLLOw_,;
    ///  ,i,``. ",k%ooW@d"I,'
    ///   '    ;^u^<:^
    ///    ` .>>(@@@@nl[::
    ///     `!}?B%&WMMW&%}-}":
    ///     ^?jZWMMWWWWMMWofc;;`
    ///     <~x&&MWWWWWWWWp-l>[<
    ///  'ljmwn~tk8MWWWWM8OXr+]nC[
    /// !JZqwwdX:^C8#MMMM@XOdpdpq0<
    /// <wwwwmmpO10@%%%%8dnqmwmqqqJl
    /// ?QOZmqqqpbt[run/?!0pwqqQj-,
    ///  ^:l<{nUUv>      ^x00J("
    ///                    ^"
    /// ```
    Vnux,
    /// Vzlinux Logo
    /// ```text
    ///              .::::::::.
    /// `/////`      :zzzzzzzz        ://///-
    ///  VVVVVu`         -zzz`       /VVVVV.
    ///  `dVVVVV        .zzz`       :VVVVV:
    ///   `VVVVVo       zzz        -VVVVV/
    ///    .VVVVV\     zzz/       .VVVVV+
    ///     -VVVVV:   zzzzzzzzz  .dVVVVs
    ///      \VVVVV-  `````````  VVVVVV
    ///       +VVVVV.           sVVVVu`
    ///        oVVVVd`         +VVVVd`
    ///         VVVVVV        /VVVVV.
    ///         `uVVVVs      -VVVVV-
    ///          `dVVVV+    .VVVVV/
    ///           .VVVVV\  `dVVVV+
    ///            -VVVVV-`uVVVVo
    ///             :VVVVVVVVVVV
    ///              \VVVVVVVVu
    ///               oVVVVVVd`
    ///                sVVVVV.
    ///                 ----.
    /// ```
    Vzlinux,
    /// Void Logo
    /// ```text
    ///                 __.;=====;.__
    ///             _.=+==++=++=+=+===;.
    ///              -=+++=+===+=+=+++++=_
    ///         .     -=:``     `--==+=++==.
    ///        _vi,    `            --+=++++:
    ///       .uvnvi.       _._       -==+==+.
    ///      .vvnvnI`    .;==|==;.     :|=||=|.
    /// +QmQQmpvvnv; _yYsyQQWUUQQQm #QmQ#:QQQWUVQQm.
    ///  -QQWQWpvvowZ?.wQQQE==<QWWQ/QWQW.QQWW(: jQWQE
    ///   -QQQQmmU'  jQQQ@+=<QWQQ)mQQQ.mQQQC+;jWQQ@'
    ///    -WQ8YnI:   QWQQwgQQWV`mWQQ.jQWQQgyyWW@!
    ///      -1vvnvv.     `~+++`        ++|+++
    ///       +vnvnnv,                 `-|===
    ///        +vnvnvns.           .      :=-
    ///         -Invnvvnsi..___..=sv=.     `
    ///           +Invnvnvnnnnnnnnvvnn;.
    ///             ~|Invnvnvvnvvvnnv}+`
    ///                -~|{*l}*|~
    /// ```
    Void,
    /// Small Void Logo
    /// ```text
    ///     _______
    ///  _ \______ -
    /// | \  ___  \ |
    /// | | /   \ | |
    /// | | \___/ | |
    /// | \______ \_|
    ///  -_______\
    /// ```
    VoidSmall,
    /// WiiLinuxNgx Logo
    /// ```text
    /// '''''''            `~;:`            -''''''   ~kQ@@g\      ,EQ@@g/
    /// h@@@@@@'          o@@@@@9`         `@@@@@@D  `@@@@@@@=     @@@@@@@?
    /// '@@@@@@X         o@@@@@@@D         v@@@@@@:   R@@@@@@,     D@@@@@@_
    ///  t@@@@@@'       _@@@@@@@@@;       `Q@@@@@U     ;fmo/-       ;fmo/-
    ///  `Q@@@@@m       d@@@@@@@@@N       7@@@@@@'
    ///   L@@@@@@'     :@@@@@&@@@@@|     `Q@@@@@Z     :]]]]]v      :]]]]]v
    ///    Q@@@@@X     R@@@@Q`g@@@@Q     f@@@@@Q-     z@@@@@Q      v@@@@@Q
    ///    r@@@@@@~   ;@@@@@/ ;@@@@@L   `@@@@@@/      z@@@@@Q      v@@@@@Q
    ///     d@@@@@q   M@@@@#   H@@@@Q   ]@@@@@Q       z@@@@@Q      v@@@@@Q
    ///     ,@@@@@@, >@@@@@;   _@@@@@c `@@@@@@>       z@@@@@Q      v@@@@@Q
    ///      X@@@@@U Q@@@@R     Z@@@@Q`{@@@@@N        z@@@@@Q      v@@@@@Q
    ///      .@@@@@@S@@@@@:     -@@@@@e@@@@@@:        z@@@@@Q      v@@@@@Q
    ///       {@@@@@@@@@@U       t@@@@@@@@@@e         z@@@@@Q      v@@@@@Q
    ///       `Q@@@@@@@@@'       `Q@@@@@@@@@-         z@@@@@Q      v@@@@@Q
    ///        :@@@@@@@@|         ;@@@@@@@@=          z@@@@@Q      v@@@@@Q
    ///         '2#@@Q6:           ,eQ@@QZ~           /QQQQQg      \QQQQQN
    /// ```
    WiiLinuxNgx,
    /// Windows 11 Logo
    /// ```text
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    ///
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// /////////////////  /////////////////
    /// ```
    Windows11,
    /// Small Windows 11 Logo
    /// ```text
    /// lllllll  lllllll
    /// lllllll  lllllll
    /// lllllll  lllllll
    ///
    /// lllllll  lllllll
    /// lllllll  lllllll
    /// lllllll  lllllll
    /// ```
    Windows11Small,
    /// Windows 8 Logo
    /// ```text
    ///                                 ..,
    ///                     ....,,:;+ccllll
    ///       ...,,+:;  cllllllllllllllllll
    /// ,cclllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    ///
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// llllllllllllll  lllllllllllllllllll
    /// `'ccllllllllll  lllllllllllllllllll
    ///        `' \*::  :ccllllllllllllllll
    ///                        ````''*::cll
    ///                                  ``
    /// ```
    Windows8,
    /// Windows Logo
    /// ```text
    ///         ,.=:!!t3Z3z.,
    ///        :tt:::tt333EE3
    ///        Et:::ztt33EEEL @Ee.,      ..,
    ///       ;tt:::tt333EE7 ;EEEEEEttttt33#
    ///      :Et:::zt333EEQ. EEEEEttttt33QL
    ///      it::::tt333EEF @EEEEEEttttt33F
    ///     ;3=*^```"*4EEV :EEEEEEttttt33@.
    ///     ,.=::::!t=., ` @EEEEEEtttz33QF
    ///    ;::::::::zt33)   "4EEEtttji3P*
    ///   :t::::::::tt33.:Z3z..  `` ,..g.
    ///   i::::::::zt33F AEEEtttt::::ztF
    ///  ;:::::::::t33V ;EEEttttt::::t3
    ///  E::::::::zt33L @EEEtttt::::z3F
    /// {3=*^```"*4E3) ;EEEtttt:::::tZ`
    ///              ` :EEEEtttt::::z7
    ///                  "VEzjt:;;z>*`
    /// ```
    Windows,
    /// Windows 95 Logo
    /// ```text
    ///                         ___
    ///                    .--=+++++=-:.
    /// .              _ *%@@@@@@@@@@@@@@*
    ///  *:+:.__ :+* @@@ @"_*&%@@%&&&*"@@@
    ///   "+.-#+ +%* " _ ++&&&%@@%&&&&&#@@
    /// "          , %@@ &&&&&%@@%&&&&&#@@
    ///    *  oo  *# " _ &&&&&%@@%&&&&&#@@
    /// "          , %@@ &&&&"@@@@#*"&&&@@
    /// .  *  oo  *# " _ %@@@@@@@@@@@@@@@@
    ///  *:+:.__ :=* %@@ @"**&%@@%&&&*"@@@
    ///   "+.-#+ +%* " _ &&&&&%@@%&&&&&#@@
    /// "          , %@@ &&&&&%@@%&&&&&#@@
    ///    *  oo  *# " _ &&&&&%@@%&&&&&#@@
    /// "          , %@@ &&*"%@@@@@@"*%&@@
    /// .  *  oo  *# " _ @@@@@@@@@@@@@@@@@
    ///  *:+:.__ :+# @@@ @%#=+""""""+==%#@
    ///   "+.-#+ +%* %+" "             ":@
    ///        " "
    /// ```
    Windows95,
    /// Xenia Logo
    /// ```text
    ///       ,c.                       .c;
    ///     .KMMMk....             ....kMMMK.
    ///    .WMMMMMX.....         .....KMMMMMW.
    ///    XMMMMMMM0.....        ....OMMMMMMMN
    ///   dMMMMMMMMM;.... ..... ....,MMMMMMMMMd
    ///   WMMMMMMMMMl;okKKKKKKKKKOo;cMMMMMMMMMM
    ///  'MMMMMMMNXK0KKKKKKKKKKKKKKK0KXNMMMMMMM;
    ///  oMMMMMMMOxoKKKKKKKKKKKKKKKKKoxOMMMMMMMd
    ///  dMMMMMMMdxxxKKKKKKKKKKKKKKKxxxdNMMMMMMk
    ///  :MMMMX0xxxxxx0KKKKKKKK0KK0xxxxxx0XMMMMc
    ///   MMMOxxxxxxxxdxkdd0x0ddkxdxxxxxxxxOMMM
    ///  ;xxkxddxxxxdodxxxxdxdxxxxdodxxxxddxkxx;
    /// dxdKMMMWXo'.....'cdxxxdc'.....'lXWMMMXdxd
    /// cxdXMMMN,..........dxd'.........'XMMMNdxl
    ///  .xxWMMl...''....'.;k:.'....''...lMMWxx.
    /// ..:kXMMx..'....''..kMk..''....'..xMMXkc..
    ///  dMMMMMMd.....'...xMMMx...''....dMMMMMMx
    ///     kMMMMWOoc:coOkolllokOoc:coOWMMMMO
    ///          .MMMMMMMMl...lNMMMMMMM.
    ///             KMMMMMMXlKMMMMMMX
    ///                .MMMMMMMMM.
    /// ```
    Xenia,
    /// Xferience Logo
    /// ```text
    ///            ``--:::::::-.`
    ///         .-/+++ooooooooo+++:-`
    ///      `-/+oooooooooooooooooo++:.
    ///     -/+oooooo/+ooooooooo+/ooo++:`
    ///   `/+oo++oo.   .+oooooo+.-: +:-o+-
    ///  `/+o/.  -o.    :oooooo+ ```:.+oo+-
    /// `:+oo-    -/`   :oooooo+ .`-`+oooo/.
    /// .+ooo+.    .`   `://///+-+..oooooo+:`
    /// -+ooo:`                ``.-+oooooo+/`
    /// -+oo/`                       :+oooo/.
    /// .+oo:            ..-/. .      -+oo+/`
    /// `/++-         -:::++::/.      -+oo+-
    ///  ./o:          `:///+-     `./ooo+:`
    ///   .++-         `` /-`   -:/+oooo+:`
    ///    .:+/:``          `-:ooooooo++-
    ///      ./+o+//:...../+oooooooo++:`
    ///        `:/++ooooooooooooo++/-`
    ///           `.-//++++++//:-.`
    ///                ``````
    /// ```
    Xferience,
    /// Xray_Os Logo
    /// ```text
    ///                rrrrrraaaaaaaaaaaayyyy
    ///            xxrrrrrrrraaaaaaaaaaaayyyyyyyyy
    ///         xxxxxrrrrrrrraaaaaaaaaaaayyyyyyyyyyyyyyyyyyyyyy
    ///       xxxxxxxrrrrrrrraaaaa aaaaayyyyyyyyyyyyyyyyyyy
    ///     xxxxxxxxxrrrrrrrraaaa aaaaaaayyyyyyyyyyyyyyyyy
    ///   xxxxxxxxxxxrrrrrrrraa aaaaaaaaayyyyyyyyyyyyyy yy
    ///  xxxxxxxxxxxxrrrrrrrra aaaaaaaaaayyyyyyyyyyyyyyyyyy
    ///  xxxxxxxxxxxxrrrrrrrr aaaaaaaaaaayyyyyyyyyyyyyyyyyyy
    /// xxxxxxxxxxxxxrrrrrr raaaaaaaaaaaayyyyyyyyyyyyyyyyyyyy
    /// xxxxxxxxxxxxxrrrrr rraaaaaaaaaaaayyyyyyyyyyyyy yyyyyy
    /// xxxxxxxxxxxxxrrrrrrrraaaaaaaaaaaayyyyyyyyyy yyyyyyyyy
    /// xxxxxxxxxxxxxrrrrrrrraaaaaaaaaaaayyyyyyy yyyyyyyyyyyy
    /// xxxxxxxxxxxxxrrrrrrrraaaaaaaaaaaayyyy yyyyyyyyyyyyyy
    ///  xxxxxxxxxxxxrrrrrrrra aaaaaaaaaay yyyyyyyyyyyyyyyy
    ///   xxxxxxxxxxxrrrrrrr aaaaaaaaaaaayyyyyyyyyyyyyyyyy
    ///    xxxxxxxxxxrrrrrr raaaaaaaaaaaa yyyyyyyyyyyyyyy
    ///      xxxxxxxxrrrr rrraaaaaaaaa aayyyyyyyyyyyyyy
    ///        xxxxxxrrrrrrr aaaaaa  aaaayyyyyyyyyyyy
    ///           xxxrrrrrr raaa  aaaaaaayyyyyyyyy
    ///               rrrr rr  aaaaaaaaaayyyyyy
    ///                  r   aaaaaaaaaa
    /// ```
    XrayOs,
    /// YiffOS Logo
    /// ```text
    ///            NK
    ///             NxKXW
    ///             WOlcoXW
    ///             0olccloxNW
    ///             XxllllllloxOKNW
    ///             Nklllllllllllodk0XWW
    ///            N0dllllllllllllllodxOKNW
    ///          WKxlllllllllllllllllllooxOKN
    ///        WKdllllllllllllllooooooooooooo
    ///       Nxllllllllllllllloooooooooooooo
    ///     XXNOolllllllllllloooooooooooooooo
    ///   WX0xolllllllllllooooooooooooooooooo
    /// XWN0xollllllllllooooooooooooooooooooo
    ///   Kkdolllllllooooddddddoooooooooooddo
    ///        K00XNW      WX0xdooooooddddddd
    ///                       WXOxdoooooodddd
    ///                          WXOxdddddddd
    ///                                NX0ddd
    ///                                  WN0d
    /// ```
    YiffOs,
    /// Zorin Logo
    /// ```text
    ///         `osssssssssssssssssssso`
    ///        .osssssssssssssssssssssso.
    ///       .+oooooooooooooooooooooooo+.
    ///
    ///
    ///   `::::::::::::::::::::::.         .:`
    ///  `+ssssssssssssssssss+:.`     `.:+ssso`
    /// .ossssssssssssssso/.       `-+ossssssso.
    /// ssssssssssssso/-`      `-/osssssssssssss
    /// .ossssssso/-`      .-/ossssssssssssssso.
    ///  `+sss+:.      `.:+ssssssssssssssssss+`
    ///   `:.         .::::::::::::::::::::::`
    ///
    ///
    ///       .+oooooooooooooooooooooooo+.
    ///        -osssssssssssssssssssssso-
    ///         `osssssssssssssssssssso`
    /// ```
    Zorin,
    /// z/OS Logo
    /// ```text
    ///              //  OOOOOOO  SSSSSSS
    ///             //  OO    OO SS
    ///     zzzzzz //  OO    OO SS
    ///       zz  //  OO    OO   SSSS
    ///     zz   //  OO    OO       SS
    ///   zz    //  OO    OO       SS
    /// zzzzzz //   OOOOOOO  SSSSSSS
    /// ```
    ZOs,
}

impl LogoSelection {
    /// returns the corresponding `AsciiLogo`
    pub fn logo(&self) -> AsciiLogo {
        match self {
            LogoSelection::Unknown => AsciiLogo::unknown(),
            LogoSelection::Adelie => AsciiLogo::adelie(),
            LogoSelection::AerOs => AsciiLogo::aer_os(),
            LogoSelection::Afterglow => AsciiLogo::afterglow(),
            LogoSelection::Aix => AsciiLogo::aix(),
            LogoSelection::Almalinux => AsciiLogo::almalinux(),
            LogoSelection::Alpine => AsciiLogo::alpine(),
            LogoSelection::AlpineSmall => AsciiLogo::alpine_small(),
            LogoSelection::Alpine2Small => AsciiLogo::alpine2_small(),
            LogoSelection::Alter => AsciiLogo::alter(),
            LogoSelection::Amazon => AsciiLogo::amazon(),
            LogoSelection::AmazonLinux => AsciiLogo::amazon_linux(),
            LogoSelection::AmogOs => AsciiLogo::amog_os(),
            LogoSelection::Anarchy => AsciiLogo::anarchy(),
            LogoSelection::Android => AsciiLogo::android(),
            LogoSelection::AndroidSmall => AsciiLogo::android_small(),
            LogoSelection::Antergos => AsciiLogo::antergos(),
            LogoSelection::Antix => AsciiLogo::antix(),
            LogoSelection::AoscOsRetro => AsciiLogo::aosc_os_retro(),
            LogoSelection::AoscOsRetroSmall => AsciiLogo::aosc_os_retro_small(),
            LogoSelection::AoscOs => AsciiLogo::aosc_os(),
            LogoSelection::AoscOsOld => AsciiLogo::aosc_os_old(),
            LogoSelection::Aperture => AsciiLogo::aperture(),
            LogoSelection::Apricity => AsciiLogo::apricity(),
            LogoSelection::ArchBox => AsciiLogo::arch_box(),
            LogoSelection::Archcraft => AsciiLogo::archcraft(),
            LogoSelection::Archcraft2 => AsciiLogo::archcraft2(),
            LogoSelection::Arch => AsciiLogo::arch(),
            LogoSelection::Arch2 => AsciiLogo::arch2(),
            LogoSelection::ArchSmall => AsciiLogo::arch_small(),
            LogoSelection::ArchLabs => AsciiLogo::arch_labs(),
            LogoSelection::ArchStrike => AsciiLogo::arch_strike(),
            LogoSelection::Artix => AsciiLogo::artix(),
            LogoSelection::ArtixSmall => AsciiLogo::artix_small(),
            LogoSelection::Artix2Small => AsciiLogo::artix2_small(),
            LogoSelection::Arco => AsciiLogo::arco(),
            LogoSelection::ArcoSmall => AsciiLogo::arco_small(),
            LogoSelection::Arse => AsciiLogo::arse(),
            LogoSelection::Arya => AsciiLogo::arya(),
            LogoSelection::Asahi => AsciiLogo::asahi(),
            LogoSelection::Asahi2 => AsciiLogo::asahi2(),
            LogoSelection::Aster => AsciiLogo::aster(),
            LogoSelection::AsteroidOs => AsciiLogo::asteroid_os(),
            LogoSelection::AstOs => AsciiLogo::ast_os(),
            LogoSelection::Astra => AsciiLogo::astra(),
            LogoSelection::Athena => AsciiLogo::athena(),
            LogoSelection::Azos => AsciiLogo::azos(),
            LogoSelection::Bedrock => AsciiLogo::bedrock(),
            LogoSelection::BigLinux => AsciiLogo::big_linux(),
            LogoSelection::Bitrig => AsciiLogo::bitrig(),
            LogoSelection::Blackarch => AsciiLogo::blackarch(),
            LogoSelection::BlackMesa => AsciiLogo::black_mesa(),
            LogoSelection::BlackPanther => AsciiLogo::black_panther(),
            LogoSelection::Blag => AsciiLogo::blag(),
            LogoSelection::BlankOn => AsciiLogo::blank_on(),
            LogoSelection::BlueLight => AsciiLogo::blue_light(),
            LogoSelection::Bodhi => AsciiLogo::bodhi(),
            LogoSelection::Bonsai => AsciiLogo::bonsai(),
            LogoSelection::Bsd => AsciiLogo::bsd(),
            LogoSelection::BunsenLabs => AsciiLogo::bunsen_labs(),
            LogoSelection::CachyOs => AsciiLogo::cachy_os(),
            LogoSelection::CachyOsSmall => AsciiLogo::cachy_os_small(),
            LogoSelection::Calculate => AsciiLogo::calculate(),
            LogoSelection::Calinix => AsciiLogo::calinix(),
            LogoSelection::CalinixSmall => AsciiLogo::calinix_small(),
            LogoSelection::Carbs => AsciiLogo::carbs(),
            LogoSelection::CblMariner => AsciiLogo::cbl_mariner(),
            LogoSelection::Celos => AsciiLogo::celos(),
            LogoSelection::Center => AsciiLogo::center(),
            LogoSelection::CentOs => AsciiLogo::cent_os(),
            LogoSelection::CentOsSmall => AsciiLogo::cent_os_small(),
            LogoSelection::Chakra => AsciiLogo::chakra(),
            LogoSelection::ChaletOs => AsciiLogo::chalet_os(),
            LogoSelection::Chapeau => AsciiLogo::chapeau(),
            LogoSelection::ChimeraLinux => AsciiLogo::chimera_linux(),
            LogoSelection::ChonkySealOs => AsciiLogo::chonky_seal_os(),
            LogoSelection::ChromOs => AsciiLogo::chrom_os(),
            LogoSelection::Cleanjaro => AsciiLogo::cleanjaro(),
            LogoSelection::CleanjaroSmall => AsciiLogo::cleanjaro_small(),
            LogoSelection::ClearLinux => AsciiLogo::clear_linux(),
            LogoSelection::ClearOs => AsciiLogo::clear_os(),
            LogoSelection::Clover => AsciiLogo::clover(),
            LogoSelection::Cobalt => AsciiLogo::cobalt(),
            LogoSelection::Condres => AsciiLogo::condres(),
            LogoSelection::Crux => AsciiLogo::crux(),
            LogoSelection::CruxSmall => AsciiLogo::crux_small(),
            LogoSelection::Crystal => AsciiLogo::crystal(),
            LogoSelection::Cucumber => AsciiLogo::cucumber(),
            LogoSelection::CutefishOs => AsciiLogo::cutefish_os(),
            LogoSelection::CuteOs => AsciiLogo::cute_os(),
            LogoSelection::CyberOs => AsciiLogo::cyber_os(),
            LogoSelection::Cycledream => AsciiLogo::cycledream(),
            LogoSelection::Dahlia => AsciiLogo::dahlia(),
            LogoSelection::DarkOs => AsciiLogo::dark_os(),
            LogoSelection::Debian => AsciiLogo::debian(),
            LogoSelection::DebianSmall => AsciiLogo::debian_small(),
            LogoSelection::Deepin => AsciiLogo::deepin(),
            LogoSelection::DesaOs => AsciiLogo::desa_os(),
            LogoSelection::Devuan => AsciiLogo::devuan(),
            LogoSelection::DevuanSmall => AsciiLogo::devuan_small(),
            LogoSelection::DietPi => AsciiLogo::diet_pi(),
            LogoSelection::DracOs => AsciiLogo::drac_os(),
            LogoSelection::DragonFly => AsciiLogo::dragon_fly(),
            LogoSelection::DragonFlySmall => AsciiLogo::dragon_fly_small(),
            LogoSelection::DragonFlyOld => AsciiLogo::dragon_fly_old(),
            LogoSelection::DraugerOs => AsciiLogo::drauger_os(),
            LogoSelection::Droidian => AsciiLogo::droidian(),
            LogoSelection::Elbrus => AsciiLogo::elbrus(),
            LogoSelection::Elementary => AsciiLogo::elementary(),
            LogoSelection::ElementarySmall => AsciiLogo::elementary_small(),
            LogoSelection::Elive => AsciiLogo::elive(),
            LogoSelection::EncryptOs => AsciiLogo::encrypt_os(),
            LogoSelection::Endeavour => AsciiLogo::endeavour(),
            LogoSelection::EndeavourSmall => AsciiLogo::endeavour_small(),
            LogoSelection::Endless => AsciiLogo::endless(),
            LogoSelection::Enso => AsciiLogo::enso(),
            LogoSelection::EshanizedOs => AsciiLogo::eshanized_os(),
            LogoSelection::EuroLinux => AsciiLogo::euro_linux(),
            LogoSelection::Evolinx => AsciiLogo::evolinx(),
            LogoSelection::EvolutionOs => AsciiLogo::evolution_os(),
            LogoSelection::EvolutionOsSmall => AsciiLogo::evolution_os_small(),
            LogoSelection::EvolutionOsOld => AsciiLogo::evolution_os_old(),
            LogoSelection::Exherbo => AsciiLogo::exherbo(),
            LogoSelection::ExodiaPredator => AsciiLogo::exodia_predator(),
            LogoSelection::Fedora => AsciiLogo::fedora(),
            LogoSelection::FedoraSmall => AsciiLogo::fedora_small(),
            LogoSelection::FedoraOld => AsciiLogo::fedora_old(),
            LogoSelection::FedoraSilverblue => AsciiLogo::fedora_silverblue(),
            LogoSelection::FedoraKinoite => AsciiLogo::fedora_kinoite(),
            LogoSelection::FedoraSericea => AsciiLogo::fedora_sericea(),
            LogoSelection::FedoraCoreos => AsciiLogo::fedora_coreos(),
            LogoSelection::FemboyOs => AsciiLogo::femboy_os(),
            LogoSelection::Feren => AsciiLogo::feren(),
            LogoSelection::Finnix => AsciiLogo::finnix(),
            LogoSelection::Floflis => AsciiLogo::floflis(),
            LogoSelection::Freebsd => AsciiLogo::freebsd(),
            LogoSelection::FreebsdSmall => AsciiLogo::freebsd_small(),
            LogoSelection::FreeMint => AsciiLogo::free_mint(),
            LogoSelection::Frugalware => AsciiLogo::frugalware(),
            LogoSelection::Funtoo => AsciiLogo::funtoo(),
            LogoSelection::GalliumOs => AsciiLogo::gallium_os(),
            LogoSelection::Garuda => AsciiLogo::garuda(),
            LogoSelection::GarudaDragon => AsciiLogo::garuda_dragon(),
            LogoSelection::GarudaSmall => AsciiLogo::garuda_small(),
            LogoSelection::Gentoo => AsciiLogo::gentoo(),
            LogoSelection::GentooSmall => AsciiLogo::gentoo_small(),
            LogoSelection::GhostBsd => AsciiLogo::ghost_bsd(),
            LogoSelection::Glaucus => AsciiLogo::glaucus(),
            LogoSelection::GNewSense => AsciiLogo::g_new_sense(),
            LogoSelection::Gnome => AsciiLogo::gnome(),
            LogoSelection::Gnu => AsciiLogo::gnu(),
            LogoSelection::GoboLinux => AsciiLogo::gobo_linux(),
            LogoSelection::GrapheneOs => AsciiLogo::graphene_os(),
            LogoSelection::Grombyang => AsciiLogo::grombyang(),
            LogoSelection::Guix => AsciiLogo::guix(),
            LogoSelection::GuixSmall => AsciiLogo::guix_small(),
            LogoSelection::Haiku => AsciiLogo::haiku(),
            LogoSelection::HaikuSmall => AsciiLogo::haiku_small(),
            LogoSelection::HamoniKr => AsciiLogo::hamoni_kr(),
            LogoSelection::HarDcLanZ => AsciiLogo::har_dc_lan_z(),
            LogoSelection::HardenedBsd => AsciiLogo::hardened_bsd(),
            LogoSelection::Hash => AsciiLogo::hash(),
            LogoSelection::Huayra => AsciiLogo::huayra(),
            LogoSelection::Hybrid => AsciiLogo::hybrid(),
            LogoSelection::HydroOs => AsciiLogo::hydro_os(),
            LogoSelection::Hyperbola => AsciiLogo::hyperbola(),
            LogoSelection::HyperbolaSmall => AsciiLogo::hyperbola_small(),
            LogoSelection::Iglunix => AsciiLogo::iglunix(),
            LogoSelection::InstantOs => AsciiLogo::instant_os(),
            LogoSelection::Interix => AsciiLogo::interix(),
            LogoSelection::Irix => AsciiLogo::irix(),
            LogoSelection::Ironclad => AsciiLogo::ironclad(),
            LogoSelection::Itc => AsciiLogo::itc(),
            LogoSelection::JanusLinux => AsciiLogo::janus_linux(),
            LogoSelection::Kaisen => AsciiLogo::kaisen(),
            LogoSelection::Kali => AsciiLogo::kali(),
            LogoSelection::KaliSmall => AsciiLogo::kali_small(),
            LogoSelection::KaOs => AsciiLogo::ka_os(),
            LogoSelection::KernelOs => AsciiLogo::kernel_os(),
            LogoSelection::KdeNeon => AsciiLogo::kde_neon(),
            LogoSelection::Kibojoe => AsciiLogo::kibojoe(),
            LogoSelection::KissLinux => AsciiLogo::kiss_linux(),
            LogoSelection::Kogaion => AsciiLogo::kogaion(),
            LogoSelection::Korora => AsciiLogo::korora(),
            LogoSelection::KrassOs => AsciiLogo::krass_os(),
            LogoSelection::KsLinux => AsciiLogo::ks_linux(),
            LogoSelection::Kubuntu => AsciiLogo::kubuntu(),
            LogoSelection::Langitketujuh => AsciiLogo::langitketujuh(),
            LogoSelection::Laxeros => AsciiLogo::laxeros(),
            LogoSelection::Lede => AsciiLogo::lede(),
            LogoSelection::LibreElec => AsciiLogo::libre_elec(),
            LogoSelection::Linspire => AsciiLogo::linspire(),
            LogoSelection::Linux => AsciiLogo::linux(),
            LogoSelection::LinuxSmall => AsciiLogo::linux_small(),
            LogoSelection::LinuxLite => AsciiLogo::linux_lite(),
            LogoSelection::LinuxLiteSmall => AsciiLogo::linux_lite_small(),
            LogoSelection::LiveRaizo => AsciiLogo::live_raizo(),
            LogoSelection::Lmde => AsciiLogo::lmde(),
            LogoSelection::LainOs => AsciiLogo::lain_os(),
            LogoSelection::Lunar => AsciiLogo::lunar(),
            LogoSelection::MacOs => AsciiLogo::mac_os(),
            LogoSelection::MacOsSmall => AsciiLogo::mac_os_small(),
            LogoSelection::MacOs2 => AsciiLogo::mac_os2(),
            LogoSelection::MacOs2Small => AsciiLogo::mac_os2_small(),
            LogoSelection::MainsailOs => AsciiLogo::mainsail_os(),
            LogoSelection::MainsailOsSmall => AsciiLogo::mainsail_os_small(),
            LogoSelection::Mageia => AsciiLogo::mageia(),
            LogoSelection::MageiaSmall => AsciiLogo::mageia_small(),
            LogoSelection::MagpieOs => AsciiLogo::magpie_os(),
            LogoSelection::Mandriva => AsciiLogo::mandriva(),
            LogoSelection::Manjaro => AsciiLogo::manjaro(),
            LogoSelection::ManjaroSmall => AsciiLogo::manjaro_small(),
            LogoSelection::MassOs => AsciiLogo::mass_os(),
            LogoSelection::MatuusOs => AsciiLogo::matuus_os(),
            LogoSelection::MaUi => AsciiLogo::ma_ui(),
            LogoSelection::Meowix => AsciiLogo::meowix(),
            LogoSelection::Mer => AsciiLogo::mer(),
            LogoSelection::Mint => AsciiLogo::mint(),
            LogoSelection::MintSmall => AsciiLogo::mint_small(),
            LogoSelection::MintOld => AsciiLogo::mint_old(),
            LogoSelection::Minix => AsciiLogo::minix(),
            LogoSelection::MiracleLinux => AsciiLogo::miracle_linux(),
            LogoSelection::Mos => AsciiLogo::mos(),
            LogoSelection::Msys2 => AsciiLogo::msys2(),
            LogoSelection::Mx => AsciiLogo::mx(),
            LogoSelection::MxSmall => AsciiLogo::mx_small(),
            LogoSelection::Mx2 => AsciiLogo::mx2(),
            LogoSelection::Namib => AsciiLogo::namib(),
            LogoSelection::Nekos => AsciiLogo::nekos(),
            LogoSelection::Neptune => AsciiLogo::neptune(),
            LogoSelection::NetRunner => AsciiLogo::net_runner(),
            LogoSelection::Nitrux => AsciiLogo::nitrux(),
            LogoSelection::NixOs => AsciiLogo::nix_os(),
            LogoSelection::NixOsSmall => AsciiLogo::nix_os_small(),
            LogoSelection::NixOsOld => AsciiLogo::nix_os_old(),
            LogoSelection::NixOsOldSmall => AsciiLogo::nix_os_old_small(),
            LogoSelection::NetBsd => AsciiLogo::net_bsd(),
            LogoSelection::Nobara => AsciiLogo::nobara(),
            LogoSelection::NomadBsd => AsciiLogo::nomad_bsd(),
            LogoSelection::Nurunner => AsciiLogo::nurunner(),
            LogoSelection::NuTyx => AsciiLogo::nu_tyx(),
            LogoSelection::Obarun => AsciiLogo::obarun(),
            LogoSelection::ObRevenge => AsciiLogo::ob_revenge(),
            LogoSelection::OmniOs => AsciiLogo::omni_os(),
            LogoSelection::OpenKylin => AsciiLogo::open_kylin(),
            LogoSelection::OpenBsd => AsciiLogo::open_bsd(),
            LogoSelection::OpenBsdSmall => AsciiLogo::open_bsd_small(),
            LogoSelection::OpenEuler => AsciiLogo::open_euler(),
            LogoSelection::OpenIndiana => AsciiLogo::open_indiana(),
            LogoSelection::OpenMamba => AsciiLogo::open_mamba(),
            LogoSelection::OpenStage => AsciiLogo::open_stage(),
            LogoSelection::OpenSuse => AsciiLogo::open_suse(),
            LogoSelection::OpenSuseSmall => AsciiLogo::open_suse_small(),
            LogoSelection::OpenSuseMicroOs => AsciiLogo::open_suse_micro_os(),
            LogoSelection::OpenSuseLeap => AsciiLogo::open_suse_leap(),
            LogoSelection::OpenSuseTumbleweed => AsciiLogo::open_suse_tumbleweed(),
            LogoSelection::OpenMandriva => AsciiLogo::open_mandriva(),
            LogoSelection::OpenWrt => AsciiLogo::open_wrt(),
            LogoSelection::OpnSense => AsciiLogo::opn_sense(),
            LogoSelection::Oracle => AsciiLogo::oracle(),
            LogoSelection::Orchid => AsciiLogo::orchid(),
            LogoSelection::OrchidSmall => AsciiLogo::orchid_small(),
            LogoSelection::OsElbrus => AsciiLogo::os_elbrus(),
            LogoSelection::OpenSourceMediaCenter => AsciiLogo::osmc(),
            LogoSelection::PacBsd => AsciiLogo::pac_bsd(),
            LogoSelection::Panwah => AsciiLogo::panwah(),
            LogoSelection::Parabola => AsciiLogo::parabola(),
            LogoSelection::ParabolaSmall => AsciiLogo::parabola_small(),
            LogoSelection::Parch => AsciiLogo::parch(),
            LogoSelection::Pardus => AsciiLogo::pardus(),
            LogoSelection::Parrot => AsciiLogo::parrot(),
            LogoSelection::Parsix => AsciiLogo::parsix(),
            LogoSelection::Pcbsd => AsciiLogo::pcbsd(),
            LogoSelection::PcLinuxOs => AsciiLogo::pc_linux_os(),
            LogoSelection::PearOs => AsciiLogo::pear_os(),
            LogoSelection::Pengwin => AsciiLogo::pengwin(),
            LogoSelection::Pentoo => AsciiLogo::pentoo(),
            LogoSelection::Peppermint => AsciiLogo::peppermint(),
            LogoSelection::Peropesis => AsciiLogo::peropesis(),
            LogoSelection::PhyOs => AsciiLogo::phy_os(),
            LogoSelection::PikaOs => AsciiLogo::pika_os(),
            LogoSelection::Pisi => AsciiLogo::pisi(),
            LogoSelection::PnmLinux => AsciiLogo::pnm_linux(),
            LogoSelection::Pop => AsciiLogo::pop(),
            LogoSelection::PopSmall => AsciiLogo::pop_small(),
            LogoSelection::Porteus => AsciiLogo::porteus(),
            LogoSelection::PostMarketOs => AsciiLogo::post_market_os(),
            LogoSelection::PostMarketOsSmall => AsciiLogo::post_market_os_small(),
            LogoSelection::Proxmox => AsciiLogo::proxmox(),
            LogoSelection::PuffOs => AsciiLogo::puff_os(),
            LogoSelection::Puppy => AsciiLogo::puppy(),
            LogoSelection::PureOs => AsciiLogo::pure_os(),
            LogoSelection::PureOsSmall => AsciiLogo::pure_os_small(),
            LogoSelection::Q4Dos => AsciiLogo::q4dos(),
            LogoSelection::Qubes => AsciiLogo::qubes(),
            LogoSelection::Qubyt => AsciiLogo::qubyt(),
            LogoSelection::Quibian => AsciiLogo::quibian(),
            LogoSelection::Radix => AsciiLogo::radix(),
            LogoSelection::Raspbian => AsciiLogo::raspbian(),
            LogoSelection::RaspbianSmall => AsciiLogo::raspbian_small(),
            LogoSelection::RavynOs => AsciiLogo::ravyn_os(),
            LogoSelection::Reborn => AsciiLogo::reborn(),
            LogoSelection::RebornSmall => AsciiLogo::reborn_small(),
            LogoSelection::RedCore => AsciiLogo::red_core(),
            LogoSelection::RedHatEnterpriseLinux => AsciiLogo::red_hat_enterprise_linux(),
            LogoSelection::RedHatEnterpriseLinuxOld => AsciiLogo::red_hat_enterprise_linux_old(),
            LogoSelection::RedstarOs => AsciiLogo::redstar_os(),
            LogoSelection::RefractedDevuan => AsciiLogo::refracted_devuan(),
            LogoSelection::Regata => AsciiLogo::regata(),
            LogoSelection::Regolith => AsciiLogo::regolith(),
            LogoSelection::RhaymOs => AsciiLogo::rhaym_os(),
            LogoSelection::RockyLinux => AsciiLogo::rocky_linux(),
            LogoSelection::RockyLinuxSmall => AsciiLogo::rocky_linux_small(),
            LogoSelection::RosaLinux => AsciiLogo::rosa_linux(),
            LogoSelection::Sabayon => AsciiLogo::sabayon(),
            LogoSelection::Sabotage => AsciiLogo::sabotage(),
            LogoSelection::Sailfish => AsciiLogo::sailfish(),
            LogoSelection::SalentOs => AsciiLogo::salent_os(),
            LogoSelection::SalientOs => AsciiLogo::salient_os(),
            LogoSelection::Salix => AsciiLogo::salix(),
            LogoSelection::SambaBox => AsciiLogo::samba_box(),
            LogoSelection::Sasanqua => AsciiLogo::sasanqua(),
            LogoSelection::Scientific => AsciiLogo::scientific(),
            LogoSelection::Semc => AsciiLogo::semc(),
            LogoSelection::Septor => AsciiLogo::septor(),
            LogoSelection::Serene => AsciiLogo::serene(),
            LogoSelection::SharkLinux => AsciiLogo::shark_linux(),
            LogoSelection::ShastraOs => AsciiLogo::shastra_os(),
            LogoSelection::Siduction => AsciiLogo::siduction(),
            LogoSelection::SkiffOs => AsciiLogo::skiff_os(),
            LogoSelection::Slitaz => AsciiLogo::slitaz(),
            LogoSelection::Slackel => AsciiLogo::slackel(),
            LogoSelection::Slackware => AsciiLogo::slackware(),
            LogoSelection::SlackwareSmall => AsciiLogo::slackware_small(),
            LogoSelection::SmartOs => AsciiLogo::smart_os(),
            LogoSelection::Soda => AsciiLogo::soda(),
            LogoSelection::SourceMage => AsciiLogo::source_mage(),
            LogoSelection::Solaris => AsciiLogo::solaris(),
            LogoSelection::SolarisSmall => AsciiLogo::solaris_small(),
            LogoSelection::Solus => AsciiLogo::solus(),
            LogoSelection::Sparky => AsciiLogo::sparky(),
            LogoSelection::Star => AsciiLogo::star(),
            LogoSelection::StockLinux => AsciiLogo::stock_linux(),
            LogoSelection::SteamOs => AsciiLogo::steam_os(),
            LogoSelection::Sulin => AsciiLogo::sulin(),
            LogoSelection::Suse => AsciiLogo::suse(),
            LogoSelection::Swagarch => AsciiLogo::swagarch(),
            LogoSelection::T2 => AsciiLogo::t2(),
            LogoSelection::Tails => AsciiLogo::tails(),
            LogoSelection::Tatra => AsciiLogo::tatra(),
            LogoSelection::TeArch => AsciiLogo::te_arch(),
            LogoSelection::TileOs => AsciiLogo::tile_os(),
            LogoSelection::TorizonCore => AsciiLogo::torizon_core(),
            LogoSelection::Trisquel => AsciiLogo::trisquel(),
            LogoSelection::TuxedoOs => AsciiLogo::tuxedo_os(),
            LogoSelection::Twister => AsciiLogo::twister(),
            LogoSelection::Ubuntu => AsciiLogo::ubuntu(),
            LogoSelection::UbuntuBudgie => AsciiLogo::ubuntu_budgie(),
            LogoSelection::UbuntuCinnamon => AsciiLogo::ubuntu_cinnamon(),
            LogoSelection::UbuntuGnome => AsciiLogo::ubuntu_gnome(),
            LogoSelection::UbuntuKylin => AsciiLogo::ubuntu_kylin(),
            LogoSelection::UbuntuMate => AsciiLogo::ubuntu_mate(),
            LogoSelection::UbuntuOld => AsciiLogo::ubuntu_old(),
            LogoSelection::UbuntuSmall => AsciiLogo::ubuntu_small(),
            LogoSelection::UbuntuStudio => AsciiLogo::ubuntu_studio(),
            LogoSelection::UbuntuSway => AsciiLogo::ubuntu_sway(),
            LogoSelection::UbuntuTouch => AsciiLogo::ubuntu_touch(),
            LogoSelection::UbuntuUnity => AsciiLogo::ubuntu_unity(),
            LogoSelection::Ubuntu2Old => AsciiLogo::ubuntu2_old(),
            LogoSelection::Ubuntu2Small => AsciiLogo::ubuntu2_small(),
            LogoSelection::Ultramarine => AsciiLogo::ultramarine(),
            LogoSelection::Univalent => AsciiLogo::univalent(),
            LogoSelection::Univention => AsciiLogo::univention(),
            LogoSelection::Uos => AsciiLogo::uos(),
            LogoSelection::UrukOs => AsciiLogo::uruk_os(),
            LogoSelection::Uwuntu => AsciiLogo::uwuntu(),
            LogoSelection::Vanilla => AsciiLogo::vanilla(),
            LogoSelection::Venom => AsciiLogo::venom(),
            LogoSelection::VenomSmall => AsciiLogo::venom_small(),
            LogoSelection::Vnux => AsciiLogo::vnux(),
            LogoSelection::Vzlinux => AsciiLogo::vzlinux(),
            LogoSelection::Void => AsciiLogo::void(),
            LogoSelection::VoidSmall => AsciiLogo::void_small(),
            LogoSelection::WiiLinuxNgx => AsciiLogo::wii_linux_ngx(),
            LogoSelection::Windows11 => AsciiLogo::windows_11(),
            LogoSelection::Windows11Small => AsciiLogo::windows_11_small(),
            LogoSelection::Windows8 => AsciiLogo::windows_8(),
            LogoSelection::Windows => AsciiLogo::windows(),
            LogoSelection::Windows95 => AsciiLogo::windows_95(),
            LogoSelection::Xenia => AsciiLogo::xenia(),
            LogoSelection::Xferience => AsciiLogo::xferience(),
            LogoSelection::XrayOs => AsciiLogo::xray_os(),
            LogoSelection::YiffOs => AsciiLogo::yiff_os(),
            LogoSelection::Zorin => AsciiLogo::zorin(),
            LogoSelection::ZOs => AsciiLogo::z_os(),
        }
    }
}
