use std::collections::BTreeMap;

use crate::{AsciiLogo, LogoSelection};
use strum::IntoEnumIterator;

/// Builds an index over all logos and provides a fast search afterwards
pub struct LogoSearch {
    index: BTreeMap<String, LogoSelection>,
}

impl Default for LogoSearch {
    fn default() -> Self {
        let mut index = BTreeMap::new();
        for logo_selection in LogoSelection::iter() {
            let logo = logo_selection.logo();
            for name in logo.names() {
                index.insert(name.to_owned().to_owned().to_lowercase(), logo_selection);
            }
        }
        Self { index }
    }
}

impl LogoSearch {
    /// searches for the distribution name and returns the corresponding `AsciiLogo`
    ///
    /// If logo is not found it returns None
    pub fn search(&self, name: &str) -> Option<AsciiLogo> {
        self.index
            .get(&name.to_lowercase())
            .map(|logo_selection| logo_selection.logo())
    }

    /// returns the list of distro names supported
    pub fn distro_names(&self) -> Vec<String> {
        self.index.keys().cloned().collect()
    }
}
