#![warn(missing_docs)]
//! Library to show logos in ASCII-Art
mod logo_list;
mod logos;
mod search;
mod selection;

use std::fmt::Display;

use bitflags::bitflags;
use colored::{Color, Colorize};

pub use search::LogoSearch;
pub use selection::LogoSelection;

bitflags! {
    /// Type of Logo
    #[derive(Debug, Clone, PartialEq, Eq, Default)]
    pub struct LogoType:u8 {
        /// Normal Logo
        const Normal = 0;
        /// Small Logo
        const Small = 1 << 0;
        /// Alternativ Logo`
        const Alter = 1 << 1;
    }
}

/// Main struct to show ASCII-Logos
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct AsciiLogo {
    lines: &'static str,
    names: Vec<&'static str>,
    colors: Vec<Color>,
    color_keys: Option<Color>,
    color_title: Option<Color>,
    logo_type: LogoType,
}

impl AsciiLogo {
    /// Prints out the logo on terminal
    pub fn print_colored(&self) {
        println!("{}", self);
    }

    /// Prints out the logo on terminal
    pub fn print_uncolored(&self, prefix: Option<&str>) {
        let result = self.to_string_uncolored();
        if let Some(prefix) = prefix {
            for line in result.lines() {
                println!("{}{}", prefix, line)
            }
        } else {
            println!("{}", result);
        }
    }

    /// returns a colored string containing the logo
    pub fn to_string_colored(&self) -> String {
        let splits = self.lines.split('$');
        let mut result = String::new();
        let mut last_color = 0;
        for split in splits {
            if !split.is_empty() {
                match split.chars().next().unwrap() {
                    x if ('1'..='9').contains(&x) => {
                        let pos = x as usize - '1' as usize;
                        result.push_str(&format!("{}", split[1..].color(self.colors[pos])));
                        last_color = pos;
                    }
                    '{' => {
                        if split.starts_with("{c") {
                            match split.as_bytes()[2] {
                                x if (b'1'..=b'9').contains(&x) => {
                                    let pos = (x - b'1') as usize;
                                    result.push_str(&format!(
                                        "{}",
                                        split[4..].color(self.colors[pos])
                                    ));
                                    last_color = pos;
                                }
                                _ => result
                                    .push_str(&format!("{}", split.color(self.colors[last_color]))),
                            }
                        } else {
                            result.push_str(&format!("{}", split.color(self.colors[last_color])))
                        }
                    }
                    _ => result.push_str(&format!("{}", split.color(self.colors[last_color]))),
                }
            }
        }
        result
    }

    /// returns a uncolored string containing the logo
    pub fn to_string_uncolored(&self) -> String {
        let splits = self.lines.split('$');
        let mut result = String::new();
        for split in splits {
            if !split.is_empty() {
                match split.chars().next().unwrap() {
                    x if ('1'..='9').contains(&x) => {
                        result.push_str(&split[1..]);
                    }
                    '{' => {
                        if split.starts_with("{c") {
                            match split.as_bytes()[2] {
                                x if (b'1'..=b'9').contains(&x) => {
                                    result.push_str(&split[4..]);
                                }
                                _ => result.push_str(split),
                            }
                        } else {
                            result.push_str(split)
                        }
                    }
                    _ => result.push_str(split),
                }
            }
        }
        result
    }

    /// returns the distro names the logo belongs to
    pub fn names(&self) -> &[&str] {
        &self.names
    }

    /// returns the colors used in the logo
    pub fn colors(&self) -> &[Color] {
        &self.colors
    }

    /// returns akzent color 2
    pub fn color_keys(&self) -> Option<Color> {
        self.color_keys
    }

    /// return akzent color 1
    pub fn color_title(&self) -> Option<Color> {
        self.color_title
    }

    /// returns the logo type
    pub fn logo_type(&self) -> &LogoType {
        &self.logo_type
    }
}

impl Display for AsciiLogo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", self.to_string_colored())
    }
}
